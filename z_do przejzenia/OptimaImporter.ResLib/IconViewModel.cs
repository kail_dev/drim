﻿namespace OptimaImporter.ResLib
{
    class IconViewModel
    {

        public string Configuration
        {
            get { return GetPath("Configuration.png"); }
        }

        public string AppFolder
        {
            get { return GetPath("AppFolder.png"); }
        }
        public string SettingsFolder
        {
            get { return GetPath("SettingsFolder.png"); }
        }

        public string Log
        {
            get { return GetPath("Log.png"); }
        }
        public string Error
        {
            get { return GetPath("Error.png"); }
        }
        public string S
        {
            get { return GetPath("S.png"); }
        }
        public string Z
        {
            get { return GetPath("z.png"); }
        }

        private string GetPath(string iconString)
        {
            return "/OptimaImporter.ResLib;component/Images/" + iconString;
        }
    }
}
