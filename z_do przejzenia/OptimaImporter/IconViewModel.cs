﻿namespace GK_Migrator_Telerik
{
    public class IconViewModel
    {
        public string Error => GetPath("error.png");

        public string Home => GetPath("home.png");

        public string VatS => GetPath("VatS.png");

        public string VatZ => GetPath("VatZ.png");

        private static string GetPath(string iconString)
        {
            return "/GK Migrator_Telerik;component/Images/" + iconString;
        }
    }
}
