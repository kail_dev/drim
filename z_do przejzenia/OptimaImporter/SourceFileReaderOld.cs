﻿using NLog;
using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Globalization;
using System.IO;
using System.Linq;
using System.Text;
using System.Xml.Linq;
using OptimaImporter.Common;

namespace OptimaImporter
{
    public static class SourceFileReaderOld
    {
        public static List<Invoice> ReadJpkFaInvoices(string sourcePath)
        {
            var invoices = new List<Invoice>();
            var xdoc = XDocument.Load(sourcePath);

            var validInvoicesCounter = 0;
            foreach (var xElement in xdoc.Descendants().Where(x => x.Name.LocalName == "Faktura"))
            {
                var invoice = new Invoice
                {
                    InvoiceDate = Convert.ToDateTime(
                        xElement.Elements()
                        .FirstOrDefault(x => x.Name.LocalName.ToString()
                        .StartsWith("P_1"))?.Value),
                    Number = xElement.Elements()
                        .FirstOrDefault(x => x.Name.LocalName.ToString()
                        .StartsWith("P_2A"))?.Value,
                    BuyerName = xElement.Elements()
                        .FirstOrDefault(x => x.Name.LocalName.ToString()
                        .StartsWith("P_3A"))?.Value,
                    BuyerAddress = xElement.Elements()
                        .FirstOrDefault(x => x.Name.LocalName.ToString()
                        .StartsWith("P_3B"))?.Value,
                    SellerName = xElement.Elements()
                        .FirstOrDefault(x => x.Name.LocalName.ToString()
                        .StartsWith("P_3C"))?.Value,
                    SellerAddress = xElement.Elements()
                        .FirstOrDefault(x => x.Name.LocalName.ToString()
                        .StartsWith("P_3D"))?.Value,
                    BuyerTaxNumberCountry = xElement.Elements()
                        .FirstOrDefault(x => x.Name.LocalName.ToString()
                        .StartsWith("P_5A"))?.Value,
                    BuyerTaxNumber = xElement.Elements()
                        .FirstOrDefault(x => x.Name.LocalName.ToString()
                        .StartsWith("P_5B"))?.Value,
                    SellerTaxNumberCountry = xElement.Elements()
                        .FirstOrDefault(x => x.Name.LocalName.ToString()
                        .StartsWith("P_4A"))?.Value,
                    SellerTaxNumber = xElement.Elements()
                        .FirstOrDefault(x => x.Name.LocalName.ToString()
                        .StartsWith("P_4B"))?.Value,
                    PaymentDate = Convert.ToDateTime(
                        xElement.Elements()
                        .FirstOrDefault(x => x.Name.LocalName.ToString()
                        .StartsWith("P_6"))?.Value),
                    NetValue = Convert.ToDecimal(
                        xElement.Elements()
                        .FirstOrDefault(x => x.Name.LocalName.ToString()
                        .StartsWith("P_13_1"))?.Value.Replace(".", ",")),
                    TaxValue = Convert.ToDecimal(
                        xElement.Elements()
                        .FirstOrDefault(x => x.Name.LocalName.ToString()
                        .StartsWith("P_14_1"))?.Value.Replace(".", ",")),
                    GrossValue = Convert.ToDecimal(
                        xElement.Elements()
                        .FirstOrDefault(x => x.Name.LocalName.ToString()
                        .StartsWith("P_15"))?.Value.Replace(".", ","))
                };
                validInvoicesCounter++;
                Debug.WriteLine(validInvoicesCounter);
                invoices.Add(invoice);
            }

            var validRowsCounter = 0;
            foreach (var xElement in xdoc.Descendants().Where(x => x.Name.LocalName == "FakturaWiersz"))
            {
                var invoiceNumberFromRow = xElement.Elements()
                    .FirstOrDefault(x => x.Name.LocalName.ToString()
                    .StartsWith("P_2B"))?.Value; //P_2B
                var invoice = invoices.FirstOrDefault(x => x.Number == invoiceNumberFromRow);
                if (invoice != null)
                {
                    try
                    {
                        var netValue = Convert.ToDecimal(
                            xElement.Elements()
                            .FirstOrDefault(x => x.Name.LocalName.ToString()
                            .StartsWith("P_11"))?.Value.Replace(".", ","));
                        var vatRate = xElement.Elements()
                            .FirstOrDefault(x => x.Name.LocalName.ToString()
                            .StartsWith("P_12"))?.Value;
                        var grossValue = Convert.ToDecimal(
                            xElement.Elements()
                            .FirstOrDefault(x => x.Name.LocalName.ToString()
                            .StartsWith("P_11A"))?.Value.Replace(".", ","));

                        invoice.Rows.Add(new InvoiceRow
                        {
                            NetValue = netValue,
                            VatRate = vatRate,
                            GrossValue = grossValue
                        });

                        validRowsCounter++;
                        Debug.WriteLine(validRowsCounter);
                    }
                    catch (Exception ex)
                    {
                        //Debug.WriteLine($"Nie udało się odczytać wiersza dla faktury: {invoiceNumberFromRow}, {ex}");
                    }
                }
            }

            return invoices;
        }

        public static List<Invoice> ReadSubiektInvoices(string sourcePath, string invoiceType)
        {
            var readCounter = 0;
            var invoices = new List<Invoice>();
            var fileContent = File.ReadAllLines(sourcePath, Encoding.GetEncoding("Windows-1250")).Where(x => x.Length > 0).ToList();//.Take(100).ToList();
            for (int i = 0; i < fileContent.Count; i++)
            {
                var line = fileContent[i];
                if (line.StartsWith("[NAGLOWEK]"))
                {
                    readCounter++;
                    Debug.WriteLine($"Licznik nagłówków: {readCounter}");
                    var invoiceContent = fileContent[i + 1];
                    Debug.WriteLine($"Linia {i}:  {invoiceContent}");
                    string[] invoiceHeadElements = null;
                    var invoiceRows = new List<InvoiceRow>();
                    var invoiceHeadLinesCounter = 0;
                    var lineIPlus2 = fileContent[i + 2];
                    if (!lineIPlus2.StartsWith("["))
                    {
                        invoiceContent += lineIPlus2;
                        var invoiceHeadElementsList = invoiceContent.Split(',').ToList();
                        invoiceHeadElements = invoiceHeadElementsList.ToArray();
                        invoiceHeadLinesCounter++;
                        var lineIPlus3 = fileContent[i + 3];
                        if (!lineIPlus3.StartsWith("["))
                        {
                            invoiceContent += lineIPlus3;
                            invoiceHeadElementsList = invoiceContent.Split(',').ToList();
                            invoiceHeadElements = invoiceHeadElementsList.ToArray();
                            invoiceHeadLinesCounter++;
                            var lineIPlus4 = fileContent[i + 4];
                            if (!lineIPlus4.StartsWith("["))
                            {
                                invoiceContent += lineIPlus4;
                                invoiceHeadElementsList = invoiceContent.Split(',').ToList();
                                invoiceHeadElements = invoiceHeadElementsList.ToArray();
                                invoiceHeadLinesCounter++;
                            }
                        }
                    }

                    if (fileContent[i + 2 + invoiceHeadLinesCounter].StartsWith("[ZAW"))
                    {
                        var j = i;
                        while (fileContent.Count > j + 3 + invoiceHeadLinesCounter
                            && !fileContent[j + 3 + invoiceHeadLinesCounter].StartsWith("[N"))
                        {
                            try
                            {
                                var invoiceRowElements = fileContent[j + 3 + invoiceHeadLinesCounter].Split(',');
                                var invoiceRow = new InvoiceRow
                                {
                                    NetValue = Convert.ToDecimal(invoiceRowElements[16], new CultureInfo("en-US")),
                                    VatRate = invoiceRowElements[15].Replace("\"", ""),
                                    GrossValue = Convert.ToDecimal(invoiceRowElements[18], new CultureInfo("en-US"))
                                };
                                if (invoiceRow.VatRate == "ue")
                                {
                                    invoiceRow.VatRate = "0";
                                }
                                invoiceRows.Add(invoiceRow);
                                j++;
                            }
                            catch (Exception ex)
                            {
                                LogManager.GetCurrentClassLogger()
                                    .Info($"Nie udało się odczytać wiersza dla faktury {invoiceContent[6]}, błąd: {ex}");
                                j++;
                            }
                        }
                    }

                    var invoiceContentTmp = invoiceContent.Split(',').ToList();
                    if (invoiceContent.StartsWith("\"FZ"))
                    {
                        if (invoiceContentTmp.Count > 22
                            && !invoiceContentTmp[21].Replace("\"", "").StartsWith("201")
                            && invoiceContentTmp[22].Replace("\"", "").StartsWith("201"))
                        {
                            invoiceContentTmp.RemoveAt(13);
                            invoiceHeadElements = invoiceContentTmp.ToArray();
                        }
                        if (invoiceContentTmp.Count > 20 && invoiceContentTmp[20].Replace("\"", "").StartsWith("201"))
                        {
                            invoiceContentTmp.Insert(20, "");
                            invoiceHeadElements = invoiceContentTmp.ToArray();
                        }
                        if (invoiceContentTmp.Count > 33 && invoiceContentTmp[33].Replace("\"", "").StartsWith("201"))
                        {
                            invoiceContentTmp.Insert(31, "");
                            invoiceHeadElements = invoiceContentTmp.ToArray();
                        }
                        invoiceHeadElements = invoiceHeadElements ?? invoiceContentTmp.ToArray();
                    }
                    if (invoiceContent.StartsWith("\"FS"))
                    {
                        if (invoiceContentTmp.Count > 24
                            && !invoiceContentTmp[23].Replace("\"", "").StartsWith("201")
                            && invoiceContentTmp[24].Replace("\"", "").StartsWith("201"))
                        {
                            invoiceContentTmp.RemoveAt(13);
                            invoiceContentTmp.RemoveAt(16);
                            invoiceHeadElements = invoiceContentTmp.ToArray();
                        }
                        while (invoiceContentTmp.Count > 22
                            && !invoiceContentTmp[21].Replace("\"", "").StartsWith("201")
                            && invoiceContentTmp[22].Replace("\"", "").StartsWith("201"))
                        {
                            invoiceContentTmp.RemoveAt(13);
                            invoiceHeadElements = invoiceContentTmp.ToArray();
                        }
                        //if (invoiceContentTmp.Count > 22
                        //    && !invoiceContentTmp[21].Replace("\"", "").StartsWith("201")
                        //    && invoiceContentTmp[22].Replace("\"", "").StartsWith("201"))
                        //{
                        //    invoiceContentTmp.RemoveAt(13);
                        //    invoiceHeadElements = invoiceContentTmp.ToArray();
                        //}
                        invoiceHeadElements = invoiceHeadElements ?? invoiceContentTmp.ToArray();
                    }
                    if (invoiceContent.StartsWith($"\"{invoiceType}\""))
                    {
                        try
                        {
                            if (invoiceType == "FZ")
                            {
                                var invoiceDateYear = Convert.ToInt32(invoiceHeadElements[21].Replace("\"", "").Substring(0, 4));
                                var invoiceDateMonth = Convert.ToInt32(invoiceHeadElements[21].Replace("\"", "").Substring(4, 2));
                                var invoiceDateDay = Convert.ToInt32(invoiceHeadElements[21].Replace("\"", "").Substring(6, 2));
                                var paymentDateYear = Convert.ToInt32(invoiceHeadElements[34].Replace("\"", "").Substring(0, 4));
                                var paymentDateMonth = Convert.ToInt32(invoiceHeadElements[34].Replace("\"", "").Substring(4, 2));
                                var paymentDateDay = Convert.ToInt32(invoiceHeadElements[34].Replace("\"", "").Substring(6, 2));
                                var invoice = new Invoice
                                {
                                    Number = invoiceHeadElements[4].Replace("\"", ""),
                                    SellerName = invoiceHeadElements[12].Replace("\"", ""),
                                    City = invoiceHeadElements[14].Replace("\"", ""),
                                    PostalCode = invoiceHeadElements[15].Replace("\"", ""),
                                    Street = invoiceHeadElements[16].Replace("\"", ""),
                                    SellerTaxNumber = invoiceHeadElements[17].Replace("\"", ""),
                                    InvoiceDate = new DateTime(Convert.ToInt32(invoiceDateYear),
                                Convert.ToInt32(invoiceDateMonth),
                                Convert.ToInt32(invoiceDateDay)
                                ),
                                    PaymentDate = new DateTime(Convert.ToInt32(paymentDateYear),
                                Convert.ToInt32(paymentDateMonth),
                                Convert.ToInt32(paymentDateDay)
                                ),
                                    Currency = invoiceHeadElements[invoiceHeadElements.Length - 16].Replace("\"", ""),
                                    CurrencyRate = invoiceHeadElements[invoiceHeadElements.Length - 15].Replace("\"", ""),
                                    Rows = invoiceRows
                                };
                                invoice.OperationDate = invoice.InvoiceDate;
                                invoices.Add(invoice);
                            }
                            if (invoiceType == "FS")
                            {
                                var invoiceDateYear = Convert.ToInt32(invoiceHeadElements[22].Replace("\"", "").Substring(0, 4));
                                var invoiceDateMonth = Convert.ToInt32(invoiceHeadElements[22].Replace("\"", "").Substring(4, 2));
                                var invoiceDateDay = Convert.ToInt32(invoiceHeadElements[22].Replace("\"", "").Substring(6, 2));
                                var paymentDateYear = Convert.ToInt32(invoiceHeadElements[34].Replace("\"", "").Substring(0, 4));
                                var paymentDateMonth = Convert.ToInt32(invoiceHeadElements[34].Replace("\"", "").Substring(4, 2));
                                var paymentDateDay = Convert.ToInt32(invoiceHeadElements[34].Replace("\"", "").Substring(6, 2));
                                var invoice = new Invoice
                                {
                                    Number = invoiceHeadElements[6].Replace("\"", ""),
                                    BuyerName = invoiceHeadElements[12].Replace("\"", ""),
                                    City = invoiceHeadElements[14].Replace("\"", ""),
                                    PostalCode = invoiceHeadElements[15].Replace("\"", ""),
                                    Street = invoiceHeadElements[16].Replace("\"", ""),
                                    BuyerTaxNumber = invoiceHeadElements[17].Replace("\"", ""),
                                    InvoiceDate = new DateTime(Convert.ToInt32(invoiceDateYear),
                                    Convert.ToInt32(invoiceDateMonth),
                                    Convert.ToInt32(invoiceDateDay)
                                    ),
                                    PaymentDate = new DateTime(Convert.ToInt32(paymentDateYear),
                                    Convert.ToInt32(paymentDateMonth),
                                    Convert.ToInt32(paymentDateDay)
                                    ),
                                    Currency = invoiceHeadElements[invoiceHeadElements.Length - 16].Replace("\"", ""),
                                    CurrencyRate = invoiceHeadElements[invoiceHeadElements.Length - 15].Replace("\"", ""),
                                    Rows = invoiceRows
                                };
                                invoice.OperationDate = invoice.InvoiceDate;
                                invoices.Add(invoice);
                            }
                        }
                        catch (Exception ex)
                        {
                            LogManager.GetCurrentClassLogger()
                                .Info($"Nie udało się stworzyć obiektu dla faktury {invoiceHeadElements[6]}, błąd: {ex}");
                        }
                    }
                }
                else if (invoiceType == "FS" && line.StartsWith($"\"{invoiceType} "))
                {
                    Invoice invoice = new Invoice() { Number = "pusty" };
                    try
                    {
                        var lineElements = line.Split(',');
                        var invoiceNumber = lineElements[0].Replace("\"", "").Replace("FS ", "");
                        invoice = invoices.FirstOrDefault(inv => inv.Number == invoiceNumber);

                        var invoiceDateYear = Convert.ToInt32(lineElements[1].Replace("\"", "").Substring(0, 4));
                        var invoiceDateMonth = Convert.ToInt32(lineElements[1].Replace("\"", "").Substring(4, 2));
                        var invoiceDateDay = Convert.ToInt32(lineElements[1].Replace("\"", "").Substring(6, 2));

                        invoice.OperationDate = new DateTime(invoiceDateYear, invoiceDateMonth, invoiceDateDay);

                    }
                    catch (Exception ex)
                    {
                        LogManager.GetCurrentClassLogger()
                            .Info($"Nie udało się podmienić daty sprzedaży dla faktury {invoice.Number}, błąd: {ex}");
                    }
                }
            }
            return invoices;
        }
    }
}
