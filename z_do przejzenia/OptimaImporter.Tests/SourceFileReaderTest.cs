﻿using Microsoft.VisualStudio.TestTools.UnitTesting;
using OptimaImporter.Common;
using System.Linq;

namespace OptimaImporter.Tests
{
    [TestClass]
    public class SourceFileReaderTest
    {
        [TestMethod]
        public void ReadJpkFaPurchaseTest()
        {
            //var invoicesCount = 1219;

            //var filePath = @"D:\Dropbox\Klienci\ES\Faktury_zakupu_02_2017.xml";
            //var jpkPurchases = SourceFileReader.ReadJpkFaInvoices(filePath);

            //Assert.AreEqual(invoicesCount, jpkPurchases.Count);
        }

        private int sampleSubiektCount = 7;

        [TestMethod]
        public void ReadSubiektPurchase1StInvoiceTest()
        {
            //var filePaht = @"D:\Dropbox\Klienci\ES\rena-pol\magan_glowny.epp";
            //var subiektPurchases = SourceFileReader.ReadSubiektInvoices(filePaht, "FZ");
            //var invoicesCount = sampleSubiektCount;
            //var invoice = new Invoice
            //{
            //    Number = "F/700642/2017/487", //4
            //    SellerName = "DECATHLON Sp. z o.o.", //12
            //    City = "Warszawa", //14
            //    PostalCode = "03-290", //15
            //    Street = "Geodezyjna  76", //16
            //    SellerTaxNumber = "9511855233", //17
            //    InvoiceDate = new DateTime(2017, 02, 10), //21
            //    PaymentDate = new DateTime(2017, 02, 10), //34
            //    Currency = "PLN",
            //    CurrencyRate = "1.0000",
            //    Rows = new List<InvoiceRow>
            //    {
            //        new InvoiceRow()
            //    }
            //};

            //var invoiceProperties = invoice.GetType().GetProperties().Where(p => !p.Name.Contains("Rows"));

            //var invoicesCountTest = subiektPurchases.Count;
            //var secondInvoiceTest = subiektPurchases.ElementAt(0);

            //Assert.AreEqual(invoicesCount, invoicesCountTest);

            //foreach (var propertyInfo in invoiceProperties)
            //{
            //    var propertyValue = propertyInfo.GetValue(invoice);
            //    var testPropertyValue =
            //        secondInvoiceTest
            //            .GetType()
            //            .GetProperties()
            //            .First(x => x.Name == propertyInfo.Name).GetValue(secondInvoiceTest);
            //    Assert.AreEqual(propertyValue, testPropertyValue);
            //}

            //Assert.AreEqual(invoice.Rows.Count, secondInvoiceTest.Rows.Count);
        }

        [TestMethod]
        public void ReadSubiektPurchase5ThTest()
        {
            //var filePaht = @"D:\Dropbox\Klienci\ES\rena-pol\magan_glowny.epp";
            //var subiektPurchases = SourceFileReader.ReadSubiektInvoices(filePaht, "FZ");
            //var invoicesCount = sampleSubiektCount;
            //var invoice = new Invoice
            //{
            //    Number = "126/2017", //4
            //    SellerName = "Ośrodek Korekcji Wzroku sp.z o.o.", //12
            //    City = "Łódź", //14
            //    PostalCode = "90-408", //15
            //    Street = "Próchnika 1", //16
            //    SellerTaxNumber = "7250010606", //17
            //    InvoiceDate = new DateTime(2017, 02, 24), //21
            //    PaymentDate = new DateTime(2017, 03, 23), //22
            //    Currency = "PLN",
            //    CurrencyRate = "1.0000",
            //    Rows = new List<InvoiceRow>
            //    {
            //        new InvoiceRow(),
            //        new InvoiceRow()
            //    }
            //};

            //var invoiceProperties = invoice.GetType().GetProperties().Where(p => !p.Name.Contains("Rows"));

            //var invoicesCountTest = subiektPurchases.Count;
            //var secondInvoiceTest = subiektPurchases.ElementAt(4);

            //Assert.AreEqual(invoicesCount, invoicesCountTest);

            //foreach (var propertyInfo in invoiceProperties)
            //{
            //    var propertyValue = propertyInfo.GetValue(invoice);
            //    var testPropertyValue =
            //        secondInvoiceTest
            //            .GetType()
            //            .GetProperties()
            //            .First(x => x.Name == propertyInfo.Name).GetValue(secondInvoiceTest);
            //    Assert.AreEqual(propertyValue, testPropertyValue);
            //}

            //Assert.AreEqual(invoice.Rows.Count, secondInvoiceTest.Rows.Count);
        }

        [TestMethod]
        public void ReadSubiektPurchase7ThInvoiceTest()
        {
            //var filePaht = @"D:\Dropbox\Klienci\ES\rena-pol\magan_glowny.epp";
            //var subiektPurchases = SourceFileReader.ReadSubiektInvoices(filePaht, "FZ");
            //var invoicesCount = sampleSubiektCount;
            //var invoice = new Invoice
            //{
            //    Number = "2950979345", //4
            //    SellerName = "Honeywell Safety Products France SAS", //12
            //    City = "Villepinte FR-95958 Roissy CDG Cedex", //14
            //    PostalCode = "BP 55288", //15
            //    Street = "rue des Vanesses 33", //16
            //    SellerTaxNumber = "FR 9440331247", //17
            //    InvoiceDate = new DateTime(2017, 02, 06), //21
            //    PaymentDate = new DateTime(2017, 04, 07), //34
            //    Currency = "EUR",
            //    CurrencyRate = "4.2985",
            //    Rows = new List<InvoiceRow>
            //    {
            //        new InvoiceRow()
            //    }
            //};

            //var invoiceProperties = invoice.GetType().GetProperties().Where(p => !p.Name.Contains("Rows"));

            //var invoicesCountTest = subiektPurchases.Count;
            //var secondInvoiceTest = subiektPurchases.ElementAt(6);

            //Assert.AreEqual(invoicesCount, invoicesCountTest);

            //foreach (var propertyInfo in invoiceProperties)
            //{
            //    var propertyValue = propertyInfo.GetValue(invoice);
            //    var testPropertyValue =
            //        secondInvoiceTest
            //            .GetType()
            //            .GetProperties()
            //            .First(x => x.Name == propertyInfo.Name).GetValue(secondInvoiceTest);
            //    Assert.AreEqual(propertyValue, testPropertyValue);
            //}

            //Assert.AreEqual(invoice.Rows.Count, secondInvoiceTest.Rows.Count);
        }

        [TestMethod]
        public void ReadKolagenPurchaseFullTest()
        {
            var filePath = @"C:\Users\a\Desktop\kolagen\tabela 02.xlsx";
            var kolagenPurchases = SourceFileReader.ReadKolagenPurchases(filePath,"Sheet");
            var invoicesCount = 256;

            var invoicesCountTest = kolagenPurchases.Count;

            Assert.AreEqual(invoicesCount, invoicesCountTest);
        }

        [TestMethod]
        public void ReadSubiektSalesCOFullTest()
        {
            var filePaht = @"D:\Dropbox\Klienci\CenterOffice\export_12_2017.epp";
            var subiektPurchases = SourceFileReader.ReadSubiektInvoices(filePaht, "FS");
            var invoicesCount = 602;
            var invoicesWithoutRows= subiektPurchases.Where(i => i.Rows.Count == 0);
            var invoicesNetSum = 162867.75m;

            var invoicesCountTest = subiektPurchases.Count;
            var invoicesNetSumTest = subiektPurchases.Sum(i => i.Rows.Sum(r => r.GrossValue));

            Assert.AreEqual(invoicesCount, invoicesCountTest);
            //Assert.AreEqual(0, invoicesWithoutRows.Count());
            Assert.AreEqual(invoicesNetSum, invoicesNetSumTest);
        }

        [TestMethod]
        public void ReadFakturusCsvFileTest()
        {
            var filePath = @"C:\Users\a\Desktop\plkosctest.csv";
            SourceFileReader.ReadFakturusCsvFile(filePath);

        }


    }
}
