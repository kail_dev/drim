﻿using OptimaImporter.Data.SQL;
using Prism.Mvvm;
using System.Collections.Generic;
using System.Linq;

namespace OptimaImporter.Data.Config
{
    public sealed class ApplicationConfig : BindableBase
    {

        private static ApplicationConfig _config;
        private static readonly object KeyLock = new object();

        public ApplicationConfig()
        {
            OptimaUsersList = SqlServerConnector.GetOptimaUsers();
            OptimaDatabaseList = SqlServerConnector.GetOptimaDatabases();
            OptimaUser = SqlServerConnector.GetLastOperator();
        }

        public static ApplicationConfig Config
        {
            get
            {
                lock (KeyLock)
                {
                    return _config ?? (_config = new ApplicationConfig());
                }
            }
        }

        private string _optimaUser;
        public string OptimaUser
        {
            get => _optimaUser;
            set => SetProperty(ref _optimaUser, value);
        }

        private string _optimaPassword;
        public string OptimaPassword
        {
            get => _optimaPassword;
            set => SetProperty(ref _optimaPassword, value);
        }

        private string _optimaDatabase;
        public string OptimaDatabase
        {
            get => _optimaDatabase;
            set => SetProperty(ref _optimaDatabase, value);
        }

        public IEnumerable<string> OptimaUsersList { get; set; }

        public IEnumerable<OptimaDatabase> OptimaDatabaseList { get; set; }
        public IEnumerable<string> OptimaDatabases => OptimaDatabaseList.Select(d => d.OptimaName);
        public string OptimaDatabaseSqlName => OptimaDatabaseList.Single(d => d.OptimaName == OptimaDatabase).SqlName;

        public string OptimaDataBaseConnectionString => SqlServerConnector.CreateOptimaDatabaseConnectionString(OptimaDatabaseSqlName);
    }
}
