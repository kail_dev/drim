namespace OptimaImporter.Data.SQL
{
    public class OptimaDatabase
    {
        public string SqlName { get; set; }
        public string OptimaName { get; set; }
    }
}