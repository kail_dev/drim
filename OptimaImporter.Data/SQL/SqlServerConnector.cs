﻿using Microsoft.Win32;
using NLog;
using System;
using System.Collections.Generic;
using System.Data.SqlClient;

// ReSharper disable StringIndexOfIsCultureSpecific.1
// ReSharper disable StringLastIndexOfIsCultureSpecific.1

namespace OptimaImporter.Data.SQL
{
    public static class SqlServerConnector
    {
        private static Logger Logger => LogManager.GetCurrentClassLogger();

        private static readonly RegistryKey RegistryKey;
        private static string _registryValue;
        private static string _sqlServer;
        private static string _sqlOptimaConfigDatabase;
        public static readonly string SqlUser = "cdnoperator";
        public static readonly string SqlPassword = "snw2bdmk";
        private static SqlConnectionStringBuilder OptimaConfigConnectionStringBuilder
        {
            get
            {
                var sqlConnectionStringBuilder = new SqlConnectionStringBuilder
                {
                    DataSource = _sqlServer,
                    InitialCatalog = _sqlOptimaConfigDatabase,
                    IntegratedSecurity = false,
                    UserID = SqlUser,
                    Password = SqlPassword
                };
                return sqlConnectionStringBuilder;
            }
        }

        static SqlServerConnector()
        {
            RegistryKey = Registry.CurrentUser.OpenSubKey(@"SOFTWARE\CDN\CDN Opt!ma\CDN Opt!ma\Login", false);
            ReadSqlServerFromWindowsRegistry();
            ReadSqlOptimaConfigDatabaseFromWindowsRegistry();
            Logger.Info($"SQL Server: {_sqlServer} | Baza konfiguracyjna: {_sqlOptimaConfigDatabase}.");
        }

        private static void ReadSqlServerFromWindowsRegistry()
        {
            try
            {
                _registryValue = (string)RegistryKey?.GetValue("KonfigConnectStr");
                _sqlServer = _registryValue?.Substring(_registryValue.IndexOf(",") + 1, _registryValue.LastIndexOf(",") - _registryValue.IndexOf(",") - 1);
            }
            catch (Exception ex)
            {
                Logger.Error(ex, "Błąd odczytu nazwy serwera na podstawie 'Optimyowych' kluczy w rejestrze wystemu.");
            }
        }

        private static void ReadSqlOptimaConfigDatabaseFromWindowsRegistry()
        {
            try
            {
                _registryValue = (string)RegistryKey?.GetValue("KonfigConnectStr");
                _sqlOptimaConfigDatabase = _registryValue?.Substring(_registryValue.IndexOf(":") + 1, _registryValue.IndexOf(",") - _registryValue.IndexOf(":") - 1);
            }
            catch (Exception ex)
            {
                Logger.Error(ex, "Błąd odczytu bazy konfiguracyjnej na podstawie 'Optimyowych' kluczy w rejestrze wystemu.");
            }
        }

        internal static IEnumerable<OptimaDatabase> GetOptimaDatabases()
        {
            var optimaDatabasesList = new List<OptimaDatabase>();
            try
            {
                var sqlConnection = new SqlConnection(OptimaConfigConnectionStringBuilder.ConnectionString);
                var query = @"select baz_nazwa
                        ,substring(Baz_Dostep, charindex(':', Baz_Dostep) + 1, charindex(',', Baz_Dostep) - charindex(':', Baz_Dostep) - 1)
                        from cdn.bazy
                        join sys.databases on substring(Baz_Dostep, charindex(':', Baz_Dostep) + 1, charindex(',', Baz_Dostep) - charindex(':', Baz_Dostep) - 1)
                        	collate SQL_Polish_CP1250_CI_AS
						    = name collate SQL_Polish_CP1250_CI_AS
                        where Baz_Nieaktywna = 0
                        order by baz_nazwa
                        ";
                var sqlCommand = new SqlCommand(query, sqlConnection);
                sqlCommand.Connection.Open();
                var sqlDataReader = sqlCommand.ExecuteReader();
                while (sqlDataReader.Read())
                {
                    optimaDatabasesList.Add(new OptimaDatabase
                    {
                        OptimaName = sqlDataReader.GetString(0),
                        SqlName = sqlDataReader.GetString(1)
                    });
                }
                sqlDataReader.Close();
                sqlCommand.Connection.Close();
            }
            catch (Exception ex)
            {
                Logger.Error(ex, "Błąd odczytu listy firmowych baz danych Optimy. Dokonaj wymaganej konfiguracji.");
            }
            return optimaDatabasesList;
        }

        internal static IEnumerable<string> GetOptimaUsers()
        {
            var optimaUsersList = new List<string>();
            try
            {
                var sqlConnection = new SqlConnection(OptimaConfigConnectionStringBuilder.ConnectionString);

                var query = @"select ope_kod from cdn.operatorzy where Ope_Nieaktywny = 0 order by ope_kod";
                var sqlCommand = new SqlCommand(query, sqlConnection);
                sqlCommand.Connection.Open();
                var sqlDataReader = sqlCommand.ExecuteReader();
                optimaUsersList.Clear();
                while (sqlDataReader.Read())
                {
                    optimaUsersList.Add(sqlDataReader.GetString(0));
                }
                sqlDataReader.Close();
                sqlCommand.Connection.Close();
            }
            catch (Exception ex)
            {
                Logger.Error(ex, "Błąd odczytu listy użytkowników Optimy z bazy konfiguracyjnej. Dokonaj wymaganej konfiguracji.");
            }

            return optimaUsersList;
        }

        internal static string GetLastOperator()
        {
            try
            {
                return (string)RegistryKey.GetValue("Operator");
            }
            catch (Exception ex)
            {
                Logger.Error(ex, "Błąd odczytu operaotora, który ostatnio logował się do Optimy na podstawie 'Optimyowych' kluczy w rejestrze wystemu.");
            }

            return "";
        }

        internal static string CreateOptimaDatabaseConnectionString(string sqlOptimaDatabase)
        {
            var optimaConnectionStringBuilder =
                new SqlConnectionStringBuilder(OptimaConfigConnectionStringBuilder.ConnectionString)
                {
                    InitialCatalog = sqlOptimaDatabase
                };

            return optimaConnectionStringBuilder.ConnectionString;
        }
    }
}
