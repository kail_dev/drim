﻿using OptimaImporter.Data.Config;
using OptimaImporter.Data.Enum;
using System.Collections.Generic;
using System.Data.SqlClient;

namespace OptimaImporter.Data.SQL
{
    public static class OptimaSqlReader
    {
        public static IEnumerable<string> GetOptimaVatRegisters(VatRegisterType registerType)
        {
            var registers = new List<string>();

            var command = new SqlCommand($@"SELECT [Gru_Nazwa] FROM [CDN].[Grupy] where gru_typ = {(int)registerType} order by gru_nazwa",
                new SqlConnection(ApplicationConfig.Config.OptimaDataBaseConnectionString));
            command.Connection.Open();
            var sqlDataReader = command.ExecuteReader();
            while (sqlDataReader.Read())
            {
                var register = sqlDataReader.GetString(0);
                registers.Add(register);
            }
            sqlDataReader.Close();
            command.Connection.Close();

            return registers;
        }

        public static IEnumerable<string> GetOptimaKbRegisters()
        {
            var registers = new List<string>();

            var command = new SqlCommand($@"select BRa_Symbol from cdn.BnkRachunki where BRa_Nieaktywny = 0 order by BRa_Symbol",
                new SqlConnection(ApplicationConfig.Config.OptimaDataBaseConnectionString));
            command.Connection.Open();
            var sqlDataReader = command.ExecuteReader();
            while (sqlDataReader.Read())
            {
                var rejestr = sqlDataReader.GetString(0);
                registers.Add(rejestr);
            }
            sqlDataReader.Close();
            command.Connection.Close();

            return registers;
        }

        public static string GetPracKodBySurnameAndName(string surname, string name)
        {
            var command = new SqlCommand($@"select top 1 pre_kod from cdn.PracEtaty where PRE_Nazwisko = '{surname}' and PRE_Imie1 = '{name}' order by pre_preid desc",
                new SqlConnection(ApplicationConfig.Config.OptimaDataBaseConnectionString));
            command.Connection.Open();
            var sqlDataReader = command.ExecuteScalar()?.ToString();
            command.Connection.Close();

            return sqlDataReader;
        }

        public static int GetPracIdByPeselOrPassport(string peselOrPassport)
        {
            var command = new SqlCommand($"select top 1 pre_praid from cdn.PracEtaty where PRE_Pesel='{peselOrPassport}' or PRE_Paszport = '{peselOrPassport}' order by PRE_PreId desc",
                new SqlConnection(ApplicationConfig.Config.OptimaDataBaseConnectionString));
            command.Connection.Open();
            var result = command.ExecuteScalar();
            var praKod = int.Parse(result.ToString());
            command.Connection.Close();

            return praKod;
        }
        public static int GetPracLastCivilContractIdByPracId(int pracId)
        {
            var command = new SqlCommand($"select top 1 umw_umwid from cdn.Umowy where umw_praid = {pracId} order by umw_umwid desc",
                new SqlConnection(ApplicationConfig.Config.OptimaDataBaseConnectionString));
            command.Connection.Open();
            var praKod = int.Parse(command.ExecuteScalar().ToString());
            command.Connection.Close();

            return praKod;
        }

        public static int GetFplIdForSalaryCashPayment()
        {
            var command = new SqlCommand("select fpl_fplid from cdn.FormyPlatnosci where fpl_nazwa like 'gotówka wynag%'",
                new SqlConnection(ApplicationConfig.Config.OptimaDataBaseConnectionString));
            command.Connection.Open();
            var praKod = int.Parse(command.ExecuteScalar().ToString());
            command.Connection.Close();

            return praKod;
        }
    }
}
