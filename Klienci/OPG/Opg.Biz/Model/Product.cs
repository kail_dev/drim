﻿namespace Opg.Biz.Model
{
    public class Product
    {
        public string Name { get; set; }
        public string Amount { get; set; }
    }
}
