﻿using System;
using System.Collections.Generic;

namespace Opg.Biz.Model
{
    public class RentInvoice
    {
        public RentInvoice()
        {
            Products = new List<RentProduct>();
        }
        public string Client { get; set; }
        public string Lp { get; set; }

        public string PaymentMethod { get; set; }
        public DateTime PaymentDate { get; set; }
        public DateTime DocDate { get; set; }
        public string Currency { get; set; }
        public string Category { get; set; }
        public string Description { get; set; }

        public List<RentProduct> Products { get; set; }

    }

    public class RentProduct
    {
        public string ProductName { get; set; }
        public string Amount { get; set; }
        public string Description { get; set; }
        public string NetPrice { get; set; }
        public string Discount { get; set; }
        public string Value { get; set; }
        public string BeforeDiscount { get; set; }
        public string ProductCategory { get; set; }
        public string Attribute { get; set; }
        public string AttributeValue { get; set; }

    }
}
