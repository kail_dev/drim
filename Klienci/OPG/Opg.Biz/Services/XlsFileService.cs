﻿using System;
using NLog;
using Opg.Biz.Model;
using System.Collections.Generic;
using System.Data.OleDb;
using System.Diagnostics;
using System.Linq;

namespace Opg.Biz.Services
{
    public class XlsFileService
    {
        public static List<RentInvoice> ReadRentInvoicesFile(string filePath, string xlsSheet)
        {
            var invoices = new List<RentInvoice>();

            var constr = "Provider=Microsoft.ACE.OLEDB.12.0;Data Source="
                + filePath + ";Extended Properties=\"Excel 12.0 Xml;HDR=YES;\"";
            using (var conn = new OleDbConnection(constr))
            {
                conn.Open();
                var command = new OleDbCommand("Select * from [" + xlsSheet + "$]", conn);
                var reader = command.ExecuteReader();
                if (reader.HasRows)
                {
                    while (reader.Read() && !string.IsNullOrWhiteSpace(reader[0].ToString()))
                    {
                        if (reader[1].ToString() == "1")
                        {
                            var inv = new RentInvoice();
                            inv.Client = reader[0].ToString();
                            inv.Lp = reader[1].ToString();
                            inv.PaymentMethod = reader[4].ToString();
                            inv.DocDate = Convert.ToDateTime(reader[13].ToString());
                            inv.PaymentDate = Convert.ToDateTime(reader[14].ToString());
                            inv.Currency = reader[10].ToString();
                            inv.Category = reader[11].ToString();
                            inv.Description = reader[17].ToString();
                            inv.Products.Add(new RentProduct
                            {
                                Amount = reader[2].ToString(),
                                ProductName = reader[3].ToString(),
                                Description = reader[5].ToString(),
                                Discount = reader[6].ToString(),
                                NetPrice = reader[7].ToString(),
                                Value = reader[8].ToString(),
                                BeforeDiscount = reader[9].ToString(),
                                ProductCategory = reader[12].ToString(),
                                Attribute = reader[15].ToString(),
                                AttributeValue = reader[16].ToString()
                            });
                            invoices.Add(inv);
                        }
                        else
                        {
                            var product = new RentProduct
                            {
                                Amount = reader[2].ToString(),
                                ProductName = reader[3].ToString(),
                                Description = reader[5].ToString(),
                                Discount = reader[6].ToString(),
                                NetPrice = reader[7].ToString(),
                                Value = reader[8].ToString(),
                                BeforeDiscount = reader[9].ToString(),
                                ProductCategory = reader[12].ToString(),
                                Attribute = reader[15].ToString(),
                                AttributeValue = reader[16].ToString()
                            };
                            invoices.LastOrDefault()?.Products.Add(product);
                        }
                    }
                    LogManager.GetCurrentClassLogger().Info($"Odczyt pliku zakończony.");
                }
                if (!reader.HasRows)
                {
                    LogManager.GetCurrentClassLogger().Info($"Plik nie posiada zawartości.");
                }
            }
            return invoices;
        }

        public static List<Invoice> ReadInvoicesFile(string filePath, string xlsSheet)
        {
            var invoices = new List<Invoice>();

            var constr = "Provider=Microsoft.ACE.OLEDB.12.0;Data Source="
                            + filePath + ";Extended Properties=\"Excel 12.0 Xml;HDR=YES;\"";
            using (var conn = new OleDbConnection(constr))
            {
                conn.Open();
                var command = new OleDbCommand("Select * from [" + xlsSheet + "$]", conn);
                var reader = command.ExecuteReader();
                if (reader.HasRows)
                {
                    while (reader.Read())
                    {
                        var columnsCount = reader.FieldCount; //ilośc kolumn w danym wierszu

                        var inv = new Invoice();
                        inv.Client = reader[0].ToString();
                        inv.Amount = reader[1].ToString();
                        inv.NetPrice = reader[columnsCount - 1].ToString();

                        for (var i = 2; i < columnsCount - 1; i++)
                        {
                            var product = new Product();
                            product.Name = reader.GetName(i);
                            product.Amount = reader[i].ToString();
                            inv.Products.Add(product);
                        } 


                        invoices.Add(inv);
                        LogManager.GetCurrentClassLogger().Info("Kontrahent: " + inv.Client+ ", ilość: " + inv.Amount+ ", netto: " + inv.NetPrice);
                        Debug.WriteLine("Kontrahent: " + inv.Client+ ", ilość: " + inv.Amount+ ", netto: " + inv.NetPrice);
                    }
                }
            }
            return invoices;
        }

    }
}
