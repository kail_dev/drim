﻿using CDNBase;
using NLog;
using Opg.Biz.Model;
using System;
using System.Collections.Generic;
using System.Diagnostics;

namespace Opg.Biz.Services
{
    public class OptimaInvoiceCreator
    {

        public static int CreateOptimaRentInvoices (IProgress<int> progress,List<RentInvoice> rentinvoices )
        {
            var invCreatedCount = 0;
            var readInvCounter = 0;
            foreach (var rentInvoice in rentinvoices)
            {
                try
                {
                    if (!string.IsNullOrWhiteSpace(rentInvoice.Client))
                    {
                        var session = new AdoSession();

                        CDNHeal.IKontrahent contractor;

                        var faktury = (CDNHlmn.DokumentyHaMag)session.CreateObject("CDN.DokumentyHaMag");
                        var faktura = (CDNHlmn.IDokumentHaMag)faktury.AddNew();
                        var formyPlatnosci = (ICollection)(session.CreateObject("CDN.FormyPlatnosci"));
                        var kategorie = (CDNHeal.Kategorie)session.CreateObject("CDN.Kategorie");

                        try
                        {
                            contractor = (CDNHeal.IKontrahent)session.CreateObject("CDN.Kontrahenci")
                                    .Item("Knt_Kod = '" + rentInvoice.Client + "'");
                            //.Item("Knt_Kod = '!NIEOKREŚLONY!'");
                        }
                        catch (Exception)
                        {
                            contractor = (CDNHeal.IKontrahent)session.CreateObject("CDN.Kontrahenci")
                                    .Item("Knt_Kod = '!NIEOKREŚLONY!'");
                            LogManager.GetCurrentClassLogger()
                                .Error($"Nie odnaleziono kontrahenta o kodzie {rentInvoice.Client}. " + $"Wybrano kontrahenta !Nieokreślonego!");
                        }
                        faktura.Rodzaj = 302000;
                        faktura.TypDokumentu = 302;
                        faktura.Bufor = 1;
                        faktura.DataWys = rentInvoice.DocDate;
                        faktura.DataDok = rentInvoice.DocDate;
                        faktura.Podmiot = contractor;
                        try
                        {
                            faktura.FormaPlatnosci = (OP_KASBOLib.FormaPlatnosci)formyPlatnosci["FPl_Nazwa = '"
                                                                                              + rentInvoice.PaymentMethod + "'"];
                        }
                        catch (Exception)
                        {
                            LogManager.GetCurrentClassLogger()
                                .Error($"Nie odnaleziono formy płatności {rentInvoice.PaymentMethod}");
                        }

                        faktura.Termin = rentInvoice.PaymentDate;

                        try
                        {
                            faktura.Kategoria = (CDNHeal.Kategoria)kategorie[$"Kat_KodSzczegol='{rentInvoice.Category}'"];
                        }
                        catch (Exception)
                        {
                            LogManager.GetCurrentClassLogger().Error($"Nie odnaleziono kategorii {rentInvoice.Category}.");
                        }

                        try
                        {
                            faktura.WalutaSymbol = rentInvoice.Currency;
                        }
                        catch (Exception)
                        {
                            LogManager.GetCurrentClassLogger().Error($"Nie udało mi się ustawić waluty {rentInvoice.Currency}.");
                        }

                        faktura.Uwagi = rentInvoice.Description;

                        var pozycje = faktura.Elementy;

                        foreach (var element in rentInvoice.Products)
                        {
                            var pozycja = (CDNHlmn.IElementHaMag)pozycje.AddNew();
                            try
                            {
                                pozycja.TowarKod = element.ProductName;
                            }
                            catch (Exception)
                            {
                                LogManager.GetCurrentClassLogger().Error($"Nie odnaleziono towaru o kodzie {element.ProductName}");
                            }
                            try
                            {
                                pozycja.Kategoria = (CDNHeal.Kategoria)kategorie[$"Kat_KodSzczegol='{element.ProductCategory}'"];
                            }
                            catch (Exception)
                            {
                                LogManager.GetCurrentClassLogger().Error($"Nie odnaleziono kategorii CZYNSZ");
                            }

                            pozycja.UstawNazweTowaru(element.Description);

                            pozycja.Ilosc = Convert.ToDouble(element.Amount);
                            if (string.IsNullOrWhiteSpace(element.BeforeDiscount))
                            {
                                element.BeforeDiscount = "0";
                            }
                            pozycja.Cena0 = Convert.ToDecimal(element.BeforeDiscount);
                            pozycja.Rabat = Convert.ToDouble(element.Discount);
                            pozycja.CenaT = Convert.ToDecimal(element.NetPrice);

                            pozycja.AtrybutHaMag1.Kod = $"{element.Attribute}";
                            pozycja.AtrybutHaMag1.Wartosc = $"{element.AttributeValue}";
                        }

                        session.Save();
                        invCreatedCount++;
                        LogManager.GetCurrentClassLogger().Info("Utworzyłem fakturę o numerze " + faktura.NumerPelny);
                    }
                }
                catch (Exception ex)
                {
                    LogManager.GetCurrentClassLogger().Error($"Wystąpił błąd podczas tworzenia faktury dla kontrahenta {rentInvoice.Client}. Błąd {ex}");
                }
                progress?.Report(++readInvCounter);
            }
            return invCreatedCount;
        }

        public static void CreateOptimaInvoice(Invoice invoice)
        {
            if (!string.IsNullOrWhiteSpace(invoice.Amount))
            {
                var session = new AdoSession();

                CDNHeal.IKontrahent contractor;

                var faktury = (CDNHlmn.DokumentyHaMag)session.CreateObject("CDN.DokumentyHaMag");
                var faktura = (CDNHlmn.IDokumentHaMag)faktury.AddNew();

                var formyPlatnosci = (ICollection)(session.CreateObject("CDN.FormyPlatnosci"));
                var fPl = (OP_KASBOLib.FormaPlatnosci)formyPlatnosci[1];

                try
                {
                    contractor = (CDNHeal.IKontrahent)session.CreateObject("CDN.Kontrahenci")
                            .Item("Knt_Kod = '" + invoice.Client + "'");
                }
                catch (Exception)
                {
                    contractor = (CDNHeal.IKontrahent)session.CreateObject("CDN.Kontrahenci")
                            .Item("Knt_Kod = '!NIEOKREŚLONY!'");
                }
                faktura.Rodzaj = 302000;
                faktura.TypDokumentu = 302;
                faktura.Bufor = 1;
                faktura.DataDok = DateTime.Now;
                faktura.FormaPlatnosci = fPl;
                faktura.Podmiot = contractor;


                foreach (var product in invoice.Products)
                {
                    var pozycje = faktura.Elementy;
                    var pozycja = (CDNHlmn.IElementHaMag)pozycje.AddNew();
                    pozycja.TowarKod = product.Name;
                    //Pozycja.Ilosc = Convert.ToDouble(product.Amount);
                    pozycja.Ilosc = 1;
                    pozycja.WartoscNetto = Convert.ToDecimal(product.Amount);
                }
                
                session.Save();

                LogManager.GetCurrentClassLogger().Info("Utworzyłem fakturę " + faktura.NumerPelny);
                Debug.WriteLine("Utworzyłem fakturę " + faktura.NumerPelny);

            }
        }
    }
}
