﻿using OptimaImporter.Data.Config;
using System.ComponentModel;
using System.Linq;
using System.Windows;
using Telerik.Windows.Controls;

namespace Opg.View
{
    /// <summary>
    /// Interaction logic for ConfigurationView.xaml
    /// </summary>
    public partial class ConfigurationView
    {
        public ConfigurationView()
        {
            InitializeComponent();
        }

        private void ConfigurationView_OnLoaded(object sender, RoutedEventArgs e)
        {
            //odczytujemy z konfiguracji listę użytkowników Optimy i jako wybranego na liście ustawiamy zapisanego użytkownika z Settingsow
            OptimaUserCombobox.ItemsSource = ApplicationConfig.Config.OptimaUsersList;
            OptimaUserCombobox.SelectedItem = ApplicationConfig.Config.OptimaUser;
            //odczytujemy hasło dla użytkownika optimowego
            OptimaPasswordPasswordBox.Password = ApplicationConfig.Config.OptimaPassword;
            //odczytujemy z konfiguracji listę baz Optimy i jako wybrana na liście ustawiamy zapisana baze z Settingsow
            OptimaDatabaseCombobox.ItemsSource = ApplicationConfig.Config.OptimaDatabaseList.Select(x => x.OptimaName);
            OptimaDatabaseCombobox.SelectedItem = ApplicationConfig.Config.OptimaDatabase;
        }

        private void ConfigurationView_OnClosing(object sender, CancelEventArgs e)
        {
            bool? dialogResult = null;
            RadWindow.Confirm(new DialogParameters
            {
                Header = "Zamykanie okna konfiguracji",
                OkButtonContent = "Tak",
                CancelButtonContent = "Nie, tylko zamknij okno",
                Content = "Czy chcesz zapisać konfigurację?",
                Closed = (confirmDialog, eventArgs) =>
                {
                    dialogResult = eventArgs.DialogResult;
                }

            });
            if (dialogResult == true)
            {
                if (OptimaUserCombobox.SelectedItem != null)
                {
                    ApplicationConfig.Config.OptimaUser = OptimaUserCombobox.SelectedItem.ToString();
                }
                if (OptimaDatabaseCombobox.SelectedItem != null)
                {
                    ApplicationConfig.Config.OptimaDatabase = OptimaDatabaseCombobox.SelectedItem.ToString();
                }
                ApplicationConfig.Config.OptimaPassword = OptimaPasswordPasswordBox.Password;
            }
        }
    }
}
