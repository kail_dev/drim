using Microsoft.Win32;
using NLog;
using Opg.Biz.Model;
using Opg.Biz.Services;
using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.IO;
using System.Threading.Tasks;
using System.Windows;
using OptimaImporter.Biz.Optima;
using Telerik.Windows.Controls;

namespace Opg.View
{
    /// <summary>
    /// Interaction logic for MainWindow.xaml
    /// </summary>
    public partial class MainWindowView
    {
        private OpenFileDialog _openedFile;

        public MainWindowView()
        {
            StyleManager.ApplicationTheme = new Office_BlueTheme();

            InitializeComponent();
        }

        private void MainWindowView_OnLoaded(object sender, RoutedEventArgs e)
        {
            LogManager.GetCurrentClassLogger().Info("Start programu " + DateTime.Now);
        }

        private void LogButton_OnClick(object sender, RoutedEventArgs e)
        {
            var fileInfo = new FileInfo(AppDomain.CurrentDomain.BaseDirectory + "\\Log");
            Process.Start(fileInfo.FullName);
        }

        private async void GenerateRentInvoicesButton_OnClick(object sender, RoutedEventArgs e)
        {
            StatusLabel.Content = " ";
            _openedFile = new OpenFileDialog();
            _openedFile.Title = "Wybierz plik do wczytania";

            var result = _openedFile.ShowDialog();
            if (result != null && (bool) result)
            {
                //sprawdzanie logowania do O!
                var optimaLoginValid = false;
                while (optimaLoginValid == false)
                {
                    optimaLoginValid = OptimaLogginIn();
                }
                StatusLabel.Content = "Zalogowa�em si� do O!";


                GenerateRentInvoicesButton.IsEnabled = false;
                
                //czytanie .xls
                var rentInvoices = XlsFileService.ReadRentInvoicesFile(_openedFile.FileName, "CZYNSZ NAJMU");

                //wczytanie do Optimy
                await GenerateRentInvoicesAsync(rentInvoices)
                    .ContinueWith(t =>
                    {
                        Dispatcher.Invoke(() =>
                        {
                            GenerateRentInvoicesButton.IsEnabled = true;
                            StatusLabel.Content += $". Zakonczono!!! Wczytano {t.Result}";
                        });
                    });


            }
        }


        private async Task<int> GenerateRentInvoicesAsync(List<RentInvoice> rentinvoices)
        {
            var stopWatch = Stopwatch.StartNew();
            var rentInvoicesCount = rentinvoices.Count;
            var progress = new Progress<int>(p =>
            {
                var percent = p / (decimal)rentInvoicesCount * 100;
                StatusLabel.Content = $"Wczytywanie faktur.. {p}/{rentInvoicesCount},   {percent:0} %," +
                                      $"   {stopWatch.Elapsed.Hours}:{stopWatch.Elapsed.Minutes}:{stopWatch.Elapsed.Seconds}";
            });
            return await Task.Run(() => OptimaInvoiceCreator.CreateOptimaRentInvoices(progress, rentinvoices));
        }

        private void GenerateInvoicesButton_OnClick(object sender, RoutedEventArgs e)
        {
            _openedFile = new OpenFileDialog();
            _openedFile.Title = "Wybierz pliki do konwersji";

            var result = _openedFile.ShowDialog();
            if (result != null && (bool)result)
            {

                //czytanie .xls
                var invoices = XlsFileService.ReadInvoicesFile(_openedFile.FileName, "OP�ATA K");
                StatusLabel.Content = "Wczytano plik .xls";


                //sprawdzanie logowania do O!
                var optimaLoginValid = false;
                while (optimaLoginValid == false)
                {
                    optimaLoginValid = OptimaLogginIn();
                    StatusLabel.Content = "Zalogowa�em si� do O!";
                }

                foreach (var invoice in invoices)
                {
                    OptimaInvoiceCreator.CreateOptimaInvoice(invoice);
                }

                //ToDo: Wylogowanie


                //status
                StatusLabel.Content = "Utworzy�em faktury w O!";
            }
        }

        private bool OptimaLogginIn()
        {
            try
            {
                OptimaLogging.LogIn();
                return true;
            }
            catch (Exception)
            {
                MessageBox.Show("B��d logowania do Optimy. Popraw konfiguracj�...");
                RunConfigurationWindow();
            }
            return false;
        }

        private void RunConfigurationWindow()
        {
            var confWindow = new ConfigurationView();
            confWindow.Owner = this;
            confWindow.ShowDialog();
        }

        private void ConfigurationButton_OnClick(object sender, RoutedEventArgs e)
        {
            RunConfigurationWindow();
        }

        private void AppFolderButton_OnClick(object sender, RoutedEventArgs e)
        {
            Process.Start(AppDomain.CurrentDomain.BaseDirectory);
        }

        private void SettingsFolderButton_OnClick(object sender, RoutedEventArgs e)
        {
            Process.Start(System.Windows.Forms.Application.LocalUserAppDataPath);
        }


    }
}
