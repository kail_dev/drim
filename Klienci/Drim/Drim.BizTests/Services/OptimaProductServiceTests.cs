﻿using Drim.Biz.Services;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using System.Linq;

namespace Drim.BizTests.Services
{
    [TestClass]
    public class OptimaProductServiceTests
    {
        [TestMethod]
        public void GetProductsAvailabilityTest()
        {
            var products = OptimaProductService.GetProductsAvailability(6);
            Assert.IsTrue(products.Any());
        }
        [TestMethod]
        public void GetProductToRemoveFromVirtualWarehouseTest()
        {
            var products = OptimaProductService.GetProductToRemoveFromVirtualWarehouse(6, 1);
            //var prodList = new List<InputFileProduct>();
            //foreach (var product in products)
            //{
            //    var inputProd = new InputFileProduct
            //    {
            //        Code = product.Code,
            //        Amount = product.Amount
            //    };
            //    prodList.Add(inputProd);
            //}

            //int counter = 0;
            //var progress = new Progress<int>();
            //OptimaDocumentService.CreateWarehouseDocument(progress, ref counter, prodList, 304000, 304, 6); //rw

            Assert.IsTrue(products.Any());
        }


    }
}