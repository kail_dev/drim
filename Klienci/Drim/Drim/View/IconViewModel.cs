﻿namespace Drim.View
{
    public class IconViewModel
    {
        public string AppFolder => GetPath("AppFolder.png");

        public string Configuration => GetPath("Configuration.png");

        public string SettingsFolder => GetPath("SettingsFolder.png");

        public string Clear => GetPath("Clear.png");

        public string Log => GetPath("Log.png");
        public string SzeranLogo => GetPath("szeran_logo.png");


        private string GetPath(string iconString)
        {
            return "/Widzew;component/Images/" + iconString;
        }
    }
}
