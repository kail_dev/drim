using Drim.Biz.Model;
using Drim.Biz.Services;
using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Linq;
using System.Threading.Tasks;
using System.Windows;
using OptimaImporter.Biz.Optima;
using Telerik.Windows.Controls;
using MessageBox = System.Windows.MessageBox;
using OpenFileDialog = Microsoft.Win32.OpenFileDialog;

namespace Drim.View
{
    public partial class MainWindowView : Window
    {
        private OpenFileDialog _openedFile;
        private readonly MainWindowViewModel _viewModel;

        public MainWindowView(MainWindowViewModel viewModel)
        {
            _viewModel = viewModel;
            DataContext = _viewModel;

            StyleManager.ApplicationTheme = new Office_BlueTheme();
            InitializeComponent();
        }

        private void LogButton_OnClick(object sender, RoutedEventArgs e)
        {
            _viewModel.OpenLogFolder();
        }
           private async void LoadFileButton_OnClick1(object sender, RoutedEventArgs e)
        {
            StatusLabel.Content = " ";
            _openedFile = new OpenFileDialog { Title = "Wybierz plik do wczytania" };

            var result = _openedFile.ShowDialog();
            if (result != null && (bool)result)
            {
                //sprawdzanie logowania do O!
                var optimaLoginValid = false;
                while (optimaLoginValid == false)
                {
                    optimaLoginValid = OptimaLogginIn();
                }

                LoadWarehouseStateButton.IsEnabled = false;

                await ClearVirtualWarehouse()
                    .ContinueWith(t1 =>
                    {
                        var products = XlsFileService.drimread(_openedFile.FileName);
                        if (products.Any())
                        {
                            GenerateOptimaDocumentsAsync(products)
                                .ContinueWith(t =>
                                {
                                    Dispatcher.Invoke(() =>
                                    {
                                        LoadWarehouseStateButton2.IsEnabled = true;
                                        StatusLabel.Content += $". Zako�czono!!!";
                                    });
                                });
                        }
                    });
            }
        }

        private async void LoadFileButton_OnClick(object sender, RoutedEventArgs e)
        {
            StatusLabel.Content = " ";
            _openedFile = new OpenFileDialog { Title = "Wybierz plik do wczytania" };

            var result = _openedFile.ShowDialog();
            if (result != null && (bool)result)
            {
                //sprawdzanie logowania do O!
                var optimaLoginValid = false;
                while (optimaLoginValid == false)
                {
                    optimaLoginValid = OptimaLogginIn();
                }

                LoadWarehouseStateButton.IsEnabled = false;

                await ClearVirtualWarehouse()
                    .ContinueWith(t1 =>
                    {
                        var products = XlsFileService.ReadWarehouseStateFile(_openedFile.FileName, "Arkusz1");
                        if (products.Any())
                        {
                            GenerateOptimaDocumentsAsync(products)
                                .ContinueWith(t =>
                                {
                                    Dispatcher.Invoke(() =>
                                    {
                                        LoadWarehouseStateButton.IsEnabled = true;
                                        StatusLabel.Content += $". Zako�czono!!!";
                                    });
                                });
                        }
                    });
            }
        }

        private async Task ClearVirtualWarehouse()
        {
            var stopWatch = Stopwatch.StartNew();
            stopWatch.Start();

            var products = OptimaProductService.GetProductToRemoveFromVirtualWarehouse(6, 1);
            var prodList = new List<InputFileProduct>();
            foreach (var product in products)
            {
                var inputProd = new InputFileProduct
                {
                    Code = product.Code,
                    Amount = product.Amount
                };
                prodList.Add(inputProd);
            }

            var counter = 0;
            var counterMax = products.Count;
            var progress = new Progress<DrimProgress>(p =>
            {
                var percent = p.Counter / (decimal)counterMax * 100;
                Dispatcher.Invoke(() => {
                    StatusLabel.Content = $"1) Czyszczenie magazynu wirtualnego {p.Counter}/{p.Max}, {percent:0} %," +
                                          $" {stopWatch.Elapsed.Hours}:{stopWatch.Elapsed.Minutes}:{stopWatch.Elapsed.Seconds}";
                });
            });
            var task = Task.Run(() => OptimaDocumentService.CreateWarehouseDocument(progress, ref counter, ref counterMax,
                prodList, 304000, 304, 6)); //rw
            await task.ContinueWith(p => stopWatch.Stop());
        }

        private async Task GenerateOptimaDocumentsAsync(List<InputFileProduct> products)
        {
            var stopWatch = Stopwatch.StartNew();
            stopWatch.Start();

            var counter = 0;
            var counterMax = 0;
            var progress = new Progress<DrimProgress>(p =>
            {
                var percent = p.Counter / (decimal)counterMax * 100;
                Dispatcher.Invoke(() =>
                {
                    StatusLabel.Content = $"2) Generowanie r�nic {p.Counter}/{p.Max},   {percent:0} %," +
                                          $"   {stopWatch.Elapsed.Hours}:{stopWatch.Elapsed.Minutes}:{stopWatch.Elapsed.Seconds}";
                });
            });
            var task = Task.Run(() => OptimaDocumentService.CreateWarehouseDifferences(progress, ref counter, ref counterMax, products));
            await task.ContinueWith(p => stopWatch.Stop());
        }

        private bool OptimaLogginIn()
        {
            try
            {
                OptimaLogging.LogIn();
                return true;
            }
            catch (Exception)
            {
                MessageBox.Show("B��d logowania do Optimy. Popraw konfiguracj�...");
                RunConfigurationWindow();
            }
            return false;
        }

        private void RunConfigurationWindow()
        {
            var confWindow = new ConfigurationView { Owner = this };
            confWindow.ShowDialog();
        }

        private void ConfigurationButton_OnClick(object sender, RoutedEventArgs e)
        {
            RunConfigurationWindow();
        }

        private void AppFolderButton_OnClick(object sender, RoutedEventArgs e)
        {
            Process.Start(AppDomain.CurrentDomain.BaseDirectory);
        }

        private void SettingsFolderButton_OnClick(object sender, RoutedEventArgs e)
        {
            Process.Start(System.Windows.Forms.Application.LocalUserAppDataPath);
        }

        private void RadRibbon_SelectionChanged(object sender, RadSelectionChangedEventArgs e)
        {

        }
    }
}
