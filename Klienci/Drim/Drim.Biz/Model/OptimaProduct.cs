﻿namespace Drim.Biz.Model
{
    public class OptimaProduct
    {
        public string Code { get; set; }
        public decimal Amount { get; set; }
        public bool IsInXlsFile { get; set; }
    }
}
