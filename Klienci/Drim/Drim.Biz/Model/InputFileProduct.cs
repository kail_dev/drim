﻿namespace Drim.Biz.Model
{
    public class InputFileProduct
    {
        public string Code { get; set; }
        public string Name { get; set; }
        public decimal Amount { get; set; }
        public decimal NetValue { get; set; }
        public string Group { get; set; }
    }

}
