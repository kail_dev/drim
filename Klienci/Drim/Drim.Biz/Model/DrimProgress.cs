﻿namespace Drim.Biz.Model
{
    public class DrimProgress
    {
        public int Counter { get; set; }
        public int Max { get; set; }
    }
}
