﻿using CDNBase;
using Drim.Biz.Model;
using NLog;
using System;
using System.Collections.Generic;
using System.Data.SqlClient;
using System.Text;
using OptimaImporter.Data.Config;

namespace Drim.Biz.Services
{
    public class OptimaProductService
    {
        public static string CreateProduct(InputFileProduct product)
        {

            try
            {
                var session = new AdoSession();
                var towar = (CDNTwrb1.ITowar)session.CreateObject("CDN.Towary").AddNew();
                towar.Kod = product.Code;
                towar.Nazwa = product.Name;
                towar.Stawka = 23m;
                towar.StawkaZak = 23m;
                towar.Flaga = 2;
                towar.FlagaZak = 2;
                towar.JM = "szt.";

                CDNTwrb1.ICena cena = towar.Ceny[4];
                cena.Wartosc = Convert.ToDecimal(product.NetValue);
                cena.Aktualizacja = 1;
                cena.Zaokraglenie = 0.5m;

                towar.NumerCeny = 5;

                var opisBuilder = new StringBuilder();
                opisBuilder.Append("<b>Skala:</b>");
                opisBuilder.Append("<br><b>Model plastikowy do sklejania</b>");
                opisBuilder.Append("<br><b>Nie zawiera kleju oraz farb.</b>");
                towar.Opis = opisBuilder.ToString();

                var grupa = (CDNTwrb1.TwrGrupa)session.CreateObject("CDN.TwrGrupy")["twg_kod = 'WIR1'"];
                towar.TwGGIDNumer = grupa.GIDNumer;


                var atrybuty = (ICollection)(session.CreateObject("CDN.DefAtrybuty"));
                var atrybut = (CDNTwrb1.IDefAtrybut)atrybuty["dea_Kod = 'ZDJĘCIE'"];
                var atrybutZdjecie = (CDNTwrb1.ITwrAtrybut)towar.Atrybuty.AddNew();
                atrybutZdjecie.DefAtrybut = (CDNTwrb1.DefAtrybut)atrybut;


                var esklep = (CDNTwrb1.TwrESklep)towar.ParametryESklep.AddNew();
                esklep.ESklepId = 1;
                esklep.ESklepDostepnosc = 3;
                esklep.Udostepnij = 0;

                session.Save();
                LogManager.GetCurrentClassLogger().Info($"Utworzyłem towar {product.Code}");
                return towar.Kod;
            }
            catch (Exception ex)
            {
                LogManager.GetCurrentClassLogger().Error($"Nie udało się utworzyć towaru {product.Code}." +
                                                         $" Błąd: {ex}, {ex.InnerException}, {ex.Message}, {ex.StackTrace}");
                return string.Empty;
            }
        }

        public static string OptimaProductCode(InputFileProduct product)
        {
            var session = new AdoSession();

            try
            {
                //szukanie towaru po kodzie
                var towar = (CDNTwrb1.ITowar)session.CreateObject("CDN.Towary").Item($"Twr_Kod = '{product.Code}'");
                LogManager.GetCurrentClassLogger().Info("Znaleziono towar" + product.Code);
                return towar.Kod;
            }
            catch (Exception)
            {
                try
                {
                    //szukanie towaru po kodzie dostawcy
                    var towarPoKodzieUDostawcy = (CDNTwrb1.ITowar)session.CreateObject("CDN.Towary")
                        .Item($"Twr_KodDostawcy = '{product.Code}'");
                    LogManager.GetCurrentClassLogger().Info("Znaleziono towar o kodzie dostawcy" + product.Code);
                    return towarPoKodzieUDostawcy.Kod;
                }
                catch (Exception)
                {
                    LogManager.GetCurrentClassLogger().Error($"Nie udało się znaleźć towaru w Optimie");
                    return string.Empty;
                }
            }
        }

        public static List<OptimaProduct> GetProductsAvailability(int warehouseId)
        {
            var products = new List<OptimaProduct>();
            try
            {
                var sqlConnection =
                    new SqlConnection(ApplicationConfig.Config.OptimaDatabaseSqlName);
                var query = $@"
                                  SELECT t.twr_kod, sum(twz_ilosc) as Ilosc
                                  FROM [CDN].[TwrZasoby] as z
                                  join cdn.towary as t on twz_twrid = twr_twrid
                                  where twz_magid = {warehouseId}
                                  group by t.twr_twrid, t.twr_kod
                                ";
                var sqlCommand = new SqlCommand(query, sqlConnection);
                sqlCommand.Connection.Open();
                var sqlDataReader = sqlCommand.ExecuteReader();
                while (sqlDataReader.Read())
                {
                    var optimaProduct = new OptimaProduct
                    {
                        Code = sqlDataReader[0].ToString(),
                        Amount = Convert.ToDecimal(sqlDataReader[1].ToString())
                    };
                    products.Add(optimaProduct);

                }
                sqlConnection.Close();

            }
            catch (Exception ex)
            {
                throw new Exception("Błąd odczytania listy baz danych. Dokonaj wymaganej konfiguracji...", ex);
            }
            return products;
        }
        public static List<OptimaProduct> GetProductToRemoveFromVirtualWarehouse(int virtualWarehouseId, int mainWarehouseId)
        {
            var products = new List<OptimaProduct>();
            try
            {
                var sqlConnection =
                    new SqlConnection(ApplicationConfig.Config.OptimaDatabaseSqlName);
                var query = $@"
                            select wir.Twr_Kod, wir.Ilosc from
                            (SELECT t.twr_kod, sum(twz_ilosc) as Ilosc
                            FROM [CDN].[TwrZasoby] as z
                            join cdn.towary as t on twz_twrid = twr_twrid
                            where twz_magid = {virtualWarehouseId}
                            group by t.twr_twrid, t.twr_kod) as wir
                            join
                            (SELECT t.twr_kod, sum(twz_ilosc) as Ilosc
                            FROM [CDN].[TwrZasoby] as z
                            join cdn.towary as t on twz_twrid = twr_twrid
                            where twz_magid = {mainWarehouseId}
                            group by t.twr_twrid, t.twr_kod) as main
                            on wir.Twr_Kod = main.Twr_Kod
                                ";
                var sqlCommand = new SqlCommand(query, sqlConnection);
                sqlCommand.Connection.Open();
                var sqlDataReader = sqlCommand.ExecuteReader();
                while (sqlDataReader.Read())
                {
                    var optimaProduct = new OptimaProduct
                    {
                        Code = sqlDataReader[0].ToString(),
                        Amount = Convert.ToDecimal(sqlDataReader[1].ToString())
                    };
                    products.Add(optimaProduct);

                }
                sqlConnection.Close();

            }
            catch (Exception ex)
            {
                throw new Exception("Błąd odczytania listy baz danych. Dokonaj wymaganej konfiguracji...", ex);
            }
            return products;
        }
    }
}
