﻿using Drim.Biz.Model;
using NLog;
using System;
using System.Collections.Generic;
using System.Data.OleDb;

using ExcelDataReader;
using System.IO;

namespace Drim.Biz.Services
{
    
    public class XlsFileService
    {
        public static List<InputFileProduct> drimread(string filePath)
        {
            FileStream stream = File.Open(filePath, FileMode.Open, FileAccess.Read);

            IExcelDataReader excelReader = ExcelReaderFactory.CreateReader(stream);

            var xls = new List<InputFileProduct>();
            while (excelReader.Read())
            {
                if (excelReader.GetString(2) == "Symbol")
                {

                }
                else
                {


                    xls.Add(new InputFileProduct
                    {
                        Code = excelReader.GetString(2),
                        Name = excelReader.GetString(3),
                        Amount = Convert.ToDecimal(excelReader.GetDouble(7)),
                              NetValue = Convert.ToDecimal(excelReader.GetDouble(10)),
                        Group = excelReader.GetString(9)
                        //





                    }) ;
                }
            }
            return xls;
        }

        public static List<InputFileProduct> ReadWarehouseStateFile(string filePath, string xlsSheet)
        {
            var invoices = new List<InputFileProduct>();
            var lineCounter = 1;
           
            try
            {
                var constr = "Provider=Microsoft.ACE.OLEDB.12.0;Data Source="
                             + filePath + ";Extended Properties=\"Excel 12.0 Xml;HDR=YES;\"";
                using (var conn = new OleDbConnection(constr))
                {
                    //var excel = new Application();
                    //Worksheet workSheet = excel.Workbooks.Open(filePath).Worksheets[1];
                    //var workSheetName = workSheet.Name;
                    conn.Open();
                    var command = new OleDbCommand("Select * from [" + xlsSheet + "$]", conn);
                    var reader = command.ExecuteReader();
                    if (reader != null && reader.HasRows)
                    {
                        while (reader.Read() )
                        {
                            lineCounter++;
                            var inv = new InputFileProduct
                            {
                                Code = reader[0].ToString(),
                                Name = reader[1].ToString(),
                                Amount = Convert.ToDecimal(reader[2].ToString()),
                                NetValue = Convert.ToDecimal(reader[3].ToString())
                            };
                            invoices.Add(inv);
                        }
                        LogManager.GetCurrentClassLogger().Info($"Odczyt pliku zakończony. Odczytano {lineCounter} linii.");
                    }
                    if (reader != null && !reader.HasRows)
                    {
                        LogManager.GetCurrentClassLogger().Info($"Plik nie posiada zawartości.");
                    }
                    //excel.Workbooks.Close();
                }
            }
            catch (Exception ex)
            {
                LogManager.GetCurrentClassLogger().Error($"Błąd odczytu pliku {ex.Message}.");
            }
            return invoices;
        }
    }
}
