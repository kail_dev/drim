﻿using CDNBase;
using CDNHlmn;
using Drim.Biz.Model;
using NLog;
using System;
using System.Collections.Generic;
using System.Linq;

namespace Drim.Biz.Services
{
    public class OptimaDocumentService
    {

        public static void CreateWarehouseDifferences (IProgress<DrimProgress> progress, ref int counter, ref int counterMax, List<InputFileProduct> xlsProducts )
        {
            //Todo: zamienić na pobierane z konfiguracji
            var session = new AdoSession();
            var warehouse = (IMagazyn)session.CreateObject("CDN.Magazyny").Item("Mag_Symbol = 'WIR1'");
            var mainWarehouse = (IMagazyn)session.CreateObject("CDN.Magazyny").Item("Mag_Symbol = 'MAGAZYN'");

            var pwList = new List<InputFileProduct>();
            var rwList = new List<InputFileProduct>();

            //sprawdzanie czy towar jest na głównym magazynie, jeśli tak, to sprawdzamy stan towaru i całą ilość dodajemy do rwlist

            //stany na magazynie WIR1
            var warehouseProducts = OptimaProductService.GetProductsAvailability(warehouse.ID).ToList();
            var mainwarehouseProducts = OptimaProductService.GetProductsAvailability(mainWarehouse.ID).ToList();

            var listaTmp = xlsProducts.Where(p => !mainwarehouseProducts.Select(mp => mp.Code).Contains(p.Code)).ToList();

            foreach (var product in listaTmp)
            {
                var warehouseProduct = warehouseProducts.SingleOrDefault(p => p.Code == product.Code);
                if (warehouseProduct != null)
                {
                    if (warehouseProduct.Amount > product.Amount)
                    {
                        product.Amount = warehouseProduct.Amount - product.Amount;
                        rwList.Add(product);
                    }
                    else if (warehouseProduct.Amount < product.Amount)
                    {
                        product.Amount = product.Amount - warehouseProduct.Amount;
                        pwList.Add(product);
                    }
                    warehouseProduct.IsInXlsFile = true;
                }
                else if (product.Amount > 0)
                {
                    pwList.Add(product);
                }
            }

            var productsInOptimaNotInFile = warehouseProducts.Where(op => !op.IsInXlsFile).Select(nif => new InputFileProduct
            {
                Code = nif.Code,
                Amount = nif.Amount
            });
            rwList.AddRange(productsInOptimaNotInFile);
            counterMax = pwList.Count + rwList.Count;

            //dzielimy na dokumenty po 300 pozycji każdy
            var filesBatches = new List<IEnumerable<InputFileProduct>>();
            const int batchSize = 300;
            if (pwList.Any())
            {
                for (var i = 0; i < pwList.Count; i += batchSize)
                {
                    filesBatches.Add(pwList.GetRange(i, Math.Min(batchSize, pwList.Count - i)));
                }

                foreach (var batch in filesBatches)
                {
                    CreateWarehouseDocument(progress, ref counter, ref counterMax, batch, 303000, 303, warehouse.ID);
                }
                filesBatches.Clear();
            }

            if (rwList.Any())
            {
                for (var i = 0; i < rwList.Count; i += batchSize)
                {
                    filesBatches.Add(rwList.GetRange(i, Math.Min(batchSize, rwList.Count - i)));
                }

                foreach (var batch in filesBatches)
                {
                    CreateWarehouseDocument(progress, ref counter, ref counterMax, batch, 304000, 304, warehouse.ID);
                }
            }
        }

        public static void CreateWarehouseDocument(IProgress<DrimProgress> progress, ref int counter, ref int counterMax,
            IEnumerable<InputFileProduct> products, int rodzaj, int typ, int warehouseId)
        {
            try
            {
                var session = new AdoSession();
                var document = (IDokumentHaMag)session.CreateObject("CDN.DokumentyHaMag").AddNew(null);

               var contractor = (CDNHeal.IKontrahent)session.CreateObject("CDN.Kontrahenci")
                    .Item("Knt_KntId = 1"); 

                document.Rodzaj = rodzaj;
                document.TypDokumentu = typ;
                document.MagazynZrodlowyID = warehouseId;
                document.Bufor = 1;
                document.Podmiot = contractor;

                var elements = document.Elementy;
                if (!products.Any()) return;
                foreach (var product in products)
                {
                    var productCode = "";
                    if (!string.IsNullOrWhiteSpace(product.Code) && product.Amount > 0)
                    {
                        productCode = !string.IsNullOrEmpty(OptimaProductService.OptimaProductCode(product)) 
                            ? OptimaProductService.OptimaProductCode(product) 
                            : OptimaProductService.CreateProduct(product);
                    }

                    if (string.IsNullOrEmpty(productCode)) continue;
                    var pozycjaDok = (IElementHaMag)elements.AddNew();
                    pozycjaDok.TowarKod = productCode;
                    pozycjaDok.Ilosc = Convert.ToDouble(product.Amount);
                    pozycjaDok.CenaT = Convert.ToDecimal(product.NetValue);

                    progress.Report(new DrimProgress { Counter = ++counter, Max = counterMax});
                }

                session.Save();
            }
            catch (Exception ex)
            {
                LogManager.GetCurrentClassLogger().Error($"Wystąpił błąd podczas tworzenia dokumentu na towar. Błąd {ex}");

            }
        }
    }
}
