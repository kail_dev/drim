﻿using System;
using DK.Biz.Model;
using NLog;
using System.Collections.Generic;
using System.Data.OleDb;
using System.Linq;

namespace DK.Biz.Services
{
    public class XlsFileService
    {
        public static List<SalesInvoice> ReadSalesInvoicesFile(string filePath, string xlsSheet)
        {
            var invoices = new List<SalesInvoice>();

            var constr = "Provider=Microsoft.ACE.OLEDB.12.0;Data Source="
                + filePath + ";Extended Properties=\"Excel 12.0 Xml;HDR=YES;\"";
            using (var conn = new OleDbConnection(constr))
            {
                conn.Open();
                var command = new OleDbCommand("Select * from [" + xlsSheet + "$]", conn);
                var reader = command.ExecuteReader();
                if (reader != null && reader.HasRows)
                {
                    while (reader.Read() && !string.IsNullOrWhiteSpace(reader[0].ToString()))
                    {
                        if (reader[2].ToString() == "1")
                        {
                            var inv = new SalesInvoice();
                            inv.DocSeries = reader[0].ToString();
                            inv.Client = reader[1].ToString();
                            inv.Lp = reader[2].ToString();
                            inv.PaymentMethod = reader[5].ToString();
                            inv.Products.Add(new InvoiceProduct
                            {
                                Amount = reader[3].ToString(),
                                ProductCode = reader[4].ToString(),
                                ProductName = reader[6].ToString(),
                                Description = reader[8].ToString(),
                                Value = reader[7].ToString(),
                                Category = reader[9].ToString()
                            });
                            invoices.Add(inv);
                        }
                        else
                        {
                            var product = new InvoiceProduct
                            {
                                Amount = reader[3].ToString(),
                                ProductCode = reader[4].ToString(),
                                ProductName = reader[6].ToString(),
                                Description = reader[8].ToString(),
                                Value = reader[7].ToString(),
                                Category = reader[9].ToString()
                            };
                            invoices.LastOrDefault()?.Products.Add(product);
                        }
                    }
                    LogManager.GetCurrentClassLogger().Info($"Odczyt pliku zakończony.");
                }
                else
                {
                    LogManager.GetCurrentClassLogger().Info($"Nie udało się odczytać zawartości pliku lub brak w nim umów.");
                }
            }
            return invoices;
        }

        public static List<CivilLegalContractAndCashAmount> ReadCivilLegalContracts(string filePath, string xlsSheet)
        {
            var counter = 0;
            var contracts = new List<CivilLegalContractAndCashAmount>();

            var constr = "Provider=Microsoft.ACE.OLEDB.12.0;Data Source="
                + filePath + ";Extended Properties=\"Excel 12.0 Xml;HDR=YES;\"";
            using (var conn = new OleDbConnection(constr))
            {
                conn.Open();
                var command = new OleDbCommand("Select * from [" + xlsSheet + "$]", conn);
                var reader = command.ExecuteReader();
                if (reader != null && reader.HasRows)
                {
                    while (reader.Read())
                    {
                        if (string.IsNullOrWhiteSpace(reader[0].ToString()))
                        {
                            break;
                        }
                        counter++;
                        Console.WriteLine(reader[0].ToString());
                        try
                        {
                            var contract = new CivilLegalContractAndCashAmount();
                            contract.PeselOrPassport = reader[0].ToString();
                            LogManager.GetCurrentClassLogger().Info($"Wiesz nr: {counter}, pesel lub paszport: {contract.PeselOrPassport}.");
                            contract.GrossValue = decimal.Parse(reader[1].ToString());
                            LogManager.GetCurrentClassLogger().Info($"Wiesz nr: {counter}, kwota brutto: {contract.GrossValue}.");
                            contract.NetValue = decimal.Parse(reader[2].ToString());
                            LogManager.GetCurrentClassLogger().Info($"Wiesz nr: {counter}, kwota netto: {contract.NetValue}.");
                            try
                            {
                                contract.StartDate = new DateTime(Convert.ToInt32(reader[3].ToString().Substring(6, 4)), Convert.ToInt32(reader[3].ToString().Substring(3, 2)), Convert.ToInt32(reader[3].ToString().Substring(0, 2)));
                                contract.EndDate = new DateTime(Convert.ToInt32(reader[4].ToString().Substring(6, 4)), Convert.ToInt32(reader[4].ToString().Substring(3, 2)), Convert.ToInt32(reader[4].ToString().Substring(0, 2)));
                            }
                            catch (Exception)
                            {
                                contract.StartDate = new DateTime(Convert.ToInt32(reader[3].ToString().Substring(0, 4)), Convert.ToInt32(reader[3].ToString().Substring(5, 2)), Convert.ToInt32(reader[3].ToString().Substring(8, 2)));
                                contract.EndDate = new DateTime(Convert.ToInt32(reader[4].ToString().Substring(0, 4)), Convert.ToInt32(reader[4].ToString().Substring(5, 2)), Convert.ToInt32(reader[4].ToString().Substring(8, 2)));
                            }
                            contract.CashAmount = decimal.Parse(reader[5].ToString());
                            if (!string.IsNullOrWhiteSpace(contract.PeselOrPassport))
                            {
                                contracts.Add(contract);
                                LogManager.GetCurrentClassLogger().Info($"Poprawny odczyt wiersza nr: {counter}."); 
                            }
                        }
                        catch (Exception ex)
                        {
                            LogManager.GetCurrentClassLogger().Info($"Błąd odczytu danych w wierszu nr: {counter}. Błąd: {ex}");
                        }
                    }
                    LogManager.GetCurrentClassLogger().Info($"Odczyt pliku zakończony.");
                }
                if (reader != null && !reader.HasRows)
                {
                    LogManager.GetCurrentClassLogger().Info($"Plik nie posiada zawartości.");
                }
            }

            return contracts;
        }

    }
}
