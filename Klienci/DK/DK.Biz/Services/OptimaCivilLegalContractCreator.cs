﻿using CDNBase;
using CDNHeal;
using CDNPlcfg;
using CDNPrac;
using DK.Biz.Model;
using NLog;
using System;
using System.Collections.Generic;
using OptimaImporter.Data.SQL;

namespace DK.Biz.Services
{
    public static class OptimaCivilLegalContractCreator
    {
        public static int CreateOptimaCivilLegalContracts(IProgress<int> progress, List<CivilLegalContractAndCashAmount> contracts)
        {
            LogManager.GetCurrentClassLogger().Info($"Do wczytania jest {contracts.Count} umów.");

            var invCreatedCount = 0;
            foreach (var contract in contracts)
            {
                try
                {
                    DodanieUmowyPracownika(contract);
                    LogManager.GetCurrentClassLogger().Info($"Poprawnie utworzono umowę dla pracownika {contract.PeselOrPassport}.");
                }
                catch (Exception ex)
                {
                    LogManager.GetCurrentClassLogger().Error($"Wystąpił błąd podczas tworzenia umowy dla pracownika {contract.PeselOrPassport}. Błąd {ex}");
                }
                progress?.Report(++invCreatedCount);
            }
            return invCreatedCount;
        }

        static void DodanieUmowyPracownika(CivilLegalContractAndCashAmount contract)
        {
            var sesja = new AdoSession();

            var pracownicy = (Pracownicy)sesja.CreateObject("CDN.Pracownicy");
            var praId = OptimaSqlReader.GetPracIdByPeselOrPassport(contract.PeselOrPassport);
            LogManager.GetCurrentClassLogger().Info($"Znalazłem pracownika o kodzie {praId}.");

            var pracownik = (Pracownik)pracownicy[$"PRA_Praid='{praId}'"];
            LogManager.GetCurrentClassLogger().Info($"Id pracownika: {pracownik.ID}, kod pracownika, {pracownik.Kod}.");
            var umowy = pracownik.Umowy;
            LogManager.GetCurrentClassLogger().Info($"Pracownik posiada {umowy.Count} umów.");
            var umwId = OptimaSqlReader.GetPracLastCivilContractIdByPracId(praId);
            var ostatniaUmowa = (Umowa)pracownik.Umowy[$"umw_umwid={umwId}"];
            LogManager.GetCurrentClassLogger().Info($"Id ostatniej umowy pracownika: {umwId}.");
            var umowa = (Umowa)umowy.AddNew();

            //umowa.Okres.Od = new DateTime(DateTime.Today.AddMonths(-1).Year, DateTime.Today.AddMonths(-1).Month, 1);
            //umowa.Okres.Do = new DateTime(DateTime.Today.AddMonths(-1).Year, DateTime.Today.AddMonths(-1).Month,
            //    (new DateTime(DateTime.Today.Year, DateTime.Today.Month, 1).AddDays(-1)).Day);
            umowa.Okres.Od = contract.StartDate;
            umowa.Okres.Do = contract.EndDate;

            //umowa.Rodzaj = "PIT-8B  6.Przychody z osob.wyk.działaln.";

            //var skladnik = (TypSkladnika) sesja.CreateObject("CDNPlcfg.TypySkladnikow").Item("TWP_TwpId=27"); //dzieło
            //var skladnik = (TypSkladnika) sesja.CreateObject("CDNPlcfg.TypySkladnikow").Item("TWP_TwpId=26");
            umowa.Skladnik.TypSkladnika = ostatniaUmowa.Skladnik.TypSkladnika;

            //tytubezp, tytulyibezp
            umowa.Ubezp.TyUb4 = ostatniaUmowa.Ubezp.TyUb4;
            umowa.Ubezp.JestEmerytal = ostatniaUmowa.Ubezp.JestEmerytal;
            umowa.Ubezp.JestRentowe = ostatniaUmowa.Ubezp.JestRentowe;
            umowa.Ubezp.JestChorobowe = ostatniaUmowa.Ubezp.JestChorobowe;
            umowa.Ubezp.JestWypad = ostatniaUmowa.Ubezp.JestWypad;
            umowa.Ubezp.ZusOd = umowa.Okres.Od;
            umowa.Ubezp.ZdrowOd = umowa.Okres.Od;
            umowa.Ubezp.JestDobrChorobowe = ostatniaUmowa.Ubezp.JestDobrChorobowe;
            umowa.Ubezp.OgraniczSklZdrow = ostatniaUmowa.Ubezp.OgraniczSklZdrow;

            umowa.StawkaPodatku = ostatniaUmowa.Skladnik.TypSkladnika.Nazwa.StartsWith("IFT") ? 20 : 17;
            umowa.KosztyProcent = ostatniaUmowa.KosztyProcent;

            var netto = contract.NetValue;
            var serwis = new PracSerwis();
            serwis.UmowaNetto(umowa, netto);
            umowa.Brutto = contract.GrossValue + netto + serwis.UmowaZUS + serwis.UmowaZdrowotne + serwis.UmowaFIS;

            sesja.Save();

            try
            {
                var salaryCashPaymentId = OptimaSqlReader.GetFplIdForSalaryCashPayment();
                ZmianaSchematuPlatnosci(praId, contract, salaryCashPaymentId);
            }
            catch (Exception ex)
            {
                LogManager.GetCurrentClassLogger().Error($"Ustawienie kwoty gotówki dla pracownika {contract.PeselOrPassport} nie powiodło się. Błąd: {ex}");
            }
        }

        static void ZmianaSchematuPlatnosci(int praId, CivilLegalContractAndCashAmount contract, int salaryCashPaymentId)
        {
            var sesja = new AdoSession();
            var pracownicy = (Pracownicy)sesja.CreateObject("CDN.Pracownicy");
            var pracownik = (Pracownik)pracownicy[$"PRA_Praid='{praId}'"];
            var schematy = pracownik.GetHistoriaAktualna(DateTime.Now, 0).SchematyPlatnosci;
            var schemat = (SchematPlatnosci)schematy[$"spl_fplid = {salaryCashPaymentId}"];
            schemat.Kwota = contract.CashAmount;
            sesja.Save();
        }
    }
}
