﻿using CDNBase;
using DK.Biz.Model;
using NLog;
using System;
using System.Collections.Generic;

namespace DK.Biz.Services
{
    public class OptimaInvoiceCreator
    {

        public static int CreateOptimaRentInvoices (IProgress<int> progress,List<SalesInvoice> salesinvoices )
        {
            var invCreatedCount = 0;
            var readInvCounter = 0;
            foreach (var salesInvoice in salesinvoices)
            {
                try
                {
                    if (!string.IsNullOrWhiteSpace(salesInvoice.Client))
                    {
                        var session = new AdoSession();

                        CDNHeal.IKontrahent contractor;

                        var optimaInvoices = (CDNHlmn.DokumentyHaMag)session.CreateObject("CDN.DokumentyHaMag");
                        var optimaInvoice = (CDNHlmn.IDokumentHaMag)optimaInvoices.AddNew();
                        var paymentMethods = (ICollection)(session.CreateObject("CDN.FormyPlatnosci"));
                        var categories = (CDNHeal.Kategorie)session.CreateObject("CDN.Kategorie");

                        

                        try
                        {
                            contractor = (CDNHeal.IKontrahent)session.CreateObject("CDN.Kontrahenci")
                                    .Item("Knt_Kod = '" + salesInvoice.Client + "'");
                        }
                        catch (Exception)
                        {
                            contractor = (CDNHeal.IKontrahent)session.CreateObject("CDN.Kontrahenci")
                                    .Item("Knt_Kod = '!NIEOKREŚLONY!'");
                            LogManager.GetCurrentClassLogger()
                                .Error($"Nie odnaleziono kontrahenta o kodzie {salesInvoice.Client}. " 
                                       + $"Wybrano kontrahenta !Nieokreślonego!");
                        }
                        optimaInvoice.Rodzaj = 302000;
                        optimaInvoice.TypDokumentu = 302;
                        optimaInvoice.Bufor = 1;
                        optimaInvoice.DataDok = DateTime.Now;
                        optimaInvoice.Podmiot = (CDNHeal.IPodmiot)contractor;
                        try
                        {
                            var numerator = (OP_KASBOLib.INumerator)optimaInvoice.Numerator;
                            numerator.Rejestr = salesInvoice.DocSeries;
                        }
                        catch (Exception)
                        {
                            LogManager.GetCurrentClassLogger()
                                .Error($"Błąd ustawiania serii {salesInvoice.DocSeries} na dokumencie.");
                        }
                        try
                        {
                            optimaInvoice.FormaPlatnosci = (OP_KASBOLib.FormaPlatnosci)paymentMethods["FPl_Nazwa = '"
                                                                                              + salesInvoice.PaymentMethod + "'"];
                        }
                        catch (Exception)
                        {
                            LogManager.GetCurrentClassLogger()
                                .Error($"Nie odnaleziono formy płatności {salesInvoice.PaymentMethod}");
                        }
                        var Pozycje = optimaInvoice.Elementy;

                        foreach (var element in salesInvoice.Products)
                        {
                            var Pozycja = (CDNHlmn.IElementHaMag)Pozycje.AddNew();
                            try
                            {
                                Pozycja.TowarKod = element.ProductCode;
                            }
                            catch (Exception)
                            {
                                LogManager.GetCurrentClassLogger().Error($"Nie odnaleziono towaru o kodzie {element.ProductCode}");
                            }
                            try
                            {
                                Pozycja.Kategoria = (CDNHeal.Kategoria)categories["Kat_KodOgolny='"+element.Category+"'"];
                            }
                            catch (Exception)
                            {
                                LogManager.GetCurrentClassLogger().Error($"Nie odnaleziono kategorii {element.Category}");
                            }
                            Pozycja.UstawNazweTowaru(element.ProductName);
                            Pozycja.TowarOpis = element.Description;
                            Pozycja.Ilosc = Convert.ToDouble(element.Amount);
                            Pozycja.WartoscNetto = Convert.ToDecimal(element.Value); 
                        }

                        session.Save();
                        invCreatedCount++;
                        LogManager.GetCurrentClassLogger().Info("Utworzyłem fakturę o numerze " + optimaInvoice.NumerPelny);
                    }
                }
                catch (Exception ex)
                {
                    LogManager.GetCurrentClassLogger().Error($"Wystąpił błąd podczas tworzenia faktury dla kontrahenta {salesInvoice.Client}. Błąd {ex}");
                }
                progress?.Report(++readInvCounter);
            }
            return invCreatedCount;
        }

        protected static void SetProperty(object o, string name, object value)  //wymagane do DodaniaOBD
        {
            o?.GetType().InvokeMember(name, System.Reflection.BindingFlags.SetProperty, null, o, new[] { value });
        } //wymagane do ustawienia numeracji dokumentów
    }
}
