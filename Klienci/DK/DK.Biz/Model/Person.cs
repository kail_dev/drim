﻿using System;

namespace DK.Biz.Model
{
    public class CivilLegalContractAndCashAmount
    {
        public string PeselOrPassport { get; set; }
        public decimal GrossValue { get; set; }
        public decimal NetValue { get; set; }
        public DateTime StartDate { get; set; }
        public DateTime EndDate { get; set; }
        public decimal CashAmount { get; set; }
    }
}