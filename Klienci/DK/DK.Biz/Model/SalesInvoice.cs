﻿using System.Collections.Generic;

namespace DK.Biz.Model
{
    public class SalesInvoice
    {
        public SalesInvoice()
        {
            Products = new List<InvoiceProduct>();
        }
        public string DocSeries { get; set; }
        public string Client { get; set; }
        public string Lp { get; set; }
        public string PaymentMethod { get; set; }
        public List<InvoiceProduct> Products { get; set; }
    }

    public class InvoiceProduct
    {
        public string ProductCode { get; set; }
        public string ProductName { get; set; }
        public string Amount { get; set; }
        public string Description { get; set; }
        public string Value { get; set; }
        public string Category { get; set; }


    }
}
