﻿using System;
using System.Diagnostics;
using System.IO;
using NLog;

namespace DK.View
{
    public class MainWindowViewModel
    {
        public MainWindowViewModel()
        {
            LogManager.GetCurrentClassLogger().Info("Start programu " + DateTime.Now);
        }

        public void OpenLogFolder()
        {
            var fileInfo = new FileInfo(AppDomain.CurrentDomain.BaseDirectory + "\\Log");
            Process.Start(fileInfo.FullName);
        }
    }
}
