using DK.Biz.Model;
using DK.Biz.Services;
using Microsoft.Win32;
using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Threading.Tasks;
using System.Windows;
using OptimaImporter.Biz.Optima;
using Telerik.Windows.Controls;

namespace DK.View
{
    /// <summary>
    /// Interaction logic for MainWindow.xaml
    /// </summary>
    public partial class MainWindowView
    {
        private OpenFileDialog _openedFile;
        private readonly MainWindowViewModel _viewModel;

        public MainWindowView(MainWindowViewModel viewModel)
        {
            _viewModel = viewModel;
            DataContext = _viewModel;

            StyleManager.ApplicationTheme = new Office_BlueTheme();
            InitializeComponent();
        }

        private void LogButton_OnClick(object sender, RoutedEventArgs e)
        {
            _viewModel.OpenLogFolder();
        }

        private async void GenerateRentInvoicesButton_OnClick(object sender, RoutedEventArgs e)
        {
            StatusLabel.Content = " ";
            _openedFile = new OpenFileDialog();
            _openedFile.Title = "Wybierz plik do wczytania";

            var result = _openedFile.ShowDialog();
            if (result != null && (bool)result)
            {
                //sprawdzanie logowania do O!
                var optimaLoginValid = false;
                while (optimaLoginValid == false)
                {
                    optimaLoginValid = OptimaLogginIn();
                }
                StatusLabel.Content = "Zalogowa�em si� do O!";


                GenerateRentInvoicesButton.IsEnabled = false;
                //czytanie .xls
                var rentInvoices = XlsFileService.ReadSalesInvoicesFile(_openedFile.FileName, "Arkusz1");

                await GenerateRentInvoicesAsync(rentInvoices)
                    .ContinueWith(t =>
                    {
                        Dispatcher.Invoke(() =>
                        {
                            GenerateRentInvoicesButton.IsEnabled = true;
                            StatusLabel.Content += $". Zakonczono!!! Wczytano {t.Result}";
                        });
                    });


            }
        }

        private async Task<int> GenerateRentInvoicesAsync(List<SalesInvoice> rentinvoices)
        {
            var stopWatch = Stopwatch.StartNew();
            var rentInvoicesCount = rentinvoices.Count;
            var progress = new Progress<int>(p =>
            {
                var percent = p / (decimal)rentInvoicesCount * 100;
                StatusLabel.Content = $"Wczytywanie faktur... {p}/{rentInvoicesCount},   {percent:0} %," +
                                      $"   {stopWatch.Elapsed.Hours}:{stopWatch.Elapsed.Minutes}:{stopWatch.Elapsed.Seconds}";
            });
            return await Task.Run(() => OptimaInvoiceCreator.CreateOptimaRentInvoices(progress, rentinvoices));
        }

        private async void GenerateCivilLegalContractsButton_OnClick(object sender, RoutedEventArgs e)
        {
            StatusLabel.Content = " ";
            _openedFile = new OpenFileDialog();
            _openedFile.Title = "Wybierz plik do wczytania";

            var result = _openedFile.ShowDialog();
            if (result != null && (bool)result)
            {
                //sprawdzanie logowania do O!
                var optimaLoginValid = false;
                while (optimaLoginValid == false)
                {
                    optimaLoginValid = OptimaLogginIn();
                }
                StatusLabel.Content = "Zalogowa�em si� do O!";


                GenerateCivilLegalContractsButton.IsEnabled = false;
                //czytanie .xlss
                var contracts = XlsFileService.ReadCivilLegalContracts(_openedFile.FileName, "Arkusz1");


                await GenerateCivilLegelContractsAsync(contracts)
                    .ContinueWith(t =>
                    {
                        Dispatcher.Invoke(() =>
                        {
                            GenerateCivilLegalContractsButton.IsEnabled = true;
                            StatusLabel.Content += $". Zakonczono!!! Wczytano {t.Result}";
                        });
                    });

                GenerateCivilLegalContractsButton.IsEnabled = true;
            }
        }
        private async Task<int> GenerateCivilLegelContractsAsync(List<CivilLegalContractAndCashAmount> contracts)
        {
            var stopWatch = Stopwatch.StartNew();
            var contractsCount = contracts.Count;
            var progress = new Progress<int>(p =>
            {
                var percent = p / (decimal)contractsCount * 100;
                StatusLabel.Content = $"Wczytywanie um�w... {p}/{contractsCount},   {percent:0} %," +
                                      $"   {stopWatch.Elapsed.Hours}:{stopWatch.Elapsed.Minutes}:{stopWatch.Elapsed.Seconds}";
            });
            return await Task.Run(() => OptimaCivilLegalContractCreator.CreateOptimaCivilLegalContracts(progress, contracts));
        }

        private bool OptimaLogginIn()
        {
            try
            {
                OptimaLogging.LogIn();
                return true;
            }
            catch (Exception)
            {
                MessageBox.Show("B��d logowania do Optimy. Popraw konfiguracj�...");
                RunConfigurationWindow();
            }
            return false;
        }

        private void RunConfigurationWindow()
        {
            var confWindow = new ConfigurationView();
            confWindow.Owner = this;
            confWindow.ShowDialog();
        }

        private void ConfigurationButton_OnClick(object sender, RoutedEventArgs e)
        {
            RunConfigurationWindow();
        }

        private void AppFolderButton_OnClick(object sender, RoutedEventArgs e)
        {
            Process.Start(AppDomain.CurrentDomain.BaseDirectory);
        }

        private void SettingsFolderButton_OnClick(object sender, RoutedEventArgs e)
        {
            Process.Start(System.Windows.Forms.Application.LocalUserAppDataPath);
        }
    }
}
