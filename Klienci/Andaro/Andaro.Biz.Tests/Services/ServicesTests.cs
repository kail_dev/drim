﻿using Andaro.Biz.Services;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using OptimaImporter.Common;

namespace Andaro.Biz.Tests.Services
{
    [TestClass]
    public class ServicesTests
    {
        public ServicesTests()
        {
            ApplicationConfig.Config.OptimaUser = "ADMIN";
            ApplicationConfig.Config.OptimaPassword = "a";
            ApplicationConfig.Config.OptimaDatabase = "ANDARO";
        }

        [TestMethod]
        public void ReadWarehouseFileFromXlsTest()
        {
            var filePath = @"C:\dev\github\optimaimporter\Klienci\Andaro\Andaro\Files\Andaro.xlsx";
            var elementsCount = 102;

            var xlsProducts = SourceFileReader.ReadWarehouseFileFromXls(filePath);

            Assert.AreEqual(elementsCount, xlsProducts.Count);
        }

        [TestMethod]
        public void LogInTest()
        {
            var loggedIn = OptimaLogging.LogIn();

            Assert.IsTrue(loggedIn);
        }

        [TestMethod]
        public void IsProductInOptimaTestTrue()
        {
            OptimaLogging.LogIn();
            var productCode = "06262 001";
            var isProductInOptima = OptimaProductService.IsProductInOptima(productCode);

            Assert.IsTrue(isProductInOptima);
        }

        [TestMethod]
        public void IsProductInOptimaTestFalse()
        {
            OptimaLogging.LogIn();
            var productCode = "qwerty";
            var isProductInOptima = OptimaProductService.IsProductInOptima(productCode);

            Assert.IsFalse(isProductInOptima);
        }

        [TestMethod]
        public void CreateWarehouseDocumentTest()
        {
            var countersInput = 102;
            var countersOuptus = 101;
            var counters = new Counters();
            var filePath = @"C:\dev\github\optimaimporter\Klienci\Andaro\Andaro\Files\Andaro.xlsx";
            var xlsProducts = SourceFileReader.ReadWarehouseFileFromXls(filePath);
            OptimaLogging.LogIn();
            var warehouseDocumentId = OptimaDocumentService.CreateWarehouseDocument(counters, xlsProducts, 307000, 307);

            Assert.AreEqual(countersInput, counters.Inputs);
            Assert.AreEqual(countersOuptus, counters.Outputs);
            Assert.IsTrue(warehouseDocumentId > 0);
        }
    }
}