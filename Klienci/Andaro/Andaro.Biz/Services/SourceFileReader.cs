﻿using Andaro.Biz.Model;
using Microsoft.Office.Interop.Excel;
using NLog;
using System;
using System.Collections.Generic;
using System.Data.OleDb;

namespace Andaro.Biz.Services
{
    public class SourceFileReader
    {
       
        public static List<XlsProduct> ReadWarehouseFileFromXls(string filePath)
        {
            var products = new List<XlsProduct>();
            var lineCounter = 1;
            try
            {
                var constr = "Provider=Microsoft.ACE.OLEDB.12.0;Data Source="
                             + filePath + ";Extended Properties=\"Excel 12.0 Xml;HDR=YES;\"";
                using (var conn = new OleDbConnection(constr))
                {
                    var excel = new Application();
                    Worksheet workSheet = excel.Workbooks.Open(filePath).Worksheets[1];
                    var workSheetName = workSheet.Name;
                    conn.Open();
                    var command = new OleDbCommand($"Select * from [{workSheetName}$]", conn);
                    var reader = command.ExecuteReader();
                    if (reader != null && reader.HasRows)
                    {
                        while (reader.Read())
                        {
                            lineCounter++;
                            decimal.TryParse(reader[2].ToString(), out var decimalValue);
                            var product = new XlsProduct
                            {
                                Code = reader[0].ToString(),
                                Amount = decimalValue,
                            };
                            if (!string.IsNullOrWhiteSpace(product.Code))
                                products.Add(product);
                        }
                        LogManager.GetCurrentClassLogger().Info($"Odczyt pliku zakończony. Odczytano {lineCounter} linii.");
                        
                    }
                    if (reader != null && !reader.HasRows)
                    {
                        LogManager.GetCurrentClassLogger().Info($"Plik nie posiada zawartości.");
                    }
                    excel.Workbooks.Close();
                }
            }
            catch (Exception ex)
            {
                LogManager.GetCurrentClassLogger().Error($"Błąd odczytu pliku {ex.Message}.");
            }
            return products;
        }
    }
}
