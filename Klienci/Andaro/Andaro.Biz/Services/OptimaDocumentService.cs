﻿using Andaro.Biz.Model;
using CDNBase;
using CDNHlmn;
using NLog;
using System;
using System.Collections.Generic;
using OptimaImporter.Common;

namespace Andaro.Biz.Services
{
    public class OptimaDocumentService
    {
        public static int CreateWarehouseDocument(Counters counters, IEnumerable<XlsProduct> products, int rodzaj, int typ)
        {
            //Todo: MagId zamienić na pobierane z konfiguracji
            //var warehouse = (IMagazyn)session.CreateObject("CDN.Magazyny").Item("Mag_Symbol = 'WIR1'");

            try
            {
                var session = new AdoSession();
                var document = (IDokumentHaMag) session.CreateObject("CDN.DokumentyHaMag").AddNew(null);

                var contractor = (CDNHeal.IKontrahent) session.CreateObject("CDN.Kontrahenci")
                    .Item("Knt_KntId = 1");

                document.Rodzaj = rodzaj;
                document.TypDokumentu = typ;
                //document.MagazynZrodlowyID = warehouseId;
                document.Bufor = 1;
                document.Podmiot = contractor;

                var elements = document.Elementy;
                foreach (var product in products)
                {
                    counters.Inputs++;
                    if (!OptimaProductService.IsProductInOptima(product.Code)) continue;
                    var pozycjaDok = (IElementHaMag) elements.AddNew();
                    pozycjaDok.TowarKod = product.Code;
                    pozycjaDok.Ilosc = Convert.ToDouble(product.Amount);
                    counters.Outputs++;
                }

                session.Save();

                return document.ID;
            }
            catch (Exception ex)
            {
                LogManager.GetCurrentClassLogger()
                    .Error($"Wystąpił błąd podczas tworzenia dokumentu na towar. Błąd {ex}");
                return 0;
            }
        }
    }
}
