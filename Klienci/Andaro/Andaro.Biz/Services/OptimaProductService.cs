﻿using Andaro.Biz.Model;
using CDNBase;
using NLog;
using System;

namespace Andaro.Biz.Services
{
    public class OptimaProductService
    {
        public static string CreateProduct(XlsProduct product)
        {
           
            try
            {
                var session = new AdoSession();
                var towar = (CDNTwrb1.ITowar)session.CreateObject("CDN.Towary").AddNew();
                towar.Kod = product.Code;
                towar.Stawka = 23m;
                towar.StawkaZak = 23m;
                towar.StawkaVatZak = "23";
                towar.StawkaVat = "23";
                towar.Flaga = 2;
                towar.FlagaZak = 2;
                towar.JM = "SZT";

                CDNTwrb1.ICena cena1 = towar.Ceny[1];
                //cena1.Wartosc = Convert.ToDecimal(product.NetValue);

                var grupa = (CDNTwrb1.TwrGrupa)session.CreateObject("CDN.TwrGrupy")["twg_kod = 'DRIM'"];
                towar.TwGGIDNumer = grupa.GIDNumer;

                session.Save();
                LogManager.GetCurrentClassLogger().Info($"Utworzyłem towar {product.Code}");
                return towar.Kod;
            }
            catch (Exception ex)
            {
                LogManager.GetCurrentClassLogger().Error($"Nie udało się utworzyć towaru {product.Code}." +
                                                         $" Błąd: {ex}, {ex.InnerException}, {ex.Message}, {ex.StackTrace}");
                return string.Empty;
            }
        }

        public static bool IsProductInOptima(string productCode)
        {
            var session = new AdoSession();
            try
            {
                var towar = (CDNTwrb1.ITowar)session.CreateObject("CDN.Towary").Item($"Twr_Kod = '{productCode}'");
                LogManager.GetCurrentClassLogger().Info("Znaleziono towar" + productCode);
                return true;
            }
            catch (Exception)
            {                
                LogManager.GetCurrentClassLogger().Error($"Nie udało się znaleźć towaru w Optimie");
                return false;
            }
        }
    }
}
