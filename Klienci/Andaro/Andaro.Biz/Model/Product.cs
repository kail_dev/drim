﻿namespace Andaro.Biz.Model
{
    public class XlsProduct
    {
        public string Code { get; set; }
        public decimal Amount { get; set; }
    }
}
