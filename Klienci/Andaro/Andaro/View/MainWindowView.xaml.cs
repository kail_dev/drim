using System;
using System.ComponentModel;
using System.Diagnostics;
using System.Windows;
using System.Windows.Input;
using NLog;
using OptimaImporter.Common;
using Telerik.Windows.Controls;
using OpenFileDialog = Microsoft.Win32.OpenFileDialog;

namespace Andaro.View
{
    public partial class MainWindowView
    {
        private OpenFileDialog _openedFile;
        private readonly MainWindowViewModel _viewModel;
        private BusyView _busyView;

        public MainWindowView(MainWindowViewModel viewModel)
        {
            _viewModel = viewModel;
            DataContext = _viewModel;

            StyleManager.ApplicationTheme = new Office_BlueTheme();
            InitializeComponent();
        }

        private void LogButton_OnClick(object sender, RoutedEventArgs e)
        {
            _viewModel.OpenLogFolder();
        }

        private async void LoadFileButton_OnClick(object sender, RoutedEventArgs e)
        {
            _openedFile = new OpenFileDialog { Title = "Wybierz plik do wczytania" };

            var result = _openedFile.ShowDialog();
            if (result != null && (bool)result)
            {
                StartImport();

                await _viewModel.GenerateOptimaDocumentsAsync(_openedFile.FileName)
                        .ContinueWith(t =>
                        {
                            Dispatcher.Invoke(() => { FinishImport(t.IsCanceled ? 0 : t.Result); });
                        });
            }
        }

        private void StartImport()
        {
            LoadWarehouseStateButton.IsEnabled = false;
            _viewModel.Counters = new Counters();
            _viewModel.Stopwatch.Start();
            _viewModel.ProgressCounter = new Progress<Counters>(c =>
            {
                _viewModel.ProgressText =
                    $"Odczyt pliku �r�d�owego, logowanie do Optimy...   {_viewModel.Stopwatch.Elapsed.Hours}:{_viewModel.Stopwatch.Elapsed.Minutes}:{_viewModel.Stopwatch.Elapsed.Seconds}";
            });
            _viewModel.Timer.Start();

            _busyView = new BusyView {DataContext = _viewModel};
            _busyView.Show();
        }

        private void FinishImport(int warehouseDocumentId)
        {
            _viewModel.Timer.Stop();
            _viewModel.Stopwatch.Stop();
            _viewModel.Stopwatch.Reset();
            _viewModel.StatusBarText = "";
            LoadWarehouseStateButton.IsEnabled = true;
            if (warehouseDocumentId == 0)
            {
                _viewModel.ProgressText = "Nie uda�o si� odczyta� pliku �r�d�owego lub zalogowa� do Optimy.";
                LogManager.GetCurrentClassLogger().Info("Nie uda�o si� odczyta� pliku �r�d�owego lub zalogowa� do Optimy.");
            }
            _viewModel.ProgressText += $".\nWczytano {_viewModel.Counters.Outputs} z {_viewModel.ProgressCounterMax} rekord�w.";
            LogManager.GetCurrentClassLogger().Info($"ID utworzonego dokumentu: {warehouseDocumentId}");
        }

        private void RunConfigurationWindow()
        {
            var confWindow = new ConfigurationView { Owner = this };
            confWindow.ShowDialog();
        }

        private void ConfigurationButton_OnClick(object sender, RoutedEventArgs e)
        {
            RunConfigurationWindow();
        }

        private void AppFolderButton_OnClick(object sender, RoutedEventArgs e)
        {
            Process.Start(AppDomain.CurrentDomain.BaseDirectory);
        }

        private void SettingsFolderButton_OnClick(object sender, RoutedEventArgs e)
        {
            Process.Start(System.Windows.Forms.Application.LocalUserAppDataPath);
        }

        private void MainWindowView_OnClosing(object sender, CancelEventArgs e)
        {
            if (UserComboBox.SelectedItem != null)
            {
                ApplicationConfig.Config.OptimaUser = UserComboBox.SelectedItem.ToString();
            }
            if (DatabaseComboBox.SelectedItem != null)
            {
                ApplicationConfig.Config.OptimaDatabase = DatabaseComboBox.SelectedItem.ToString();
            }
            ApplicationConfig.Config.OptimaPassword = PasswordBox.Password;
        }
    }
}
