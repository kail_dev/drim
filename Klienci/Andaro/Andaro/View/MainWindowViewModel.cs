﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading;
using System.Threading.Tasks;
using System.Windows.Input;
using Andaro.Biz.Model;
using Andaro.Biz.Services;
using OptimaImporter.Common;

namespace Andaro.View
{
    public class MainWindowViewModel : MainWindowViewModelBase
    {
        public async Task<int> GenerateOptimaDocumentsAsync(string filePath)
        {
            var products = new List<XlsProduct>();

            var tokenSource = new CancellationTokenSource();
            var token = tokenSource.Token;

            var task = Task.Run(() =>
            {
                products = SourceFileReader.ReadWarehouseFileFromXls(filePath).Where(p => !string.IsNullOrWhiteSpace(p.Code)).ToList();
                if (products.Count == 0)
                {
                    StatusBarText = "Nie udało się odczytać danych z pliku źródłowego.";
                    tokenSource.Cancel();
                }
                ProgressCounterMax = products.Count;
            }, token)
            .ContinueWith(t1 =>
            {
                var loggedIn = OptimaLogging.LogIn();
                if (loggedIn) return true;
                StatusBarText = "Logowanie do Optimy nie powiodło się.";
                tokenSource.Cancel();
                return false;
            }, token)
            .ContinueWith(t2 =>
            {
                if (t2.IsCanceled) return;
                StatusBarText = "Zalogowałem się do Optimy";
                ProgressCounter = new Progress<Counters>(c =>
                {
                    var percent = c.Inputs / (decimal)ProgressCounterMax * 100;
                    ProgressText = $"Wczytywanie... {c.Inputs}/{ProgressCounterMax},   {percent:0} %," +
                                              $"   {Stopwatch.Elapsed.Hours}:{Stopwatch.Elapsed.Minutes}:{Stopwatch.Elapsed.Seconds}";
                });
            }, token)
            .ContinueWith(t3 =>
            {
                if (products.Count != 0)
                    return OptimaDocumentService.CreateWarehouseDocument(Counters, products, 307000, 307);
                StatusBarText = "Nie udało się utworzyć dokumentu w Optimie.";
                tokenSource.Cancel();
                return 0;
            }, token);

            return await task;
        }
    }
}
