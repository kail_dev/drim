﻿using System;

namespace Frejno.Ftp
{
    public class DokumentFtp
    {
        public DateTime DataDokumentu { get; set; }

        public string PlikXml { get; set; }

        public string FormaPlatnosci { get; set; }


        public bool CzyWOptimie { get; set; }

        public bool CzyImportowac { get; set; }

        public int TypDokumentu { get; set; }

        public string CzyKasowac { get; set; }
    }
}
