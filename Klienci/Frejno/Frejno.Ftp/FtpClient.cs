﻿using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.IO;
using System.Linq;
using System.Net;
using System.Threading.Tasks;

namespace Frejno.Ftp
{
    public class FtpClient
    {
        private string _server;
        public string Server
        {
            get => _server ?? (_server = "ftp://" + FtpSettings.Default.Server);
            set => _server = value;
        }
        private string _user;
        public string User
        {
            get => _user ?? (_user = FtpSettings.Default.User);
            set => _user = value;
        }
        private string _password;
        public string Password
        {
            get => _password ?? (_password = FtpSettings.Default.Password);
            set => _password = value;
        }

        private string _katalogProduktow;
        public string KatalogProduktow
        {
            get => _katalogProduktow ?? (_katalogProduktow = FtpSettings.Default.Produkty);
            set => _katalogProduktow = value;
        }

        private string _katalogZamowien;
        public string KatalogZamowien
        {
            get => _katalogZamowien ?? (_katalogZamowien = FtpSettings.Default.ZamowieniaZaplacone);
            set => _katalogZamowien = value;
        }

        private string _katalogFpf;
        public string KatalogFpf
        {
            get => _katalogFpf ?? (_katalogFpf = FtpSettings.Default.Proformy);
            set => _katalogFpf = value;
        }

        private FtpWebRequest _request;
        public FtpWebRequest Request
        {
            get
            {
                if (_request != null) return _request;
                _request = (FtpWebRequest)WebRequest.Create(Server + KatalogZamowien);
                _request.Method = WebRequestMethods.Ftp.ListDirectory;
                _request.Credentials = new NetworkCredential(User, Password);
                _request.KeepAlive = true;
                return _request;
            }
            set => _request = value;
        }

        private FtpWebRequest _requestDateTime;
        public FtpWebRequest RequestDateTime
        {
            get
            {
                if (_requestDateTime != null) return _requestDateTime;
                _requestDateTime = (FtpWebRequest)WebRequest.Create(Server);
                _requestDateTime.Method = WebRequestMethods.Ftp.GetDateTimestamp;
                _requestDateTime.Credentials = new NetworkCredential(User, Password);
                _requestDateTime.KeepAlive = true;
                return _requestDateTime;
            }
            set => _requestDateTime = value;
        }
        private List<string> _noweProdukty;
        public List<string> NoweProdukty => _noweProdukty ?? (_noweProdukty = new List<string>());
        private List<DokumentFtp> _zamowienia;
        public List<DokumentFtp> Zamowienia => _zamowienia ?? (_zamowienia = new List<DokumentFtp>());

        private List<DokumentFtp> _proformy;
        public List<DokumentFtp> Proformy => _proformy ?? (_proformy = new List<DokumentFtp>());

        public int SprawdzPolaczenie()
        {
            var licznik = 0;
            try
            {
                using (var response = (FtpWebResponse)Request.GetResponse())
                {
                    var stream = response.GetResponseStream();
                    if (stream != null)
                        using (var reader = new StreamReader(stream))
                        {
                            while (reader.ReadLine() != null)
                            {
                                licznik++;
                            }
                        }
                }
                _request = (FtpWebRequest)WebRequest.Create(Server + KatalogFpf);
                _request.Method = WebRequestMethods.Ftp.ListDirectory;
                _request.Credentials = new NetworkCredential(User, Password);
                _request.KeepAlive = true;

                using (var response = (FtpWebResponse)Request.GetResponse())
                {
                    var stream = response.GetResponseStream();
                    if (stream != null)
                        using (var reader = new StreamReader(stream))
                        {
                            while (reader.ReadLine() != null)
                            {
                                licznik++;
                            }
                        }
                }
                licznik -= 2;
                return licznik;
            }
            catch (Exception ex)
            {
                Debug.WriteLine(ex.ToString());
            }
            return licznik;
        }

        public int Licznik { get; set; }

        public List<string> PobierzNoweProdukty()
        {
            try
            {
                Request = (FtpWebRequest)WebRequest.Create(Server.Replace("/com", "") + KatalogProduktow);
                Request.Method = WebRequestMethods.Ftp.ListDirectoryDetails;
                Request.Credentials = new NetworkCredential(User, Password);
                Request.KeepAlive = true;
                Request.UsePassive = true;

                using (var response = (FtpWebResponse)Request.GetResponse())
                {
                    var stream = response.GetResponseStream();
                    if (stream != null)
                        using (var reader = new StreamReader(stream))
                        {
                            string line;
                            while ((line = reader.ReadLine()) != null)
                            {

                                var lineElements = line.Split(' ');
                                foreach (var item in lineElements)
                                {
                                    if (item.Contains(".xml"))
                                    {
                                        NoweProdukty.Add(item);
                                    }
                                }
                            }
                        }
                }
            }
            catch (Exception ex)
            {
                Console.WriteLine(ex.Message);
            }
            return NoweProdukty;
        }

        public List<DokumentFtp> PobierzZamowienia()
        {
            try
            {
                Request = (FtpWebRequest)WebRequest.Create(Server + KatalogZamowien);
                Request.ConnectionGroupName = "MyGroup";
                Request.Method = WebRequestMethods.Ftp.ListDirectoryDetails;
                Request.Credentials = new NetworkCredential(User, Password);
                Request.KeepAlive = true;
                Request.UsePassive = true;

                using (var response = (FtpWebResponse)Request.GetResponse())
                {
                    var stream = response.GetResponseStream();
                    if (stream != null)
                        using (var reader = new StreamReader(stream))
                        {
                            var listaPlikow = new List<string>();
                            string linia;
                            while ((linia = reader.ReadLine()) != null)
                            {
                                listaPlikow.Add(linia);
                            }
                            Parallel.ForEach(listaPlikow, new ParallelOptions { MaxDegreeOfParallelism = 4 }, liniaZListyPlikow =>
                            {
                                var liniaElementy = liniaZListyPlikow.Split(' ');
                                var nazwaPliku = liniaElementy.Where(x => x.Contains(".xml")).Select(x => x).FirstOrDefault();
                                if (nazwaPliku != null)
                                {
                                    var dokumentFtp = new DokumentFtp { PlikXml = nazwaPliku };
                                    var requestDateTime = (FtpWebRequest)WebRequest.Create(Server + KatalogZamowien + "/" + nazwaPliku);
                                    requestDateTime.ConnectionGroupName = "MyGroup";
                                    requestDateTime.Method = WebRequestMethods.Ftp.GetDateTimestamp;
                                    requestDateTime.Credentials = new NetworkCredential(User, Password);
                                    requestDateTime.KeepAlive = true;
                                    requestDateTime.UsePassive = true;
                                    var dateResponse = (FtpWebResponse)requestDateTime.GetResponse();
                                    dokumentFtp.DataDokumentu = Convert.ToDateTime(dateResponse.LastModified);
                                    dokumentFtp.TypDokumentu = 302;
                                    //int drugiMiesiacWstecz = DateTime.Now.Month - 2;
                                    //if (drugiMiesiacWstecz >= dokumentFTP.DataDokumentu.Month)
                                    //{
                                    //    //todo: kasowanie
                                    //    //kasowanie zamówień starszych niż 2mce
                                    //    try
                                    //    {
                                    //        FtpWebRequest DeleteRequest = (FtpWebRequest)WebRequest.Create(Server + KatalogZamowien + "/" + nazwaPliku);
                                    //        DeleteRequest.ConnectionGroupName = "MyGroup";
                                    //        DeleteRequest.Method = WebRequestMethods.Ftp.DeleteFile;
                                    //        DeleteRequest.Credentials = new NetworkCredential(this.User, this.Password);
                                    //        DeleteRequest.KeepAlive = true;
                                    //        DeleteRequest.UsePassive = true;
                                    //        FtpWebResponse deleteResponse = (FtpWebResponse)DeleteRequest.GetResponse();
                                    //        Debug.WriteLine("Delete status: {0}", deleteResponse.StatusDescription);
                                    //        deleteResponse.Close();
                                    //    }
                                    //    catch (Exception ex)
                                    //    {
                                    //        Debug.WriteLine("Błąd kasowania zamówień: " + ex.ToString());
                                    //    }
                                    //}
                                    if (!File.Exists(@"PlikiXML\Zamowienia\" + nazwaPliku))
                                    {
                                        var request2 = (FtpWebRequest)WebRequest.Create(Server + KatalogZamowien + "/" + nazwaPliku);
                                        request2.Method = WebRequestMethods.Ftp.DownloadFile;
                                        request2.Credentials = new NetworkCredential(User, Password);
                                        request2.KeepAlive = true;
                                        request2.UsePassive = true;

                                        using (var response2 = (FtpWebResponse)request2.GetResponse())
                                        {
                                            var stream2 = response2.GetResponseStream();
                                            if (stream2 != null)
                                                using (var reader2 = new StreamReader(stream2))
                                                {
                                                    using (var destination = new StreamWriter(@"PlikiXML\Zamowienia\" + nazwaPliku))
                                                    {
                                                        destination.Write(reader2.ReadToEnd());
                                                    }
                                                }
                                        }
                                    }
                                    if (dokumentFtp.PlikXml != null && dokumentFtp.DataDokumentu != null)
                                    {
                                        Zamowienia.Add(dokumentFtp);
                                        Licznik++;
                                        Debug.WriteLine(Licznik);
                                        Console.WriteLine(Licznik);
                                    }
                                }
                            });
                        }
                }
            }
            catch (Exception ex)
            {
                Debug.WriteLine(ex.ToString());
            }
            return Zamowienia;
        }

        public List<DokumentFtp> PobierzProformy()
        {
            try
            {
                Request = (FtpWebRequest)WebRequest.Create(Server + KatalogFpf);
                Request.ConnectionGroupName = "MyGroup";
                Request.Method = WebRequestMethods.Ftp.ListDirectoryDetails;
                Request.Credentials = new NetworkCredential(User, Password);
                Request.KeepAlive = true;
                Request.UsePassive = true;

                using (var response = (FtpWebResponse)Request.GetResponse())
                {
                    var stream = response.GetResponseStream();
                    if (stream != null)
                        using (var reader = new StreamReader(stream))
                        {
                            string line;
                            while ((line = reader.ReadLine()) != null)
                            {
                                Debug.WriteLine(line);
                                var lineElements = line.Split(' ');
                                foreach (var item in lineElements)
                                {
                                    var dokumentFtp = new DokumentFtp();
                                    if (item.Contains(".xml"))
                                    {
                                        dokumentFtp.PlikXml = item;
                                        RequestDateTime = (FtpWebRequest)WebRequest.Create(Server + KatalogFpf + "/" + item);
                                        RequestDateTime.ConnectionGroupName = "MyGroup";
                                        RequestDateTime.Method = WebRequestMethods.Ftp.GetDateTimestamp;
                                        RequestDateTime.Credentials = new NetworkCredential(User, Password);
                                        RequestDateTime.KeepAlive = true;
                                        RequestDateTime.UsePassive = true;
                                        var dateResponse = (FtpWebResponse)RequestDateTime.GetResponse();
                                        dokumentFtp.DataDokumentu = Convert.ToDateTime(dateResponse.LastModified);
                                        dokumentFtp.TypDokumentu = 320;
                                        if (!File.Exists(@"PlikiXML\Proformy\" + item))
                                        {
                                            var request2 = (FtpWebRequest)WebRequest.Create(Server + KatalogFpf + "/" + item);
                                            request2.Method = WebRequestMethods.Ftp.DownloadFile;
                                            request2.Credentials = new NetworkCredential(User, Password);
                                            request2.KeepAlive = true;
                                            request2.UsePassive = true;

                                            using (var response2 = (FtpWebResponse)request2.GetResponse())
                                            {
                                                var stream2 = response2.GetResponseStream();
                                                if (stream2 != null)
                                                    using (var reader2 = new StreamReader(stream2))
                                                    {
                                                        using (var destination = new StreamWriter(@"PlikiXML\Proformy\" + item))
                                                        {
                                                            destination.Write(reader2.ReadToEnd());
                                                        }
                                                    }
                                            }
                                        }
                                    }
                                    if (dokumentFtp.PlikXml != null && dokumentFtp.DataDokumentu != null)
                                    {
                                        Proformy.Add(dokumentFtp);
                                        Licznik++;
                                        Debug.WriteLine(Licznik);
                                        Console.WriteLine(Licznik);
                                    }
                                }
                            }
                        }
                }
            }
            catch (Exception ex)
            {
                Debug.WriteLine(ex.ToString());
            }
            return Proformy;
        }

        public void UtworzFolderyDlaSciagnychPlikow()
        {
            if (!Directory.Exists(AppDomain.CurrentDomain.BaseDirectory + "\\PlikiXML"))
            {
                Directory.CreateDirectory(AppDomain.CurrentDomain.BaseDirectory + "\\PlikiXML");
            }
            if (!Directory.Exists(AppDomain.CurrentDomain.BaseDirectory + "\\PlikiXML\\Proformy"))
            {
                Directory.CreateDirectory(AppDomain.CurrentDomain.BaseDirectory + "\\PlikiXML\\Proformy");
            }
            if (!Directory.Exists(AppDomain.CurrentDomain.BaseDirectory + "\\PlikiXML\\Zamowienia"))
            {
                Directory.CreateDirectory(AppDomain.CurrentDomain.BaseDirectory + "\\PlikiXML\\Zamowienia");
            }
        }
    }
}
