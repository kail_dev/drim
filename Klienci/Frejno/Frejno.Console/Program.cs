﻿using CDNBase;
using CDNHlmn;
using Frejno.Ftp;
using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.IO;
using System.Linq;
using System.Net;
using System.Runtime.InteropServices;
using System.Xml.Linq;

namespace Frejno.Console
{
    class Program
    {
        private static string machineName = "SZERYFWIN10";

        private static FtpClient _client = new FtpClient
        {
            Server = "ftp://54.36.165.50",
            User = "ultras1312.com.pl",
            Password = @"27wrzesien@",
            KatalogProduktow = "/optima/produkty",
            KatalogZamowien = "/optima/zamowienia/zaplacone",
            KatalogFpf = "/optima/zamowienia/pro_forma"
        };

        private static readonly List<string> _dokumentyZBazy = new List<string>();
        private static int _licznikZnalezionychProduktow; //ilosc plikow .xml dla towarów
        private static int _licznikDodanychProduktow; //licznik nonododanych produktów do Optimy

        static void Main(string[] args)
        {
            AppDomain.CurrentDomain.UnhandledException += (sender, eventArgs) =>
            {
                System.Console.WriteLine($"Złapano wyjątek: {(eventArgs.ExceptionObject as Exception).InnerException}");
            };
            _client.UtworzFolderyDlaSciagnychPlikow();
            var noweProdukty = _client.PobierzNoweProdukty();
            var noweDokumenty = _client.PobierzZamowienia();
            var licznik = 0;

            var optimaApp = new Application();
            if (Environment.MachineName == machineName)
            {
                optimaApp.Login("ADMIN", "", "FREJNO_S_C__BR");
            }
            else
            {
                optimaApp.Login("ADMIN", "", "FREJNO S.C. BR");
            }
            OdczytajIstniejace();
            DodajProdukty(noweProdukty, _client, out licznik);
            licznik = 0;
            DodajDokumentyFtp(noweDokumenty, _client, out licznik);
        }

        private static void DodajProdukty(IEnumerable<string> noweProdukty, FtpClient ftp, out int licznik)
        {
            licznik = 0;
            foreach (var produkt in noweProdukty)
            {
                licznik++;
                var sesja = new AdoSession();
                var towary = (CDNTwrb1.Towary)sesja.CreateObject("CDN.Towary");
                CDNTwrb1.Towar towar;

                try
                {
                    // ReSharper disable once RedundantAssignment
                    towar = towary["Twr_Kod='" + produkt.Replace("prod_", "").Replace(".xml", "") + "'"];
                }
                catch (Exception)
                {
                    var requestDownload =
                        (FtpWebRequest)WebRequest.Create(ftp.Server + ftp.KatalogProduktow + "//" + produkt);
                    requestDownload.Method = WebRequestMethods.Ftp.DownloadFile;
                    requestDownload.Credentials = new NetworkCredential(ftp.User, ftp.Password);
                    requestDownload.KeepAlive = true;
                    requestDownload.UsePassive = true;

                    try
                    {
                        using (var response = (FtpWebResponse)requestDownload.GetResponse())
                        {
                            using (var responseStream = response.GetResponseStream())
                            {
                                using (var reader = new StreamReader(responseStream))
                                {
                                    using (var destination = new StreamWriter(produkt))
                                    {
                                        destination.Write(reader.ReadToEnd());
                                        destination.Dispose();

                                        var doc = XDocument.Load(produkt);
                                        var elementyProduktu =
                                            (from e in doc.Descendants("produkt") select e).ToList();
                                        var kodTowaru = elementyProduktu.Select(x => x.Element("kod")?.Value).FirstOrDefault() ??
                                                           elementyProduktu.Select(x => x.Element("nazwa_optima")?.Value).FirstOrDefault();
                                        try
                                        {
                                            // ReSharper disable once RedundantAssignment
                                            towar = towary["Twr_Kod='" + kodTowaru + "'"];
                                            _licznikZnalezionychProduktow++;
                                        }
                                        catch
                                        {
                                            towar = towary.AddNew();
                                            towar.Kod = kodTowaru;
                                            var nazwa = "";
                                            var nazwaXml1 = elementyProduktu.Select(x => x.Element("nazwa"))
                                                    .FirstOrDefault();
                                            //var nazwaXml2 = elementyProduktu.Select(x => x.Element("nazwa_optima"))
                                            //        .FirstOrDefault();
                                            //if (nazwaXml2 != null)
                                            //{
                                            //    nazwa = nazwaXml2.Value;
                                            //}
                                            if (nazwaXml1 != null)
                                            {
                                                nazwa = nazwaXml1.Value;
                                            }
                                            if (nazwa == "")
                                            {
                                                nazwa = kodTowaru;
                                            }
                                            towar.Nazwa = nazwa;
                                            towar.Stawka = 23m;
                                            towar.StawkaZak = 23m;
                                            towar.Flaga = 2;
                                            towar.FlagaZak = 2;
                                            towar.JM =
                                                elementyProduktu.Select(x => x.Element("jednostka_miary")?.Value)
                                                    .FirstOrDefault() ?? "szt";
                                            towar.EdycjaNazwy = 1;
                                            try
                                            {
                                                sesja.Save();
                                                _licznikDodanychProduktow++;
                                            }
                                            catch (Exception)
                                            {
                                                System.Console.WriteLine("Blad dodawania towaru o kodzie: " + kodTowaru);
                                            }
                                        }
                                        File.Delete(produkt);
                                    }
                                }
                            }
                        }
                    }
                    catch (WebException)
                    {
                        System.Console.WriteLine($"Nie udalo sie pobrac pliku: {produkt}");
                    }
                }

                System.Console.WriteLine(licznik);
            }
        }

        private static void DodajDokumentyFtp(List<DokumentFtp> dokumentyFtp, FtpClient ftp, out int licznik)
        {
            licznik = 0;
            var licznikZaznaczonych = 0;
            dokumentyFtp = dokumentyFtp.OrderBy(x => x.PlikXml).ToList();
            //dokumentyFtp = dokumentyFtp.OrderByDescending(x => x.PlikXml).Take(10).ToList();
            var iloscDokumentow = dokumentyFtp.Count;
            foreach (var dokument in dokumentyFtp)
            {
                var nrZamowienia = dokument.PlikXml.IndexOf("_", StringComparison.Ordinal);
                var pelnyNumerZamowienia =
                    dokument.PlikXml.Substring(nrZamowienia, dokument.PlikXml.Length - nrZamowienia);
                if (!_dokumentyZBazy.Contains(pelnyNumerZamowienia))
                {
                    licznikZaznaczonych++;
                    System.Console.WriteLine(licznikZaznaczonych + " z " + iloscDokumentow + ": " + dokument.PlikXml);
                    var sesja = new AdoSession();

                    FtpWebRequest requestDownload;
                    if (dokument.TypDokumentu == (int)TypDokumentu.Proforma)
                    {
                        requestDownload = (FtpWebRequest)WebRequest.Create(ftp.Server + ftp.KatalogFpf + "//" + dokument.PlikXml);
                    }
                    else
                    {
                        requestDownload = (FtpWebRequest)WebRequest.Create(ftp.Server + ftp.KatalogZamowien + "//" + dokument.PlikXml);
                    }
                    requestDownload.Method = WebRequestMethods.Ftp.DownloadFile;
                    requestDownload.Credentials = new NetworkCredential(ftp.User, ftp.Password);
                    requestDownload.KeepAlive = true;
                    requestDownload.UsePassive = true;

                    if (!File.Exists(@"PlikiXML\Proformy\" + dokument.PlikXml)
                            && !File.Exists(@"PlikiXML\Zamowienia\" + dokument.PlikXml))
                    {
                        using (var response = (FtpWebResponse)requestDownload.GetResponse())
                        {
                            using (var responseStream = response.GetResponseStream())
                            {
                                using (var reader = new StreamReader(responseStream))
                                {
                                    if (dokument.TypDokumentu == (int)TypDokumentu.Proforma)
                                    {
                                        using (var destination = new StreamWriter(@"PlikiXML\Proformy\" + dokument.PlikXml))
                                        {
                                            destination.Write(reader.ReadToEnd());
                                        }
                                    }
                                    else
                                    {
                                        using (var destination = new StreamWriter(@"PlikiXML\Zamowienia\" + dokument.PlikXml))
                                        {
                                            destination.Write(reader.ReadToEnd());
                                        }
                                    }
                                }
                            }
                        }
                    }
                    try
                    {
                        var dokumentPoprawny = 1;
                        var doc = dokument.TypDokumentu == (int)TypDokumentu.Proforma ?
                            XDocument.Load(@"PlikiXML\Proformy\" + dokument.PlikXml) :
                            XDocument.Load(@"PlikiXML\Zamowienia\" + dokument.PlikXml);
                        var elementyZamowienia = (from e in doc.Descendants("zamowienie") select e).ToList();
                        decimal stawkaVat = 1;
                        stawkaVat = Convert.ToDecimal(elementyZamowienia.Descendants("vat").FirstOrDefault().Value);

                        decimal procentDoplaty = 0;
                        var doplata = elementyZamowienia.Descendants("doplata_procent").FirstOrDefault();
                        if (doplata != null)
                        {
                            System.Console.WriteLine("Bedzie doplata {0} procent.", doplata.Value.Replace(".", ","));
                            procentDoplaty = Convert.ToDecimal(doplata.Value.Replace(".", ","));
                        }
                        decimal procentRabatu = 0;
                        var rabat = elementyZamowienia.Descendants("rabat").FirstOrDefault();
                        if (rabat != null)
                        {
                            var kwotaRazem = Convert.ToDecimal(elementyZamowienia.Descendants("kwota_razem").FirstOrDefault().Value.Replace(".", ","));
                            procentRabatu = (Convert.ToDecimal(rabat.Value.Replace(".", ",")) / kwotaRazem) * 100;
                            System.Console.WriteLine("Bedzie rabat w kwocie {0}.", rabat.Value.Replace(".", ","));
                        }
                        decimal kwotaVouchera = 0;
                        var voucher = elementyZamowienia.Descendants("voucher_kwota").FirstOrDefault();
                        if (voucher != null)
                        {
                            System.Console.WriteLine("Bedzie voucher na kwotę {0}.", voucher.Value);
                            kwotaVouchera = Convert.ToDecimal(voucher.Value.Replace(".", ","));
                        }
                        var dokumentyHaMag = (DokumentyHaMag)sesja.CreateObject("CDN.DokumentyHaMag");
                        DokumentHaMag dokumentHaMag = dokumentyHaMag.AddNew();
                        var formyPlatnosci = (ICollection)sesja.CreateObject("CDN.FormyPlatnosci");
                        OP_KASBOLib.FormaPlatnosci forma = formyPlatnosci["Fpl_FplID = 3"];

                        if (elementyZamowienia.Descendants("kiedy_zaplacone") != null)
                        {
                            if (elementyZamowienia.Descendants("kiedy_zaplacone").FirstOrDefault() != null)
                            {
                                if (!string.IsNullOrEmpty(elementyZamowienia.Descendants("kiedy_zaplacone")
                                    .FirstOrDefault().Value))
                                {
                                    dokumentHaMag.DataDok = Convert.ToDateTime(elementyZamowienia.Descendants("kiedy_zaplacone").FirstOrDefault().Value);
                                    Debug.WriteLine("dokumentHaMag.DataDok = " + dokumentHaMag.DataDok);
                                    Debug.WriteLine("dokument.DataDokumentu = " + dokument.DataDokumentu);
                                }
                                else
                                {
                                    dokumentHaMag.DataDok = dokument.DataDokumentu;
                                }
                            }
                        }
                        else
                        {
                            dokumentHaMag.DataDok = dokument.DataDokumentu;
                        }
                        //dokumentHaMag.DataDok = dokument.DataDokumentu; 
                        dokumentHaMag.DataWys = dokumentHaMag.DataDok;
                        dokumentHaMag.DataSprzedazy = dokumentHaMag.DataDok;
                        dokumentHaMag.Podmiot = (CDNHeal.IPodmiot)sesja.CreateObject("CDN.Kontrahenci")["Knt_Kod = '!NIEOKREŚLONY!'"];
                        var odbiorca = elementyZamowienia.Descendants("odbiorca").ToList();
                        dokumentHaMag.PodNazwa1 = odbiorca.Descendants("nazwa").FirstOrDefault().Value;
                        var nip_eu = odbiorca.Descendants("vat_eu").FirstOrDefault()?.Value;
                        if (!string.IsNullOrWhiteSpace(nip_eu))
                        {
                            dokumentHaMag.PodNumerNIP.NIPKraj = nip_eu.Substring(0, 2);
                            dokumentHaMag.PodNumerNIP.Nip = nip_eu.Substring(2, nip_eu.Length - 2);
                            dokumentHaMag.Export = 3;
                            try
                            {
                                dokumentHaMag.ZmienionoExport();
                            }
                            catch (Exception)
                            {
                                System.Console.WriteLine($"Nie udało się ustawić rodzaju transakcji dla dokumentu: {dokument.DataDokumentu}");
                            }
                        }
                        dokumentHaMag.PodAdres.Miasto = odbiorca.Descendants("miasto").FirstOrDefault()?.Value;
                        dokumentHaMag.PodAdres.Ulica = odbiorca.Descendants("ulica").FirstOrDefault()?.Value;
                        if (odbiorca.Descendants("adres").FirstOrDefault() != null)
                        {
                            dokumentHaMag.PodAdres.Ulica = odbiorca.Descendants("adres").FirstOrDefault().Value;
                        }
                        dokumentHaMag.PodAdres.Kraj = odbiorca.Descendants("kraj").FirstOrDefault().Value;
                        dokumentHaMag.OdbNazwa1 = dokumentHaMag.PodNazwa1;
                        dokumentHaMag.OdbAdres.Miasto = dokumentHaMag.PodAdres.Miasto;
                        dokumentHaMag.OdbAdres.Ulica = dokumentHaMag.PodAdres.Ulica;
                        dokumentHaMag.OdbAdres.Kraj = dokumentHaMag.PodAdres.Kraj;

                        TypDokumentu typDokumentu = (TypDokumentu)dokument.TypDokumentu;
                        if (typDokumentu == TypDokumentu.Faktura)
                        {
                            dokumentHaMag.Rodzaj = 302000;
                            dokumentHaMag.TypDokumentu = 302;
                            dokumentHaMag.FormaPlatnosci = forma;
                        }
                        else if (typDokumentu == TypDokumentu.Proforma)
                        {
                            dokumentHaMag.Rodzaj = 320000;
                            dokumentHaMag.TypDokumentu = 320;
                        }
                        dokumentHaMag.Bufor = 1;
                        dokumentHaMag.TypNB = 2;

                        if (Environment.MachineName != machineName)
                        {
                            var definicjeDokumentow = (CDNHeal.DefinicjeDokumentow)sesja.CreateObject("CDN.DefinicjeDokumentow");
                            var definicjaDokumentu = (CDNHeal.DefinicjaDokumentu)definicjeDokumentow["Ddf_Symbol=\'FAI\'"];
                            SetProperty(dokumentHaMag.Numerator, "DefinicjaDokumentu", definicjaDokumentu);
                        }

                        if (elementyZamowienia.Descendants("uwagi").FirstOrDefault() != null)
                        {
                            dokumentHaMag.Uwagi = elementyZamowienia.Descendants("uwagi").FirstOrDefault().Value;
                        }

                        var walutaZPliku = elementyZamowienia.Descendants("kod_waluty").FirstOrDefault();
                        var kodWaluty = "";
                        if (walutaZPliku != null)
                        {
                            kodWaluty = walutaZPliku.Value;
                        }
                        if (kodWaluty != "")
                        {
                            try
                            {
                                var waluty = (CDNHeal.Waluty)(sesja.CreateObject("CDN.Waluty"));
                                CDNHeal.Waluta waluta = waluty["WNa_Symbol='" + kodWaluty + "'"];
                                dokumentHaMag.Waluta = waluta;
                            }
                            catch (Exception)
                            {
                                System.Console.WriteLine("Nie znalazłem waluty o kodzie: {0}", kodWaluty);
                            }
                        }
                        dokumentHaMag.Termin = dokumentHaMag.DataDok;
                        var atrybut = sesja.CreateObject("CDN.DefAtrybuty").Item("dea_Kod = 'NRZAMOWIENIA'");
                        var atrybutDok = dokumentHaMag.Atrybuty.AddNew();
                        atrybutDok.DefAtrybut = atrybut;
                        atrybutDok.Wartosc = pelnyNumerZamowienia;//elementyZamowienia.Descendants("nr_zamowienia").FirstOrDefault().Value;
                        if (elementyZamowienia.Descendants("uwagi").FirstOrDefault() != null)
                        {
                            dokumentHaMag.Opis.Insert(0, elementyZamowienia.Descendants("uwagi").FirstOrDefault().Value);
                        }
                        var pozycjeZamowienia =
                            elementyZamowienia.Descendants("pozycja").ToList();
                        var pozycje = dokumentHaMag.Elementy;
                        foreach (var pozycjaZamowienia in pozycjeZamowienia)
                        {
                            var pozycjaIstnieje = 0;
                            var kodTowaru = pozycjaZamowienia.Element("kod").Value;
                            if (kodTowaru != "Doplata")
                            {
                                try
                                {
                                    try
                                    {
                                        //AdoSession sesja2 = new AdoSession();
                                        var towary = (CDNTwrb1.Towary)sesja.CreateObject("CDN.Towary");
                                        // ReSharper disable once UnusedVariable
                                        CDNTwrb1.Towar towar = towary["Twr_Kod='" + kodTowaru + "'"];
                                        //towar.NumerCeny = ZwrocNrCeny(kodWaluty);
                                        //sesja2.Save();
                                        pozycjaIstnieje = 1;
                                    }
                                    catch (COMException)
                                    {
                                        System.Console.WriteLine("W systemie nie ma karty towarowej o kodzie: {0}", kodTowaru);
                                        dokumentPoprawny = 0;
                                    }
                                    if (pozycjaIstnieje == 1)
                                    {
                                        var pozycja = (IElementHaMag)pozycje.AddNew();
                                        pozycja.TowarKod = kodTowaru;
                                        try
                                        {
                                            pozycja.UstawNazweTowaru(pozycjaZamowienia.Element("opis").Value);

                                        }
                                        catch (Exception)
                                        {
                                            System.Console.WriteLine($"Zmiana nazwy towaru: {kodTowaru} nie powiodła się.");
                                        }
                                        pozycja.Flaga = 2;
                                        pozycja.Stawka = stawkaVat;
                                        pozycja.NumerCeny = ZwrocNrCeny(kodWaluty);
                                        try
                                        {
                                            pozycja.Ilosc = Convert.ToDouble(pozycjaZamowienia.Element("ilosc").Value.Replace(".", ","));
                                        }
                                        catch (Exception)
                                        {
                                            System.Console.WriteLine("Zły format ilości dla towaru: {0}", kodTowaru);
                                            dokumentPoprawny = 0;
                                        }
                                        try
                                        {
                                            pozycja.CenaWWD = Convert.ToDecimal(pozycjaZamowienia.Element("cena_j").Value.Replace(".", ","))
                                                                * (1 + procentDoplaty / 100) * (1 - procentRabatu / 100);
                                            pozycja.WartoscBruttoWal = Convert.ToDecimal(pozycjaZamowienia.Element("cena_r").Value.Replace(".", ","))
                                                                * (1 + procentDoplaty / 100) * (1 - procentRabatu / 100);
                                        }
                                        catch (Exception)
                                        {
                                            System.Console.WriteLine("Zły format ceny dla towaru: {0}", kodTowaru);
                                            dokumentPoprawny = 0;
                                        }
                                        //pozycja.TowarOpis = pozycjaZamowienia.Element("opis").Value;
                                        if (pozycjaZamowienia.Element("jednostka_miary") != null)
                                        {
                                            pozycja.JM = pozycjaZamowienia.Element("jednostka_miary").Value;
                                        }
                                        if (pozycjaZamowienia.Element("vat") != null)
                                        {
                                            pozycja.Stawka =
                                                Convert.ToDecimal(pozycjaZamowienia.Element("vat").Value);
                                        }
                                    }
                                }
                                catch (Exception ex)
                                {
                                    System.Console.WriteLine(@"Błąd ustawiania parametrow pozycji: {0} - {1}", kodTowaru, ex);
                                }
                            }
                        }
                        var wysylka = elementyZamowienia.Descendants("wysylka").FirstOrDefault();
                        var kwotaWysylki = 0.0m;
                        ElementHaMag pozycjaWysylka = null;
                        if (wysylka != null && wysylka.HasElements && !wysylka.Element("nazwa").Value.StartsWith("Free Delivery"))
                        {
                            kwotaWysylki = Convert.ToDecimal(wysylka.Element("kwota").Value.Replace(".", ","));
                            pozycjaWysylka = (ElementHaMag)pozycje.AddNew();
                            //var towarKod = wysylka.Element("nazwa").Value.Replace("Courier", "DOSTAWA");
                            pozycjaWysylka.TowarKod = "DOSTAWA";
                            pozycjaWysylka.Flaga = 2;
                            pozycjaWysylka.Stawka = stawkaVat;
                            pozycjaWysylka.Ilosc = 1;
                            pozycjaWysylka.CenaWWD = kwotaWysylki
                                                        * (1 + procentDoplaty / 100) * (1 - procentRabatu / 100)
                                                        + procentDoplaty;
                        }
                        var kosztyPlatnosci = elementyZamowienia.Descendants("koszty_platnosci").FirstOrDefault();
                        if (kosztyPlatnosci != null)
                        {
                            pozycjaWysylka = (ElementHaMag)pozycje.AddNew();
                            //var towarKod = wysylka.Element("nazwa").Value.Replace("Courier", "DOSTAWA");
                            pozycjaWysylka.TowarKod = "PC";
                            pozycjaWysylka.Flaga = 2;
                            pozycjaWysylka.Stawka = stawkaVat;
                            pozycjaWysylka.Ilosc = 1;
                            pozycjaWysylka.CenaWWD = (Convert.ToDecimal(kosztyPlatnosci.Value.Replace(".", ","))
                                                      * (1 + procentDoplaty / 100) * (1 - procentRabatu / 100))
                                                     + procentDoplaty;
                        }
                        if (dokumentPoprawny == 1)
                        {
                            var kwotaBruttoDokumentu = pozycje.Cast<IElementHaMag>().Sum(pozycja => pozycja.WartoscBruttoWal);
                            decimal kwotaDoZaplaty = 0;
                            var kwotaDoZaplatyZxml = elementyZamowienia.Descendants("do_zaplaty").FirstOrDefault();
                            if (kwotaDoZaplatyZxml != null)
                            {
                                kwotaDoZaplaty = Convert.ToDecimal(kwotaDoZaplatyZxml.Value.Replace(".", ","));
                            }
                            if (Math.Abs(kwotaBruttoDokumentu - kwotaDoZaplaty) < 3 && kwotaDoZaplaty != 0)
                            {
                                if (kwotaBruttoDokumentu > kwotaDoZaplaty)
                                {
                                    if (pozycjaWysylka != null)
                                    {
                                        pozycjaWysylka.CenaWWD = pozycjaWysylka.CenaWWD - kwotaBruttoDokumentu + kwotaDoZaplaty;
                                    }
                                    else
                                    {
                                        var pozycja = (IElementHaMag)pozycje[pozycje.Count];
                                        pozycja.WartoscBruttoWal = pozycja.WartoscBruttoWal - kwotaBruttoDokumentu + kwotaDoZaplaty;
                                        pozycja.CenaWWD = pozycja.WartoscBruttoWal / (decimal)pozycja.Ilosc;
                                    }
                                }
                                else if (kwotaBruttoDokumentu < kwotaDoZaplaty)
                                {
                                    if (pozycjaWysylka != null)
                                    {
                                        pozycjaWysylka.CenaWWD = pozycjaWysylka.CenaWWD + kwotaBruttoDokumentu - kwotaDoZaplaty;
                                    }
                                    else
                                    {
                                        var pozycja = (IElementHaMag)pozycje[pozycje.Count];
                                        pozycja.WartoscBruttoWal = pozycja.WartoscBruttoWal + kwotaBruttoDokumentu - kwotaDoZaplaty;
                                        pozycja.CenaWWD = pozycja.WartoscBruttoWal / (decimal)pozycja.Ilosc;
                                    }
                                }
                                if (pozycjaWysylka != null)
                                {
                                    pozycjaWysylka.CenaWWD -= kwotaVouchera;
                                    if (pozycjaWysylka.CenaWWD <= 0)
                                    {
                                        dokumentHaMag.Uwagi.Insert(0, "DO SPRAWDZENIA!!!");
                                    }
                                }
                                sesja.Save();
                                dokument.CzyWOptimie = true;
                            }
                            else if (Math.Abs(kwotaBruttoDokumentu - kwotaDoZaplaty) >= 3 && kwotaDoZaplaty != 0)
                            {
                                pozycjaWysylka.CenaWWD -= kwotaVouchera;
                                if (pozycjaWysylka.CenaWWD == 0)
                                    pozycjaWysylka.CenaWWD = 0.01m;
                                kwotaBruttoDokumentu = pozycje.Cast<IElementHaMag>().Sum(pozycja => pozycja.WartoscBruttoWal);
                                if (kwotaBruttoDokumentu > kwotaDoZaplaty)
                                {
                                    var pozycja = (IElementHaMag)pozycje[pozycje.Count - 2];
                                    pozycja.WartoscBruttoWal = pozycja.WartoscBruttoWal -
                                        kwotaVouchera + kwotaWysylki - 0.01m;
                                    var pozycjaWartoscBrutto = pozycja.WartoscBruttoWal;
                                    pozycja.CenaWWD = pozycja.WartoscBruttoWal / (decimal)pozycja.Ilosc;
                                    pozycja.WartoscBruttoWal = pozycjaWartoscBrutto;
                                }
                                dokumentHaMag.Opis.Insert(0, "DO SPRAWDZENIA!!!");
                                sesja.Save();
                                dokument.CzyWOptimie = true;
                                //dokument = null;
                                //streamWriter.WriteLine("Kwoty dokumentu zaimportowanego oraz pliku z serwera nie zgadzają się. Kasuję dokument...");
                                //streamWriter.WriteLine("Roznica to: {0}", dokument.RazemBruttoWal - kwotaDoZaplaty);
                            }
                            sesja.Save();
                            dokument.CzyWOptimie = true;
                            //File.Delete(dokument.PlikXML);
                        }
                    }
                    catch (Exception ex)
                    {
                        System.Console.WriteLine("Błąd wczytywania zamówienia. " + ex);
                    }
                }
                licznik++;
                System.Console.WriteLine(licznikZaznaczonych);
            }
        }

        private static void OdczytajIstniejace()
        {
            try
            {
                var sesja = new AdoSession();
                object recordsetAffected;
                var zapytanie = "SELECT [DAt_WartoscTxt] FROM [CDN].[DokAtrybuty] join cdn.Tranag on Dat_TrnID = trn_trnid where DAt_Kod = 'NRZAMOWIENIA' and TrN_TypDokumentu in (302, 320)";
                var command = new ADODB.Command
                {
                    ActiveConnection = sesja.Connection,
                    CommandText = zapytanie
                };
                var recordset = command.Execute(out recordsetAffected, new object());
                while (!recordset.EOF)
                {
                    _dokumentyZBazy.Add(recordset.Fields[0].Value.ToString());
                    recordset.MoveNext();
                }
            }
            catch (Exception ex)
            {
                Debug.WriteLine("Logowanie lub pobranie istniejących zamówień z Optimy nie powiodło się:\n" + ex.ToString());
            }
        }

        private static int ZwrocNrCeny(string waluta)
        {
            if (waluta != null)
            {
                if (waluta == "PLN")
                {
                    return 2;
                }
                if (waluta == "EUR")
                {
                    return 3;
                }
                if (waluta == "GBP")
                {
                    return 4;
                }
                if (waluta == "USD")
                {
                    return 5;
                }
            }
            return 2;
        }

        protected static void SetProperty(object o, string name, object value)  //wymagane do ustawienia numeracji dokumentów
        {
            o?.GetType().InvokeMember(name, System.Reflection.BindingFlags.SetProperty, null, o, new[] { value });
        }
    }
}
