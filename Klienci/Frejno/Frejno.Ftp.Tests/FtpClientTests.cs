﻿using Microsoft.VisualStudio.TestTools.UnitTesting;

namespace Frejno.Ftp.Tests
{
    [TestClass]
    public class FtpClientTests
    {
        readonly FtpClient _client = new FtpClient
        {
            Server = "ftp://ftp.cluster027.hosting.ovh.net",
            User = "ultrascoau",
            Password = "BVkOj9v34g1fsv",
            KatalogProduktow = "/www/optima/produkty",
            KatalogZamowien = "/www/optima/zamowienia/zaplacone",
            KatalogFpf = "/www/optima/zamowienia/pro_forma"
        };

        [TestMethod]
        public void SprawdzPolaczenieTest()
        {
            Assert.AreNotEqual(0, _client.SprawdzPolaczenie());
        }

        [TestMethod]
        public void PobierzNoweProduktyTest()
        {
            var iloscProduktow = 188;
            Assert.AreEqual(iloscProduktow, _client.PobierzNoweProdukty().Count);
        }

        [TestMethod]
        public void PobierzProformyTest()
        {
            var iloscProform = 21;
            Assert.AreEqual(iloscProform, _client.PobierzProformy().Count);
        }

        [TestMethod]
        public void PobierzZamowieniaTest()
        {
            var iloscZamowien = 3;
            Assert.AreEqual(iloscZamowien, _client.PobierzZamowienia().Count);
        }
    }
}