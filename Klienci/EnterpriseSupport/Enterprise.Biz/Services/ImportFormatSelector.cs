﻿using Enterprise.Biz.Model;
using OptimaImporter.Data.Enum;

namespace Enterprise.Biz.Services
{
    public class ImportFormatSelector
    {
        public static ImportFormat Select(string buttonName)
        {
            switch (buttonName)
            {
                case "VatzSubiektMobisoftButton":
                    return new ImportFormat { SourceFileRead = SourceFileReader.ReadSubiektInvoices, VatRegisterType = VatRegisterType.Purchase, InputAdditionalParameter = "FZ" };
                case "VatsSubiektMobisoftButton":
                    return new ImportFormat { SourceFileRead = SourceFileReader.ReadSubiektInvoices, VatRegisterType = VatRegisterType.Sales, InputAdditionalParameter = "FS" };
                case "VatzSubiektButton":
                    return new ImportFormat { SourceFileRead = SourceFileReader.ReadSubiektInvoices, VatRegisterType = VatRegisterType.Purchase, InputAdditionalParameter = "FZ" };
                case "VatsSubiektButton":
                    return new ImportFormat { SourceFileRead = SourceFileReader.ReadSubiektInvoices, VatRegisterType = VatRegisterType.Sales, InputAdditionalParameter = "FS" };
                case "VatsKolagenButton":
                    return new ImportFormat { SourceFileRead = SourceFileReader.ReadKolagenPurchases, VatRegisterType = VatRegisterType.Sales, InputAdditionalParameter = "Sheet" };
                case "VatsKolagen2Button":
                    return new ImportFormat { SourceFileRead = SourceFileReader.ReadKolagen2Purchases, VatRegisterType = VatRegisterType.Sales, InputAdditionalParameter = "Sheet" };
                case "FakturowniaJpksButton":
                    return new ImportFormat { SourceFileRead = SourceFileReader.ReadJpkFaInvoices, VatRegisterType = VatRegisterType.Sales };
                case "SurchemButton":
                    return new ImportFormat { SourceFileRead = SourceFileReader.ReadSurchemSales, VatRegisterType = VatRegisterType.Sales };
                case "UberSalesButton":
                    return new ImportFormat { SourceFileRead = SourceFileReader.ReadUberSalesInvoices, VatRegisterType = VatRegisterType.Sales, InputAdditionalParameter = "Sheet1" };
                case "BoltSalesButton":
                    return new ImportFormat { SourceFileRead = SourceFileReader.ReadBoltSalesInvoices, VatRegisterType = VatRegisterType.Sales };

                default:
                    return null;
            }
        }
    }
}
