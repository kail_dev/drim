﻿using ExcelDataReader;
using NLog;
using OptimaImporter.Biz.Helper;
using OptimaImporter.Biz.Model;
//using Optima.Common.Helpers;
using System;
using System.Collections.Generic;
using System.Data;
using System.Data.OleDb;
using System.Diagnostics;
using System.Globalization;
using System.IO;
using System.Linq;
using System.Text;
using System.Xml.Linq;

namespace Enterprise.Biz.Services
{
    public class SourceFileReader
    {

        public static List<VatInvoice> ReadJpkFaInvoices(string sourcePath, string additionalParameter = "")
        {
            var invoices = new List<VatInvoice>();
            var xdoc = XDocument.Load(sourcePath);

            var validInvoicesCounter = 0;
            foreach (var xElement in xdoc.Descendants().Where(x => x.Name.LocalName == "Faktura"))
            {
                var invoice = new VatInvoice();
                invoice.InvoiceDate = Convert.ToDateTime(
                    xElement.Elements()
                        .FirstOrDefault(x => x.Name.LocalName.ToString()
                            .StartsWith("P_1"))?.Value);
                invoice.Number = xElement.Elements()
                    .FirstOrDefault(x => x.Name.LocalName.ToString()
                        .StartsWith("P_2A"))?.Value;
                invoice.BuyerName = xElement.Elements()
                    .FirstOrDefault(x => x.Name.LocalName.ToString()
                        .StartsWith("P_3A"))?.Value;
                invoice.BuyerAddress = xElement.Elements()
                    .FirstOrDefault(x => x.Name.LocalName.ToString()
                        .StartsWith("P_3B"))?.Value;
                invoice.SellerName = xElement.Elements()
                    .FirstOrDefault(x => x.Name.LocalName.ToString()
                        .StartsWith("P_3C"))?.Value;
                invoice.SellerAddress = xElement.Elements()
                    .FirstOrDefault(x => x.Name.LocalName.ToString()
                        .StartsWith("P_3D"))?.Value;
                invoice.BuyerTaxNumberCountry = xElement.Elements()
                    .FirstOrDefault(x => x.Name.LocalName.ToString()
                        .StartsWith("P_4A"))?.Value;
                invoice.BuyerTaxNumber = xElement.Elements()
                    .FirstOrDefault(x => x.Name.LocalName.ToString()
                        .StartsWith("P_4B"))?.Value;
                invoice.SellerTaxNumberCountry = xElement.Elements()
                    .FirstOrDefault(x => x.Name.LocalName.ToString()
                        .StartsWith("P_5A"))?.Value;
                invoice.SellerTaxNumber = xElement.Elements()
                    .FirstOrDefault(x => x.Name.LocalName.ToString()
                        .StartsWith("P_5B"))?.Value;
                invoice.PaymentDate = Convert.ToDateTime(
                    xElement.Elements()
                        .FirstOrDefault(x => x.Name.LocalName.ToString()
                            .StartsWith("P_6"))?.Value);
                invoice.NetValue = Convert.ToDecimal(
                    xElement.Elements()
                        .FirstOrDefault(x => x.Name.LocalName.ToString()
                            .StartsWith("P_13_7"))?.Value.Replace(".", ","));
                //TaxValue = Convert.ToDecimal(
                //    xElement.Elements()
                //        .FirstOrDefault(x => x.Name.LocalName.ToString()
                //            .StartsWith("P_14_1"))?.Value.Replace(".", ",")),
                //invoice.GrossValue = Convert.ToDecimal(
                //    xElement.Elements()
                //        .FirstOrDefault(x => x.Name.LocalName.ToString()
                //            .StartsWith("P_15"))?.Value.Replace(".", ","));

                validInvoicesCounter++;
                Debug.WriteLine(validInvoicesCounter);
                invoices.Add(invoice);
            }

            var validRowsCounter = 0;
            foreach (var xElement in xdoc.Descendants().Where(x => x.Name.LocalName == "FakturaWiersz"))
            {
                var invoiceNumberFromRow = xElement.Elements()
                    .FirstOrDefault(x => x.Name.LocalName.ToString()
                        .StartsWith("P_2B"))?.Value; //P_2B
                var invoice = invoices.FirstOrDefault(x => x.Number == invoiceNumberFromRow);
                if (invoice != null)
                {
                    try
                    {
                        var netValue = Convert.ToDecimal(
                            xElement.Elements()
                                .FirstOrDefault(x => x.Name.LocalName.ToString()
                                    .StartsWith("P_11"))?.Value.Replace(".", ","));
                        var vatRate = xElement.Elements()
                            .FirstOrDefault(x => x.Name.LocalName.ToString()
                                .StartsWith("P_12"))?.Value;
                        var grossValue = Convert.ToDecimal(
                            xElement.Elements()
                                .FirstOrDefault(x => x.Name.LocalName.ToString()
                                    .StartsWith("P_11A"))?.Value.Replace(".", ","));

                        invoice.Rows.Add(new VatInvoiceRow
                        {
                            NetValue = netValue,
                            VatRate = vatRate,
                            GrossValue = grossValue
                        });

                        validRowsCounter++;
                        Debug.WriteLine(validRowsCounter);
                    }
                    catch (Exception)
                    {
                        //Debug.WriteLine($"Nie udało się odczytać wiersza dla faktury: {invoiceNumberFromRow}, {ex}");
                    }
                }
            }

            return invoices;
        }

        public static List<VatInvoice> ReadSubiektInvoices(string sourcePath, string invoiceType)
        {
            var invoices = new List<VatInvoice>();
            var fileContent = File.ReadAllLines(sourcePath, Encoding.GetEncoding("Windows-1250"))
            //var fileContent = File.ReadAllLines(sourcePath, Encoding.UTF8)
                .Where(x => x.Length > 0).ToList();
            for (var i = 0; i < fileContent.Count - 1; i++)
            {
                var line = fileContent[i];
                var nextLine = fileContent[i + 1];
                if (line.StartsWith("[NAGLOWEK]") && nextLine.StartsWith($"\"{invoiceType}"))
                {
                    var invoiceContent = fileContent[i + 1];
                    Debug.WriteLine($"{i}:  {invoiceContent}");
                    string[] invoiceHeadElements = null;
                    var invoiceRows = new List<VatInvoiceRow>();
                    var invoiceHeadLinesCounter = 0;
                    var lineIPlus2 = fileContent[i + 2];

                    var nextLineCounter = 3;
                    while (!lineIPlus2.StartsWith("["))
                    {

                        invoiceContent += lineIPlus2;
                        var invoiceHeadElementsList = invoiceContent.Split(',').ToList();
                        invoiceHeadElements = invoiceHeadElementsList.ToArray();
                        invoiceHeadLinesCounter++;
                        lineIPlus2 = fileContent[i + nextLineCounter];
                        nextLineCounter++;
                    }

                    if (fileContent[i + 2 + invoiceHeadLinesCounter].StartsWith("[ZAW"))
                    {
                        var j = i;
                        while (fileContent.Count > j + 3 + invoiceHeadLinesCounter
                               && !fileContent[j + 3 + invoiceHeadLinesCounter].StartsWith("[N"))
                        {
                            try
                            {
                                var invoiceRowElements = fileContent[j + 3 + invoiceHeadLinesCounter].Split(',');
                                try
                                {
                                    var invoiceRow = new VatInvoiceRow
                                    {
                                        NetValue = Convert.ToDecimal(invoiceRowElements[16], new CultureInfo("en-US")),
                                        VatRate = invoiceRowElements[15].Replace("\"", ""),
                                        GrossValue = Convert.ToDecimal(invoiceRowElements[18], new CultureInfo("en-US"))
                                    };
                                    if (invoiceRow.VatRate == "ue")
                                    {
                                        invoiceRow.VatRate = "0";
                                    }

                                    invoiceRows.Add(invoiceRow);
                                }
                                catch (Exception)
                                {
                                    var invoiceRow = new VatInvoiceRow
                                    {
                                        NetValue = Convert.ToDecimal(invoiceRowElements[2], new CultureInfo("en-US")),
                                        VatRate = invoiceRowElements[0].Replace("\"", ""),
                                        GrossValue = Convert.ToDecimal(invoiceRowElements[4], new CultureInfo("en-US"))
                                    };
                                    if (invoiceRow.VatRate == "ue")
                                    {
                                        invoiceRow.VatRate = "0";
                                    }

                                    invoiceRows.Add(invoiceRow);
                                }
                            }
                            catch (Exception ex)
                            {
                                LogManager.GetCurrentClassLogger()
                                    .Info(
                                        $"Nie udało się odczytać wiersza dla faktury {invoiceContent[6]}, błąd: {ex}");
                            }
                            finally
                            {
                                j++;
                            }
                        }
                    }

                    var invoiceContentTmp = invoiceContent.Split(',').ToList();
                    if (invoiceContent.StartsWith("\"FZ"))
                    {
                        if (invoiceContentTmp.Count > 22
                            && !invoiceContentTmp[21].Replace("\"", "").StartsWith("20")
                            && invoiceContentTmp[22].Replace("\"", "").StartsWith("20"))
                        {
                            invoiceContentTmp.RemoveAt(13);
                            invoiceHeadElements = invoiceContentTmp.ToArray();
                        }

                        if (invoiceContentTmp.Count > 20 && invoiceContentTmp[20].Replace("\"", "").StartsWith("20"))
                        {
                            invoiceContentTmp.Insert(20, "");
                            invoiceHeadElements = invoiceContentTmp.ToArray();
                        }

                        if (invoiceContentTmp.Count > 33 && invoiceContentTmp[33].Replace("\"", "").StartsWith("20"))
                        {
                            invoiceContentTmp.Insert(31, "");
                            invoiceHeadElements = invoiceContentTmp.ToArray();
                        }

                        invoiceHeadElements = invoiceHeadElements ?? invoiceContentTmp.ToArray();
                    }

                    if (invoiceContent.StartsWith("\"FS"))
                    {
                        if (invoiceContentTmp.Count > 24
                            && !invoiceContentTmp[23].Replace("\"", "").StartsWith("20")
                            && invoiceContentTmp[24].Replace("\"", "").StartsWith("20"))
                        {
                            invoiceContentTmp.RemoveAt(13);
                            invoiceContentTmp.RemoveAt(16);
                            invoiceHeadElements = invoiceContentTmp.ToArray();
                        }

                        if (invoiceContentTmp.Count > 23
                            && !invoiceContentTmp[22].Replace("\"", "").StartsWith("20")
                            && invoiceContentTmp[23].Replace("\"", "").StartsWith("20"))
                        {
                            invoiceContentTmp.RemoveAt(1);
                            invoiceHeadElements = invoiceContentTmp.ToArray();
                        }

                        if (invoiceContentTmp.Count > 23
                            && !invoiceContentTmp[21].Replace("\"", "").StartsWith("20")
                            && invoiceContentTmp[22].Replace("\"", "").StartsWith("20"))
                        {
                            invoiceContentTmp.RemoveAt(19);
                            invoiceHeadElements = invoiceContentTmp.ToArray();
                        }

                        if (sourcePath.Contains("magazyn"))
                        {
                            invoiceContentTmp.RemoveAt(21);
                            invoiceHeadElements = invoiceContentTmp.ToArray();
                        }

                        invoiceHeadElements = invoiceHeadElements ?? invoiceContentTmp.ToArray();
                    }

                    if (invoiceContent.StartsWith($"\"{invoiceType}"))
                    {
                        try
                        {
                            if (invoiceType == "FZ")
                            {
                                var invoiceDateYear =
                                    Convert.ToInt32(invoiceHeadElements[21].Replace("\"", "").Substring(0, 4));
                                var invoiceDateMonth =
                                    Convert.ToInt32(invoiceHeadElements[21].Replace("\"", "").Substring(4, 2));
                                var invoiceDateDay =
                                    Convert.ToInt32(invoiceHeadElements[21].Replace("\"", "").Substring(6, 2));
                                var paymentDateYear =
                                    Convert.ToInt32(invoiceHeadElements[34].Replace("\"", "").Substring(0, 4));
                                var paymentDateMonth =
                                    Convert.ToInt32(invoiceHeadElements[34].Replace("\"", "").Substring(4, 2));
                                var paymentDateDay =
                                    Convert.ToInt32(invoiceHeadElements[34].Replace("\"", "").Substring(6, 2));
                                var invoice = new VatInvoice
                                {
                                    Number = invoiceHeadElements[4].Replace("\"", ""),
                                    SellerName = invoiceHeadElements[12].Replace("\"", ""),
                                    City = invoiceHeadElements[14].Replace("\"", ""),
                                    PostalCode = invoiceHeadElements[15].Replace("\"", ""),
                                    Street = invoiceHeadElements[16].Replace("\"", ""),
                                    SellerTaxNumber = invoiceHeadElements[17].Replace("\"", ""),
                                    InvoiceDate = new DateTime(Convert.ToInt32(invoiceDateYear),
                                        Convert.ToInt32(invoiceDateMonth),
                                        Convert.ToInt32(invoiceDateDay)
                                    ),
                                    PaymentDate = new DateTime(Convert.ToInt32(paymentDateYear),
                                        Convert.ToInt32(paymentDateMonth),
                                        Convert.ToInt32(paymentDateDay)
                                    ),
                                    Currency = invoiceHeadElements[invoiceHeadElements.Length - 16].Replace("\"", ""),
                                    CurrencyRate =
                                        invoiceHeadElements[invoiceHeadElements.Length - 15].Replace("\"", ""),
                                    Rows = invoiceRows
                                };
                                invoices.Add(invoice);
                            }

                            if (invoiceType == "FS")
                            {
                                var invoiceDateYear =
                                    Convert.ToInt32(invoiceHeadElements[21].Replace("\"", "").Substring(0, 4));
                                var invoiceDateMonth =
                                    Convert.ToInt32(invoiceHeadElements[21].Replace("\"", "").Substring(4, 2));
                                var invoiceDateDay =
                                    Convert.ToInt32(invoiceHeadElements[21].Replace("\"", "").Substring(6, 2));
                                var paymentDateYear = invoiceHeadElements[33].Length > 0
                                    ? Convert.ToInt32(invoiceHeadElements[33].Replace("\"", "").Substring(0, 4))
                                    : Convert.ToInt32(invoiceHeadElements[34].Replace("\"", "").Substring(0, 4));
                                var paymentDateMonth = invoiceHeadElements[33].Length > 0
                                    ? Convert.ToInt32(invoiceHeadElements[33].Replace("\"", "").Substring(4, 2))
                                    : Convert.ToInt32(invoiceHeadElements[34].Replace("\"", "").Substring(4, 2));
                                var paymentDateDay = invoiceHeadElements[33].Length > 0
                                    ? Convert.ToInt32(invoiceHeadElements[33].Replace("\"", "").Substring(6, 2))
                                    : Convert.ToInt32(invoiceHeadElements[34].Replace("\"", "").Substring(6, 2));
                                var invoice = new VatInvoice
                                {
                                    Number = invoiceHeadElements[6].Replace("\"", ""),
                                    BuyerName = invoiceHeadElements[12].Replace("\"", ""),
                                    City = invoiceHeadElements[14].Replace("\"", ""),
                                    PostalCode = invoiceHeadElements[15].Replace("\"", ""),
                                    Street = invoiceHeadElements[16].Replace("\"", ""),
                                    BuyerTaxNumber = invoiceHeadElements[17].Replace("\"", ""),
                                    InvoiceDate = new DateTime(Convert.ToInt32(invoiceDateYear),
                                        Convert.ToInt32(invoiceDateMonth),
                                        Convert.ToInt32(invoiceDateDay)
                                    ),
                                    PaymentDate = new DateTime(Convert.ToInt32(paymentDateYear),
                                        Convert.ToInt32(paymentDateMonth),
                                        Convert.ToInt32(paymentDateDay)
                                    ),
                                    Currency = invoiceHeadElements[invoiceHeadElements.Length - 16].Replace("\"", ""),
                                    CurrencyRate =
                                        invoiceHeadElements[invoiceHeadElements.Length - 15].Replace("\"", ""),
                                    Rows = invoiceRows
                                };
                                invoices.Add(invoice);
                            }
                        }
                        catch (Exception ex)
                        {
                            LogManager.GetCurrentClassLogger()
                                .Info(
                                    $"Nie udało się utworzyć faktury sprzedaży nr {invoiceHeadElements[6]}, błąd: {ex}");
                        }
                    }
                }
                else if (invoiceType == "FS" && line.StartsWith($"\"{invoiceType} "))
                {
                    var invoice = new VatInvoice { Number = "pusty" };
                    try
                    {
                        var lineElements = line.Split(',');
                        var invoiceNumber = lineElements[0].Replace("\"", "").Replace("FS ", "");
                        invoice = invoices.FirstOrDefault(inv => inv.Number == invoiceNumber);

                        var invoiceDateYear = Convert.ToInt32(lineElements[1].Replace("\"", "").Substring(0, 4));
                        var invoiceDateMonth = Convert.ToInt32(lineElements[1].Replace("\"", "").Substring(4, 2));
                        var invoiceDateDay = Convert.ToInt32(lineElements[1].Replace("\"", "").Substring(6, 2));

                        invoice.OperationDate = new DateTime(invoiceDateYear, invoiceDateMonth, invoiceDateDay);

                    }
                    catch (Exception ex)
                    {
                        LogManager.GetCurrentClassLogger()
                            .Info($"Nie udało się podmienić daty sprzedaży dla faktury sprzedaży, błąd: {ex}");
                    }
                }
            }
            LogManager.GetCurrentClassLogger().Info($"Odczytano {invoices.Count} faktur z Subiekta.");
            return invoices;
        }

        public static List<VatInvoice> ReadKolagenPurchases(string filePath, string xlsSheet)
        {
            var invoices = new List<VatInvoice>();

            var constr = "Provider=Microsoft.ACE.OLEDB.12.0;Data Source="
                            + filePath + ";Extended Properties=\"Excel 12.0 Xml;HDR=YES;\"";
            using (var conn = new OleDbConnection(constr))
            {
                conn.Open();
                var command = new OleDbCommand("Select * from [" + xlsSheet + "$]", conn);
                var reader = command.ExecuteReader();
                if (reader.HasRows)
                {
                    while (reader.Read() && !string.IsNullOrWhiteSpace(reader[0].ToString()))
                    {
                        var inv = new VatInvoice();
                        try
                        {
                            inv.Number = reader[1].ToString();
                            inv.InvoiceDate = Convert.ToDateTime(reader[4].ToString());
                            inv.OperationDate = Convert.ToDateTime(reader[4].ToString());
                            inv.BuyerTaxNumber = reader[27].ToString();
                            inv.BuyerName = reader[3].ToString();
                            inv.PaymentDate = Convert.ToDateTime(reader[5].ToString());
                            inv.BuyerAddress = reader[26].ToString();
                            inv.Rows.Add(new VatInvoiceRow
                            {
                                NetValue = Convert.ToDecimal(reader[6].ToString()),
                                VatRate = Math.Round(Convert.ToDecimal(reader[7].ToString())
                                                     / Convert.ToDecimal(reader[6].ToString()) * 100 - 100
                                    , 0).ToString(),
                                GrossValue = Convert.ToDecimal(reader[7].ToString())
                            });
                            inv.PaymentForm = reader[12].ToString();
                            invoices.Insert(0, inv);
                        }
                        catch (Exception ex)
                        {
                            LogManager.GetCurrentClassLogger()
                                .Error($"Błąd odczytu linii {inv.Number}.Błąd: {ex.Message}");
                        }
                    }
                }
            }

            return invoices;
        }
        public static List<VatInvoice> ReadKolagen2Purchases(string filePath, string xlsSheet)
        {
            var invoices = new List<VatInvoice>();

            var constr = "Provider=Microsoft.ACE.OLEDB.12.0;Data Source="
                            + filePath + ";Extended Properties=\"Excel 12.0 Xml;HDR=YES;\"";
            using (var conn = new OleDbConnection(constr))
            {
                conn.Open();
                var command = new OleDbCommand("Select * from [" + xlsSheet + "$]", conn);
                var reader = command.ExecuteReader();
                if (reader.HasRows)
                {
                    while (reader.Read() && !string.IsNullOrWhiteSpace(reader[0].ToString()))
                    {
                        var inv = new VatInvoice();
                        try
                        {
                            inv.Number = reader[1].ToString();
                            inv.InvoiceDate = Convert.ToDateTime(reader[2].ToString());
                            inv.BuyerTaxNumber = reader[3].ToString();
                            inv.BuyerName = reader[4].ToString();
                            inv.PaymentDate = Convert.ToDateTime(reader[5].ToString());
                            inv.BuyerAddress = reader[6].ToString();
                            inv.Rows.Add(new VatInvoiceRow
                            {
                                NetValue = Convert.ToDecimal(reader[8].ToString()),
                                VatRate = Math.Round(Convert.ToDecimal(reader[7].ToString())
                                                     / Convert.ToDecimal(reader[8].ToString()) * 100 - 100
                                    , 0).ToString(),
                                GrossValue = Convert.ToDecimal(reader[7].ToString())
                            });
                            inv.PaymentForm = reader[11].ToString();
                            invoices.Add(inv);
                        }
                        catch (Exception ex)
                        {
                            LogManager.GetCurrentClassLogger()
                                .Error($"Błąd odczytu linii {inv.Number}.Błąd: {ex.Message}");
                        }
                    }
                }
            }

            return invoices;
        }

        public static List<VatInvoice> ReadSurchemPurchases2(string filePath)
        {
            var invoices = new List<VatInvoice>();

            using (var stream = File.Open(filePath, FileMode.Open, FileAccess.Read))
            {
                using (var reader = ExcelReaderFactory.CreateReader(stream))
                {
                    var invoiceNumber = "";
                    var result = reader.AsDataSet().Tables[0];
                    reader.Read(); //we ommit headers line
                    while (reader.Read() && !string.IsNullOrWhiteSpace(reader.GetString(1)))
                    {
                        if (invoiceNumber != reader[1].ToString())
                        {
                            var inv = new VatInvoice();
                            try
                            {
                                inv.Number = reader[1].ToString();
                                inv.BuyerName = reader[4].ToString();
                                inv.InvoiceDate = Convert.ToDateTime(reader[5].ToString());
                                inv.OperationDate = Convert.ToDateTime(reader[6].ToString());
                                inv.PostalCode = reader[8].ToString();
                                inv.City = reader[9].ToString();
                                inv.Street = reader[10].ToString();
                                inv.BuyerTaxNumber = reader[11].ToString().Replace("-", "");
                                inv.Currency = reader[12].ToString();
                                inv.CurrencyRate = reader[13].ToString();
                                inv.PaymentDate = Convert.ToDateTime(reader[15].ToString()).AddDays(7);
                                inv.Description = reader[17].ToString();
                                if (inv.Currency == "PLN")
                                {
                                    inv.Rows.Add(new VatInvoiceRow
                                    {
                                        NetValue = Convert.ToDecimal(reader[23].ToString()),
                                        VatRate = Math.Round(Convert.ToDecimal(reader[25].ToString())
                                                     / Convert.ToDecimal(reader[23].ToString()) * 100 - 100
                                    , 0).ToString(),
                                        GrossValue = Convert.ToDecimal(reader[25].ToString())
                                    });
                                }
                                else if (inv.Currency == "EUR")
                                {
                                    inv.Rows.Add(new VatInvoiceRow
                                    {
                                        NetValue = Convert.ToDecimal(reader[23].ToString()) / Convert.ToDecimal(inv.CurrencyRate),
                                        VatRate = Math.Round(Convert.ToDecimal(reader[25].ToString())
                                                             / Convert.ToDecimal(reader[23].ToString()) * 100 - 100
                                            , 0).ToString(),
                                        GrossValue = Convert.ToDecimal(reader[25].ToString()) / Convert.ToDecimal(inv.CurrencyRate)
                                    });
                                }
                                invoices.Add(inv);
                            }
                            catch (Exception ex)
                            {
                                LogManager.GetCurrentClassLogger()
                                    .Error($"Błąd odczytu linii {inv.Number}.Błąd: {ex.Message}");
                            }
                        }
                        else
                        {
                            var inv = invoices.LastOrDefault();
                            if (inv?.Currency == "PLN")
                            {
                                try
                                {
                                    inv.Rows.Add(new VatInvoiceRow
                                    {
                                        NetValue = Convert.ToDecimal(reader[23].ToString()),
                                        VatRate = Math.Round(Convert.ToDecimal(reader[25].ToString())
                                                     / Convert.ToDecimal(reader[23].ToString()) * 100 - 100
                                    , 0).ToString(),
                                        GrossValue = Convert.ToDecimal(reader[25].ToString())
                                    });

                                }
                                catch (Exception)
                                {

                                }
                            }
                            else if (inv.Currency == "EUR")
                            {
                                try
                                {
                                    inv.Rows.Add(new VatInvoiceRow
                                    {
                                        NetValue = Convert.ToDecimal(reader[23].ToString()) / Convert.ToDecimal(inv.CurrencyRate),
                                        VatRate = Math.Round(Convert.ToDecimal(reader[25].ToString())
                                                     / Convert.ToDecimal(reader[23].ToString()) * 100 - 100
                                    , 0).ToString(),
                                        GrossValue = Convert.ToDecimal(reader[25].ToString()) / Convert.ToDecimal(inv.CurrencyRate)
                                    });

                                }
                                catch (Exception)
                                {

                                }
                            }
                        }

                        invoiceNumber = reader[1].ToString();
                    }
                }
            }

            return invoices;
        }

        public static List<VatInvoice> ReadSurchemSales(string filePath, string additionalParameter = "")
        {
            var invoices = new List<VatInvoice>();

            using (var stream = File.Open(filePath, FileMode.Open, FileAccess.Read))
            {
                using (var reader = ExcelReaderFactory.CreateReader(stream))
                {
                    var invoiceNumber = "";
                    var result = reader.AsDataSet().Tables[0];
                    reader.Read(); //we ommit headers line
                    while (reader.Read() && !string.IsNullOrWhiteSpace(reader.GetString(0)))
                    {
                        if (invoiceNumber != reader[0].ToString())
                        {
                            var inv = new VatInvoice();
                            try
                            {
                                inv.Number = reader.GetString(0);
                                inv.BuyerName = reader.GetString(6).Trim();
                                inv.InvoiceDate = Convert.ToDateTime(reader[1].ToString());
                                inv.OperationDate = Convert.ToDateTime(reader[1].ToString());
                                inv.PostalCode = reader.GetString(10);
                                inv.City = reader.GetString(8)?.Trim();
                                inv.Street = reader.GetString(9)?.Trim();
                                inv.BuyerTaxNumber = reader.GetString(7)?.Replace("-", "");
                                inv.Currency = reader.GetString(11);
                                if (inv.Currency != null && inv.Currency != "PLN")
                                    inv.CurrencyRate = reader[12].ToString();
                                inv.PaymentDate = Convert.ToDateTime(reader[3].ToString());
                                inv.Description = reader.GetString(29);
                                inv.PaymentForm = reader.GetString(2).Split(' ')[0];
                                inv.Rows = new List<VatInvoiceRow>
                                {
                                    new VatInvoiceRow
                                    {
                                        NetValue = Convert.ToDecimal(reader[26].ToString()),
                                        VatRate = reader[15].ToString(),
                                        GrossValue = Convert.ToDecimal(reader[28].ToString())
                                    }
                                };
                                invoices.Add(inv);
                            }
                            catch (Exception e)
                            {
                                LogManager.GetCurrentClassLogger()
                                    .Error($"Błąd odczytu linii {reader.GetString(0)}_{reader.GetString(4)}.Błąd: {e.Message}");
                            }
                        }
                        else
                        {
                            var inv = invoices.LastOrDefault();
                            inv?.Rows.Add(new VatInvoiceRow
                            {
                                NetValue = Convert.ToDecimal(reader[26].ToString()),
                                VatRate = reader[15].ToString(),
                                GrossValue = Convert.ToDecimal(reader[28].ToString())
                            });
                        }
                        invoiceNumber = reader.GetString(0);
                    }
                }
            }

            return invoices;
        }

        public static List<VatInvoice> ReadUberSalesInvoices(string filePath, string sheet)
        {
            var invoices = new List<VatInvoice>();

            using (var stream = File.Open(filePath, FileMode.Open, FileAccess.Read))
            {
                using (var reader = ExcelReaderFactory.CreateReader(stream))
                {
                    var invoiceNumber = "";
                    var result = reader.AsDataSet().Tables[0];
                    reader.Read(); //we ommit headers line
                    while (reader.Read() && !string.IsNullOrWhiteSpace(reader.GetString(11)))
                    {
                        if (invoiceNumber != reader[11].ToString())
                        {
                            var inv = new VatInvoice
                            {
                                Rows = new List<VatInvoiceRow>()
                            };
                            try
                            {
                                inv.Number = reader[11].ToString();
                                inv.BuyerCode = "!NIEOKREŚLONY!";
                                inv.BuyerName = reader[9] + " " + reader[13];
                                inv.InvoiceDate = Convert.ToDateTime(reader[17].ToString().Substring(0, 10));
                                inv.OperationDate = Convert.ToDateTime(reader[17].ToString().Substring(0, 10));
                                inv.PostalCode = reader[15].ToString();
                                inv.City = reader[6].ToString();
                                inv.BuyerAddress = reader[1] + " " + reader[2];
                                //inv.BuyerTaxNumber = reader[11].ToString().Replace("-", "");
                                //inv.Currency = reader[12].ToString();
                                //inv.CurrencyRate = reader[13].ToString();
                                inv.PaymentDate = Convert.ToDateTime(reader[17].ToString().Substring(0, 10)).AddDays(1);
                                //inv.Description = reader[17].ToString();
                                inv.Currency = "PLN";
                                inv.Rows.Add(new VatInvoiceRow
                                {
                                    NetValue = Convert.ToDecimal(reader[14].ToString().Replace(".", ",")),
                                    VatRate = reader[24].ToString().Replace("0,0", ""),
                                    GrossValue = Convert.ToDecimal(reader[10].ToString().Replace(".", ","))
                                });
                                //else if (inv.Currency == "EUR")
                                //{
                                //    inv.Rows.Add(new InvoiceRow
                                //    {
                                //        NetValue = Convert.ToDecimal(reader[23].ToString()) / Convert.ToDecimal(inv.CurrencyRate),
                                //        VatRate = Math.Round(Convert.ToDecimal(reader[25].ToString())
                                //                             / Convert.ToDecimal(reader[23].ToString()) * 100 - 100
                                //            , 0).ToString(),
                                //        GrossValue = Convert.ToDecimal(reader[25].ToString()) / Convert.ToDecimal(inv.CurrencyRate)
                                //    });
                                //}
                                invoices.Add(inv);
                            }
                            catch (Exception ex)
                            {
                                LogManager.GetCurrentClassLogger()
                                    .Error($"Błąd odczytu linii {inv.Number}.Błąd: {ex.Message}");
                            }
                        }
                        else
                        {
                            var inv = invoices.LastOrDefault();
                            if (inv?.Currency == "PLN")
                            {
                                try
                                {
                                    inv.Rows.Add(new VatInvoiceRow
                                    {
                                        NetValue = Convert.ToDecimal(reader[23].ToString()),
                                        VatRate = Math.Round(Convert.ToDecimal(reader[25].ToString())
                                                     / Convert.ToDecimal(reader[23].ToString()) * 100 - 100
                                    , 0).ToString(),
                                        GrossValue = Convert.ToDecimal(reader[25].ToString())
                                    });

                                }
                                catch (Exception)
                                {

                                }
                            }
                            else if (inv.Currency == "EUR")
                            {
                                try
                                {
                                    inv.Rows.Add(new VatInvoiceRow
                                    {
                                        NetValue = Convert.ToDecimal(reader[23].ToString()) / Convert.ToDecimal(inv.CurrencyRate),
                                        VatRate = Math.Round(Convert.ToDecimal(reader[25].ToString())
                                                     / Convert.ToDecimal(reader[23].ToString()) * 100 - 100
                                    , 0).ToString(),
                                        GrossValue = Convert.ToDecimal(reader[25].ToString()) / Convert.ToDecimal(inv.CurrencyRate)
                                    });

                                }
                                catch (Exception)
                                {

                                }
                            }
                        }

                        invoiceNumber = reader[11].ToString();
                    }
                }
            }

            return invoices;
        }

        public static List<VatInvoice> ReadBoltSalesInvoices(string filePath, string additionalParameter = "")
        {
            var invoices = new List<VatInvoice>();

            using (var stream = File.Open(filePath, FileMode.Open, FileAccess.Read))
            {
                using (var streamRreader = new StreamReader(stream))
                {
                    streamRreader.ReadLine(); //we ommit headers line
                    while (!streamRreader.EndOfStream)
                    {
                        var line = streamRreader.ReadLine()?.Replace("\"\"", "\"").Insert(15, "\"").SplitWithTextDelimiter(',', "\"");
                        if (line != null && !string.IsNullOrWhiteSpace(line[0]))
                        {
                            var inv = new VatInvoice
                            {
                                Rows = new List<VatInvoiceRow>()
                            };
                            try
                            {
                                var dateString = line[5].Substring(6, 4) + "-" + line[5].Substring(3, 2) + "-" + line[5].Substring(0, 2);
                                inv.Number = line[0];
                                inv.BuyerCode = "!NIEOKREŚLONY!";
                                inv.BuyerName = line[6];
                                inv.InvoiceDate = Convert.ToDateTime(dateString);
                                inv.OperationDate = Convert.ToDateTime(dateString);
                                //inv.PostalCode = line[15];
                                //inv.City = line[6];
                                inv.BuyerAddress = line[7];
                                //inv.BuyerTaxNumber = reader[11].ToString().Replace("-", "");
                                //inv.Currency = reader[12].ToString();
                                //inv.CurrencyRate = reader[13].ToString();
                                inv.PaymentDate = Convert.ToDateTime(dateString).AddDays(1);
                                //inv.Description = reader[17].ToString();
                                inv.Currency = "PLN";
                                inv.Rows.Add(new VatInvoiceRow
                                {
                                    NetValue = Convert.ToDecimal(line[14].Replace(".", ",")),
                                    VatRate = Math.Round(Convert.ToDecimal(line[16]) / Convert.ToDecimal(line[14]) * 100 - 100, 0).ToString(),
                                    GrossValue = Convert.ToDecimal(line[16].Replace(".", ","))
                                });
                                //else if (inv.Currency == "EUR")
                                //{
                                //    inv.Rows.Add(new InvoiceRow
                                //    {
                                //        NetValue = Convert.ToDecimal(reader[23].ToString()) / Convert.ToDecimal(inv.CurrencyRate),
                                //        VatRate = Math.Round(Convert.ToDecimal(reader[25].ToString())
                                //                             / Convert.ToDecimal(reader[23].ToString()) * 100 - 100
                                //            , 0).ToString(),
                                //        GrossValue = Convert.ToDecimal(reader[25].ToString()) / Convert.ToDecimal(inv.CurrencyRate)
                                //    });
                                //}
                                invoices.Add(inv);
                            }
                            catch (Exception ex)
                            {
                                LogManager.GetCurrentClassLogger()
                                    .Error($"Błąd odczytu linii {inv.Number}.Błąd: {ex.Message}");
                            }
                        }
                        else
                        {
                            var inv = invoices.LastOrDefault();
                            if (inv?.Currency == "PLN")
                            {
                                try
                                {
                                    inv.Rows.Add(new VatInvoiceRow
                                    {
                                        NetValue = Convert.ToDecimal(line[23]),
                                        VatRate = Math.Round(Convert.ToDecimal(line[25])
                                                     / Convert.ToDecimal(line[23]) * 100 - 100
                                    , 0).ToString(),
                                        GrossValue = Convert.ToDecimal(line[25])
                                    });

                                }
                                catch (Exception)
                                {

                                }
                            }
                            else if (inv.Currency == "EUR")
                            {
                                try
                                {
                                    inv.Rows.Add(new VatInvoiceRow
                                    {
                                        NetValue = Convert.ToDecimal(line[23]) / Convert.ToDecimal(inv.CurrencyRate),
                                        VatRate = Math.Round(Convert.ToDecimal(line[25])
                                                     / Convert.ToDecimal(line[23]) * 100 - 100
                                    , 0).ToString(),
                                        GrossValue = Convert.ToDecimal(line[25]) / Convert.ToDecimal(inv.CurrencyRate)
                                    });

                                }
                                catch (Exception)
                                {

                                }
                            }
                        }
                    }
                }
            }

            return invoices;
        }
        public static List<Payment> ReadKasaKbPayments(string filePath)
        {
            var payments = new List<Payment>();

            using (var stream = File.Open(filePath, FileMode.Open, FileAccess.Read))
            {
                using (var reader = ExcelReaderFactory.CreateReader(stream))
                {
                    var result = reader.AsDataSet().Tables[0];
                    reader.Read(); //we ommit headers line
                    while (reader.Read())
                    {
                        var i = 0;
                        try
                        {
                            var firstCell = reader.GetDateTime(0);
                        }
                        catch
                        {
                            try
                            {
                                var firstCell = reader.GetDateTime(1);
                                i = 1;
                            }
                            catch (Exception)
                            {
                                continue;
                            }
                        }
                        var payment = new Payment();
                        try
                        {
                            var date = reader.GetDateTime(0 + i).ToString("dd-MM-yyyy");
                            payment.TransactionDate = date.Substring(6, 4) + "-" +
                                                      date.Substring(3, 2) + "-" +
                                                      date.Substring(0, 2);
                            payment.TransactionDescription = $"{reader.GetString(1 + i)}, {reader.GetString(4 + i)}";
                            payment.ContractorName = $"{reader.GetString(2 + i)}, {reader.GetString(3 + i)}";
                            payment.TransactionAmount = reader[5 + i] != null
                                ? reader.GetDouble(5 + i).ToString()
                                : reader.GetDouble(6 + i).ToString();
                            payment.TransactionAmountWithSigh = reader[5 + i] != null
                                ? Convert.ToDecimal(reader.GetDouble(5 + i))
                                : Convert.ToDecimal(reader.GetDouble(6 + i) * -1);

                            if (!string.IsNullOrWhiteSpace(payment.TransactionDate)) payments.Add(payment);
                        }
                        catch (Exception ex)
                        {
                            LogManager.GetCurrentClassLogger()
                                .Error($"Błąd odczytu linii {payment.TransactionDescription}.Błąd: {ex.Message}");
                        }
                    }
                }
            }

            return payments;
        }

        public static List<VatInvoice> ReadMexBo(string filePath, int zakupCzySprzedaz)
        {
            var invoices = new List<VatInvoice>();

            using (var stream = File.Open(filePath, FileMode.Open, FileAccess.Read))
            {
                using (var reader = ExcelReaderFactory.CreateReader(stream))
                {
                    var result = reader.AsDataSet().Tables[0];
                    var invoiceNumber = "";
                    reader.Read(); //we ommit headers line
                    while (reader.Read())
                    {
                        var documentTypeCell = reader.GetString(10);

                        if (documentTypeCell == null) continue;
                        if (zakupCzySprzedaz == 1 && !documentTypeCell.Contains("aktura zakupu") && !documentTypeCell.Contains("Inne")) continue;
                        if (zakupCzySprzedaz == 2 && !documentTypeCell.Contains("aktura sprze") && !documentTypeCell.Contains("Inne")) continue;

                        if (invoiceNumber != reader.GetString(9))
                        {
                            var invoice = new VatInvoice
                            {
                                Number = reader.GetString(9),
                                BuyerTaxNumber = reader.GetString(2),
                                SellerTaxNumber = reader.GetString(2),
                                InvoiceDate = reader.GetDateTime(3),
                                PaymentDate = reader.GetDateTime(4),
                                Description = reader.GetString(6),
                                PaymentForm = "przelew"
                            };

                            var netValue = Convert.ToDecimal(reader.GetDouble(13));
                            var vatValue = Convert.ToDecimal(reader.GetDouble(12));
                            var grossValue = Convert.ToDecimal(reader.GetDouble(11));
                            //var vatRate = grossValue / netValue;
                            invoice.Rows.Add(new VatInvoiceRow
                            {
                                NetValue = netValue,
                                VatValue = vatValue,
                                GrossValue = grossValue,
                                VatRate = "23.00" //Math.Round(((grossValue / netValue - 1) * 100), 0).ToString()
                            });
                            if (invoice.Rows.First().VatValue == 0)
                                invoice.Rows.First().VatRate = "np";


                            invoices.Add(invoice);
                        }
                        else
                        {
                            var invoice = invoices.Last();

                            var netValue = Convert.ToDecimal(reader.GetDouble(13));
                            var vatValue = Convert.ToDecimal(reader.GetDouble(12));
                            var grossValue = Convert.ToDecimal(reader.GetDouble(11));
                            //var vatRate = grossValue / netValue;
                            invoice.Rows.Add(new VatInvoiceRow
                            {
                                NetValue = netValue,
                                VatValue = vatValue,
                                GrossValue = grossValue,
                                VatRate = "23.00" //Math.Round(((grossValue / netValue - 1) * 100), 0).ToString()
                            });
                            if (invoice.Rows.First().VatValue == 0)
                                invoice.Rows.First().VatRate = "np";
                        }

                        invoiceNumber = reader.GetString(9);
                    }
                }
            }

            return invoices;
        }

        public static List<VatInvoice> ReadMexInvoices(string filePath)
        {
            var invoices = new List<VatInvoice>();

            using (var stream = File.Open(filePath, FileMode.Open, FileAccess.Read))
            {
                using (var reader = ExcelReaderFactory.CreateReader(stream))
                {
                    var result = reader.AsDataSet(
                            new ExcelDataSetConfiguration
                            {
                                ConfigureDataTable = tableReader => new ExcelDataTableConfiguration
                                {
                                    UseHeaderRow = true
                                }
                            })
                        .Tables[0];
                    foreach (DataRow row in result.Rows)
                    {
                        if (row["FaLp"].ToString() == "1")
                        {
                            var invoice = new VatInvoice
                            {
                                Issuer = row["BazaWystawcy"].ToString(),
                                BuyerTaxNumber = row["NIP"].ToString(),
                                NumberSeries = row["seria"].ToString(),
                                InvoiceDate = Convert.ToDateTime(row["DataWystawienia"]),
                                OperationDate = Convert.ToDateTime(row["DataSprzedaży"]),
                                PaymentDate = Convert.ToDateTime(row["Termin"]),
                                PaymentForm = row["Forma"].ToString(),
                                Atr1Kod = row["Atrybut"].ToString()
                            };
                            invoice.Rows.Add(new VatInvoiceRow
                            {
                                Code = row["KodTowaru"].ToString(),
                                Name = row["OpisDokumentuTrescPozycjiNaFakturze"].ToString(),
                                Amount = Convert.ToDecimal(row["Ilość"]),
                                VatRate = (Convert.ToDecimal(row["Stawka"]) * 100).ToString(CultureInfo.InvariantCulture),
                                NetValue = Convert.ToDecimal(row["Netto"])
                            });
                            invoices.Add(invoice);
                        }
                        else
                        {
                            invoices.LastOrDefault()?.Rows.Add(new VatInvoiceRow
                            {
                                Code = row["KodTowaru"].ToString(),
                                Name = row["OpisDokumentuTrescPozycjiNaFakturze"].ToString(),
                                Amount = Convert.ToDecimal(row["Ilość"]),
                                VatRate = (Convert.ToDecimal(row["Stawka"]) * 100).ToString(CultureInfo.InvariantCulture),
                                NetValue = Convert.ToDecimal(row["Netto"])
                            });
                        }
                    }
                }
            }

            return invoices;
        }
    }
}
