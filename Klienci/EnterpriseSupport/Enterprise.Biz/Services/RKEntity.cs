﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using LinqToExcel;
using LinqToExcel.Attributes;
namespace Enterprise.Biz.Services
{
    public class RKEntity
    {
        public class ConnexionExcel
        {
            public string _pathExcelFile;
            public ExcelQueryFactory _urlConnexion;
            public ConnexionExcel(string path)
            {
                this._pathExcelFile = path;
                this._urlConnexion = new ExcelQueryFactory(_pathExcelFile);
            }
            public string PathExcelFile
            {
                get
                {
                    return _pathExcelFile;
                }
            }
            public ExcelQueryFactory UrlConnexion
            {
                get
                {
                    return _urlConnexion;
                }
            }
        }
        public class Reports
        {



            public string Data { get; set; }
            [ExcelColumn("utarg z paragonu")]
            public string utarg1 { get; set; }
            public string fa { get; set; }
            [ExcelColumn("utarg netto")]

            public string netto { get; set; }
            public string karty { get; set; }
            public string portal1 { get; set; }
            public string portal2 { get; set; }
            public string portal3 { get; set; }
            public string portal4 { get; set; }
            public string portal5 { get; set; }
            public string portal6 { get; set; }
            public string portal7 { get; set; }
            public string portal8 { get; set; }
            public string portal9 { get; set; }
            public string utarg { get; set; }
        }


        public class BO
        {
            //  Lp.
            [ExcelColumn("Lp.")]
            public int Lp { get; set; }
            public string Data { get; set; }
            [ExcelColumn("Miesiąc")]
            public string Miesiac { get; set; }
            [ExcelColumn("Nr dowodu")]
            // Nr dowodu
            public string Dowod { get; set; }
            [ExcelColumn("Data wpisu")]
            public string Datawpisu { get; set; }
            [ExcelColumn("Operacja gospodarcza")]
            public string Operacja_gospodarcza { get; set; }
            public string Symbol { get; set; }
            public string Suma { get; set; }
            [ExcelColumn("nr konta optima")]
            public string nrkontaoptima { get; set; }
            public string Wn { get; set; }
            public string Ma { get; set; }

            public string Kolumna1 { get; set; }
            public string Kolumna2 { get; set; }
            public string Kolumna3 { get; set; }
            public string Kolumna4 { get; set; }
            public string Kolumna5 { get; set; }
            public string Kolumna6 { get; set; }
            public string Kolumna7 { get; set; }
            public string Kolumna8 { get; set; }
            public string Kolumna9 { get; set; }
            public string Kolumna10 { get; set; }
            public string Kolumna11 { get; set; }
            public string Kolumna12 { get; set; }

        }
    }
}
