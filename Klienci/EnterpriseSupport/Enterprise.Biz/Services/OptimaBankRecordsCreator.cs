﻿using CDNBase;
using NLog;
using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Runtime.InteropServices;
using System.Windows;
using OptimaImporter.Biz.Model;

//using Optima.Common.Exceptions;

namespace Enterprise.Biz.Services
{
    public class OptimaBankRecordsCreator
    {
        public int Counter { get; set; }
        public event Action<int> BankRecordImported;

        public void CreateBankRecords(ILogin login, List<Payment> paymentsList, Register selectedRegister)
        {
            var successfullyLoadedRecordsCounter = 0;
            var recordsFailCounter = 0;
            foreach (var payment in paymentsList)
            {
                var session = new AdoSession();
                Counter++;
                try
                {
                    var register = (OP_KASBOLib.Rachunek)session.CreateObject("CDN.Rachunki")
                        .Item("BRa_Symbol = '" + selectedRegister.Symbol + "'");
                    var kw = (CDNHeal.DefinicjaDokumentu)session.CreateObject("CDN.DefinicjeDokumentow")
                        .Item("DDf_Symbol='KW'");
                    var kp = (CDNHeal.DefinicjaDokumentu)session.CreateObject("CDN.DefinicjeDokumentow")
                        .Item("DDf_Symbol='KP'");
                    var record = session.CreateObject("CDN.ZapisyKB").AddNew();
                    var stronglyTypedRecord = record as OP_KASBOLib.ZapisKB;

                    if (stronglyTypedRecord != null)
                    {
                        try
                        {
                            stronglyTypedRecord.Rachunek = register;
                            Debug.WriteLine($"Rejestr {stronglyTypedRecord.Rachunek.Akronim}");
                            if (payment.TransactionAmountWithSigh < 0)
                            {
                                //stronglyTypedRecord.Typ = 1;
                                stronglyTypedRecord.DataDok = Convert.ToDateTime(payment.TransactionDate);
                                Debug.WriteLine($"DataDok {stronglyTypedRecord.DataDok}");

                                if (register.KursHistorycznyMW == 1)
                                {
                                    stronglyTypedRecord.MagazynWalut = 0;
                                }
                                stronglyTypedRecord.Kwota = payment.TransactionAmountWithSigh * (-1);
                                stronglyTypedRecord.DefinicjaDokumentu = kw;
                                Debug.WriteLine($"Data {stronglyTypedRecord.DataDok}, Kwota {stronglyTypedRecord.Kwota}");
                            }
                            if (payment.TransactionAmountWithSigh > 0)
                            {
                                //stronglyTypedRecord.Typ = 2;
                                stronglyTypedRecord.DataDok =
                                    Convert.ToDateTime(payment.TransactionDate);
                                stronglyTypedRecord.Kwota = payment.TransactionAmountWithSigh;
                                stronglyTypedRecord.DefinicjaDokumentu = kp;
                            }
                            //decimal.TryParse(payment.TransactionAmount.Replace("\"", ""), out decimal amount);
                            //Debug.WriteLine(stronglyTypedRecord.TypKursuWaluty.GetType());

                            stronglyTypedRecord.Opis = payment.TransactionDescription;

                            var contractorData = new[] { 0 };
                            CDNHeal.IPodmiot contractor;
                            if (contractorData[0] != 0)
                            {
                                if (contractorData[1] == 3)
                                {
                                    try
                                    {
                                        stronglyTypedRecord.PodTypZrodla = 3;
                                        contractor = (CDNHeal.IPodmiot)session.CreateObject("CDN.PodmiotyView").Item("Pod_PodID = " + contractorData[0]);
                                    }
                                    catch (Exception)
                                    {
                                        contractor = (CDNHeal.IKontrahent)session.CreateObject("CDN.Kontrahenci").Item("Knt_kntID = 1");
                                    }
                                }
                                else
                                {
                                    try
                                    {
                                        contractor = (CDNHeal.IKontrahent)session.CreateObject("CDN.Kontrahenci").Item("Knt_kntID = " + contractorData[0]);
                                    }
                                    catch (Exception)
                                    {
                                        contractor = (CDNHeal.IKontrahent)session.CreateObject("CDN.Kontrahenci").Item("Knt_kntID = 1");

                                    }
                                }
                                stronglyTypedRecord.Podmiot = contractor;
                            }
                            else
                            {
                                contractor = (CDNHeal.IKontrahent)session.CreateObject("CDN.Kontrahenci").Item("Knt_kntID = 1");
                                stronglyTypedRecord.Podmiot = contractor;
                                //if (payment.ContractorBankAccount.Replace(" ", "").Length == 26)
                                if (!string.IsNullOrWhiteSpace(payment.ContractorBankAccount)) stronglyTypedRecord.RachunekNr = payment.ContractorBankAccount.Replace(" ", "");
                            }
                            Debug.WriteLine($"Podmiot {stronglyTypedRecord.Podmiot.Akronim}");

                            if (!string.IsNullOrWhiteSpace(payment.ContractorName))
                            {
                                stronglyTypedRecord.Nazwa1 = payment.ContractorName.Substring(0, payment.ContractorName.Length).Trim();
                                if (payment.ContractorName.Length > 50)
                                    stronglyTypedRecord.Nazwa2 = payment.ContractorName.Substring(50, payment.ContractorName.Length - 50).Trim();
                                if (payment.ContractorName.Length > 100)
                                    stronglyTypedRecord.Nazwa3 = payment.ContractorName.Substring(100, payment.ContractorName.Length - 100).Trim();
                            }
                            Debug.WriteLine($"Nazwa {stronglyTypedRecord.Nazwa1}");
                            Debug.WriteLine($"DataDok {stronglyTypedRecord.DataDok}");
                            Debug.WriteLine($"Kwota {stronglyTypedRecord.Kwota}");
                            stronglyTypedRecord.Importowany = 1;

                            session.Save();

                            if (payment.TransactionAmountWithSigh < 0 && register.KursHistorycznyMW == 1)
                            {
                                OP_KASBOLib.ZapisKB stronglyTypedRecord2 = session.CreateObject("CDN.ZapisyKB").Item("bzp_bzpid = " + stronglyTypedRecord.ID);
                                stronglyTypedRecord2.MagazynWalut = 1;
                                stronglyTypedRecord2.WgKursuMW = 2;
                                stronglyTypedRecord2.RozliczajMWPrzyZapisie = 1;
                                session.Save();
                            }
                            successfullyLoadedRecordsCounter++;
                        }
                        //catch (OptimaMessageException omex)
                        //{
                        //    LogManager.GetCurrentClassLogger()
                        //        .Info(
                        //            $"Błąd tworzenia kp/kw z datą: {stronglyTypedRecord.DataDok} na kwotę {stronglyTypedRecord.Kwota} - {omex}");
                        //}
                        catch (COMException)
                        {
                            var session1 = new AdoSession();
                            var reports = (OP_KASBOLib.RaportyKB)session1.CreateObject("CDN.RaportyKB");
                            var report = (OP_KASBOLib.RaportKB)reports.AddNew();
                            report.Rachunek = register;

                            if (register.BRpOkres == 4)
                            {
                                report.DataOtw = new DateTime(
                                    Convert.ToInt32(payment.TransactionDate.Substring(0, 4))
                                    , Convert.ToInt32(payment.TransactionDate.Substring(5, 2))
                                    , 01);
                                report.DataZam = report.DataOtw.AddMonths(1).AddDays(-1);
                            }
                            else if (register.BRpOkres == 1)
                            {
                                report.DataOtw = Convert.ToDateTime(payment.TransactionDate);
                                report.DataZam = report.DataOtw;
                            }
                            else
                            {
                                MessageBox.Show("Automatyczne zakładanie wyciągów jest obsługiwane tylko dla dziennych lub miesięcznych" +
                                    " raportów kasowo-bankowych.", "Informacja", MessageBoxButton.OK, MessageBoxImage.Stop);
                                break;
                            }
                            report.Zamkniety = 0;
                            try
                            {
                                session1.Save();
                            }
                            //catch (OptimaMessageException omex1)
                            //{
                            //    LogManager.GetCurrentClassLogger().Info(
                            //        $"Błąd tworzenia zapisu KB nr {Counter} lub " +
                            //        $"BŁĄD ZAKŁADANIA RAPORTU KB z datami od {report.DataOtw} do {report.DataZam}: {omex1}");
                            //}
                            catch (COMException cex1)
                            {
                                LogManager.GetCurrentClassLogger().Info(
                                    $"Błąd tworzenia zapisu KB nr {Counter} lub " +
                                    $"BŁĄD ZAKŁADANIA RAPORTU KB z datami od {report.DataOtw} do {report.DataZam}: {cex1}");
                            }
                            catch (Exception ex1)
                            {
                                LogManager.GetCurrentClassLogger().Info(
                                    $"Błąd tworzenia zapisu KB nr {Counter} lub " +
                                    $"BŁĄD ZAKŁADANIA RAPORTU KB z datami od {report.DataOtw} do {report.DataZam}: {ex1}");
                            }
                            try
                            {
                                session.Save();
                                successfullyLoadedRecordsCounter++;
                            }
                            catch (Exception ex2)
                            {
                                LogManager.GetCurrentClassLogger()
                                    .Info(
                                        $"Błąd tworzenia kp/kw z datą: {stronglyTypedRecord.DataDok} na kwotę {stronglyTypedRecord.Kwota} - {ex2}");
                                recordsFailCounter++;
                            }
                        }
                        BankRecordImported?.Invoke(Counter);
                    }
                }
                catch (Exception ex)
                {
                    LogManager.GetCurrentClassLogger()
                        .Info($"Wystąpił błąd przy imporcie zapisu nr: {Counter} - {ex}");
                }
            }
        }
    }
}
