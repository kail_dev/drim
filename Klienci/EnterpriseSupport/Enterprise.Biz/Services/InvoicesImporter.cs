﻿using CDNBase;
using CDNHeal;
using CDNHlmn;
using CDNTwrb1;
using NLog;
using OP_KASBOLib;
using OptimaImporter.Biz.Model;
using OptimaImporter.Biz.Optima;
using System;
using System.Collections.Generic;

namespace Enterprise.Biz.Services
{
    public class InvoicesImporter
    {
        private Logger Logger => LogManager.GetCurrentClassLogger();

        private DokumentHaMag invoice = null;
        public event Action<string> Created;
        public event Action CreateFailed;

        public void Create(IEnumerable<VatInvoice> inputInvoices)
        {
            var loggedIn = false;
            var bazaWystawcy = "";
            foreach (var inputInvoice in inputInvoices)
            {
                if (bazaWystawcy != inputInvoice.Issuer)
                {
                    if (!string.IsNullOrWhiteSpace(bazaWystawcy)) OptimaLogging.LogOut();
                    loggedIn = OptimaLogging.LogIn(inputInvoice.Issuer);
                }
                bazaWystawcy = inputInvoice.Issuer;

                var session = new AdoSession();

                invoice = CreateHeader(inputInvoice, session);
                SetDocumentNumber(inputInvoice, session, invoice);
                invoice.Podmiot = TrySetContractor(inputInvoice, session);
                SetUnrecognizedContractorName(invoice, inputInvoice);
                TrySetPayment(inputInvoice, session, invoice);
                TryAddAtr(inputInvoice, session, invoice);
                AddElements(inputInvoice, invoice);
                TrySave(session, inputInvoice);
            }
            if (loggedIn) OptimaLogging.LogOut();
        }


        private DokumentHaMag CreateHeader(VatInvoice inputInvoice, IAdoSession session)
        {
            var invoice = (DokumentHaMag)session.CreateObject("CDN.DokumentyHaMag").AddNew();
            invoice.Rodzaj = 302000;
            invoice.TypDokumentu = 302;
            invoice.Bufor = 1;
            invoice.DataWys = inputInvoice.InvoiceDate;
            invoice.DataDok = inputInvoice.OperationDate;

            return invoice;
        }

        private void SetDocumentNumber(VatInvoice inputInvoice, IAdoSession session, DokumentHaMag invoice)
        {
            var docDefinition = (DefinicjaDokumentu)session.CreateObject("CDN.DefinicjeDokumentow")
                .Item("DDf_Symbol = 'FS'");
            SetProperty(invoice.Numerator, "DefinicjaDokumentu", docDefinition);
            var numerator = (INumerator)invoice.Numerator;
            numerator.Rejestr = inputInvoice.NumberSeries;
        }

        private IPodmiot TrySetContractor(VatInvoice inputInvoice, IAdoSession session)
        {
            //wybór kontrahenta po  nipie
            try
            {
                var contractor = (IPodmiot)session.CreateObject("CDN.Kontrahenci").Item($"knt_nip = '{inputInvoice.BuyerTaxNumber}'");
                return contractor;
            }
            catch (Exception)
            {
            }

            return (IPodmiot)session.CreateObject("CDN.Kontrahenci").Item("knt_kntid = 1");
        }

        private void SetUnrecognizedContractorName(DokumentHaMag invoice, VatInvoice inputInvoice)
        {
            if (invoice.Podmiot.Akronim == "!NIEOKREŚLONY!")
            {
                invoice.PodNipE = inputInvoice.BuyerTaxNumber;
            }
        }

        private void TrySetPayment(VatInvoice inputInvoice, IAdoSession session, DokumentHaMag invoice)
        {
            var paymentForms = (ICollection)session.CreateObject("CDN.FormyPlatnosci");
            FormaPlatnosci paymentForm = null;

            try
            {
                paymentForm = (FormaPlatnosci)paymentForms[$"Fpl_Nazwa='{inputInvoice.PaymentForm}'"];
                invoice.FormaPlatnosci = paymentForm;
                invoice.Termin = inputInvoice.PaymentDate;
            }
            catch (Exception ex)
            {
                Logger.Error($"Nie ondaleziono formy płatności dla kontrahenta o NIP {inputInvoice.BuyerTaxNumber}, błąd: {ex}.");
                paymentForm = (FormaPlatnosci)paymentForms["Fpl_FplId=3"];
                invoice.FormaPlatnosci = paymentForm;
                invoice.Termin = inputInvoice.PaymentDate;
            }
        }

        private void TryAddAtr(VatInvoice inputInvoice, IAdoSession session, DokumentHaMag invoice)
        {
            if (string.IsNullOrWhiteSpace(inputInvoice.Atr1Kod)) return;

            var atr = (IDokAtrybut)invoice.Atrybuty.AddNew();
            atr.Kod = inputInvoice.Atr1Kod;
        }

        private void AddElements(VatInvoice inputInvoice, DokumentHaMag invoice)
        {
            var elements = invoice.Elementy;
            foreach (var inputInvoiceRow in inputInvoice.Rows)
            {
                var element = (IElementHaMag)elements.AddNew();
                element.TowarKod = inputInvoiceRow.Code;
                element.Ilosc = (double)inputInvoiceRow.Amount;
                element.WartoscNetto = inputInvoiceRow.NetValue;
                element.UstawNazweTowaru(inputInvoiceRow.Name);
            }
        }

        private void TrySave(AdoSession session, VatInvoice inputInvoice)
        {
            try
            {
                session.Save();
                Logger.Info($"Dokument numer: {invoice.NumerPelny} na kontrahenta {inputInvoice.BuyerTaxNumber} utworzony.");
                Created?.Invoke(invoice.NumerPelny);
            }
            catch (Exception ex)
            {
                Logger.Error($"Dokument na kontrahenta {inputInvoice.BuyerTaxNumber} nie utworzony. {ex}");
                CreateFailed?.Invoke();
            }
        }

        //wymagane do ustawienia numeracji dokumentów
        protected static void SetProperty(object o, string name, object value)  //wymagane do zmiany numeracji
        {
            o?.GetType().InvokeMember(name, System.Reflection.BindingFlags.SetProperty, null, o, new[] { value });
        }
    }
}
