﻿using System;
using System.Collections.Generic;
using OptimaImporter.Biz.Model;
using OptimaImporter.Data.Enum;

namespace Enterprise.Biz.Model
{
    public class ImportFormat
    {
        public Func<string, string, List<VatInvoice>> SourceFileRead { get; set; }
        public VatRegisterType VatRegisterType { get; set; }
        public string InputAdditionalParameter { get; set; }

    }
}
