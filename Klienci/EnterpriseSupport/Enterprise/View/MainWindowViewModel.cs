﻿using Enterprise.Biz.Services;
using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Linq;
using OptimaImporter.Biz.Model;
using OptimaImporter.Biz.Optima;
using OptimaImporter.Data.Config;
using OptimaImporter.Data.Enum;
using OptimaImporter.Data.SQL;
using OptimaImporter.Wpf.ViewModel;

namespace Enterprise.View
{
    public class MainWindowViewModel : MainWindowViewModelBase
    {
        public string ConnectionString { get; set; }

        private IEnumerable<string> _purchaseVatRegisterList;
        public IEnumerable<string> PurchaseVatRegisterList
        {
            get => _purchaseVatRegisterList;
            set => SetProperty(ref _purchaseVatRegisterList, value);
        }

        public string SelectedPurchaseVatRegister { get; set; }


        private IEnumerable<string> _salesVatRegisterList;
        public IEnumerable<string> SalesVatRegisterList
        {
            get => _salesVatRegisterList;
            set => SetProperty(ref _salesVatRegisterList, value);
        }

        public string SelectedSalesVatRegister { get; set; }

        public IconViewModel Icons { get; set; }

        public MainWindowViewModel()
        {
            Icons = new IconViewModel();
        }

        public void ImportVatRegister(IList<VatInvoice> invoices, VatRegisterType vatRegisterType)
        {
            if (invoices == null || !invoices.Any())
            {
                StatusBarText = "Nie udało się odczytać listy dokumentów do importu.";
                return;
            }

            var loggedIn = OptimaLogging.LogIn();
            if (!loggedIn)
            {
                StatusBarText = "Nie udało się zalogować do optimy. Import niemożliwy.";
                return;
            }

            var stopWatch = Stopwatch.StartNew();
            var inputInvoicesCounter = 0;
            var createdInvoicesCounter = 0;
            var failedInvoicesCounter = 0;
            var invoicesFromFileCount = invoices.Count;
            IProgress<Tuple<int, int>> progress = new Progress<Tuple<int, int>>(t =>
            {
                var percent = inputInvoicesCounter / (decimal)invoicesFromFileCount * 100;
                StatusBarText = $"Wczytywanie... ({percent:0}%), czas: {stopWatch.Elapsed:hh\\:mm\\:ss}. Wczytanych poprawnie: {t.Item1}/{invoicesFromFileCount}, błędy: {t.Item2}/{invoicesFromFileCount}.";
            });

            var optimaCreator = new VatRegisterCreator();
            optimaCreator.Created += () =>
            {
                inputInvoicesCounter++;
                createdInvoicesCounter++;
                progress.Report(Tuple.Create(createdInvoicesCounter, failedInvoicesCounter));
            };
            optimaCreator.CreateFailed += () =>
            {
                inputInvoicesCounter++;
                failedInvoicesCounter++;
                progress.Report(Tuple.Create(createdInvoicesCounter, failedInvoicesCounter));
            };

            var vatRegisterName = vatRegisterType == VatRegisterType.Purchase ? SelectedPurchaseVatRegister : SelectedSalesVatRegister;
            optimaCreator.Create(invoices, vatRegisterType, vatRegisterName);

            OptimaLogging.LogOut();
            StatusBarText += " Robota zrobiona!!!";
        }

        public void InitializeVatRegisters()
        {
            ConnectionString = ApplicationConfig.Config.OptimaDataBaseConnectionString;
            PurchaseVatRegisterList = OptimaSqlReader.GetOptimaVatRegisters(VatRegisterType.Purchase);
            SalesVatRegisterList = OptimaSqlReader.GetOptimaVatRegisters(VatRegisterType.Sales);
        }
    }
}
