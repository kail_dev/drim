﻿using Enterprise.Biz.Services;
using Microsoft.Win32;
using NLog;
using OptimaImporter.Biz.Model;
using OptimaImporter.Biz.Optima;
using OptimaImporter.Data.Config;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using Telerik.Windows.Controls;
using ExcelDataReader;
using LinqToExcel;
using static Enterprise.Biz.Services.RKEntity;
//
//72H88Tyk:-bk

namespace Enterprise.View
{
    public partial class MainWindow
    {
        private static Logger Logger => LogManager.GetCurrentClassLogger();
        private readonly MainWindowViewModel _viewModel;

        public MainWindow()
        {
            StyleManager.ApplicationTheme = new Office_BlueTheme();
            InitializeComponent();

            _viewModel = new MainWindowViewModel();
            DataContext = _viewModel;
        }

        private void dbComboBox_SelectionChanged(object sender, SelectionChangedEventArgs e)
        {
            try
            {
                _viewModel.InitializeVatRegisters();
            }
            catch (Exception ex)
            {
                Logger.Error($"Błąd odczytu listy baz danych. {ex}");
            }
        }

        private async void ImportVat_OnClick(object sender, RoutedEventArgs e)
        {
            if (!(sender is RadRibbonButton button)) return;
            var buttonName = button.Name;

            ApplicationConfig.Config.OptimaPassword = PasswordTextBox.Password;

            var ofd = new OpenFileDialog();
            ofd.ShowDialog();

            if (string.IsNullOrWhiteSpace(ofd.FileName)) return;

            _viewModel.StatusBarText = "Uwaga startuję...";

            var importFormat = ImportFormatSelector.Select(buttonName);
            List<VatInvoice> invoices = null;
            try
            {
                invoices = importFormat.SourceFileRead.Invoke(ofd.FileName, importFormat.InputAdditionalParameter);
            }
            catch (IOException ex)
            {
                Logger.Error($"Nie udało się odczytać pliku z danymi. Sprawdź czy plik nie jest otwarty w innym pogramie. {ex}");
                RadWindow.Alert("Nie udało się odczytać pliku z danymi. Sprawdź czy plik nie jest otwarty w innym pogramie.");
            }

            if (invoices == null || !invoices.Any()) return;

            await Task.Run(() => _viewModel.ImportVatRegister(invoices, importFormat.VatRegisterType));
        }

        private void KasaKBButton_OnClick(object sender, RoutedEventArgs e)
        {
            var ofd = new OpenFileDialog();

            ofd.ShowDialog();
            if (!string.IsNullOrWhiteSpace(ofd.FileName))
            {
                try
                {


                    var payments = SourceFileReader.ReadKasaKbPayments(ofd.FileName).ToList();

                    //próby Ani
                    OptimaLogging.LogIn();
                    var optimaCreator = new OptimaBankRecordImporter();
                    optimaCreator.Create(payments, "TEST");
                    OptimaLogging.LogOut();

                    //var impoter = new OptimaBankRecordsCreator();
                    //impoter.BankRecordImported += x =>
                    //{
                    //    Dispatcher?.Invoke(() =>
                    //    {
                    //        KasaKbButton.Text = "Import kasy" + Environment.NewLine + x;
                    //    });
                    //};
                    //Task.Run(() =>
                    //{
                    //    //impoter.CreateBankRecords(_iLogin, payments, new Register { Symbol = "KASA_" });
                    //});
                }
                catch (Exception ex)
                {
                    LogManager.GetCurrentClassLogger().Info($"Błąd odczytu pliku źródłowego i importu do Otpimy: {ex}");
                }
            }

        }

        private void MexBoZButton_OnClickButton_OnClick(object sender, RoutedEventArgs e)
        {
            var ofd = new OpenFileDialog();

            ofd.ShowDialog();
            if (!string.IsNullOrWhiteSpace(ofd.FileName))
            {
                //var invoices = SourceFileReader.ReadMexBo(_ofd.FileName, 1);
                //LoadDataToOptima(invoices, VatRegisterType.Purchase, MexBoZButton);
            }
        }
        private void MexBoSButton_OnClickButton_OnClick(object sender, RoutedEventArgs e)
        {
            var ofd = new OpenFileDialog();

            ofd.ShowDialog();
            if (!string.IsNullOrWhiteSpace(ofd.FileName))
            {
                //var invoices = SourceFileReader.ReadMexBo(_ofd.FileName, 2);
                //LoadDataToOptima(invoices, VatRegisterType.Sales, MexBoSButton);
            }
        }





        private async void MexInvoicesButton_OnClick(object sender, RoutedEventArgs e)
        {
            var ofd = new OpenFileDialog();
            ofd.ShowDialog();
            if (string.IsNullOrWhiteSpace(ofd.FileName)) return;

            IList<VatInvoice> invoices = null;

            try
            {
                invoices = SourceFileReader.ReadMexInvoices(ofd.FileName);
                Logger.Info($"Odczytałem {invoices.Count} faktur z pliku.");
            }
            catch (Exception ex)
            {
                Logger.Error($"Nie udało się odczytać pliku z danymi. Sprawdź czy plik nie jest otwarty w innym pogramie. {ex}");
                RadWindow.Alert("Nie udało się odczytać pliku z danymi. Sprawdź czy plik nie jest otwarty w innym pogramie.");
            }

            await Task.Run(() =>
            {
                var counter = 0;
                var importer = new InvoicesImporter();
                importer.Created += x =>
                {
                    counter++;
                    Dispatcher?.Invoke(() =>
                    {
                        MexInvoicesButton.Text = $"Mex Faktury: {counter}" + Environment.NewLine + x;
                    });
                };
                importer.Create(invoices);
            });
        }

        private void MexRKS_Click(object sender, RoutedEventArgs e)
        {
            var ofd = new OpenFileDialog();
            ofd.ShowDialog();
            string pathxls = @"c:\dev\z.xls";
            // System.Text.Encoding.RegisterProvider(System.Text.CodePagesEncodingProvider.Instance);

            ConnexionExcel ConxObject = new ConnexionExcel(ofd.FileName);

            var query1 = from a in ConxObject.UrlConnexion.Worksheet<Reports>("Kas")
                         select a;
            foreach (var result in query1)
            {

                //  optima log = new optima();
                // log.LogowanieAutomatyczne();
                //    log.Payments(result.Data, result.utarg);
                OptimaLogging.LogIn();
                string kasa = result.karty;

            }



        }
    }
}
