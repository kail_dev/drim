﻿using Enterprise.View;
using System;
using System.Windows;

namespace Enterprise
{
    /// <summary>
    /// Interaction logic for App.xaml
    /// </summary>
    public partial class App
    {
        protected override void OnStartup(StartupEventArgs e)
        {
            base.OnStartup(e);
            NLog.LogManager.GetCurrentClassLogger().Info("Start aplikacji");
            DispatcherUnhandledException += (o, ea) =>
            {
                NLog.LogManager.GetCurrentClassLogger().Info(ea.Exception.ToString());
                ea.Handled = true;
            };

            try
            {
                var window = new MainWindow();
                window.Show();
            }
            catch (Exception)
            {
                var window = new MainWindow();
                window.Show();
            }
        }
    }
}
