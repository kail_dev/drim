﻿using Enterprise.Biz.Services;
using Enterprise.View;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using System;
using System.Linq;
using OptimaImporter.Biz.Optima;
using OptimaImporter.Data.Config;

namespace Enterprise.Test.Services
{
    [TestClass]
    public class VatRegisterImporterTests
    {
        [TestMethod]
        public void ImportSuchemSalesToOptima_Test()
        {
            var filePath = @"C:\Users\anye_\Desktop\Things\Rachunki z NIP lokal 001 luty 2020.xlsx";
            //OptimaLogging.LogIn("ADMIN", "a", "TWFM");
            ApplicationConfig.Config.OptimaUser = "ADMIN";
            ApplicationConfig.Config.OptimaPassword = "a";
            ApplicationConfig.Config.OptimaDatabase = "TWFM";

            if (Environment.MachineName.StartsWith("SZERYF"))
            {
                filePath = @"C:\Users\marci\OneDrive - ITDF\KlienciPriv\ES\Surchem\sprzedaż 03 2020 - Copy.xlsx";
                ApplicationConfig.Config.OptimaUser = "ADMIN";
                ApplicationConfig.Config.OptimaPassword = "";
                ApplicationConfig.Config.OptimaDatabase = "Test";
            }

            var importFormat = ImportFormatSelector.Select("SurchemButton");
            var invoices = importFormat.SourceFileRead.Invoke(filePath, "").ToList();
            var vm = new MainWindowViewModel();
            vm.ImportVatRegister(invoices, importFormat.VatRegisterType);
        }


        [TestMethod]
        public void NewBankRecordsImportTest()
        {
            var filePath = @"C:\_C\ITDF\Marcin Malczewski - Szeran\Klienci\ES\import RK\10.2019.xlsx";
            ApplicationConfig.Config.OptimaUser = "ADMIN";
            ApplicationConfig.Config.OptimaPassword = "a";
            ApplicationConfig.Config.OptimaDatabase = "TWFM";

            if (Environment.MachineName.StartsWith("SZERYF"))
            {
                filePath = @"C:\Users\marci\OneDrive - ITDF\Klienci\Lachesis\import RK\Kasa TWFM - 202001.xlsx";
                ApplicationConfig.Config.OptimaUser = "ADMIN";
                ApplicationConfig.Config.OptimaPassword = "";
                ApplicationConfig.Config.OptimaDatabase = "Test";
            }

            var payments = SourceFileReader.ReadKasaKbPayments(filePath).ToList().Take(10);
            OptimaLogging.LogIn();
            var oi = new OptimaBankRecordImporter();
            oi.Created += (s) => { Console.WriteLine(s + DateTime.Now); };
            oi.Create(payments, "TEST");
            OptimaLogging.LogOut();
        }
    }
}
