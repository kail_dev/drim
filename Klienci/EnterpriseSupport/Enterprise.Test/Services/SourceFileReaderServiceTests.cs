﻿using Enterprise.Biz.Services;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using OptimaImporter.Biz.Model;
using System;
using System.Collections.Generic;
using System.Linq;

namespace Enterprise.Test.Services
{
    [TestClass]
    public class SourceFileReaderServiceTests
    {
        [TestMethod]
        public void ReadJpkFaInvoices_Test()
        {
            var filePath = @"W:\Klienci\ES\JPK\jpk_fa_01.08.2018-31.08.2018.xml";
            var invoices = SourceFileReader.ReadJpkFaInvoices(filePath);

            Assert.IsTrue(invoices.Any());
        }

        [TestMethod]
        public void ReadSubiektInvoices_Test()
        {
            var filePath = @"C:\Users\marci\OneDrive - ITDF\KlienciPriv\ES\Renapol\Mag główny 07-2020.epp";
            var invoices = SourceFileReader.ReadSubiektInvoices(filePath, "FS");

            Assert.AreEqual(58, invoices.Count);
        }

        [TestMethod]
        public void ReadSubiektMobisoftInvoices_Test()
        {
            var filePath = @"C:\Users\marci\OneDrive\KlienciPriv\ES\Mobisoft\2.2019 mobisoft EDI.epp";
            var invoices = SourceFileReader.ReadSubiektInvoices(filePath, "FZ");

            Assert.AreEqual(350, invoices.Count);
        }

        [TestMethod]
        public void ReadKolagen2Invoices_Test()
        {
            var filePath = @"W:\Klienci\ES\Kolagen\06.xlsx";
            var invoices = SourceFileReader.ReadKolagen2Purchases(filePath, "Sheet");

            Assert.IsTrue(invoices.Any());
        }

        [TestMethod]
        public void ReadUberSalesInvoices_Test()
        {
            var filePath = @"C:\Users\marci\OneDrive\KlienciPriv\ES\Uberkwiecień_2019.xls";
            var invoices = SourceFileReader.ReadUberSalesInvoices(filePath, "Sheet1");

            var testInvoice = new VatInvoice
            {
                Number = "UTNTZGXR-03-2019-0012329",
                BuyerName = "Izabella Karkowska",
                InvoiceDate = new DateTime(2019, 03, 01),
                OperationDate = new DateTime(2019, 03, 01),
                PaymentDate = new DateTime(2019, 03, 01),
                PostalCode = "01-001",
                City = "Warszawa",
                BuyerAddress = "Skierniewicka 28",
                BuyerTaxNumber = "",
                Rows = new List<VatInvoiceRow>
                {
                    new VatInvoiceRow
                    {
                        NetValue = 17.98m,
                        VatRate = "8",
                        GrossValue = 19.42m
                    }
                }
            };

            var firstInvoice = invoices.First();

            Assert.AreEqual(5388, invoices.Count());
            Assert.AreEqual(testInvoice.Number, firstInvoice.Number);
            Assert.AreEqual(testInvoice.BuyerName, firstInvoice.BuyerName);
            Assert.AreEqual(testInvoice.InvoiceDate, firstInvoice.InvoiceDate);
            Assert.AreEqual(testInvoice.OperationDate, firstInvoice.OperationDate);
            Assert.AreEqual(testInvoice.PaymentDate, firstInvoice.PaymentDate);
            Assert.AreEqual(testInvoice.PostalCode, firstInvoice.PostalCode);
            Assert.AreEqual(testInvoice.City, firstInvoice.City);
            Assert.AreEqual(testInvoice.BuyerAddress, firstInvoice.BuyerAddress);
            //Assert.AreEqual(testInvoice.BuyerTaxNumber, firstInvoice.BuyerTaxNumber);
            Assert.AreEqual(testInvoice.Rows.First().NetValue, firstInvoice.Rows.First().NetValue);
            Assert.AreEqual(testInvoice.Rows.First().VatRate, firstInvoice.Rows.First().VatRate);
            Assert.AreEqual(testInvoice.Rows.First().GrossValue, firstInvoice.Rows.First().GrossValue);
        }

        [TestMethod]
        public void ReadBoltSalesInvoices_Test()
        {
            var filePath = @"C:\Users\marci\OneDrive\KlienciPriv\ES\Bolt - Faktury za przejazdy - Kwiecień 2019.csv";
            var invoices = SourceFileReader.ReadBoltSalesInvoices(filePath);

            var testInvoice = new VatInvoice
            {
                Number = "15541078032594",
                BuyerCode = "!NIEOKREŚLONY!",
                BuyerName = "Bartosz Brykala",
                InvoiceDate = new DateTime(2019, 04, 01),
                OperationDate = new DateTime(2019, 04, 01),
                PaymentDate = new DateTime(2019, 04, 01),
                BuyerAddress = "Łubinowa 4A, Warszawa",
                Rows = new List<VatInvoiceRow>
                {
                    new VatInvoiceRow
                    {
                        NetValue = 34.86m,
                        VatRate = "8",
                        GrossValue = 37.65m
                    }
                }
            };

            var firstInvoice = invoices.First();

            //Assert.AreEqual(3732, invoices.Count);
            Assert.AreEqual(testInvoice.Number, firstInvoice.Number);
            Assert.AreEqual(testInvoice.BuyerCode, firstInvoice.BuyerCode);
            Assert.AreEqual(testInvoice.BuyerName, firstInvoice.BuyerName);
            Assert.AreEqual(testInvoice.InvoiceDate, firstInvoice.InvoiceDate);
            Assert.AreEqual(testInvoice.OperationDate, firstInvoice.OperationDate);
            Assert.AreEqual(testInvoice.PaymentDate, firstInvoice.PaymentDate);
            Assert.AreEqual(testInvoice.BuyerAddress, firstInvoice.BuyerAddress);
            //Assert.AreEqual(testInvoice.BuyerTaxNumber, firstInvoice.BuyerTaxNumber);
            Assert.AreEqual(testInvoice.Rows.First().NetValue, firstInvoice.Rows.First().NetValue);
            Assert.AreEqual(testInvoice.Rows.First().VatRate, firstInvoice.Rows.First().VatRate);
            Assert.AreEqual(testInvoice.Rows.First().GrossValue, firstInvoice.Rows.First().GrossValue);
        }

        [TestMethod]
        public void ReadSurchemInvoices_Test()
        {
            var filePath = @"C:\Users\marci\OneDrive\KlienciPriv\ES\Surchem\sprzedaż maj 2019.xlsx";
            var invoices = SourceFileReader.ReadSurchemSales(filePath);

            var testInvoice = new VatInvoice
            {
                Number = "F001/201905",
                BuyerName = "Podanfol S.A.",
                InvoiceDate = new DateTime(2019, 05, 13),
                OperationDate = new DateTime(2019, 05, 13),
                PaymentDate = new DateTime(2019, 05, 27),
                PostalCode = "64-800",
                City = "Chocież",
                Street = "Podanin",
                BuyerTaxNumber = "7641009978",
                Rows = new List<VatInvoiceRow>
                {
                    new VatInvoiceRow
                    {
                        NetValue = 1208.75m,
                        VatRate = "23",
                        GrossValue = 1486.76m
                    }
                },
                Description = "WZ/12/001/0519"
            };

            var firstInvoice = invoices.First();

            Assert.AreEqual(208, invoices.Count);
            Assert.AreEqual(1261564.00m, invoices.SelectMany(i => i.Rows).Sum(r => r.NetValue));
            Assert.AreEqual(testInvoice.Number, firstInvoice.Number);
            Assert.AreEqual(testInvoice.BuyerName, firstInvoice.BuyerName);
            Assert.AreEqual(testInvoice.InvoiceDate, firstInvoice.InvoiceDate);
            Assert.AreEqual(testInvoice.OperationDate, firstInvoice.OperationDate);
            Assert.AreEqual(testInvoice.PaymentDate, firstInvoice.PaymentDate);
            Assert.AreEqual(testInvoice.PostalCode, firstInvoice.PostalCode);
            Assert.AreEqual(testInvoice.City, firstInvoice.City);
            Assert.AreEqual(testInvoice.Street, firstInvoice.Street);
            Assert.AreEqual(testInvoice.BuyerTaxNumber, firstInvoice.BuyerTaxNumber);
            Assert.AreEqual(testInvoice.Rows.First().NetValue, firstInvoice.Rows.First().NetValue);
            Assert.AreEqual(testInvoice.Description, firstInvoice.Description);
        }

        [TestMethod]
        public void ReadCashRecords_Test()
        {
            var filePath = @"C:\Users\marci\OneDrive - ITDF\KlienciPriv\ES\Katarzyna Budzik - raport kasowy\2.2019.xlsx";
            var cashRecords = SourceFileReader.ReadKasaKbPayments(filePath);

            var testRecord = new Payment
            {
                TransactionDate = "2019-09-02",
                TransactionType = "",
                TransactionDescription = "KW 84/KAS/09/2019, rozliczenie delegacji od 21.08.19 do 28.08.2019",
                ContractorName = "Koczywąs Paweł, Południowa 9D",
                ContractorBankAccount = "",
                TransactionAmount = "1221,59",
                TransactionAmountWithSigh = -1221.59m,
                OurName = "",
                OurBankAccount = ""
            };

            var firstRecord = cashRecords.FirstOrDefault();

            Assert.IsTrue(testRecord.Equals(firstRecord));
        }

        [TestMethod]
        public void ReadMexBo_Test()
        {
            var filePath = @"C:\Users\marci\OneDrive - ITDF\KlienciPriv\ES\MexBO\otwarte Z Mex Sop.xlsx";
            var invoices = SourceFileReader.ReadMexBo(filePath, 1);

            var testInvoicesCount = 263;

            var testInvoice = new VatInvoice
            {
                Number = "07/12/2019/TRAN",
                SellerTaxNumber = "7251967625",
                InvoiceDate = new DateTime(2019, 12, 12),
                PaymentDate = new DateTime(2020, 01, 11),
                Description = "usł. transportu T",
                Rows = new List<VatInvoiceRow>
                {
                    new VatInvoiceRow
                    {
                        GrossValue = 159.90m,
                        NetValue = 130.00m,
                        VatValue = 29.90m,
                        VatRate = "NP"
                    }
                }
            };

            var firstInvoice = invoices.FirstOrDefault();

            Assert.AreEqual(testInvoicesCount, invoices.Count);
            //Assert.IsTrue(testInvoice.Equals(firstInvoice));
        }

        [TestMethod]
        public void ReadMexInvoices_Test()
        {
            var filePath = @"C:\Users\marci\OneDrive - ITDF\KlienciPriv\ES\MexFaktury\Wzor do FS.xlsx";
            var invoices = SourceFileReader.ReadMexInvoices(filePath);

            var testInvoicesCount = 54;

            var testInvoice = new VatInvoice
            {
                Issuer = "Test",
                BuyerTaxNumber = "7252063038",
                NumberSeries = "Z",
                InvoiceDate = new DateTime(2020, 01, 31),
                OperationDate = new DateTime(2020, 01, 31),
                PaymentDate = new DateTime(2020, 02, 07),
                PaymentForm = "przelew",
                Rows = new List<VatInvoiceRow>
                {
                    new VatInvoiceRow
                    {
                        Code = "Opłata za Znak Towarowy",
                        Name = "Opłata za znak towarowy rata planowa 01.2020",
                        Amount = 1,
                        VatRate = "23.00",
                        NetValue = 4130.85m
                    }
                }
            };

            var firstInvoice = invoices.First();

            Assert.AreEqual(testInvoicesCount, invoices.Count);
            Assert.AreEqual(testInvoice.Issuer, firstInvoice.Issuer);
            Assert.AreEqual(testInvoice.BuyerTaxNumber, firstInvoice.BuyerTaxNumber);
            Assert.AreEqual(testInvoice.NumberSeries, firstInvoice.NumberSeries);
            Assert.AreEqual(testInvoice.InvoiceDate, firstInvoice.InvoiceDate);
            Assert.AreEqual(testInvoice.OperationDate, firstInvoice.OperationDate);
            Assert.AreEqual(testInvoice.PaymentDate, firstInvoice.PaymentDate);
            Assert.AreEqual(testInvoice.PaymentForm, firstInvoice.PaymentForm);
            Assert.AreEqual(testInvoice.Rows[0].Code, firstInvoice.Rows[0].Code);
            Assert.AreEqual(testInvoice.Rows[0].Name, firstInvoice.Rows[0].Name);
            Assert.AreEqual(testInvoice.Rows[0].Amount, firstInvoice.Rows[0].Amount);
            Assert.AreEqual(testInvoice.Rows[0].VatRate, firstInvoice.Rows[0].VatRate);
            Assert.AreEqual(testInvoice.Rows[0].NetValue, firstInvoice.Rows[0].NetValue);
        }
    }
}

