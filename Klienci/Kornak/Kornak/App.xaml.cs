﻿using System;
using System.Windows;
using Kornak.View;

namespace Kornak
{
    public partial class App
    {
        protected override void OnStartup(StartupEventArgs e)
        {

            base.OnStartup(e);

            DispatcherUnhandledException += (o, ea) =>
            {
                NLog.LogManager.GetCurrentClassLogger().Info(ea.Exception.ToString());
                ea.Handled = true;
            };

            try
            {
                var window = new MainWindow();
                window.Show();
            }
            catch (Exception)
            {
                var window = new MainWindow();
                window.Show();
            }
        }
    }
}
