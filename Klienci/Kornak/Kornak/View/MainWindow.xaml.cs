﻿using Kornak.Biz.Services;
using Microsoft.Win32;
using NLog;
using System;
using System.Collections.Generic;
using System.Data.SqlClient;
using System.Diagnostics;
using System.IO;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using OptimaImporter.Data.Enum;
using Telerik.Windows.Controls;

//72H88Tyk:-bk
namespace Kornak.View
{
    /// <summary>
    /// Interaction logic for MainWindow.xaml
    /// </summary>
    public partial class MainWindow
    {
        public string ConnectionDataSource = "";
        public string ConnectionInitialCatalog = "";
        public string BazaFirmowa = "";

        private string _optimaUser;
        private string _optimaPassword;
        private string _optimaDatabase;

        public CDNBase.IApplication Application;
        public CDNBase.ILogin ILogin;
        public List<string> FailedImporter = new List<string>();

        OpenFileDialog _ofd = new OpenFileDialog();

        RadGridView _errorListGridView = new RadGridView();

        public MainWindow()
        {
            StyleManager.ApplicationTheme = new Office_BlueTheme();

            InitializeComponent();
            RadRibbon.DataContext = new IconViewModel();

            try
            {
                var registry = Registry.CurrentUser.OpenSubKey(@"SOFTWARE\CDN\CDN Opt!ma\CDN Opt!ma\Login", false);
                var registryValue = (string)registry?.GetValue("KonfigConnectStr");
                ConnectionDataSource = registryValue.Substring(registryValue.IndexOf(",") + 1, registryValue.LastIndexOf(",") - registryValue.IndexOf(",") - 1);
                ConnectionInitialCatalog = registryValue.Substring(registryValue.IndexOf(":") + 1, registryValue.IndexOf(",") - registryValue.IndexOf(":") - 1);
                //MessageBox.Show("Nazwa serwera: " + connectionDataSource + "\nBaza konfiguracyjna: " + connectionInitialCatalog);
            }
            catch (Exception ex)
            {
                LogManager.GetCurrentClassLogger().Info($"Nie udało się odczytać informacji z rejestru: {ex}");
            }

            try
            {
                //podłączanie by pobrać listę baz do logowania
                var sqlConnectionStringBuilder = new SqlConnectionStringBuilder
                {
                    DataSource = ConnectionDataSource,
                    InitialCatalog = ConnectionInitialCatalog,
                    IntegratedSecurity = false,
                    UserID = "cdnoperator",
                    Password = "snw2bdmk"
                };

                var sqlConnection = new SqlConnection(sqlConnectionStringBuilder.ConnectionString);

                var query = @"select ope_kod from cdn.operatorzy order by ope_kod";
                var sqlCommand = new SqlCommand(query, sqlConnection);
                sqlCommand.Connection.Open();
                var sqlDataReader = sqlCommand.ExecuteReader();
                UserComboBox.Items.Clear();
                while (sqlDataReader.Read())
                {
                    var uzytkownikaNazwa = sqlDataReader.GetString(0);
                    UserComboBox.Items.Add(uzytkownikaNazwa);
                }
                sqlDataReader.Close();
                sqlCommand.Connection.Close();
                UserComboBox.IsEnabled = true;

                query = @"select baz_nazwa from cdn.bazy order by baz_nazwa";
                sqlCommand = new SqlCommand(query, sqlConnection);
                sqlCommand.Connection.Open();
                sqlDataReader = sqlCommand.ExecuteReader();
                DbComboBox.Items.Clear();
                while (sqlDataReader.Read())
                {
                    var bazaNazwa = sqlDataReader.GetString(0);
                    DbComboBox.Items.Add(bazaNazwa);
                }
                sqlDataReader.Close();
                sqlCommand.Connection.Close();
                DbComboBox.IsEnabled = true;
            }
            catch (Exception ex)
            {
                LogManager.GetCurrentClassLogger().Info($"Nie udało się odczytać podstawowej konfiguracji firmy: {ex}");
            }
        }

        private void Window_Closed(object sender, EventArgs e)
        {
            try
            {
                ILogin = null;
                Application.UnlockApp();
                Application = null;
            }
            catch (Exception)
            {
                ILogin = null;
                Application = null;
            }
        }

        private void dbComboBox_SelectionChanged(object sender, SelectionChangedEventArgs e)
        {
            var comboBox = sender as ComboBox;
            try
            {
                var sqlConnectionStringBuilder0 = new SqlConnectionStringBuilder
                {
                    DataSource = ConnectionDataSource,
                    InitialCatalog = ConnectionInitialCatalog,
                    IntegratedSecurity = false,
                    UserID = "cdnoperator",
                    Password = "snw2bdmk"
                };

                var sqlConnection0 = new SqlConnection(sqlConnectionStringBuilder0.ConnectionString);

                var query0 = $@"select substring(baz_dostep,5,CHARINDEX(',',baz_dostep)-5) from cdn.bazy where Baz_Nazwa = '{comboBox?.SelectedValue}'";
                var sqlCommand0 = new SqlCommand(query0, sqlConnection0);
                sqlCommand0.Connection.Open();
                var sqlDataReader0 = sqlCommand0.ExecuteReader();
                while (sqlDataReader0.Read())
                {
                    BazaFirmowa = sqlDataReader0.GetString(0);
                }
                sqlDataReader0.Close();
                sqlCommand0.Connection.Close();

                var sqlConnectionStringBuilder = new SqlConnectionStringBuilder
                {
                    DataSource = ConnectionDataSource,
                    InitialCatalog = BazaFirmowa,
                    IntegratedSecurity = false,
                    UserID = "cdnoperator",
                    Password = "snw2bdmk"
                    //UserID = "sa",
                    //Password = "ESsa@dmin14"
                };

                var sqlConnection = new SqlConnection(sqlConnectionStringBuilder.ConnectionString);

                var query = @"SELECT [Gru_Nazwa] FROM [CDN].[Grupy] where gru_typ = 1";
                var sqlCommand = new SqlCommand(query, sqlConnection);
                sqlCommand.Connection.Open();
                var sqlDataReader = sqlCommand.ExecuteReader();
                RejestrVatZakupuComboBox.Items.Clear();
                while (sqlDataReader.Read())
                {
                    var rejestrZakupu = sqlDataReader.GetString(0);
                    RejestrVatZakupuComboBox.Items.Add(rejestrZakupu);
                }
                sqlDataReader.Close();
                sqlCommand.Connection.Close();
                RejestrVatZakupuComboBox.IsEnabled = true;

                var query2 = @"SELECT [Gru_Nazwa] FROM [CDN].[Grupy] where gru_typ = 2";
                var sqlCommand2 = new SqlCommand(query2, sqlConnection);
                sqlCommand2.Connection.Open();
                var sqlDataReader2 = sqlCommand2.ExecuteReader();
                RejestrVatSprzedazyComboBox.Items.Clear();
                while (sqlDataReader2.Read())
                {
                    var rejestrSprzedazy = sqlDataReader2.GetString(0);
                    RejestrVatSprzedazyComboBox.Items.Add(rejestrSprzedazy);
                }
                sqlDataReader2.Close();
                RejestrVatSprzedazyComboBox.IsEnabled = true;
                sqlCommand2.Connection.Close();

                InstructionsTextBlock.Text = "Wybierz rejestr, do którego chcesz wczytać dane.";
            }
            catch (Exception ex)
            {
                LogManager.GetCurrentClassLogger()
                    .Info($"Błąd odczytu listy baz danych: {ex}");
            }

        }

        private void RadRibbonButton_Click(object sender, RoutedEventArgs e)
        {
            Grid.SetRow(_errorListGridView, 1);
            MainGrid.Children.Add(_errorListGridView);
        }

        private void RadRibbonButton_Click_1(object sender, RoutedEventArgs e)
        {
            MainGrid.Children.Remove(_errorListGridView);
        }

        private void userComboBox_SelectionChanged(object sender, SelectionChangedEventArgs e)
        {
            DbComboBox.IsEnabled = true;
            InstructionsTextBlock.Text = "Wpisz hasło i wybierz bazę danych.";
        }

        private void rejestrVATZakupuComboBox_SelectionChanged(object sender, SelectionChangedEventArgs e)
        {
            InstructionsTextBlock.Text = "By wczytać dane naciśnij przycisk na górze z literką Z i wybierz plik, z którego chcesz wczytywać dane.";
        }

        private void rejestrVATSprzedazyComboBox_SelectionChanged(object sender, SelectionChangedEventArgs e)
        {
            UberSalesButton.IsEnabled = true;
        }

        private void OptimaLogIn()
        {
            try
            {
                _optimaUser = UserComboBox.SelectedItem.ToString();
                _optimaPassword = PasswordTextBox.Password;
                _optimaDatabase = DbComboBox.SelectedItem.ToString();

                Application = new CDNBase.Application();
                Application.LockApp(1);
                ILogin = Application.Login(_optimaUser, _optimaPassword, _optimaDatabase);
            }
            catch (Exception ex)
            {
                LogManager.GetCurrentClassLogger().Info($"Błąd logowania: {ex}");
            }
        }

        private void LogButton_OnClick(object sender, RoutedEventArgs e)
        {
            var fileInfo = new FileInfo(AppDomain.CurrentDomain.BaseDirectory + "\\Log");
            Process.Start(fileInfo.FullName);
        }

        private void UberSalesButton_OnClick(object sender, RoutedEventArgs e)
        {
            OptimaLogIn();

            _ofd.ShowDialog();
            if (!string.IsNullOrWhiteSpace(_ofd.FileName))
            {
                try
                {
                    var invoices = SourceFileReader.ReadUberSalesInvoices(_ofd.FileName, "Sheet1");

                    var impoter = new VatRegisterImporter();
                    impoter.InvoiceImported += x =>
                    {
                        Dispatcher.Invoke(() =>
                        {
                            UberSalesButton.Text = "Wczytaj sprzedaż z Ubera" + Environment.NewLine + x;
                        });
                    };
                    string registerVatName;
                    if (RejestrVatSprzedazyComboBox.SelectedValue != null)
                    {
                        registerVatName = RejestrVatSprzedazyComboBox.SelectedValue.ToString();
                    }
                    else
                    {
                        registerVatName = "SPRZEDAŻ";
                    }
                    Task.Run(() =>
                    {
                        impoter.VatRegisterImport(
                            ILogin, invoices, VatRegisterType.Sales,
                            registerVatName);
                    });
                }
                catch (Exception ex)
                {
                    LogManager.GetCurrentClassLogger().Info($"Błąd odczytu pliku źródłowego i importu do Otpimy: {ex}");
                }
            }
        }
    }
}
