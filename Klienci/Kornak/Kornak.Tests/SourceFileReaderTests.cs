﻿using Kornak.Biz.Services;
using Microsoft.VisualStudio.TestTools.UnitTesting;

namespace Kornak.Tests
{
    [TestClass]
    public class SourceFileReaderTests
    {
        [TestMethod]
        public void ReadBoltSalesInvoices_Test()
        {
            var filePath = @"C:\Users\marci\OneDrive - ITDF\KlienciPriv\Kornak\BOLT CZERWIEC.csv";
            var invoices = SourceFileReader.ReadBoltSalesInvoices(filePath);
            Assert.IsTrue(invoices.Count > 100);
        }

        [TestMethod]
        public void ReadUberSalesInvoices_Test()
        {
            var filePath = @"C:\Users\marci\OneDrive - ITDF\KlienciPriv\Kornak\Uber_d4edc586-1519-4432-91fd-ea1ccee3e9db_ZestawienieFakturZaPrzejazdy_październik_2019.xls";
            var invoices = SourceFileReader.ReadUberSalesInvoices(filePath, "Sheet1");
            Assert.IsTrue(invoices.Count > 100);
        }
    }
}
