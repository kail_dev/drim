﻿using ExcelDataReader;
using NLog;
using OptimaImporter.Biz.Model;
using System;
using System.Collections.Generic;
using System.Globalization;
using System.IO;
using System.Linq;
using OptimaImporter.Biz.Helper;
// ReSharper disable EmptyGeneralCatchClause

namespace Kornak.Biz.Services
{
    public class SourceFileReader
    {
        public static List<VatInvoice> ReadBoltSalesInvoices(string filePath)
        {
            var invoices = new List<VatInvoice>();

            using (var stream = File.Open(filePath, FileMode.Open, FileAccess.Read))
            {
                using (var streamRreader = new StreamReader(stream))
                {
                    streamRreader.ReadLine(); //we ommit headers line
                    while (!streamRreader.EndOfStream)
                    {
                        var line = streamRreader.ReadLine()?.SplitWithTextDelimiter(';', "\"");
                        if (line != null && !string.IsNullOrWhiteSpace(line[0]))
                        {
                            var inv = new VatInvoice
                            {
                                Rows = new List<VatInvoiceRow>()
                            };
                            try
                            {
                                var dateString = line[5].Substring(6, 4) + "-" + line[5].Substring(3, 2) + "-" + line[5].Substring(0, 2);
                                inv.Number = line[0];
                                inv.BuyerCode = "!NIEOKREŚLONY!";
                                inv.BuyerName = line[6];
                                inv.InvoiceDate = Convert.ToDateTime(dateString);
                                inv.OperationDate = Convert.ToDateTime(dateString);
                                //inv.PostalCode = line[15];
                                //inv.City = line[6];
                                inv.BuyerAddress = line[7];
                                //inv.BuyerTaxNumber = reader[11].ToString().Replace("-", "");
                                //inv.Currency = reader[12].ToString();
                                //inv.CurrencyRate = reader[13].ToString();
                                inv.PaymentDate = Convert.ToDateTime(dateString).AddDays(1);
                                //inv.Description = reader[17].ToString();
                                inv.Currency = "PLN";
                                inv.Rows.Add(new VatInvoiceRow
                                {
                                    NetValue = Convert.ToDecimal(line[14].Replace(".", ",")),
                                    VatRate = Math.Round(Convert.ToDecimal(line[16]) / Convert.ToDecimal(line[14]) * 100 - 100, 0).ToString(CultureInfo.InvariantCulture),
                                    GrossValue = Convert.ToDecimal(line[16].Replace(".", ","))
                                });
                                //else if (inv.Currency == "EUR")
                                //{
                                //    inv.Rows.Add(new InvoiceRow
                                //    {
                                //        NetValue = Convert.ToDecimal(reader[23].ToString()) / Convert.ToDecimal(inv.CurrencyRate),
                                //        VatRate = Math.Round(Convert.ToDecimal(reader[25].ToString())
                                //                             / Convert.ToDecimal(reader[23].ToString()) * 100 - 100
                                //            , 0).ToString(),
                                //        GrossValue = Convert.ToDecimal(reader[25].ToString()) / Convert.ToDecimal(inv.CurrencyRate)
                                //    });
                                //}
                                invoices.Add(inv);
                            }
                            catch (Exception ex)
                            {
                                LogManager.GetCurrentClassLogger()
                                    .Error($"Błąd odczytu linii {inv.Number}.Błąd: {ex.Message}");
                            }
                        }
                        else if (line != null && !string.IsNullOrWhiteSpace(line[0]))
                        {
                            var inv = invoices.LastOrDefault();
                            if (inv?.Currency == "PLN")
                            {
                                try
                                {
                                    inv.Rows.Add(new VatInvoiceRow
                                    {
                                        NetValue = Convert.ToDecimal(line[23]),
                                        VatRate = Math.Round(Convert.ToDecimal(line[25]) / Convert.ToDecimal(line[23]) * 100 - 100, 0).ToString(CultureInfo.InvariantCulture),
                                        GrossValue = Convert.ToDecimal(line[25])
                                    });

                                }
                                catch (Exception)
                                {

                                }
                            }
                            else if (inv?.Currency == "EUR")
                            {
                                try
                                {
                                    inv.Rows.Add(new VatInvoiceRow
                                    {
                                        NetValue = Convert.ToDecimal(line[23]) / Convert.ToDecimal(inv.CurrencyRate),
                                        VatRate = Math.Round(Convert.ToDecimal(line[25]) / Convert.ToDecimal(line[23]) * 100 - 100, 0).ToString(),
                                        GrossValue = Convert.ToDecimal(line[25]) / Convert.ToDecimal(inv.CurrencyRate)
                                    });

                                }
                                catch (Exception)
                                {

                                }
                            }
                        }
                    }
                }
            }

            return invoices;
        }
        public static List<VatInvoice> ReadUberSalesInvoices(string filePath, string sheet)
        {
            var invoices = new List<VatInvoice>();

            using (var stream = File.Open(filePath, FileMode.Open, FileAccess.Read))
            {
                using (var reader = ExcelReaderFactory.CreateReader(stream))
                {
                    var invoiceNumber = "";
                    var result = reader.AsDataSet().Tables[0];
                    reader.Read(); //we ommit headers line
                    while (reader.Read() && !string.IsNullOrWhiteSpace(reader.GetString(11)))
                    {
                        if (invoiceNumber != reader[11].ToString() && reader[4] != null)
                        {
                            var inv = new VatInvoice
                            {
                                Rows = new List<VatInvoiceRow>()
                            };
                            try
                            {
                                inv.Number = reader[11].ToString();
                                inv.BuyerCode = "";
                                if (reader[4] != null && reader[4].ToString().Contains("NIP"))
                                {
                                    var tmpTaxNumber = reader[4].ToString();
                                    inv.BuyerTaxNumber = reader[4].ToString().Substring(tmpTaxNumber.Length - 10, 10);
                                }
                                else if (reader[4] != null && reader[4].ToString().Contains("VAT ID"))
                                {
                                    var tmpTaxNumber = reader[4].ToString().Replace("-", "");
                                    inv.BuyerTaxNumber = tmpTaxNumber.Substring(8);
                                }
                                inv.BuyerName = reader[3].ToString();
                                inv.InvoiceDate = Convert.ToDateTime(reader[17].ToString().Substring(0, 10));
                                inv.OperationDate = Convert.ToDateTime(reader[17].ToString().Substring(0, 10));
                                inv.PostalCode = reader[15].ToString();
                                inv.City = reader[6].ToString();
                                inv.BuyerAddress = reader[1] + " " + reader[2];
                                //inv.BuyerTaxNumber = reader[11].ToString().Replace("-", "");
                                //inv.Currency = reader[12].ToString();
                                //inv.CurrencyRate = reader[13].ToString();
                                inv.PaymentDate = Convert.ToDateTime(reader[17].ToString().Substring(0, 10)).AddDays(1);
                                //inv.Description = reader[17].ToString();
                                inv.PaymentForm = "gotówka";
                                inv.Currency = "PLN";
                                inv.Rows.Add(new VatInvoiceRow
                                {
                                    NetValue = Convert.ToDecimal(reader[14].ToString().Replace(".", ",")),
                                    VatRate = reader[24].ToString().Replace("0,0", ""),
                                    GrossValue = Convert.ToDecimal(reader[10].ToString().Replace(".", ","))
                                });
                                invoices.Add(inv);
                            }
                            catch (Exception ex)
                            {
                                LogManager.GetCurrentClassLogger()
                                    .Error($"Błąd odczytu linii {inv.Number}.Błąd: {ex.Message}");
                            }
                        }

                        invoiceNumber = reader[11].ToString();
                    }
                }
            }

            return invoices;
        }
    }
}
