﻿using CDNBase;
using NLog;
using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Linq;
using OptimaImporter.Biz.Helper;
using OptimaImporter.Biz.Model;
using OptimaImporter.Data.Enum;

namespace Kornak.Biz.Services
{
    public class VatRegisterImporter
    {
        public event Action<int> InvoiceImported;
        public void VatRegisterImport(ILogin login, IEnumerable<VatInvoice> invoices, VatRegisterType vatRegisterType, string vatTegisterName, bool nameAsCustomerCode = false)
        {
            var allInvoicesAmount = invoices.Count();
            var validInvoicesCounter = 0;
            var inValidInvoicesCounter = 0;
            foreach (var invoice in invoices) //.Where(i => i.Number == "53/SFV/00001403/2018" || i.Number == "53/SFV/00001404/2018"))
            {
                var session = login.CreateSession();

                var paymentForms = (ICollection)session.CreateObject("CDN.FormyPlatnosci");
                var vatRegisters = (ICollection)session.CreateObject("CDN.RejestryVAT");
                CDNHeal.Kontrahenci customers;
                CDNHeal.IKontrahent customer;

                try
                {
                    //odwołania do istniejącego kontrahenta
                    customers = (CDNHeal.Kontrahenci)session.CreateObject("CDN.Kontrahenci");
                    if ((int)vatRegisterType == 1)
                    {
                        customer = customers["Knt_NIP='" + NipSplit.GetNipNumber(invoice.SellerTaxNumber.Replace("-", "")) + "'"];
                    }
                    else if (invoice.BuyerTaxNumber != null)
                    {
                        customer = customers["Knt_NIP='" + NipSplit.GetNipNumber(invoice.BuyerTaxNumber.Replace("-", "")) + "'"];
                    }
                    else if (invoice.BuyerCode != null)
                    {
                        customer = customers["Knt_Kod='" + invoice.BuyerCode + "'"];
                    }
                    else if (invoice.BuyerName != null)
                    {
                        customer = customers["Knt_Nazwa1='" + invoice.BuyerName + "'"];
                    }
                    else
                    {
                        customer = customers["knt_kntid = 1"];
                    }
                }
                catch (Exception)
                {
                    var session1 = login.CreateSession();
                    customers = (CDNHeal.Kontrahenci)session1.CreateObject("CDN.Kontrahenci");
                    customer = customers.AddNew();
                    if ((int)vatRegisterType == 1)
                    {
                        customer.Akronim = invoice.SellerTaxNumber;
                        customer.Nazwa1 = invoice.SellerName;
                        if (string.IsNullOrWhiteSpace(invoice.SellerTaxNumber))
                        {
                            customer.Akronim = DateTime.Today.ToShortDateString() + DateTime.Now.ToLongTimeString();
                        }
                        else
                        {
                            try
                            {
                                customer.NumerNIP.NIPKraj = NipSplit.GetNipCountry(invoice.SellerTaxNumber);
                                customer.NumerNIP.NipE = NipSplit.GetNipNumber(invoice.SellerTaxNumber);
                            }
                            catch (Exception e)
                            {
                                LogManager.GetCurrentClassLogger()
                                    .Info($"Nie udało się zapisać NIPu dla faktury {invoice.Number}, błąd: {e}");
                            }
                        }

                        customer.Adres.Ulica = invoice.Street;
                        customer.Adres.Miasto = invoice.City;
                        customer.Adres.KodPocztowy = invoice.PostalCode;
                        customer.Rodzaj_Dostawca = 1;
                        customer.Rodzaj_Odbiorca = 0;
                    }
                    else
                    {
                        customer.Akronim = invoice.BuyerTaxNumber;
                        customer.Nazwa1 = invoice.BuyerName;
                        if (string.IsNullOrWhiteSpace(invoice.BuyerTaxNumber) && !nameAsCustomerCode)
                        {
                            customer.Akronim = DateTime.Today.ToShortDateString() + DateTime.Now.ToLongTimeString();
                        }
                        else if (string.IsNullOrWhiteSpace(invoice.BuyerTaxNumber) && nameAsCustomerCode)
                        {
                            customer.Akronim = invoice.BuyerName;
                        }
                        else
                        {
                            try
                            {
                                customer.NumerNIP.NIPKraj = NipSplit.GetNipCountry(invoice.BuyerTaxNumber);
                                customer.NumerNIP.NipE = NipSplit.GetNipNumber(invoice.BuyerTaxNumber);
                            }
                            catch (Exception e)
                            {
                                LogManager.GetCurrentClassLogger()
                                    .Info($"Nie udało się zapisać NIPu dla faktury {invoice.Number}, błąd: {e}");
                            }
                        }
                        customer.Adres.Ulica = invoice.Street;
                        customer.Adres.Miasto = invoice.City;
                        customer.Adres.KodPocztowy = invoice.PostalCode;
                        if (string.IsNullOrWhiteSpace(invoice.Street) && nameAsCustomerCode)
                        {
                            try
                            {
                                customer.Adres.Ulica = invoice.BuyerAddress.Substring(0, invoice.BuyerAddress.IndexOf(','));
                                customer.Adres.Miasto = invoice.BuyerAddress.Substring(invoice.BuyerAddress.IndexOf(',') + 9);
                                customer.Adres.KodPocztowy = invoice.BuyerAddress.Substring(invoice.BuyerAddress.IndexOf(',') + 2, 6);
                            }
                            catch (Exception e)
                            {
                                LogManager.GetCurrentClassLogger()
                                    .Info($"Nie udało się zapisać adresu dla faktury {invoice.Number}, błąd: {e}");
                            }
                        }
                        customer.Rodzaj_Dostawca = 0;
                        customer.Rodzaj_Odbiorca = 1;
                    }
                    try
                    {
                        session1.Save();
                    }
                    catch (Exception ex)
                    {
                        LogManager.GetCurrentClassLogger()
                            .Info($"Nie udało się zapisać kontrahenta dla faktury {invoice.Number}, błąd: {ex}");
                    }
                }

                var paymentForm = (invoice.InvoiceDate == invoice.PaymentDate)
                    ? (OP_KASBOLib.FormaPlatnosci)paymentForms["Fpl_FplId=1"]
                    : (OP_KASBOLib.FormaPlatnosci)paymentForms["Fpl_FplId=3"];
                try
                {
                    paymentForm = (OP_KASBOLib.FormaPlatnosci)paymentForms[$"Fpl_Nazwa='{invoice.PaymentForm}'"];
                }
                catch (Exception e)
                {
                    LogManager.GetCurrentClassLogger()
                        .Info($"Nie udało się ustawić formy płatności dla faktury {invoice.Number}, błąd: {e}");
                }
                var vatRegister = (CDNRVAT.VAT)vatRegisters.AddNew();
                vatRegister.Typ = vatRegisterType == VatRegisterType.Purchase ? 1 : 2;
                vatRegister.Rejestr = string.IsNullOrWhiteSpace(vatTegisterName) && vatRegister.Typ == 1
                    ? "ZAKUP" : vatTegisterName;
                vatRegister.Rejestr = string.IsNullOrWhiteSpace(vatTegisterName) && vatRegister.Typ == 2
                    ? "SPRZEDAŻ" : vatTegisterName;
                vatRegister.Dokument = invoice.Number;
                vatRegister.DataWys = invoice.InvoiceDate;
                vatRegister.DataZap = invoice.InvoiceDate;
                vatRegister.DataOpe = invoice.OperationDate == new DateTime(1, 1, 1) ? invoice.InvoiceDate : invoice.OperationDate;
                try
                {
                    vatRegister.Podmiot = customer;
                }
                catch (Exception)
                {
                    vatRegister.Podmiot = customers["Knt_KntId=1"];
                }

                if ((vatRegister.Podmiot == customers["Knt_KntId=1"] && nameAsCustomerCode) || invoice.BuyerCode == "!NIEOKREŚLONY!")
                {
                    vatRegister.Nazwa1 = invoice.BuyerName;
                    vatRegister.PodAdres.Ulica = invoice.BuyerAddress;
                    vatRegister.PodAdres.KodPocztowyZKreska = invoice.PostalCode;
                    vatRegister.PodAdres.Miasto = invoice.City;
                }
                try
                {
                    vatRegister.FormaPlatnosci = paymentForm;
                }
                catch (Exception e)
                {
                    LogManager.GetCurrentClassLogger().Info($"Brak terminu płatności dla faktury o numerze {invoice.Number}, błąd: {e}");
                }

                var currencies = new List<string> { "EUR", "USD" };
                if (!string.IsNullOrWhiteSpace(invoice.Currency) && currencies.Contains(invoice.Currency))
                {
                    var waluty = (ICollection)session.CreateObject("CDN.Waluty");
                    var waluta = (CDNHeal.Waluta)waluty[$"WNa_Symbol='{invoice.Currency}'"];
                    try
                    {
                        vatRegister.Waluta = waluta;
                        vatRegister.WalutaDoVAT = waluta;
                        var kurs = session.CreateObject("CDN.TypyKursowWalut").Item("WKu_Symbol='NBP'");
                        vatRegister.TypKursuWaluty = kurs;
                        vatRegister.TypKursuWalutyDoVAT = kurs;
                    }
                    catch (Exception e)
                    {
                        LogManager.GetCurrentClassLogger().Info($"Blad ustawienia waluty dla faktury o numerze {invoice.Number}, błąd: {e}");

                    }
                }

                var vatElements = vatRegister.Elementy;
                foreach (var row in invoice.Rows)
                {
                    var vatElement = (CDNRVAT.VATElement)vatElements.AddNew();
                    try
                    {
                        var stawkaString = row.VatRate.Replace("\"", "").Replace(".0000", ",00");
                        vatElement.Stawka = Convert.ToDouble(stawkaString);
                    }
                    catch (Exception)
                    {
                        if (row.VatRate.Replace("\"", "").Replace(".0000", ",00").StartsWith("zw"))
                        {
                            vatElement.Stawka = 0;
                            vatElement.Flaga = 1;
                        }
                        else if (row.VatRate.Replace("\"", "").Replace(".0000", ",00").StartsWith("np"))
                        {
                            vatElement.Stawka = 0;
                            vatElement.Flaga = 4;
                        }
                        else
                        {
                            vatElement.Stawka = 23;
                        }
                    }
                    vatElement.RodzajZakupu = 1;
                    vatElement.Netto = row.NetValue;
                    vatElement.Odliczenia = 1;
                    vatElement.Brutto = row.GrossValue;
                }

                if (!string.IsNullOrWhiteSpace(invoice.Description))
                {
                    vatRegister.KategoriaOpis = invoice.Description;
                }

                try
                {
                    session.Save();
                    validInvoicesCounter++;
                    Debug.WriteLine(validInvoicesCounter);
                    InvoiceImported?.Invoke(validInvoicesCounter);
                }
                catch (Exception e)
                {
                    inValidInvoicesCounter++;
                    Debug.WriteLine($"{inValidInvoicesCounter} błędnych faktur");
                    LogManager.GetCurrentClassLogger()
                        .Info($"Nie udało się zapisać faktury o numerze {invoice.Number}, błąd: {e}");
                }
            }
        }
    }
}
