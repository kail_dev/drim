﻿using JLR.Biz.Model;
using JLR.Biz.Services;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using System;
using System.Linq;

namespace JLR.BizTests.Services
{
    [TestClass]
    public class EmailServicesTests
    {
        EmailService emailService = new EmailService();
        EmailConnectionParameters cp01 = new EmailConnectionParameters { Hostname = "imap.gmail.com", Port = 993, UseSsl = true, Username = "kioskus01@gmail.com", Password = "Jojowski123" };
        EmailConnectionParameters cp02 = new EmailConnectionParameters { Hostname = "imap.gmail.com", Port = 993, UseSsl = true, Username = "kioskus02@gmail.com", Password = "Jojowski123" };
        EmailConnectionParameters cp03 = new EmailConnectionParameters { Hostname = "imap.gmail.com", Port = 993, UseSsl = true, Username = "kioskus03@gmail.com", Password = "Jojowski123" };
        EmailConnectionParameters cp04 = new EmailConnectionParameters { Hostname = "imap.gmail.com", Port = 993, UseSsl = true, Username = "kioskus04@gmail.com", Password = "Jojowski123" };

        [TestMethod]
        public void ConnectToAllMailBoxesAndFetchUidsTest()
        {
            var mailbox01Uids = emailService.FetchUids(cp01);
            var mailbox02Uids = emailService.FetchUids(cp02);
            var mailbox03Uids = emailService.FetchUids(cp03);
            var mailbox04Uids = emailService.FetchUids(cp04);

            Assert.IsTrue(mailbox01Uids.Any());
            Assert.IsTrue(mailbox02Uids.Any());
            Assert.IsTrue(mailbox03Uids.Any());
            Assert.IsTrue(mailbox04Uids.Any());
        }


        [TestMethod]
        public void GetGarmondMessagesKioskus01Test()
        {
            var kingMessages = emailService.FetchUids(cp01, "inwentaryzacja.pp@garmondpress.pl", "dostawy");
            Assert.AreEqual(77, kingMessages.Count());
        }

        [TestMethod]
        public void GetKingMessagesKioskus01Test()
        {
            var kingMessages = emailService.FetchUids(cp01, "raporty@kinghurt.pl", "Dokumenty");
            Assert.AreEqual(1, kingMessages.Count());
        }

        [TestMethod]
        public void DownloadGarmondMessageAttachementsTest()
        {
            var garmondMessages = emailService.FetchUids(cp01, "inwentaryzacja.pp@garmondpress.pl", "dostawy");
            var message = emailService.FetchMessageAndAttachements(cp01, garmondMessages.FirstOrDefault());
            var attachements = emailService.DownloadAttachements(message.Attachements, Environment.GetFolderPath(Environment.SpecialFolder.DesktopDirectory), ".xml");
            Assert.AreEqual(1, attachements.Count());
        }

        [TestMethod]
        public void DownloadKingMessageAttachementsKiosk1Test()
        {
            var kingMessages = emailService.FetchUids(cp01, "raporty@kinghurt.pl", "Dokumenty");
            var message = emailService.FetchMessageAndAttachements(cp01, kingMessages.FirstOrDefault());
            var attachements = emailService.DownloadAttachements(message.Attachements, Environment.GetFolderPath(Environment.SpecialFolder.DesktopDirectory), ".txt4");
            Assert.AreEqual(1, attachements.Count());
        }

        [TestMethod]
        public void DownloadKingMessageAttachementsKiosk2Test()
        {
            var kingMessages = emailService.FetchUids(cp02, "raporty@kinghurt.pl", "Dokumenty");
            var message = emailService.FetchMessageAndAttachements(cp02, kingMessages.FirstOrDefault());
            var attachements = emailService.DownloadAttachements(message.Attachements, Environment.GetFolderPath(Environment.SpecialFolder.DesktopDirectory), ".txt4");
            Assert.AreEqual(1, attachements.Count());
        }
        [TestMethod]
        public void DownloadKingMessageAttachementsKiosk3Test()
        {
            var kingMessages = emailService.FetchUids(cp03, "raporty@kinghurt.pl", "Dokumenty");
            var message = emailService.FetchMessageAndAttachements(cp03, kingMessages.FirstOrDefault());
            var attachements = emailService.DownloadAttachements(message.Attachements, Environment.GetFolderPath(Environment.SpecialFolder.DesktopDirectory), ".txt4");
            Assert.AreEqual(1, attachements.Count());
        }
        [TestMethod]
        public void DownloadKingMessageAttachementsKiosk4Test()
        {
            var kingMessages = emailService.FetchUids(cp04, "raporty@kinghurt.pl", "Dokumenty");
            var message = emailService.FetchMessageAndAttachements(cp04, kingMessages.FirstOrDefault());
            var attachements = emailService.DownloadAttachements(message.Attachements, Environment.GetFolderPath(Environment.SpecialFolder.DesktopDirectory), ".txt4");
            Assert.AreEqual(1, attachements.Count());
        }
    }
}
