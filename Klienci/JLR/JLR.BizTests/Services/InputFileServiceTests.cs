﻿using JLR.Biz.Model;
using JLR.Biz.Services;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using System;

namespace JLR.BizTests.Services
{
    [TestClass()]
    public class InputFileServiceTests
    {
        [TestMethod()]
        public void GetKingDocumentHeaderTest()
        {
            var docHeader = new DocHeader()
            {
                Date = new DateTime(2019, 04, 26),
                Gender = 307000,
                Type = 307,
                Number = "6620/650/2019"
            };

            var testDocHeader = InputFileService.GetKingDocumentHeader(@"C:\Users\marci\OneDrive\Desktop\6620_650_2019_FA_20190426_7862084.txt4");

            Assert.AreEqual(docHeader.Date, testDocHeader.Date);
            Assert.AreEqual(docHeader.Gender, testDocHeader.Gender);
            Assert.AreEqual(docHeader.Type, testDocHeader.Type);
            Assert.AreEqual(docHeader.Number, testDocHeader.Number);
        }

        [TestMethod()]
        public void GetKingDocumentElementsTest()
        {
            var testProducts = InputFileService.ExtractProductInfoFromKingFile(@"C:\Users\marci\OneDrive\Desktop\6620_650_2019_FA_20190426_7862084.txt4");
            Assert.AreEqual(19, testProducts.Count);
        }
    }
}