﻿using CDNBase;
using JLR.Biz.Services;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using System.IO;
using System.Linq;

namespace JLR.BizTests.Services
{
    [TestClass]
    public class OptimaDocumentServiceTests
    {
        private static IApplication _szeranApp;

        [TestMethod]
        public void CreateOptimaDocGarmondTest()
        {
            _szeranApp = new Application();
            _szeranApp.Login("ADMIN", "", "JRL");
            //const string file = @"C:\_C\Szeran\Klienci\JRL\Kioskus01\DC403786.xml";
            const string file = @"C:\Users\marci\OneDrive\Desktop\DC403786.xml";

            var success = false;
            var fileWithoutPath = Path.GetFileName(file);
            var docHeader = InputFileService.GetGarmondDocumentHeader(file);
            docHeader.Number = docHeader.Number.Replace("49778", "40378");

            var products = InputFileService.ExtractProductInfoFromGarmondFile(file);
            if (products.Any())
            {
                OptimaProductService.CheckIfProductExists_UpdatePrice(products);
            }

            var isDocInOptima = OptimaDocumentService.IsInOptima(docHeader.Number);
            if (!isDocInOptima)
            {
                success = OptimaDocumentService.CreateDoc(fileWithoutPath, docHeader, products); //todo: dorobić magazyn selector
            }
            if (isDocInOptima)
            {
                success = false;
            }
        }

        [TestMethod]
        public void CreateOptimaDocKingTest()
        {
            _szeranApp = new Application();
            _szeranApp.Login("ADMIN", "", "JRL");
            //const string file = @"C:\_C\Szeran\Klienci\JRL\Kioskus01\DC403786.xml";
            const string file = @"C:\Users\marci\OneDrive\Desktop\6620_650_2019_FA_20190426_7862084.txt4";

            var success = false;
            var fileWithoutPath = Path.GetFileName(file);
            var docHeader = InputFileService.GetKingDocumentHeader(file);
            docHeader.Number = docHeader.Number.Replace("49778", "40378");

            var products = InputFileService.ExtractProductInfoFromKingFile(file);
            if (products.Any())
            {
                OptimaProductService.CheckIfProductExists_UpdatePrice(products);
            }

            var isDocInOptima = OptimaDocumentService.IsInOptima(docHeader.Number);
            if (!isDocInOptima)
            {
                success = OptimaDocumentService.CreateDoc(fileWithoutPath, docHeader, products); //todo: dorobić magazyn selector
            }
            if (isDocInOptima)
            {
                success = false;
            }
        }

        [TestMethod]
        public void GetUids_GetMessageAndAttachements_CreateProduct_CreateDoc_Test()
        {
            Run.GetUids_GetMessageAndAttachements_CreateProduct_CreateDoc_MoveFile("ADMIN", "admin", "Andaro",
                "Kioskus02", @"C:\_C\Szeran\Klienci\JRL", "Kioskus02@gmail.com", "Jojowski123",2);
        }
    }
}

