using JLR.Biz.Services;
using NLog;

namespace JRL
{
    class Program
    {
        static void Main(string[] args)
        {
            var agencies = new [] { "Kioskus01", "Kioskus02", "Kioskus03", "Kioskus04", "Kioskus05", "Kioskus06", "Kioskus07", "Kioskus08" };

            var emails = new [] { "Kioskus01@gmail.com", "Kioskus02@gmail.com", "Kioskus03@gmail.com", "Kioskus04@gmail.com",
                "Kioskus05@gmail.com", "Kioskus06@gmail.com", "Kioskus07@gmail.com", "Kioskus08@gmail.com"  };

            var emailsPassword = "Jojowski123";
            var warehouseIds = new [] { 3, 4, 5, 6, 7, 8, 9, 10 };

            var optimaUser = AppSettings.Default.optimaUser;
            var optimaPassword = AppSettings.Default.optimaPassword;
            var optimaDatabase = AppSettings.Default.optimaDatabase;

            for (int i = 0; i < 6; i++)
            {
                try
                {
                    Run.GetUids_GetMessageAndAttachements_CreateProduct_CreateDoc_MoveFile
                    (optimaUser, optimaPassword, optimaDatabase, agencies[i], AppSettings.Default.fileCatalog, emails[i], emailsPassword, warehouseIds[i]);

                }
                catch (System.Exception ex)
                {
                    LogManager.GetCurrentClassLogger().Error($"B��d aplikacji: {ex.Message}, {ex.InnerException}");

                }
            }
        }
    }
}
