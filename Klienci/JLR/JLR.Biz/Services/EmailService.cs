﻿using JLR.Biz.Model;
using NLog;
using S22.Imap;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Net.Mail;


namespace JLR.Biz.Services
{
    public class EmailService
    {
        public IEnumerable<uint> FetchUids(EmailConnectionParameters connectionParameters, string fromFilter = "", string subjectFilter = "")
        {
            Console.WriteLine("Pobieranie listy wiadomości rozpoczęte...");
            using (var client = new ImapClient(connectionParameters.Hostname, connectionParameters.Port, connectionParameters.Username, connectionParameters.Password, AuthMethod.Login, true))
            {
                IEnumerable<uint> uids;
                SearchCondition searchConditions = SearchCondition.SentSince(new DateTime(2019,04, 15));
                if (!string.IsNullOrWhiteSpace(fromFilter))
                {
                    searchConditions = searchConditions.And(SearchCondition.From(fromFilter));
                    if (!string.IsNullOrWhiteSpace(subjectFilter))
                        searchConditions = searchConditions.And(SearchCondition.Subject(subjectFilter));
                }
                else
                {
                    searchConditions = SearchCondition.All();
                }
                uids = client.Search(searchConditions, "INBOX");
                return uids;
            }
        }

        public EmailMessage FetchMessageAndAttachements(EmailConnectionParameters connectionParameters, uint uid)
        {
            Console.WriteLine("Pobieranie wiadomości rozpoczęte...");

            using (var client = new ImapClient(connectionParameters.Hostname, connectionParameters.Port, connectionParameters.Username, connectionParameters.Password, AuthMethod.Login, true))
            {
                var message = client.GetMessage(uid);
                return new EmailMessage()
                {
                    Uid = uid,
                    Message = message,
                    Attachements = message.Attachments.ToList()
                };

            }
        }

        public List<string> DownloadAttachements(List<Attachment> attachements, string path, params string[] extensions)
        {
            Console.WriteLine("Pobieranie załączników do wiadomości rozpoczęte...");
            var attachementsNamesList = new List<string>();
            foreach (var attachment in attachements)
            {
                if (!extensions.Contains(Path.GetExtension(attachment.Name))) continue;

                var attachementName = $@"{path}\{attachment.Name}";
                using (var fileStream = File.Create(attachementName))
                {
                    attachment.ContentStream.Seek(0, SeekOrigin.Begin);
                    attachment.ContentStream.CopyTo(fileStream);

                    attachementsNamesList.Add(attachementName);
                }
            }

            return attachementsNamesList;
        }
        public bool MoveMessagesToFolders(EmailConnectionParameters connectionParameters, uint uid, string folderName)
        {
            try
            {
                using (var client = new ImapClient(connectionParameters.Hostname, connectionParameters.Port, connectionParameters.Username, connectionParameters.Password, AuthMethod.Login, true))
                {
                    var message = client.Search(SearchCondition.UID(uid));
                    try
                    {
                        client.CreateMailbox($"{folderName}");
                    }
                    catch (Exception) { }
                    {
                    }
                    client.MoveMessages(message, $"{folderName}");
                    Console.WriteLine($"Przeniesiono plik do folderu {folderName}");
                    return true;
                }
            }
            catch (Exception ex)
            {
                LogManager.GetCurrentClassLogger().Error($"Błąd przenoszenia wiadomości e-mail do katalogu {folderName}. Błąd {ex.Message},{ex.InnerException}");
                Console.WriteLine($"Błąd przenoszenia wiadomości e-mail do katalogu {folderName}. Błąd {ex.Message},{ex.InnerException}");
                return false;
            }

        }






        public List<Attachment> GetAttachements(string email, string password)
        {
            var messages = FetchAllMessages("imap.gmail.com", 993, false, email, password);
            var attachementsList = new List<Attachment>();
            foreach (var message in messages)
            {
                if (message?.Message?.Attachments != null)
                {
                    foreach (var attachement in message.Message.Attachments
                        .Where(x => x.Name.EndsWith(".xml")))
                    {
                        attachementsList.Add(attachement);
                    }
                }
               

            }

            return attachementsList;
        }

        public IEnumerable<EmailMessage> FetchAllMessages(string hostname, int port, bool useSsl, 
                                                                    string username, string password)
        {
            using (var client = new ImapClient(hostname, port, username, password, AuthMethod.Login, true))
            {
                //var uids = client.Search(SearchCondition.SentOn(DateTime.Today.AddDays(-21)));
                //client.CreateMailbox("Done");
                //client.MoveMessages(uids, "Szeran");
                var uids = client.Search(SearchCondition.All());
                var messagesList = new List<EmailMessage>();

                foreach (var uid in uids)
                {
                    var newMessage = new EmailMessage()
                    {
                        Uid = uid,
                    };
                    messagesList.Add(newMessage);
                }

                return messagesList;
            }
        }





    }
}
