﻿using CDNBase;
using JLR.Biz.Model;
using NLog;
using System;
using System.Collections.Generic;
using System.Diagnostics;

namespace JLR.Biz.Services
{
    public class OptimaProductService
    {
        public static string CreateProduct(Product product)
        {
            try
            {
                var session = new AdoSession();
                var towar = (CDNTwrb1.ITowar)session.CreateObject("CDN.Towary").AddNew();
                towar.Kod = product.Grupa.StartsWith("PRASA") ? product.Ean : product.Code;
                towar.Nazwa = product.Name;
                towar.EAN = product.Ean;
                towar.Stawka = product.Rate;
                towar.StawkaZak = product.Rate;
                towar.StawkaVatZak = product.Rate.ToString();
                towar.StawkaVat = product.Rate.ToString();
                towar.Flaga = product.Flag;
                towar.FlagaZak = product.Flag;
                towar.JM = "SZT";

                CDNTwrb1.ICena cena1 = towar.Ceny[4];
                cena1.Wartosc = Convert.ToDecimal(product.Price);

                //CDNTwrb1.ICena cena2 = towar.Ceny[2];
                //cena2.Wartosc = Convert.ToDecimal(product.PriceAfterDiscount);

                if (!string.IsNullOrWhiteSpace(product.Grupa))
                {
                    var grupa = (CDNTwrb1.TwrGrupa)session.CreateObject("CDN.TwrGrupy")[$"twg_kod = '{product.Grupa}'"];
                    towar.TwGGIDNumer = grupa.GIDNumer;
                }
                else
                {
                    var grupa = (CDNTwrb1.TwrGrupa)session.CreateObject("CDN.TwrGrupy")["twg_kod = 'MIGRATOR'"];
                    towar.TwGGIDNumer = grupa.GIDNumer;
                }
               
                session.Save();
                LogManager.GetCurrentClassLogger().Info($"Utworzyłem towar {product.Code}");
                Debug.WriteLine($"Utworzyłem towar {product.Code}");
                Console.WriteLine($"Utworzyłem towar {product.Code}");
                return towar.Kod;
            }
            catch (Exception ex)
            {
                LogManager.GetCurrentClassLogger().Error($"Nie udało się utworzyć towaru {product.Code}." +
                                                         $" Błąd: {ex}, {ex.InnerException}, {ex.Message}, {ex.StackTrace}");
                Console.WriteLine($"Nie udało się utworzyć towaru {product.Code}." +
                                                         $" Błąd: {ex}, {ex.InnerException}, {ex.Message}, {ex.StackTrace}");
                return string.Empty;
            }
        }

        public static bool IsProductInOptima(Product product)
        {
            var session = new AdoSession();
            try
            {
                 CDNTwrb1.ITowar towar;
                if (string.IsNullOrWhiteSpace(product.Ean))
                {
                    towar = (CDNTwrb1.ITowar)session.CreateObject("CDN.Towary").Item($"Twr_Kod = '{product.Code}'");
                }
                else
                {
                    towar = (CDNTwrb1.ITowar)session.CreateObject("CDN.Towary").Item($"Twr_EAN = '{product.Ean}'");
                }
                LogManager.GetCurrentClassLogger().Info("Znaleziono towaru o eanie" + product.Ean);
                Console.WriteLine("Znaleziono towaru o eanie" + product.Ean);

                CDNTwrb1.ICena cena1 = towar.Ceny[4];
                cena1.Wartosc = Convert.ToDecimal(product.Price);
                session.Save();
                Console.WriteLine("Uaktualniono cenę na towarze o eanie" + product.Ean);
                LogManager.GetCurrentClassLogger().Info("Uaktualniono cenę na towarze o eanie" + product.Ean);

                return true;
            }
            catch (Exception)
            {
                LogManager.GetCurrentClassLogger().Error($"Nie udało się znaleźć towaru w Optimie");
                Console.WriteLine("Nie znaleziono towaru o eanie" + product.Ean);

                return false;
            }
        }


        public static void CheckIfProductExists_UpdatePrice(List<Product> products)
        {
            foreach (var product in products)
            {
                if (!IsProductInOptima(product)) CreateProduct(product);
            }
        }

      
    }
}
