﻿using CDNBase;
using OptimaImporter.Common;

namespace JLR.Biz.Services
{
    public class OptimaLogging
    {
        private static IApplication _szeranApp;

        public static bool LogIn()
        {
           
                _szeranApp = new Application();
                _szeranApp.Login(ApplicationConfig.Config.OptimaUser,
                                 ApplicationConfig.Config.OptimaPassword,
                                 ApplicationConfig.Config.OptimaDatabase);

                return true;
           
        }

       
    }
}
