﻿using CDNBase;
using CDNHlmn;
using JLR.Biz.Model;
using NLog;
using System;
using System.Collections.Generic;
using System.Linq;

namespace JLR.Biz.Services
{
    public class OptimaDocumentService
    {
        public static bool IsInOptima(string number)
        {
            var session = new AdoSession();
            try
            {
                var product = (IDokumentHaMag)session.CreateObject("CDN.DokumentyHaMag")
                    .Item($"TrN_NumerObcy = '{number}'");
                LogManager.GetCurrentClassLogger().Info($"Dokument o numerze {number} istnieje już w Optimie.");
                Console.WriteLine($"Dokument o numerze {number} istnieje już w Optimie.");

                return true;
            }
            catch (Exception)
            {
                return false;
            }
        }

        public static bool CreateDoc(string fileName, DocHeader docHeader, List<Product> products)
        {
            try
            {
                var session = new AdoSession();

                var docs = (DokumentyHaMag)session.CreateObject("CDN.DokumentyHaMag");
                var doc = (IDokumentHaMag)docs.AddNew();

                var clients = (ICollection)(session.CreateObject("CDN.Kontrahenci"));
                CDNHeal.IKontrahent client;
                try
                {
                    switch (docHeader.InputFormat)
                    {
                        case InputFormat.GarmondDzienniki:
                            client = (CDNHeal.IKontrahent)clients["Knt_Kod='GARMOND PRESS'"];
                            break;
                        case InputFormat.GarmondCzasopisma:
                            client = (CDNHeal.IKontrahent)clients["Knt_Kod='GARMOND PRESS'"];
                            break;
                        case InputFormat.GarmondDziennikiZwroty:
                            client = (CDNHeal.IKontrahent)clients["Knt_Kod='GARMOND PRESS'"];
                            break;
                        case InputFormat.GarmondCzasopismaZwroty:
                            client = (CDNHeal.IKontrahent)clients["Knt_Kod='GARMOND PRESS'"];
                            break;
                        case InputFormat.King:
                            client = (CDNHeal.IKontrahent)clients["Knt_Kod='KING'"];
                            break;
                        default:
                    client = (CDNHeal.IKontrahent)clients["Knt_Kod='!NIEOKREŚLONY!'"];
                            break;
                    }
                }
                catch (Exception)
                {
                    client = (CDNHeal.IKontrahent)clients["Knt_Kod='!NIEOKREŚLONY!'"];
                    LogManager.GetCurrentClassLogger().Error($"Nie znaleziono kontrahenta Garmond Press. " +
                                                             $"Dokument utworzony na kontrahenta Nieokreślonego.");
                    Console.WriteLine($"Nie znaleziono kontrahenta Garmond Press. " +
                                                             $"Dokument utworzony na kontrahenta Nieokreślonego.");
                }
                

                doc.Rodzaj = Convert.ToInt32(docHeader.Gender);
                doc.TypDokumentu = Convert.ToInt32(docHeader.Type);
                switch (docHeader.InputFormat)
                {
                    case InputFormat.GarmondDzienniki:
                        doc.Bufor = 0;
                        break;
                    case InputFormat.GarmondCzasopisma:
                        doc.Bufor = 0;
                        break;
                    case InputFormat.GarmondDziennikiZwroty:
                        doc.Bufor = 1;
                        break;
                    case InputFormat.GarmondCzasopismaZwroty:
                        doc.Bufor = 1;
                        break;
                    case InputFormat.King:
                        doc.Bufor = 1;
                        break;
                    default:
                        doc.Bufor = 1;
                        break;
                }
                doc.DataDok = docHeader.Date;
                doc.DataWys = docHeader.Date;
                doc.DataSprzedazy = docHeader.Date;

                doc.Podmiot = client;
                //doc.TypNB = 2;

                doc.MagazynZrodlowyID = docHeader.WarehouseId;


                doc.NumerObcy = docHeader.Number;
                doc.Uwagi = $"import z pliku {fileName}.";

                if (!products.Any())
                {
                    doc.Uwagi += "Brak produktów w pliku.";
                }
                if (products.Any())
                {
                    foreach (var product in products)
                    {
                        var pozycje = doc.Elementy;
                        var pozycja = (IElementHaMag)pozycje.AddNew();
                        try
                        {
                            pozycja.TowarKod = product.Ean;
                        }
                        catch (Exception)
                        {
                            pozycja.TowarKod = product.Code;
                        }
                        pozycja.Ilosc = product.Amount;
                        pozycja.WartoscNetto = product.NetValue;
                    }
                }
                session.Save();
                LogManager.GetCurrentClassLogger().Info($"Utworzono dokument o numerze obcym {docHeader.Number}");
                Console.WriteLine($"Utworzono dokument o numerze obcym {docHeader.Number}");
                return true;
            }
            catch (Exception e)
            {
                LogManager.GetCurrentClassLogger().Error($"Nie udało się utworzyć dokumentu o numerze obcym {docHeader.Number}" 
                                                         + e.Message + e.InnerException);
                Console.WriteLine($"Nie udało się utworzyć dokumentu o numerze obcym {docHeader.Number}"
                                                         + e.Message + e.InnerException);
                return false;
            }
           
        }
    }
}
