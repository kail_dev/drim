﻿using CDNBase;
using JLR.Biz.Model;
using NLog;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;

namespace JLR.Biz.Services
{
    public class Run
    {
        private static IApplication _szeranApp;
        public static ILogin iLogin = null;

        public static void GetUids_GetMessageAndAttachements_CreateProduct_CreateDoc_MoveFile
                            (string optimaUser, string optimaPassword, string optimaDatabase,
                              string agencyName, string localCatalogPath, string email, string emailPassword, int warehouseId)
        {
            Console.WriteLine("Logowanie do Optimy.");
            _szeranApp = new Application();
            iLogin = _szeranApp.Login(optimaUser, optimaPassword, optimaDatabase);

            var kioskName = agencyName;
            var fileCatalogPath = $@"{localCatalogPath}\{kioskName}";
            if (!Directory.Exists($@"{fileCatalogPath}")) Directory.CreateDirectory($@"{fileCatalogPath}");
            if (!Directory.Exists($@"{fileCatalogPath}\Done")) Directory.CreateDirectory($@"{fileCatalogPath}\Done");
            if (!Directory.Exists($@"{fileCatalogPath}\Errors")) Directory.CreateDirectory($@"{fileCatalogPath}\Errors");

            var emailService = new EmailService();
            EmailConnectionParameters cp = new EmailConnectionParameters { Hostname = "imap.gmail.com", Port = 993, UseSsl = true, Username = email, Password = emailPassword };

            var attachements1 = DoGarmondDostawy(cp, fileCatalogPath);
            var attachements2 = DoKing(cp, fileCatalogPath);

            var attachements = new List<string>();
            attachements.AddRange(attachements1);
            attachements.AddRange(attachements2);

            foreach (var file in attachements)
            {
                var success = false;
                var fileWithoutPath = Path.GetFileName(file);
                var docHeader = file.EndsWith(".xml")
                        ? InputFileService.GetGarmondDocumentHeader(file)
                        : InputFileService.GetKingDocumentHeader(file);
                docHeader.WarehouseId = warehouseId;
                docHeader.Number = docHeader.Number.Replace("49778", "40378");

                var products = file.EndsWith(".xml")
                        ? InputFileService.ExtractProductInfoFromGarmondFile(file)
                        : InputFileService.ExtractProductInfoFromKingFile(file);

                if (products.Any())
                    OptimaProductService.CheckIfProductExists_UpdatePrice(products);

                var isDocInOptima = OptimaDocumentService.IsInOptima(docHeader.Number);
                if (!isDocInOptima)
                {
                    success = OptimaDocumentService.CreateDoc(fileWithoutPath, docHeader, products);
                }
                if (isDocInOptima)
                {
                    success = false;
                }

                try
                {
                    if (success)
                    {
                        if (File.Exists($@"{fileCatalogPath}\Done\{fileWithoutPath}"))
                            File.Delete($@"{fileCatalogPath}\Done\{fileWithoutPath}");
                        //przesuń pliki do folderów
                        File.Move($@"{fileCatalogPath}\{fileWithoutPath}", $@"{fileCatalogPath}\Done\{fileWithoutPath}");

                        //przesuń wiadomości
                        //emailService.MoveMessagesToFolders(cp, uid, "Done");
                    }
                }
                catch (Exception ex)
                {
                    LogManager.GetCurrentClassLogger().Error($"Błąd przenoszenia: {ex.Message},{ex.InnerException}");
                }

            }

            Console.WriteLine("Koniec.");
            try
            {
                iLogin = null;
                _szeranApp.UnlockApp();
                _szeranApp = null;
            }
            catch (Exception)
            {
                iLogin = null;
                _szeranApp = null;
            }
            Console.WriteLine("Wylogowanie z Optimy.");
            LogManager.GetCurrentClassLogger().Info("Wylogowanie z Optimy.");
            LogManager.GetCurrentClassLogger().Info("Koniec.");
        }

        private static IList<string> DoGarmondDostawy(EmailConnectionParameters cp, string filePath)
        {
            var attachements = new List<string>();

            var emailService = new EmailService();
            var uids = emailService.FetchUids(cp, "inwentaryzacja.pp@garmondpress.pl", "dostawy").OrderByDescending(m => m).Take(2);
            foreach (var garmondUid in uids)
            {
                var message = emailService.FetchMessageAndAttachements(cp, garmondUid);
                attachements.Add(emailService.DownloadAttachements(message.Attachements, filePath, ".xml").FirstOrDefault());
            }

            return attachements;
        }

        private static IList<string> DoKing(EmailConnectionParameters cp, string filePath)
        {
            var attachements = new List<string>();

            var emailService = new EmailService();
            var uids = emailService.FetchUids(cp, "raporty@kinghurt.pl", "Dokumenty").ToList();
            if (!uids.Any()) return attachements;
            var message = emailService.FetchMessageAndAttachements(cp, uids.OrderByDescending(m => m).FirstOrDefault());
            attachements = emailService.DownloadAttachements(message.Attachements, filePath, ".txt4");
            return attachements;
        }
    }
}
