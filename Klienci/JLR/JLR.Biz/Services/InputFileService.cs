﻿using JLR.Biz.Model;
using System;
using System.Collections.Generic;
using System.Globalization;
using System.IO;
using System.Linq;
using System.Xml.Linq;

namespace JLR.Biz.Services
{
    public class InputFileService
    {
        public static DocHeader GetGarmondDocumentHeader(string file)
        {
            var doc = XDocument.Load(file);
            var docHeader = new DocHeader();
            var pozycje = doc.Descendants().Where(p => p.Name.LocalName == "NAGLOWEK");
            foreach (var xElement in pozycje)
            {
                docHeader.Type = Convert.ToInt32(xElement.Descendants()
                    .FirstOrDefault(p => p.Name.LocalName == "TYP_DOKUMENTU")?.Value);
                docHeader.Gender = Convert.ToInt32(xElement.Descendants()
                    .FirstOrDefault(p => p.Name.LocalName == "RODZAJ_DOKUMENTU")?.Value);
                docHeader.Number = xElement.Descendants()
                    .FirstOrDefault(p => p.Name.LocalName == "NUMER_PELNY")?.Value;
                docHeader.Date = Convert.ToDateTime(xElement.Descendants()
                    .FirstOrDefault(p => p.Name.LocalName == "DATA_DOKUMENTU")?.Value);
            }

            return docHeader;
        }

        public static List<Product> ExtractProductInfoFromGarmondFile(string file)
        {
            var doc = XDocument.Load(file);
            var pozycje = doc.Descendants().Where(p => p.Name.LocalName == "POZYCJA");
            var products = new List<Product>();
            foreach (var xElement in pozycje)
            {
                var product = new Product();
                product.Code = xElement.Descendants().FirstOrDefault(p => p.Name.LocalName == "KOD")?.Value;
                product.Name = xElement.Descendants().FirstOrDefault(p => p.Name.LocalName == "NAZWA")?.Value;
                product.Ean = xElement.Descendants().FirstOrDefault(p => p.Name.LocalName == "EAN")?.Value;
                product.Rate = Convert.ToDecimal(xElement.Descendants().FirstOrDefault(p => p.Name.LocalName == "STAWKA")?.Value);
                product.Flag = Convert.ToInt32(xElement.Descendants().FirstOrDefault(p => p.Name.LocalName == "FLAGA")?.Value);
                product.Amount = Convert.ToDouble(xElement.Descendants().FirstOrDefault(p => p.Name.LocalName == "ILOSC")?.Value);
                product.Price = Convert.ToDecimal(xElement.Descendants()
                    .FirstOrDefault(p => p.Name.LocalName == "POCZATKOWA_WAL_CENNIKA")?.Value.Replace(".", ","));
                product.PriceAfterDiscount = Convert.ToDecimal(xElement.Descendants()
                    .FirstOrDefault(p => p.Name.LocalName.StartsWith("PO_RABACIE_PLN"))?.Value.Replace(".", ","));
                product.NetValue = Convert.ToDecimal(xElement.Descendants()
                    .FirstOrDefault(p => p.Name.LocalName.StartsWith("WARTOSC_NETTO"))?.Value.Replace(".", ","));
                product.GrossValue = Convert.ToDecimal(xElement.Descendants()
                    .FirstOrDefault(p => p.Name.LocalName.StartsWith("WARTOSC_BRUTTO"))?.Value.Replace(".", ","));
                product.Grupa = file.Split('\\').Last().Substring(1, 1) == "D" ? "PRASA - DZIENNIKI" : "PRASA - TYGODNIKI";
                products.Add(product);
            }

            return products;
        }

        public static DocHeader GetKingDocumentHeader(string file)
        {
            var docHeader = new DocHeader
            {
                Gender = 307000,
                Type = 307
            };
            var fileNameContent = Path.GetFileNameWithoutExtension(file).Split('_');
            docHeader.Date = Convert.ToDateTime(DateTime.ParseExact(fileNameContent[4], "yyyyMMdd", CultureInfo.InvariantCulture));
            docHeader.Number = $"{fileNameContent[0]}/{fileNameContent[1]}/{fileNameContent[2]}";
            docHeader.InputFormat = InputFormat.King;

            return docHeader;
        }

        public static List<Product> ExtractProductInfoFromKingFile(string file)
        {
            var products = new List<Product>();

            var lines = File.ReadAllLines(file);

            foreach (string line in lines.Where(l => l.Length > 3))
            {
                var elements = line.Split(':');
                var product = new Product();
                product.Code = elements[2].Trim();
                product.Name = elements[4];
                product.Ean = elements[2].Trim();
                product.Rate = 23m;
                product.Flag = 2;
                product.Amount = Convert.ToDouble(elements[9].Replace(".", ",")) * 10;
                product.NetValue = Convert.ToDecimal(elements[6].Replace(".", ","));
                product.Grupa = "PAPIEROSY";
                products.Add(product);

                if (string.IsNullOrWhiteSpace(product.Code))
                {
                    product.Code = product.Name;
                    product.Ean = "";
                }
            }

            return products;
        }
    }
}

