﻿namespace JLR.Biz.Model
{
    public class Product
    {
        public string Code { get; set; }
        public string Name { get; set; }
        public string Ean { get; set; }
        public decimal Rate { get; set; }
        public int Flag { get; set; }
        public double Amount { get; set; }
        public decimal Price { get; set; }
        public decimal PriceAfterDiscount { get; set; }
        public decimal NetValue { get; set; }
        public decimal GrossValue { get; set; }
        public string Grupa { get; set; }
    }
}
