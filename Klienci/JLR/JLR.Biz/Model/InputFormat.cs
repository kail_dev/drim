﻿namespace JLR.Biz.Model
{
    public enum InputFormat
    {
        GarmondDzienniki,
        GarmondCzasopisma,
        GarmondDziennikiZwroty,
        GarmondCzasopismaZwroty,
        King
    }
}