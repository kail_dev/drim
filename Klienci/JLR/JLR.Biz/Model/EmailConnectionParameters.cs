﻿namespace JLR.Biz.Model
{
    public class EmailConnectionParameters
    {
        //string hostname, int port, bool useSsl, string username, string password
        public string Hostname { get; set; }
        public int Port { get; set; }
        public bool UseSsl { get; set; }
        public string Username { get; set; }
        public string Password { get; set; }
    }
}
