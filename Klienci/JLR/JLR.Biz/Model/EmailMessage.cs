﻿using System.Collections.Generic;
using System.Net.Mail;

namespace JLR.Biz.Model
{
    public class EmailMessage
    {
        public uint Uid { get; set; }
        public MailMessage Message { get; set; }
        public List<Attachment> Attachements { get; set; }
    }
}
