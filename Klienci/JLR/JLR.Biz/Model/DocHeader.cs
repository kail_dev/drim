﻿using System;

namespace JLR.Biz.Model
{
    public class DocHeader
    {
        public string Number { get; set; }
        public DateTime Date { get; set; }
        public int Type { get; set; }
        public int Gender { get; set; }
        public InputFormat InputFormat { get; set; }
        public int WarehouseId { get; set; }
    }
}
