﻿using NLog;
using Santa.Biz.Model;
using System;
using System.Collections.Generic;
using System.Data;
using System.Data.OleDb;
using System.Diagnostics;
using System.Globalization;
using System.IO;
using System.Linq;
using System.Text;
using System.Xml.Linq;

namespace Santa.Biz.Services
{
    public static class SourceFileReader
    {
        public static List<Invoice> ReadJpkFaInvoices(string sourcePath)
        {
            var invoices = new List<Invoice>();
            var xdoc = XDocument.Load(sourcePath);

            var validInvoicesCounter = 0;
            foreach (var xElement in xdoc.Descendants().Where(x => x.Name.LocalName == "Faktura"))
            {
                var invoice = new Invoice
                {
                    InvoiceDate = Convert.ToDateTime(
                        xElement.Elements()
                            .FirstOrDefault(x => x.Name.LocalName.ToString()
                                .StartsWith("P_1"))?.Value),
                    Number = xElement.Elements()
                        .FirstOrDefault(x => x.Name.LocalName.ToString()
                            .StartsWith("P_2A"))?.Value,
                    BuyerName = xElement.Elements()
                        .FirstOrDefault(x => x.Name.LocalName.ToString()
                            .StartsWith("P_3A"))?.Value,
                    BuyerAddress = xElement.Elements()
                        .FirstOrDefault(x => x.Name.LocalName.ToString()
                            .StartsWith("P_3B"))?.Value,
                    SellerName = xElement.Elements()
                        .FirstOrDefault(x => x.Name.LocalName.ToString()
                            .StartsWith("P_3C"))?.Value,
                    SellerAddress = xElement.Elements()
                        .FirstOrDefault(x => x.Name.LocalName.ToString()
                            .StartsWith("P_3D"))?.Value,
                    BuyerTaxNumberCountry = xElement.Elements()
                        .FirstOrDefault(x => x.Name.LocalName.ToString()
                            .StartsWith("P_5A"))?.Value,
                    BuyerTaxNumber = xElement.Elements()
                        .FirstOrDefault(x => x.Name.LocalName.ToString()
                            .StartsWith("P_5B"))?.Value,
                    SellerTaxNumberCountry = xElement.Elements()
                        .FirstOrDefault(x => x.Name.LocalName.ToString()
                            .StartsWith("P_4A"))?.Value,
                    SellerTaxNumber = xElement.Elements()
                        .FirstOrDefault(x => x.Name.LocalName.ToString()
                            .StartsWith("P_4B"))?.Value,
                    PaymentDate = Convert.ToDateTime(
                        xElement.Elements()
                            .FirstOrDefault(x => x.Name.LocalName.ToString()
                                .StartsWith("P_6"))?.Value),
                    NetValue = Convert.ToDecimal(
                        xElement.Elements()
                            .FirstOrDefault(x => x.Name.LocalName.ToString()
                                .StartsWith("P_13_1"))?.Value.Replace(".", ",")),
                    TaxValue = Convert.ToDecimal(
                        xElement.Elements()
                            .FirstOrDefault(x => x.Name.LocalName.ToString()
                                .StartsWith("P_14_1"))?.Value.Replace(".", ",")),
                    GrossValue = Convert.ToDecimal(
                        xElement.Elements()
                            .FirstOrDefault(x => x.Name.LocalName.ToString()
                                .StartsWith("P_15"))?.Value.Replace(".", ","))
                };
                validInvoicesCounter++;
                Debug.WriteLine(validInvoicesCounter);
                invoices.Add(invoice);
            }

            var validRowsCounter = 0;
            foreach (var xElement in xdoc.Descendants().Where(x => x.Name.LocalName == "FakturaWiersz"))
            {
                var invoiceNumberFromRow = xElement.Elements()
                    .FirstOrDefault(x => x.Name.LocalName.ToString()
                        .StartsWith("P_2B"))?.Value; //P_2B
                var invoice = invoices.FirstOrDefault(x => x.Number == invoiceNumberFromRow);
                if (invoice != null)
                {
                    try
                    {
                        var netValue = Convert.ToDecimal(
                            xElement.Elements()
                                .FirstOrDefault(x => x.Name.LocalName.ToString()
                                    .StartsWith("P_11"))?.Value.Replace(".", ","));
                        var vatRate = xElement.Elements()
                            .FirstOrDefault(x => x.Name.LocalName.ToString()
                                .StartsWith("P_12"))?.Value;
                        var grossValue = Convert.ToDecimal(
                            xElement.Elements()
                                .FirstOrDefault(x => x.Name.LocalName.ToString()
                                    .StartsWith("P_11A"))?.Value.Replace(".", ","));

                        invoice.Rows.Add(new InvoiceRow
                        {
                            NetValue = netValue,
                            VatRate = vatRate,
                            GrossValue = grossValue
                        });

                        validRowsCounter++;
                        Debug.WriteLine(validRowsCounter);
                    }
                    catch (Exception)
                    {
                        //Debug.WriteLine($"Nie udało się odczytać wiersza dla faktury: {invoiceNumberFromRow}, {ex}");
                    }
                }
            }

            return invoices;
        }

        public static List<Invoice> ReadSubiektInvoices(string sourcePath, string invoiceType)
        {
            var invoices = new List<Invoice>();
            var fileContent = File.ReadAllLines(sourcePath, Encoding.GetEncoding("Windows-1250"))
                .Where(x => x.Length > 0).ToList();
            for (var i = 0; i < fileContent.Count - 1; i++)
            {
                var line = fileContent[i];
                var nextLine = fileContent[i + 1];
                if (line.StartsWith("[NAGLOWEK]") && nextLine.StartsWith($"\"{invoiceType}"))
                {
                    var invoiceContent = fileContent[i + 1];
                    Debug.WriteLine($"{i}:  {invoiceContent}");
                    string[] invoiceHeadElements = null;
                    var invoiceRows = new List<InvoiceRow>();
                    var invoiceHeadLinesCounter = 0;
                    var lineIPlus2 = fileContent[i + 2];

                    //if (1 == 2)
                    //{
                    //    if (!lineIPlus2.StartsWith("["))
                    //    {
                    //        invoiceContent += lineIPlus2;
                    //        var invoiceHeadElementsList = invoiceContent.Split(',').ToList();
                    //        invoiceHeadElements = invoiceHeadElementsList.ToArray();
                    //        invoiceHeadLinesCounter++;
                    //        var lineIPlus3 = fileContent[i + 3];
                    //        if (!lineIPlus3.StartsWith("["))
                    //        {
                    //            invoiceContent += lineIPlus3;
                    //            invoiceHeadElementsList = invoiceContent.Split(',').ToList();
                    //            invoiceHeadElements = invoiceHeadElementsList.ToArray();
                    //            invoiceHeadLinesCounter++;
                    //            var lineIPlus4 = fileContent[i + 4];
                    //            if (!lineIPlus4.StartsWith("["))
                    //            {
                    //                invoiceContent += lineIPlus4;
                    //                invoiceHeadElementsList = invoiceContent.Split(',').ToList();
                    //                invoiceHeadElements = invoiceHeadElementsList.ToArray();
                    //                invoiceHeadLinesCounter++;
                    //                var lineIPlus5 = fileContent[i + 5];
                    //                if (!lineIPlus5.StartsWith("["))
                    //                {
                    //                    invoiceContent += lineIPlus5;
                    //                    invoiceHeadElementsList = invoiceContent.Split(',').ToList();
                    //                    invoiceHeadElements = invoiceHeadElementsList.ToArray();
                    //                    invoiceHeadLinesCounter++;
                    //                }
                    //            }
                    //        }
                    //    }
                    //}

                    var nextLineCounter = 3;
                    while (!lineIPlus2.StartsWith("["))
                    {

                        invoiceContent += lineIPlus2;
                        var invoiceHeadElementsList = invoiceContent.Split(',').ToList();
                        invoiceHeadElements = invoiceHeadElementsList.ToArray();
                        invoiceHeadLinesCounter++;
                        lineIPlus2 = fileContent[i + nextLineCounter];
                        nextLineCounter++;
                    }

                    if (fileContent[i + 2 + invoiceHeadLinesCounter].StartsWith("[ZAW"))
                    {
                        var j = i;
                        while (fileContent.Count > j + 3 + invoiceHeadLinesCounter
                               && !fileContent[j + 3 + invoiceHeadLinesCounter].StartsWith("[N"))
                        {
                            try
                            {
                                var invoiceRowElements = fileContent[j + 3 + invoiceHeadLinesCounter].Split(',');
                                try
                                {
                                    var invoiceRow = new InvoiceRow
                                    {
                                        NetValue = Convert.ToDecimal(invoiceRowElements[16], new CultureInfo("en-US")),
                                        VatRate = invoiceRowElements[15].Replace("\"", ""),
                                        GrossValue = Convert.ToDecimal(invoiceRowElements[18], new CultureInfo("en-US"))
                                    };
                                    if (invoiceRow.VatRate == "ue")
                                    {
                                        invoiceRow.VatRate = "0";
                                    }

                                    invoiceRows.Add(invoiceRow);
                                }
                                catch (Exception)
                                {
                                    var invoiceRow = new InvoiceRow
                                    {
                                        NetValue = Convert.ToDecimal(invoiceRowElements[2], new CultureInfo("en-US")),
                                        VatRate = invoiceRowElements[0].Replace("\"", ""),
                                        GrossValue = Convert.ToDecimal(invoiceRowElements[4], new CultureInfo("en-US"))
                                    };
                                    if (invoiceRow.VatRate == "ue")
                                    {
                                        invoiceRow.VatRate = "0";
                                    }

                                    invoiceRows.Add(invoiceRow);
                                }
                            }
                            catch (Exception ex)
                            {
                                LogManager.GetCurrentClassLogger()
                                    .Info(
                                        $"Nie udało się odczytać wiersza dla faktury {invoiceContent[6]}, błąd: {ex}");
                            }
                            finally
                            {
                                j++;
                            }
                        }
                    }

                    var invoiceContentTmp = invoiceContent.Split(',').ToList();
                    if (invoiceContent.StartsWith("\"FZ"))
                    {
                        if (invoiceContentTmp.Count > 22
                            && !invoiceContentTmp[21].Replace("\"", "").StartsWith("201")
                            && invoiceContentTmp[22].Replace("\"", "").StartsWith("201"))
                        {
                            invoiceContentTmp.RemoveAt(13);
                            invoiceHeadElements = invoiceContentTmp.ToArray();
                        }

                        if (invoiceContentTmp.Count > 20 && invoiceContentTmp[20].Replace("\"", "").StartsWith("201"))
                        {
                            invoiceContentTmp.Insert(20, "");
                            invoiceHeadElements = invoiceContentTmp.ToArray();
                        }

                        if (invoiceContentTmp.Count > 33 && invoiceContentTmp[33].Replace("\"", "").StartsWith("201"))
                        {
                            invoiceContentTmp.Insert(31, "");
                            invoiceHeadElements = invoiceContentTmp.ToArray();
                        }

                        invoiceHeadElements = invoiceHeadElements ?? invoiceContentTmp.ToArray();
                    }

                    if (invoiceContent.StartsWith("\"FS"))
                    {
                        if (invoiceContentTmp.Count > 24
                            && !invoiceContentTmp[23].Replace("\"", "").StartsWith("201")
                            && invoiceContentTmp[24].Replace("\"", "").StartsWith("201"))
                        {
                            invoiceContentTmp.RemoveAt(13);
                            invoiceContentTmp.RemoveAt(16);
                            invoiceHeadElements = invoiceContentTmp.ToArray();
                        }

                        if (invoiceContentTmp.Count > 23
                            && !invoiceContentTmp[22].Replace("\"", "").StartsWith("201")
                            && invoiceContentTmp[23].Replace("\"", "").StartsWith("201"))
                        {
                            invoiceContentTmp.RemoveAt(1);
                            invoiceHeadElements = invoiceContentTmp.ToArray();
                        }

                        if (sourcePath.Contains("magazyn"))
                        {
                            invoiceContentTmp.RemoveAt(21);
                            invoiceHeadElements = invoiceContentTmp.ToArray();
                        }

                        invoiceHeadElements = invoiceHeadElements ?? invoiceContentTmp.ToArray();
                    }

                    if (invoiceContent.StartsWith($"\"{invoiceType}"))
                    {
                        try
                        {
                            if (invoiceType == "FZ")
                            {
                                var invoiceDateYear =
                                    Convert.ToInt32(invoiceHeadElements[21].Replace("\"", "").Substring(0, 4));
                                var invoiceDateMonth =
                                    Convert.ToInt32(invoiceHeadElements[21].Replace("\"", "").Substring(4, 2));
                                var invoiceDateDay =
                                    Convert.ToInt32(invoiceHeadElements[21].Replace("\"", "").Substring(6, 2));
                                var paymentDateYear =
                                    Convert.ToInt32(invoiceHeadElements[34].Replace("\"", "").Substring(0, 4));
                                var paymentDateMonth =
                                    Convert.ToInt32(invoiceHeadElements[34].Replace("\"", "").Substring(4, 2));
                                var paymentDateDay =
                                    Convert.ToInt32(invoiceHeadElements[34].Replace("\"", "").Substring(6, 2));
                                var invoice = new Invoice
                                {
                                    Number = invoiceHeadElements[4].Replace("\"", ""),
                                    SellerName = invoiceHeadElements[12].Replace("\"", ""),
                                    City = invoiceHeadElements[14].Replace("\"", ""),
                                    PostalCode = invoiceHeadElements[15].Replace("\"", ""),
                                    Street = invoiceHeadElements[16].Replace("\"", ""),
                                    SellerTaxNumber = invoiceHeadElements[17].Replace("\"", ""),
                                    InvoiceDate = new DateTime(Convert.ToInt32(invoiceDateYear),
                                        Convert.ToInt32(invoiceDateMonth),
                                        Convert.ToInt32(invoiceDateDay)
                                    ),
                                    PaymentDate = new DateTime(Convert.ToInt32(paymentDateYear),
                                        Convert.ToInt32(paymentDateMonth),
                                        Convert.ToInt32(paymentDateDay)
                                    ),
                                    Currency = invoiceHeadElements[invoiceHeadElements.Length - 16].Replace("\"", ""),
                                    CurrencyRate =
                                        invoiceHeadElements[invoiceHeadElements.Length - 15].Replace("\"", ""),
                                    Rows = invoiceRows
                                };
                                invoices.Add(invoice);
                            }

                            if (invoiceType == "FS")
                            {
                                var invoiceDateYear =
                                    Convert.ToInt32(invoiceHeadElements[21].Replace("\"", "").Substring(0, 4));
                                var invoiceDateMonth =
                                    Convert.ToInt32(invoiceHeadElements[21].Replace("\"", "").Substring(4, 2));
                                var invoiceDateDay =
                                    Convert.ToInt32(invoiceHeadElements[21].Replace("\"", "").Substring(6, 2));
                                var paymentDateYear =
                                    Convert.ToInt32(invoiceHeadElements[34].Replace("\"", "").Substring(0, 4));
                                var paymentDateMonth =
                                    Convert.ToInt32(invoiceHeadElements[34].Replace("\"", "").Substring(4, 2));
                                var paymentDateDay =
                                    Convert.ToInt32(invoiceHeadElements[34].Replace("\"", "").Substring(6, 2));
                                var invoice = new Invoice
                                {
                                    Number = invoiceHeadElements[6].Replace("\"", ""),
                                    BuyerName = invoiceHeadElements[12].Replace("\"", ""),
                                    City = invoiceHeadElements[14].Replace("\"", ""),
                                    PostalCode = invoiceHeadElements[15].Replace("\"", ""),
                                    Street = invoiceHeadElements[16].Replace("\"", ""),
                                    BuyerTaxNumber = invoiceHeadElements[17].Replace("\"", ""),
                                    InvoiceDate = new DateTime(Convert.ToInt32(invoiceDateYear),
                                        Convert.ToInt32(invoiceDateMonth),
                                        Convert.ToInt32(invoiceDateDay)
                                    ),
                                    PaymentDate = new DateTime(Convert.ToInt32(paymentDateYear),
                                        Convert.ToInt32(paymentDateMonth),
                                        Convert.ToInt32(paymentDateDay)
                                    ),
                                    Currency = invoiceHeadElements[invoiceHeadElements.Length - 16].Replace("\"", ""),
                                    CurrencyRate =
                                        invoiceHeadElements[invoiceHeadElements.Length - 15].Replace("\"", ""),
                                    Rows = invoiceRows
                                };
                                invoices.Add(invoice);
                            }
                        }
                        catch (Exception ex)
                        {
                            LogManager.GetCurrentClassLogger()
                                .Info(
                                    $"Nie udało się utworzyć faktury sprzedaży nr {invoiceHeadElements[6]}, błąd: {ex}");
                        }
                    }
                }
                else if (invoiceType == "FS" && line.StartsWith($"\"{invoiceType} "))
                {
                    var invoice = new Invoice {Number = "pusty"};
                    try
                    {
                        var lineElements = line.Split(',');
                        var invoiceNumber = lineElements[0].Replace("\"", "").Replace("FS ", "");
                        invoice = invoices.FirstOrDefault(inv => inv.Number == invoiceNumber);

                        var invoiceDateYear = Convert.ToInt32(lineElements[1].Replace("\"", "").Substring(0, 4));
                        var invoiceDateMonth = Convert.ToInt32(lineElements[1].Replace("\"", "").Substring(4, 2));
                        var invoiceDateDay = Convert.ToInt32(lineElements[1].Replace("\"", "").Substring(6, 2));

                        invoice.OperationDate = new DateTime(invoiceDateYear, invoiceDateMonth, invoiceDateDay);

                    }
                    catch (Exception ex)
                    {
                        LogManager.GetCurrentClassLogger()
                            .Info($"Nie udało się podmienić daty sprzedaży dla faktury sprzedaży, błąd: {ex}");
                    }
                }
            }

            return invoices;
        }

        public static List<InvoiceSanta> ReadSantaInvoices(string sourcePath)
        {
            var invoices = new List<InvoiceSanta>();

            //var licznik = 0;

            if (!string.IsNullOrWhiteSpace(sourcePath))
            {
                var excelConnectionString =
                    $"Provider=Microsoft.ACE.OLEDB.12.0;Data Source={sourcePath};Extended Properties=\"Excel 12.0 Xml;IMEX=1\"";
                using (var connection = new OleDbConnection(excelConnectionString))
                {
                    var oleDbQuery = "Select * FROM [Arkusz1$]";

                    var command = new OleDbCommand(oleDbQuery, connection);

                    var ds = new DataSet("ds");
                    try
                    {
                        var oleDbDataAdapter = new OleDbDataAdapter(command);
                        try
                        {
                            oleDbDataAdapter.Fill(ds, "MojaTabela");
                            LogManager.GetCurrentClassLogger()
                                .Info($"Liczba odczytanych wierszy: {ds.Tables[0]?.Rows.Count}");
                        }
                        catch (Exception ex)
                        {
                            LogManager.GetCurrentClassLogger()
                                .Info("Wypełnienie obiektu DataSet danymi z arkusza nie powiodło się. " + ex);
                        }

                        if (ds.Tables[0]?.Rows.Count > 0)
                        {
                            foreach (DataRow item in ds.Tables["MojaTabela"].Rows)
                            {
                                var nip = item.ItemArray[0].ToString().Replace("-", "");
                                if (!string.IsNullOrWhiteSpace(nip) &&
                                    !invoices.Select(i => i.BuyerTaxNumber).Contains(nip))
                                {
                                    var invoice = new InvoiceSanta
                                    {
                                        BuyerTaxNumber = nip,
                                        Series = item.ItemArray[7].ToString(),
                                    };
                                    invoice.RowsSanta.Add(new InvoiceRowSanta
                                    {
                                        ItemCode = item.ItemArray[8].ToString(),
                                        NetValue = Convert.ToDecimal(item.ItemArray[9].ToString())
                                    });
                                    //double ilosc;
                                    //double.TryParse(item.ItemArray[1].ToString(), out ilosc);
                                    //plik.Ilosc = ilosc;
                                    //if (item.ItemArray.Length > 2)
                                    //{
                                    //    plik.Cena = Convert.ToDecimal(item.ItemArray[2].ToString());
                                    //}
                                    //listaFzZPliku.Add(plik);

                                    invoices.Add(invoice);
                                }
                                else
                                {
                                    invoices.Last().RowsSanta.Add(new InvoiceRowSanta
                                    {
                                        ItemCode = item.ItemArray[8].ToString(),
                                        NetValue = Convert.ToDecimal(item.ItemArray[9].ToString())
                                    });
                                }
                            }
                        }
                    }
                    catch (Exception ex)
                    {
                        LogManager.GetCurrentClassLogger().Info("Nie udało się wczytać dokumentu..." + ex);
                    }
                }
            }
            else
            {
                LogManager.GetCurrentClassLogger().Info("Wczytany arkusz Excela nie zawierał pozycji...");
            }

            return invoices;
        }

        public static List<InvoiceSanta> ReadSantaInvoicesPmp(string sourcePath)
        {
            var invoices = new List<InvoiceSanta>();

            //var licznik = 0;

            if (!string.IsNullOrWhiteSpace(sourcePath))
            {
                var excelConnectionString =
                    $"Provider=Microsoft.ACE.OLEDB.12.0;Data Source={sourcePath};Extended Properties=\"Excel 12.0 Xml;IMEX=1\"";
                using (var connection = new OleDbConnection(excelConnectionString))
                {
                    var oleDbQuery = "Select * FROM [Arkusz1$]";

                    var command = new OleDbCommand(oleDbQuery, connection);

                    var ds = new DataSet("ds");
                    try
                    {
                        var oleDbDataAdapter = new OleDbDataAdapter(command);
                        try
                        {
                            oleDbDataAdapter.Fill(ds, "MojaTabela");
                            LogManager.GetCurrentClassLogger()
                                .Info($"Liczba odczytanych wierszy: {ds.Tables[0]?.Rows.Count}");
                        }
                        catch (Exception ex)
                        {
                            LogManager.GetCurrentClassLogger()
                                .Info("Wypełnienie obiektu DataSet danymi z arkusza nie powiodło się. " + ex);
                        }

                        if (ds.Tables[0]?.Rows.Count > 0)
                        {
                            var previousReceiverCode = "";
                            foreach (DataRow item in ds.Tables["MojaTabela"].Rows)
                            {
                                var netValue = item.ItemArray[6].ToString();
                                if (!string.IsNullOrWhiteSpace(netValue) &&
                                    previousReceiverCode != item.ItemArray[2].ToString())
                                {
                                    var invoice = new InvoiceSanta
                                    {
                                        BuyerCode = item.ItemArray[1].ToString(),
                                        ReceiverCode = item.ItemArray[2].ToString(),
                                        Series = item.ItemArray[3].ToString(),
                                    };
                                    invoice.RowsSanta.Add(new InvoiceRowSanta
                                    {
                                        ItemCode = item.ItemArray[4].ToString(),
                                        NetValue = string.IsNullOrWhiteSpace(item.ItemArray[6].ToString())
                                            ? 0
                                            : Convert.ToDecimal(item.ItemArray[6].ToString()),
                                        Amount = string.IsNullOrWhiteSpace(item.ItemArray[7].ToString())
                                            ? 0
                                            : Convert.ToDouble(item.ItemArray[7].ToString()),
                                        Price = string.IsNullOrWhiteSpace(item.ItemArray[8].ToString())
                                            ? 0
                                            : Convert.ToDecimal(item.ItemArray[8].ToString())
                                    });
                                    //double ilosc;
                                    //double.TryParse(item.ItemArray[1].ToString(), out ilosc);
                                    //plik.Ilosc = ilosc;
                                    //if (item.ItemArray.Length > 2)
                                    //{
                                    //    plik.Cena = Convert.ToDecimal(item.ItemArray[2].ToString());
                                    //}
                                    //listaFzZPliku.Add(plik);

                                    invoices.Add(invoice);
                                }
                                else if (!string.IsNullOrWhiteSpace(netValue) &&
                                         previousReceiverCode == item.ItemArray[2].ToString())
                                {
                                    invoices.Last().RowsSanta.Add(new InvoiceRowSanta
                                    {
                                        ItemCode = item.ItemArray[4].ToString(),
                                        NetValue = string.IsNullOrWhiteSpace(item.ItemArray[6].ToString())
                                            ? 0
                                            : Convert.ToDecimal(item.ItemArray[6].ToString()),
                                        Amount = string.IsNullOrWhiteSpace(item.ItemArray[7].ToString())
                                            ? 0
                                            : Convert.ToDouble(item.ItemArray[7].ToString()),
                                        Price = string.IsNullOrWhiteSpace(item.ItemArray[8].ToString())
                                            ? 0
                                            : Convert.ToDecimal(item.ItemArray[8].ToString())
                                    });
                                }

                                previousReceiverCode = item.ItemArray[1].ToString();
                            }
                        }
                    }
                    catch (Exception ex)
                    {
                        LogManager.GetCurrentClassLogger().Info("Nie udało się wczytać dokumentu..." + ex);
                    }
                }
            }
            else
            {
                LogManager.GetCurrentClassLogger().Info("Wczytany arkusz Excela nie zawierał pozycji...");
            }

            return invoices;
        }

        public static List<Invoice> ReadFakturusCsvFile(string sourcePath)
        {
            var invoices = new List<Invoice>();
            var lineCounter = 0;
            using (var file = new StreamReader(sourcePath, Encoding.Unicode))
            {
                while (!file.EndOfStream)
                {
                    var invoice = new Invoice();
                    try
                    {
                        lineCounter++;
                        var line = file.ReadLine();
                        if (line != null)
                        {
                            var lineElementsList = line.Split(';');
                            if (!line.StartsWith("Numer umowy"))
                            {
                                invoice.Number = lineElementsList[0];
                                invoice.BuyerName = lineElementsList[1];

                                var address = lineElementsList[2].Split(',');
                                invoice.City = address[0];
                                invoice.BuyerAddress = address[1].TrimStart();

                                invoice.InvoiceDate = new DateTime(Convert.ToInt32(lineElementsList[3].Substring(6, 4)),
                                    Convert.ToInt32(lineElementsList[3].Substring(3, 2)),
                                    Convert.ToInt32(lineElementsList[3].Substring(0, 2)));
                                if (string.IsNullOrWhiteSpace(lineElementsList[4]))
                                {
                                    invoice.PaymentDate = invoice.InvoiceDate;
                                }
                                else
                                {
                                    invoice.PaymentDate = new DateTime(
                                        Convert.ToInt32(lineElementsList[4].Substring(6, 4)),
                                        Convert.ToInt32(lineElementsList[4].Substring(3, 2)),
                                        Convert.ToInt32(lineElementsList[4].Substring(0, 2)));
                                }

                                invoice.NetValue = Convert.ToDecimal(lineElementsList[5]);
                                invoices.Add(invoice);
                            }
                        }
                    }
                    catch (Exception ex)
                    {
                        if (ex.InnerException != null)
                            LogManager.GetCurrentClassLogger().Error($"Błąd w odczycie pliku. Błąd: {ex}; " +
                                                                     $"{ex.InnerException.InnerException}");
                    }
                }
            }

            LogManager.GetCurrentClassLogger().Info("Odczytano plik");
            return invoices;
        }

        public static List<Invoice> ReadKolagenPurchases(string filePath, string xlsSheet)
        {
            var invoices = new List<Invoice>();

            var constr = "Provider=Microsoft.ACE.OLEDB.12.0;Data Source="
                            + filePath + ";Extended Properties=\"Excel 12.0 Xml;HDR=YES;\"";
            using (var conn = new OleDbConnection(constr))
            {
                conn.Open();
                var command = new OleDbCommand("Select * from [" + xlsSheet + "$]", conn);
                var reader = command.ExecuteReader();
                if (reader.HasRows)
                {
                    while (reader.Read() && !string.IsNullOrWhiteSpace(reader[0].ToString()))
                    {
                        var inv = new Invoice();
                        try
                        {
                            inv.Number = reader[0].ToString();
                            inv.InvoiceDate = Convert.ToDateTime(reader[1].ToString());
                            inv.SellerTaxNumber = reader[2].ToString();
                            inv.BuyerName = reader[3].ToString();
                            inv.PaymentDate = Convert.ToDateTime(reader[4].ToString());
                            inv.BuyerAddress = reader[5].ToString();
                            inv.Rows.Add(new InvoiceRow
                            {
                                NetValue = Convert.ToDecimal(reader[7].ToString()),
                                VatRate = Math.Round(Convert.ToDecimal(reader[6].ToString())
                                                     / Convert.ToDecimal(reader[7].ToString()) * 100 - 100
                                    , 0).ToString(),
                                GrossValue = Convert.ToDecimal(reader[6].ToString())
                            });
                            invoices.Add(inv);
                        }
                        catch (Exception ex)
                        {
                            LogManager.GetCurrentClassLogger()
                                .Error($"Błąd odczytu linii {inv.Number}.Błąd: {ex.Message}");
                        }
                    }
                }
            }

            return invoices;
        }
    }
}
