﻿using CDNBase;
using CDNHeal;
using CDNHlmn;
using Microsoft.Win32;
using NLog;
using Santa.Biz.Model;
using System;
using System.Collections.Generic;
using System.Data.SqlClient;
using System.Diagnostics;
using System.Threading.Tasks;
using OptimaImporter.Biz.Helper;
using OptimaImporter.Data.Enum;

namespace Santa.Biz.Services
{
    public class OptimaGenerator
    {
        public event Action<int> InvoiceImported;
        public event Action ImportCompleted;
        public void VatRegisterImport(ILogin login, IEnumerable<Invoice> invoices, VatRegisterType vatRegisterType, string vatTegisterName)
        {
            var validInvoicesCounter = 0;
            var inValidInvoicesCounter = 0;
            foreach (var invoice in invoices)//.Where(i => i.Number == "7035255800" || i.Number == "FA/86/03/2017"))
            {
                var session = login.CreateSession();

                var paymentForms = (ICollection)session.CreateObject("CDN.FormyPlatnosci");
                var vatRegisters = (ICollection)session.CreateObject("CDN.RejestryVAT");
                Kontrahenci customers;
                IKontrahent customer;

                try
                {
                    //odwołania do istniejącego kontrahenta
                    customers = (Kontrahenci)session.CreateObject("CDN.Kontrahenci");
                    if ((int)vatRegisterType == 1)
                    {
                        customer = customers["Knt_NIP='" + NipSplit.GetNipNumber(invoice.SellerTaxNumber.Replace("-", "")) + "'"];
                    }
                    else
                    {
                        customer = customers["Knt_NIP='" + NipSplit.GetNipNumber(invoice.BuyerTaxNumber.Replace("-", "")) + "'"];
                    }
                }
                catch (Exception)
                {
                    var session1 = login.CreateSession();
                    customers = (Kontrahenci)session1.CreateObject("CDN.Kontrahenci");
                    customer = customers.AddNew();
                    if ((int)vatRegisterType == 1)
                    {
                        customer.Akronim = invoice.SellerTaxNumber;
                        customer.Nazwa1 = invoice.SellerName;
                        if (string.IsNullOrWhiteSpace(invoice.SellerTaxNumber))
                        {
                            customer.Akronim = DateTime.Today.ToShortDateString() + DateTime.Now.ToLongTimeString();
                        }
                        else
                        {
                            try
                            {
                                customer.NumerNIP.NIPKraj = NipSplit.GetNipCountry(invoice.SellerTaxNumber);
                                customer.NumerNIP.NipE = NipSplit.GetNipNumber(invoice.SellerTaxNumber);
                            }
                            catch (Exception e)
                            {
                                LogManager.GetCurrentClassLogger()
                                    .Info($"Nie udało się zapisać NIPu dla faktury {invoice.Number}, błąd: {e}");
                            }
                        }
                        customer.Adres.Ulica = invoice.Street;
                        customer.Adres.Miasto = invoice.City;
                        customer.Adres.KodPocztowy = invoice.PostalCode;
                        customer.Rodzaj_Dostawca = 1;
                        customer.Rodzaj_Odbiorca = 0;
                    }
                    else
                    {
                        customer.Akronim = invoice.BuyerTaxNumber;
                        customer.Nazwa1 = invoice.BuyerName;
                        if (string.IsNullOrWhiteSpace(invoice.BuyerTaxNumber))
                        {
                            customer.Akronim = DateTime.Today.ToShortDateString() + DateTime.Now.ToLongTimeString();
                        }
                        else
                        {
                            try
                            {
                                customer.NumerNIP.NIPKraj = NipSplit.GetNipCountry(invoice.BuyerTaxNumber);
                                customer.NumerNIP.NipE = NipSplit.GetNipNumber(invoice.BuyerTaxNumber);
                            }
                            catch (Exception e)
                            {
                                LogManager.GetCurrentClassLogger()
                                    .Info($"Nie udało się zapisać NIPu dla faktury {invoice.Number}, błąd: {e}");
                            }
                        }
                        customer.Adres.Ulica = invoice.Street;
                        customer.Adres.Miasto = invoice.City;
                        customer.Adres.KodPocztowy = invoice.PostalCode;
                        customer.Rodzaj_Dostawca = 0;
                        customer.Rodzaj_Odbiorca = 1;
                    }
                    try
                    {
                        session1.Save();
                    }
                    catch (Exception ex)
                    {
                        LogManager.GetCurrentClassLogger()
                            .Info($"Nie udało się zapisać kontrahenta dla faktury {invoice.Number}, błąd: {ex}");
                    }
                }

                var paymentForm = (invoice.InvoiceDate == invoice.PaymentDate)
                    ? (OP_KASBOLib.FormaPlatnosci)paymentForms["Fpl_Nazwa='gotówka'"]
                    : (OP_KASBOLib.FormaPlatnosci)paymentForms["Fpl_Nazwa='przelew'"];
                var vatRegister = (CDNRVAT.VAT)vatRegisters.AddNew();
                vatRegister.Typ = (int)vatRegisterType;
                vatRegister.Rejestr = vatTegisterName;
                vatRegister.Dokument = invoice.Number;
                vatRegister.DataZap = invoice.InvoiceDate;
                vatRegister.DataOpe = invoice.InvoiceDate;
                vatRegister.DataWys = invoice.InvoiceDate;
                try
                {
                    vatRegister.Podmiot = customer;
                }
                catch (Exception)
                {
                    vatRegister.Podmiot = customers["Knt_KntId=1"];
                }
                vatRegister.FormaPlatnosci = paymentForm;
                vatRegister.Termin = invoice.PaymentDate;

                var currencies = new List<string> { "EUR", "USD" };
                if (!string.IsNullOrWhiteSpace(invoice.Currency) && currencies.Contains(invoice.Currency))
                {
                    var waluty = (ICollection)session.CreateObject("CDN.Waluty");
                    var waluta = (Waluta)waluty[$"WNa_Symbol='{invoice.Currency}'"];
                    vatRegister.Waluta = waluta;
                    vatRegister.WalutaDoVAT = waluta;
                    var kurs = session.CreateObject("CDN.TypyKursowWalut").Item("WKu_Symbol='NBP'");
                    vatRegister.TypKursuWaluty = kurs;
                    vatRegister.TypKursuWalutyDoVAT = kurs;
                }

                var vatElements = vatRegister.Elementy;
                foreach (var row in invoice.Rows)
                {
                    var vatElement = (CDNRVAT.VATElement)vatElements.AddNew();
                    try
                    {
                        vatElement.Stawka = Convert.ToDouble(row.VatRate.Replace("\"", ""));
                    }
                    catch (Exception)
                    {
                        vatElement.Stawka = 0;
                    }
                    vatElement.RodzajZakupu = 1;
                    vatElement.Netto = row.NetValue;
                    //vatElement.VAT = invoice.TaxValue;
                    vatElement.Brutto = row.GrossValue;
                    vatElement.Odliczenia = 1;
                }
                try
                {
                    session.Save();
                    validInvoicesCounter++;
                    Debug.WriteLine(validInvoicesCounter);
                    InvoiceImported?.Invoke(validInvoicesCounter);
                }
                catch (Exception e)
                {
                    inValidInvoicesCounter++;
                    Debug.WriteLine($"{inValidInvoicesCounter} błędnych faktur");
                    LogManager.GetCurrentClassLogger()
                        .Info($"Nie udało się zapisać faktury o numerze {invoice.Number}, błąd: {e}");
                }
            }
        }

        public async void ImportSantaInvoicesAsync(ILogin login, IEnumerable<InvoiceSanta> invoices, DateTime date)
        {
            var licznik = 0; //bo pierwsza linia jest linią z nagłówkiem
            foreach (var invoiceSanta in invoices)
            {
                await GenerujRo(login, invoiceSanta, date);
                licznik++;
                InvoiceImported?.Invoke(licznik);
            }
            ImportCompleted?.Invoke();
        }

        public static async Task<string> GenerujRo(ILogin szeranLogin, InvoiceSanta invoice, DateTime date)
        {
            var nrDokumentu = "";
            await Task.Run(() =>
            {
                var session = new AdoSession();
                try
                {
                    var kontrahenci = (Kontrahenci)session.CreateObject("CDN.Kontrahenci");
                    var kontrahent = !string.IsNullOrWhiteSpace(invoice.BuyerCode)
                        ? (IKontrahent)kontrahenci["Knt_Kod = '" + invoice.BuyerCode + "'"]
                        : (IKontrahent)kontrahenci["Knt_NIP = '" + invoice.BuyerTaxNumber + "'"];
                    var odbiorca = !string.IsNullOrWhiteSpace(invoice.ReceiverCode)
                        ? (IKontrahent)kontrahenci["Knt_Kod = '" + invoice.ReceiverCode + "'"]
                        : (IKontrahent)kontrahenci["Knt_NIP = '" + invoice.BuyerTaxNumber + "'"];

                    if (!CheckIfInvoiceExistsInSelectedMonth(session, odbiorca.ID, date))
                    {
                        var faktury = (DokumentyHaMag)session.CreateObject("CDN.DokumentyHaMag");
                        var faktura = (IDokumentHaMag)faktury.AddNew();

                        faktura.Rodzaj = 302000;
                        faktura.TypDokumentu = 302;
                        faktura.Bufor = 1;

                        var definicjeDokumentow = (DefinicjeDokumentow)session.CreateObject("CDN.DefinicjeDokumentow");
                        var definicjaDokumentu = (DefinicjaDokumentu)definicjeDokumentow[$"Ddf_Symbol=\'{invoice.Series}\'"];
                        SetProperty(faktura.Numerator, "DefinicjaDokumentu", definicjaDokumentu);
                        //var numerator = (OP_KASBOLib.INumerator)faktura.Numerator;
                        //numerator.Rejestr = invoice.Series;


                        faktura.DataDok = date;
                        faktura.Podmiot = kontrahent;
                        faktura.Odbiorca = odbiorca;

                        var pozycje = faktura.Elementy;

                        foreach (var invoiceRow in invoice.RowsSanta)
                        {
                            IElementHaMag pozycja = pozycje.AddNew();
                            pozycja.TowarKod = invoiceRow.ItemCode;
                            pozycja.Ilosc = invoiceRow.Amount > 0 ? invoiceRow.Amount : 1;
                            pozycja.CenaT = invoiceRow.Amount > 0 ? invoiceRow.Price : invoiceRow.NetValue;
                            pozycja.WartoscNetto = invoiceRow.NetValue;
                        }

                        session.Save();
                        nrDokumentu = faktura.NumerPelny;

                    }
                }
                catch (Exception ex)
                {
                    LogManager.GetCurrentClassLogger().Info(
                        $"\r\n Wystapil błąd w tworzeniu dokumentu FA {ex.Message}" + ex);
                }
            });
            return nrDokumentu;
        }

        private static bool CheckIfInvoiceExistsInSelectedMonth(AdoSession session, int odbId, DateTime documentDate)
        {
            var registry = Registry.CurrentUser.OpenSubKey(@"SOFTWARE\CDN\CDN Opt!ma\CDN Opt!ma\Login", false);
            var registryValue = (string)registry?.GetValue("KonfigConnectStr");
            var connectionDataSource = registryValue.Substring(registryValue.IndexOf(",") + 1, registryValue.LastIndexOf(",") - registryValue.IndexOf(",") - 1);

            var sessionConnectionDatabase = session.Connection.DefaultDatabase;

            var sqlConnectionStringBuilder = new SqlConnectionStringBuilder
            {
                DataSource = connectionDataSource,
                InitialCatalog = sessionConnectionDatabase,
                IntegratedSecurity = false,
                UserID = "cdnoperator",
                Password = "snw2bdmk"
            };

            var sqlConnection = new SqlConnection(sqlConnectionStringBuilder.ConnectionString);

            var query = $@"select top 1 trn_trnid from cdn.tranag
                where year(trn_datadok) = {documentDate.Year} and month(TrN_DataDok) = {documentDate.Month}
                and day(trn_datadok) = {documentDate.Day} and trn_odbid = {odbId}";
            var sqlCommand = new SqlCommand(query, sqlConnection);
            sqlCommand.Connection.Open();
            var result = sqlCommand.ExecuteScalar();
            sqlCommand.Connection.Close();
            return result != null;
        }

        //wymagane do ustawienia numeracji dokumentów
        protected static void SetProperty(object o, string name, object value)  //wymagane do zmiany numeracji
        {
            o?.GetType().InvokeMember(name, System.Reflection.BindingFlags.SetProperty, null, o, new[] { value });
        }
    }
}
