﻿using CDNBase;
using NLog;
using Santa.Biz.Model;
using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Linq;
using OptimaImporter.Biz.Helper;
using OptimaImporter.Data.Enum;

namespace Santa.Biz.Services
{
    public class VatRegisterImporter
    {
        public event Action<int> InvoiceImported;
        public void VatRegisterImport(ILogin login, IEnumerable<Invoice> invoices, VatRegisterType vatRegisterType, string vatTegisterName)
        {
            var allInvoicesAmount = invoices.Count();
            var validInvoicesCounter = 0;
            var inValidInvoicesCounter = 0;
            foreach (var invoice in invoices)//.Where(i => i.Number == "7035255800" || i.Number == "FA/86/03/2017"))
            {
                var session = login.CreateSession();

                var paymentForms = (ICollection)session.CreateObject("CDN.FormyPlatnosci");
                var vatRegisters = (ICollection)session.CreateObject("CDN.RejestryVAT");
                CDNHeal.Kontrahenci customers;
                CDNHeal.IKontrahent customer;

                try
                {
                    //odwołania do istniejącego kontrahenta
                    customers = (CDNHeal.Kontrahenci)session.CreateObject("CDN.Kontrahenci");
                    if ((int)vatRegisterType == 1)
                    {
                        customer = customers["Knt_NIP='" + NipSplit.GetNipNumber(invoice.SellerTaxNumber.Replace("-", "")) + "'"];
                    }
                    else
                    {
                        customer = customers["Knt_NIP='" + NipSplit.GetNipNumber(invoice.BuyerTaxNumber.Replace("-", "")) + "'"];
                    }
                }
                catch (Exception)
                {
                    var session1 = login.CreateSession();
                    customers = (CDNHeal.Kontrahenci)session1.CreateObject("CDN.Kontrahenci");
                    customer = customers.AddNew();
                    if ((int)vatRegisterType == 1)
                    {
                        customer.Akronim = invoice.SellerTaxNumber;
                        customer.Nazwa1 = invoice.SellerName;
                        if (string.IsNullOrWhiteSpace(invoice.SellerTaxNumber))
                        {
                            customer.Akronim = DateTime.Today.ToShortDateString() + DateTime.Now.ToLongTimeString();
                        }
                        else
                        {
                            try
                            {
                                customer.NumerNIP.NIPKraj = NipSplit.GetNipCountry(invoice.SellerTaxNumber);
                                customer.NumerNIP.NipE = NipSplit.GetNipNumber(invoice.SellerTaxNumber);
                            }
                            catch (Exception e)
                            {
                                LogManager.GetCurrentClassLogger()
                                    .Info($"Nie udało się zapisać NIPu dla faktury {invoice.Number}, błąd: {e}");
                            }
                        }
                        customer.Adres.Ulica = invoice.Street;
                        customer.Adres.Miasto = invoice.City;
                        customer.Adres.KodPocztowy = invoice.PostalCode;
                        customer.Rodzaj_Dostawca = 1;
                        customer.Rodzaj_Odbiorca = 0;
                    }
                    else
                    {
                        customer.Akronim = invoice.BuyerTaxNumber;
                        customer.Nazwa1 = invoice.BuyerName;
                        if (string.IsNullOrWhiteSpace(invoice.BuyerTaxNumber))
                        {
                            customer.Akronim = DateTime.Today.ToShortDateString() + DateTime.Now.ToLongTimeString();
                        }
                        else
                        {
                            try
                            {
                                customer.NumerNIP.NIPKraj = NipSplit.GetNipCountry(invoice.BuyerTaxNumber);
                                customer.NumerNIP.NipE = NipSplit.GetNipNumber(invoice.BuyerTaxNumber);
                            }
                            catch (Exception e)
                            {
                                LogManager.GetCurrentClassLogger()
                                    .Info($"Nie udało się zapisać NIPu dla faktury {invoice.Number}, błąd: {e}");
                            }
                        }
                        customer.Adres.Ulica = invoice.Street;
                        customer.Adres.Miasto = invoice.City;
                        customer.Adres.KodPocztowy = invoice.PostalCode;
                        customer.Rodzaj_Dostawca = 0;
                        customer.Rodzaj_Odbiorca = 1;
                    }
                    try
                    {
                        session1.Save();
                    }
                    catch (Exception ex)
                    {
                        LogManager.GetCurrentClassLogger()
                            .Info($"Nie udało się zapisać kontrahenta dla faktury {invoice.Number}, błąd: {ex}");
                    }
                }

                var paymentForm = (invoice.InvoiceDate == invoice.PaymentDate)
                    ? (OP_KASBOLib.FormaPlatnosci)paymentForms["Fpl_Nazwa='gotówka'"]
                    : (OP_KASBOLib.FormaPlatnosci)paymentForms["Fpl_Nazwa='przelew'"];
                var vatRegister = (CDNRVAT.VAT)vatRegisters.AddNew();
                vatRegister.Typ = vatRegisterType == VatRegisterType.Purchase ? 1 : 2;
                vatRegister.Rejestr = string.IsNullOrWhiteSpace(vatTegisterName) && vatRegister.Typ == 1
                    ? "ZAKUP" : vatTegisterName;
                vatRegister.Rejestr = string.IsNullOrWhiteSpace(vatTegisterName) && vatRegister.Typ == 2
                    ? "SPRZEDAŻ" : vatTegisterName;
                vatRegister.Dokument = invoice.Number;
                vatRegister.DataWys = invoice.InvoiceDate;
                vatRegister.DataZap = invoice.InvoiceDate;
                vatRegister.DataOpe = invoice.OperationDate == new DateTime(1, 1, 1) ? invoice.InvoiceDate : invoice.OperationDate;
                try
                {
                    vatRegister.Podmiot = customer;
                }
                catch (Exception)
                {
                    vatRegister.Podmiot = customers["Knt_KntId=1"];
                }
                vatRegister.FormaPlatnosci = paymentForm;
                vatRegister.Termin = invoice.PaymentDate;

                var currencies = new List<string> { "EUR", "USD" };
                if (!string.IsNullOrWhiteSpace(invoice.Currency) && currencies.Contains(invoice.Currency))
                {
                    var waluty = (ICollection)session.CreateObject("CDN.Waluty");
                    var waluta = (CDNHeal.Waluta)waluty[$"WNa_Symbol='{invoice.Currency}'"];
                    vatRegister.Waluta = waluta;
                    vatRegister.WalutaDoVAT = waluta;
                    var kurs = session.CreateObject("CDN.TypyKursowWalut").Item("WKu_Symbol='NBP'");
                    vatRegister.TypKursuWaluty = kurs;
                    vatRegister.TypKursuWalutyDoVAT = kurs;
                }

                var vatElements = vatRegister.Elementy;
                foreach (var row in invoice.Rows)
                {
                    var vatElement = (CDNRVAT.VATElement)vatElements.AddNew();
                    try
                    {
                        var stawkaString = row.VatRate.Replace("\"", "").Replace(".0000", ",00");
                        vatElement.Stawka = Convert.ToDouble(stawkaString);
                    }
                    catch (Exception)
                    {
                        vatElement.Stawka = 0;
                    }
                    vatElement.RodzajZakupu = 1;
                    vatElement.Netto = row.NetValue;
                    //vatElement.VAT = invoice.TaxValue;
                    vatElement.Brutto = row.GrossValue;
                    vatElement.Odliczenia = 1;
                }
                try
                {
                    session.Save();
                    validInvoicesCounter++;
                    Debug.WriteLine(validInvoicesCounter);
                    InvoiceImported?.Invoke(validInvoicesCounter);
                }
                catch (Exception e)
                {
                    inValidInvoicesCounter++;
                    Debug.WriteLine($"{inValidInvoicesCounter} błędnych faktur");
                    LogManager.GetCurrentClassLogger()
                        .Info($"Nie udało się zapisać faktury o numerze {invoice.Number}, błąd: {e}");
                }
            }
        }
    }
}
