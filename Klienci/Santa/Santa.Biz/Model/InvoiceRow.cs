﻿namespace Santa.Biz.Model
{
    public class InvoiceRow
    {
        public decimal NetValue { get; set; } //P_11
        public string VatRate { get; set; } //P_12
        public decimal GrossValue { get; set; } //P_11A
    }
}
