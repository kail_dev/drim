﻿using System;
using System.Collections.Generic;

namespace Santa.Biz.Model
{
    public class Invoice
    {
        public DateTime InvoiceDate { get; set; } //P_1
        public DateTime OperationDate { get; set; } //P_1
        public string Number { get; set; } //P_2A
        public string BuyerName { get; set; } //P_3A
        public string BuyerAddress { get; set; } //P_3B
        public string SellerName { get; set; } //P_3C
        public string SellerAddress { get; set; } //P_3D
        public string BuyerTaxNumberCountry { get; set; } //P_5A
        public string BuyerTaxNumber { get; set; } //P_5B
        public string SellerTaxNumberCountry { get; set; } //P_4A
        public string SellerTaxNumber { get; set; } //P_4B
        public DateTime PaymentDate { get; set; } //P_6
        public decimal NetValue { get; set; } //P_13_1
        public decimal TaxValue { get; set; } //P_14_1
        public decimal GrossValue { get; set; } //P_15_1
        public string Street { get; set; }
        public string City { get; set; }
        public string PostalCode { get; set; }
        public string Currency { get; set; }
        public string CurrencyRate { get; set; }
        public virtual List<InvoiceRow> Rows { get; set; } = new List<InvoiceRow>();
    }
}
