﻿namespace Santa.Biz.Model
{
    public class InvoiceRowSanta : InvoiceRow
    {
        public string ItemCode { get; set; }
        public double Amount { get; set; }
        public decimal Price { get; set; }
    }
}
