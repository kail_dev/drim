﻿using System.Collections.Generic;

namespace Santa.Biz.Model
{
    public class InvoiceSanta : Invoice
    {
        public string BuyerCode { get; set; }
        public string ReceiverCode { get; set; }
        public string Series { get; set; }
        public List<InvoiceRowSanta> RowsSanta { get; set; } = new List<InvoiceRowSanta>();
    }
}
