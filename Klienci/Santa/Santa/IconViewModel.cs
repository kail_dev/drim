﻿namespace Santa
{
    public class IconViewModel
    {
        public string Error => GetPath("error.png");

        public string Home => GetPath("home.png");

        public string VatS => GetPath("VatS.png");

        public string VatZ => GetPath("VatZ.png");

        private static string GetPath(string iconString)
        {
            return "/OptimaImporterSanta;component/Images/" + iconString;
        }
    }
}
