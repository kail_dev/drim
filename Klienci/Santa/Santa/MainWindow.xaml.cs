﻿using Microsoft.Win32;
using NLog;
using OptimaImporter.Data.Enum;
using Santa.Biz.Services;
using System;
using System.Collections.Generic;
using System.Data.OleDb;
using System.Data.SqlClient;
using System.Globalization;
using System.Linq;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using Telerik.Windows.Controls;

//72H88Tyk:-bk
namespace Santa
{
    /// <summary>
    /// Interaction logic for MainWindow.xaml
    /// </summary>
    public partial class MainWindow
    {
        public string ConnectionDataSource = "";
        public string ConnectionInitialCatalog = "";
        public string BazaFirmowa = "";

        private string _optimaUser;
        private string _optimaPassword;
        private string _optimaDatabase;

        public CDNBase.IApplication Application;
        public CDNBase.ILogin ILogin;
        public List<string> FailedImporter = new List<string>();

        //private string logOutMessage = "Wylogowałem się z Opt!my";
        //private string logInMessage = "Zalogowałem się do Optimy";
        //private string logFailedMessage = "Błąd logowania";

        OpenFileDialog _ofd = new OpenFileDialog();

        RadGridView _errorListGridView = new RadGridView();

        public MainWindow()
        {
            StyleManager.ApplicationTheme = new Office_BlueTheme();

            InitializeComponent();

            try
            {
                var registry = Registry.CurrentUser.OpenSubKey(@"SOFTWARE\CDN\CDN Opt!ma\CDN Opt!ma\Login", false);
                var registryValue = (string)registry?.GetValue("KonfigConnectStr");
                ConnectionDataSource = registryValue.Substring(registryValue.IndexOf(",") + 1, registryValue.LastIndexOf(",") - registryValue.IndexOf(",") - 1);
                ConnectionInitialCatalog = registryValue.Substring(registryValue.IndexOf(":") + 1, registryValue.IndexOf(",") - registryValue.IndexOf(":") - 1);
                //MessageBox.Show("Nazwa serwera: " + connectionDataSource + "\nBaza konfiguracyjna: " + connectionInitialCatalog);
            }
            catch (Exception ex)
            {
                LogManager.GetCurrentClassLogger().Info($"Nie udało się odczytać informacji z rejestru: {ex}");
            }

            try
            {
                //podłączanie by pobrać listę baz do logowania
                var sqlConnectionStringBuilder = new SqlConnectionStringBuilder
                {
                    DataSource = ConnectionDataSource,
                    InitialCatalog = ConnectionInitialCatalog,
                    IntegratedSecurity = false,
                    UserID = "cdnoperator",
                    Password = "snw2bdmk"
                };

                var sqlConnection = new SqlConnection(sqlConnectionStringBuilder.ConnectionString);

                var query = @"select ope_kod from cdn.operatorzy";
                var sqlCommand = new SqlCommand(query, sqlConnection);
                sqlCommand.Connection.Open();
                var sqlDataReader = sqlCommand.ExecuteReader();
                userComboBox.Items.Clear();
                while (sqlDataReader.Read())
                {
                    var uzytkownikaNazwa = sqlDataReader.GetString(0);
                    userComboBox.Items.Add(uzytkownikaNazwa);
                }
                sqlDataReader.Close();
                sqlCommand.Connection.Close();
                userComboBox.IsEnabled = true;

                query = @"select baz_nazwa from cdn.bazy";
                sqlCommand = new SqlCommand(query, sqlConnection);
                sqlCommand.Connection.Open();
                sqlDataReader = sqlCommand.ExecuteReader();
                dbComboBox.Items.Clear();
                while (sqlDataReader.Read())
                {
                    var bazaNazwa = sqlDataReader.GetString(0);
                    dbComboBox.Items.Add(bazaNazwa);
                }
                sqlDataReader.Close();
                sqlCommand.Connection.Close();
                dbComboBox.IsEnabled = true;
            }
            catch (Exception ex)
            {
                LogManager.GetCurrentClassLogger().Info($"Nie udało się odczytać podstawowej konfiguracji firmy: {ex}");
            }
        }

        private void openVATzakup_Click(object sender, RoutedEventArgs e)
        {
            _optimaUser = userComboBox.SelectedItem.ToString();
            _optimaPassword = passwordTextBox.Password;
            _optimaDatabase = dbComboBox.SelectedItem.ToString();

            try
            {
                Application = new CDNBase.Application();
                Application.LockApp(1);
                ILogin = Application.Login(_optimaUser, _optimaPassword, _optimaDatabase);
            }
            catch (Exception ex)
            {
                LogManager.GetCurrentClassLogger().Info($"Błąd logowania: {ex}");
            }

            try
            {
                _ofd.ShowDialog();
                if (!string.IsNullOrWhiteSpace(_ofd.FileName))
                {
                    var excelConnectionString =
                        $"Provider=Microsoft.ACE.OLEDB.12.0;Data Source={_ofd.FileName};Extended Properties=Excel 12.0 Xml";
                    using (var connection = new OleDbConnection(excelConnectionString))
                    {
                        var command = new OleDbCommand("Select * FROM [Zakup$]", connection);
                        connection.Open();
                        using (var dr = command.ExecuteReader())
                        {
                            _errorListGridView.ItemsSource = null;
                            while (dr.Read())
                            {
                                if (!string.IsNullOrWhiteSpace(dr[1].ToString()))
                                {
                                    try
                                    {
                                        var session = ILogin.CreateSession();
                                        var formyPlatnosci = (CDNBase.ICollection)session.CreateObject("CDN.FormyPlatnosci");
                                        var rejestryVat = (CDNBase.ICollection)session.CreateObject("CDN.RejestryVAT");
                                        CDNHeal.Kontrahenci kontrahenci;
                                        CDNHeal.IKontrahent kontrahent;
                                        try
                                        {
                                            kontrahenci = (CDNHeal.Kontrahenci)session.CreateObject("CDN.Kontrahenci");
                                            kontrahent = kontrahenci["Knt_NIP='" + dr[12].ToString().Replace("-", "") + "'"];
                                        }
                                        catch (Exception)
                                        {
                                            var session1 = ILogin.CreateSession();
                                            kontrahenci = (CDNHeal.Kontrahenci)session1.CreateObject("CDN.Kontrahenci");
                                            kontrahent = kontrahenci.AddNew();
                                            kontrahent.Akronim = "XXX" + dr[12];
                                            kontrahent.Nazwa1 = dr[6].ToString();
                                            kontrahent.NipE = dr[12].ToString();
                                            kontrahent.Adres.Miasto = dr[8].ToString();
                                            kontrahent.Adres.KodPocztowyZKreska = dr[7].ToString();
                                            kontrahent.Adres.Ulica = dr[9].ToString();
                                            kontrahent.Adres.NrDomu = dr[10].ToString();
                                            kontrahent.Adres.NrLokalu = dr[11].ToString();
                                            session1.Save();
                                        }

                                        var fPl = (OP_KASBOLib.FormaPlatnosci)formyPlatnosci["Fpl_Nazwa='gotówka'"];

                                        CDNRVAT.VAT rejestrVat = null;

                                        switch (dr[5].ToString())
                                        {
                                            case "karta":
                                                fPl = (OP_KASBOLib.FormaPlatnosci)formyPlatnosci["Fpl_Nazwa='karta'"];
                                                break;
                                            case "przelew":
                                                fPl = (OP_KASBOLib.FormaPlatnosci)formyPlatnosci["Fpl_Nazwa='przelew'"];
                                                break;
                                        }

                                        //Trace.WriteLine("Wybrana forma płatności: " + FPl.Nazwa);
                                        //utworzenie obiektu w rej VAT

                                        rejestrVat = (CDNRVAT.VAT)rejestryVat.AddNew();

                                        try
                                        {
                                            rejestrVat.Dokument = dr[1].ToString();
                                        }
                                        catch (Exception)
                                        {
                                            rejestrVat.Dokument = "brak numeru";
                                        }

                                        // nazwa rejestru
                                        rejestrVat.Rejestr = "ZAKUP"; //rejestrVATZakupuComboBox.SelectedValue.ToString();

                                        //	1 - zakupu; 2 - sprzedaży
                                        rejestrVat.Typ = 1;
                                        //MessageBox.Show(dr[2].ToString());

                                        if (DataWplywuDateTimePicker.SelectedDate.HasValue
                                            && DateTime.ParseExact(dr[3].ToString().Substring(0, 10), "dd.MM.yyyy", new CultureInfo("pl-PL"), DateTimeStyles.None).Month
                                                == rejestrVat.DataZap.Month)  //data wpływu
                                        {
                                            rejestrVat.DataZap = DataWplywuDateTimePicker.SelectedDate.Value;
                                        }
                                        else
                                        {
                                            rejestrVat.DataZap = DateTime.Now;
                                        }
                                        if (DateTime.ParseExact(dr[3].ToString().Substring(0, 10), "dd.MM.yyyy", new CultureInfo("pl-PL"), DateTimeStyles.None).Month
                                            == rejestrVat.DataZap.Month)
                                        {
                                            rejestrVat.DataOpe = DateTime.ParseExact(dr[3].ToString(), "dd.MM.yyyy", new CultureInfo("pl-PL"), DateTimeStyles.None);  //data zakupu                                            
                                        }
                                        else
                                        {
                                            rejestrVat.DataOpe = rejestrVat.DataZap;
                                        }
                                        rejestrVat.DataWys = DateTime.ParseExact(dr[2].ToString().Substring(0, 10), "dd.MM.yyyy", new CultureInfo("pl-PL"), DateTimeStyles.None);  //data wystawienia
                                        rejestrVat.Podmiot = (CDNHeal.IPodmiot)kontrahent;
                                        rejestrVat.FormaPlatnosci = fPl;

                                        if (string.IsNullOrWhiteSpace(dr[5].ToString()))
                                        {
                                            rejestrVat.Termin = rejestrVat.DataWys;
                                        }
                                        else
                                        {
                                            try
                                            {
                                                //termin płatności
                                                rejestrVat.Termin = DateTime.ParseExact(dr[2].ToString().Substring(0, 10), "dd.MM.yyyy", new CultureInfo("pl-PL"), DateTimeStyles.None).AddDays(Convert.ToDouble(dr[4]));
                                            }
                                            catch (Exception)
                                            {
                                                rejestrVat.Termin = DateTime.ParseExact(dr[2].ToString().Substring(0, 10), "dd.MM.yyyy", new CultureInfo("pl-PL"), DateTimeStyles.None);
                                            }
                                        }

                                        //Trace.WriteLine("Data zapadalności (termin płatności): " + RejestrVAT.DataZap);
                                        //Trace.WriteLine("Data operacji (data zakupu/sprzedaży): " + RejestrVAT.DataZap);
                                        //Trace.WriteLine("Data wystawienia: " + RejestrVAT.DataZap);
                                        //Trace.WriteLine("Termin ujęcia w deklaracji VAT: " + RejestrVAT.Termin);

                                        var elementy = rejestrVat.Elementy;
                                        var element = (CDNRVAT.VATElement)elementy.AddNew();
                                        try
                                        {
                                            element.Stawka = Convert.ToDouble(dr[14]) * 100;
                                            //Trace.WriteLine("Stawka VAT: " + element.Stawka);
                                        }
                                        catch (Exception)
                                        {
                                            element.Stawka = 0;
                                        }
                                        element.Flaga = 2; //stawka VAT: 1-zwolniona, 2-opodatkowana, 4-nie podlega
                                        // dla stawki NP trzeba ustawić jeszcze element.Stawka = 0;
                                        try
                                        {
                                            element.RodzajZakupu = 2;
                                            //Rodzaje zakupów:
                                            //Towary		 = 1;
                                            //Inne			 = 2;
                                            //Trwale		 = 3;
                                            //Uslugi		 = 4;
                                            //NoweSrTran	 = 5;
                                            //Nieruchomosci = 6;
                                            //Paliwo		 = 7; 
                                        }
                                        catch (Exception)
                                        {
                                            element.RodzajZakupu = 2;
                                        }
                                        element.KatOpis = dr[17].ToString();
                                        try
                                        {
                                            //Trace.WriteLine("Kwota odczytana z arkusza Excel: " + Convert.ToDecimal(string.Format("{0:D2}", dr[13].ToString())).ToString());
                                            element.Brutto = Convert.ToDecimal($"{dr[16]:D2}");
                                            //Trace.WriteLine("Kwota netto: " + element.Netto);
                                            //Trace.WriteLine("Kwota VAT: " + element.VAT);
                                            //Trace.WriteLine("Kwota brutto: " + element.Brutto);
                                        }
                                        catch (Exception ex)
                                        {
                                            LogManager.GetCurrentClassLogger().Info($"Odczyt kwoty brutto faktury nie powiódł się: {ex}");
                                        }
                                        //Trace.WriteLine("Opis kategorii: " + element.KatOpis);
                                        element.Odliczenia = 1;
                                        //dla rej zakupu(odliczenia): 0-nie, 1-tak, 2-warunkowo
                                        //dla rej sprzed(uwz. w prop): 0-nie uwzgledniaj, 1-Uwzgledniaj w proporcji, 2- tylko w mianowniku

                                        var error = rejestrVat.SprawdzDuplikatVATEx(1);
                                        try
                                        {
                                            session.Save();
                                        }
                                        catch (Exception ex)
                                        {
                                            FailedImporter.Add(dr[0] + ": " + ex);
                                        }
                                    }
                                    catch (FormatException ex)
                                    {
                                        if (ex.ToString().Contains("DateTime"))
                                        {
                                            FailedImporter.Add(dr[0] + ": błędny format daty");
                                        }
                                        else
                                        {
                                            FailedImporter.Add(dr[0] + ": " + ex);
                                        }
                                    }
                                    catch (Exception ex)
                                    {
                                        if (ex.ToString().Contains("SprawdzDuplikatVATEx(Int32 ThrowException)"))
                                        {
                                            FailedImporter.Add(dr[0] + ": istnieje już podobny dokument, nie utworzono.");
                                        }
                                        else
                                        {
                                            FailedImporter.Add(dr[0] + ": " + ex);
                                        }
                                    }
                                }
                            }
                            //backgroundWorker.CancelAsync();
                        }
                        if (FailedImporter.Count == 0)
                        {
                            FailedImporter.Add("Brak błędów, wszystkie wiersze zostały zaimportowane.");
                        }
                        _errorListGridView.ItemsSource = FailedImporter;
                        RadRibbonButton_Click(new object(), new RoutedEventArgs());
                    }
                }
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.ToString());
            }
            instructionsTextBlock.Text = "Wczytywanie zakończone.";
            errorButton.IsEnabled = true;
        }

        private async void openVATsprzedaz_Click(object sender, RoutedEventArgs e)
        {
            _optimaUser = userComboBox.SelectedItem.ToString();
            _optimaPassword = passwordTextBox.Password;
            _optimaDatabase = dbComboBox.SelectedItem.ToString();

            try
            {
                Application = new CDNBase.Application();
                Application.LockApp(1);
                ILogin = Application.Login(_optimaUser, _optimaPassword, _optimaDatabase);
            }
            catch (Exception ex)
            {
                LogManager.GetCurrentClassLogger().Info($"Błąd logowania: {ex}");
            }

            _ofd.ShowDialog();
            if (!string.IsNullOrWhiteSpace(_ofd.FileName))
            {
                var invoices = SourceFileReader.ReadSantaInvoicesPmp(_ofd.FileName);
                var impoter = new OptimaGenerator();
                impoter.InvoiceImported += x =>
                {
                    Dispatcher.Invoke(() =>
                    {
                        vatsButton.Text = $"Wczytywanie {x} z {invoices.Count} faktur.";
                    });
                };
                impoter.ImportCompleted += () =>
                {
                    Dispatcher.InvokeAsync(() =>
                    {
                        vatsButton.Text = "Wczytaj faktury VAT sprzedaży";
                    });
                };
                if (invoices.Any())
                {
                    var date = documentSalesDate.SelectedDate ?? DateTime.Now;
                    await Task.Run(() =>
                    {
                        impoter.ImportSantaInvoicesAsync(ILogin, invoices, date);
                    });
                }
            }

        }

        private void Window_Closed(object sender, EventArgs e)
        {
            try
            {
                ILogin = null;
                Application.UnlockApp();
                Application = null;
            }
            catch (Exception)
            {
                ILogin = null;
                Application = null;
            }
        }

        private void dbComboBox_SelectionChanged(object sender, SelectionChangedEventArgs e)
        {
            var sqlConnectionStringBuilder0 = new SqlConnectionStringBuilder
            {
                DataSource = ConnectionDataSource,
                InitialCatalog = ConnectionInitialCatalog,
                IntegratedSecurity = false,
                UserID = "cdnoperator",
                Password = "snw2bdmk"
            };

            var sqlConnection0 = new SqlConnection(sqlConnectionStringBuilder0.ConnectionString);

            var query0 = @"select substring(baz_dostep,5,CHARINDEX(',',baz_dostep)-5) from cdn.bazy";
            var sqlCommand0 = new SqlCommand(query0, sqlConnection0);
            sqlCommand0.Connection.Open();
            var sqlDataReader0 = sqlCommand0.ExecuteReader();
            while (sqlDataReader0.Read())
            {
                BazaFirmowa = sqlDataReader0.GetString(0);
            }
            sqlDataReader0.Close();
            sqlCommand0.Connection.Close();

            var sqlConnectionStringBuilder = new SqlConnectionStringBuilder
            {
                DataSource = ConnectionDataSource,
                InitialCatalog = BazaFirmowa,
                IntegratedSecurity = false,
                UserID = "cdnoperator",
                Password = "snw2bdmk"
            };

            var sqlConnection = new SqlConnection(sqlConnectionStringBuilder.ConnectionString);

            var query = @"SELECT [Gru_Nazwa] FROM [CDN].[Grupy] where gru_typ = 1";
            var sqlCommand = new SqlCommand(query, sqlConnection);
            sqlCommand.Connection.Open();
            var sqlDataReader = sqlCommand.ExecuteReader();
            rejestrVATZakupuComboBox.Items.Clear();
            while (sqlDataReader.Read())
            {
                var rejestrZakupu = sqlDataReader.GetString(0);
                rejestrVATZakupuComboBox.Items.Add(rejestrZakupu);
            }
            sqlDataReader.Close();
            sqlCommand.Connection.Close();
            rejestrVATZakupuComboBox.IsEnabled = true;

            var query2 = @"SELECT [Gru_Nazwa] FROM [CDN].[Grupy] where gru_typ = 2";
            var sqlCommand2 = new SqlCommand(query2, sqlConnection);
            sqlCommand2.Connection.Open();
            var sqlDataReader2 = sqlCommand2.ExecuteReader();
            rejestrVATSprzedazyComboBox.Items.Clear();
            while (sqlDataReader2.Read())
            {
                var rejestrSprzedazy = sqlDataReader2.GetString(0);
                rejestrVATSprzedazyComboBox.Items.Add(rejestrSprzedazy);
            }
            sqlDataReader2.Close();
            rejestrVATSprzedazyComboBox.IsEnabled = true;
            sqlCommand2.Connection.Close();
        }

        private void RadRibbonButton_Click(object sender, RoutedEventArgs e)
        {
            Grid.SetRow(_errorListGridView, 1);
            MainGrid.Children.Add(_errorListGridView);
        }

        private void RadRibbonButton_Click_1(object sender, RoutedEventArgs e)
        {
            MainGrid.Children.Remove(_errorListGridView);
        }

        private void userComboBox_SelectionChanged(object sender, SelectionChangedEventArgs e)
        {
            dbComboBox.IsEnabled = true;
            instructionsTextBlock.Text = "Wpisz hasło i wybierz bazę danych.";
        }

        private void rejestrVATZakupuComboBox_SelectionChanged(object sender, SelectionChangedEventArgs e)
        {
            vatzButton.IsEnabled = true;
            VatzJpkButton.IsEnabled = true;
            VatzSubiektButton.IsEnabled = true;
            instructionsTextBlock.Text = "By wczytać dane naciśnij przycisk na górze z literką Z i wybierz plik, z którego chcesz wczytywać dane.";
        }

        private void rejestrVATSprzedazyComboBox_SelectionChanged(object sender, SelectionChangedEventArgs e)
        {
            vatsButton.IsEnabled = true;
            VatsJpkButton.IsEnabled = true;
            VatsSubiektButton.IsEnabled = true;
        }


        private void openVATzakupJpk_Click(object sender, RoutedEventArgs e)
        {
            _optimaUser = userComboBox.SelectedItem.ToString();
            _optimaPassword = passwordTextBox.Password;
            _optimaDatabase = dbComboBox.SelectedItem.ToString();

            try
            {
                Application = new CDNBase.Application();
                Application.LockApp(1);
                ILogin = Application.Login(_optimaUser, _optimaPassword, _optimaDatabase);
            }
            catch (Exception ex)
            {
                LogManager.GetCurrentClassLogger().Info($"Błąd logowania: {ex}");
            }

            _ofd.ShowDialog();
            if (!string.IsNullOrWhiteSpace(_ofd.FileName))
            {
                var invoices = SourceFileReader.ReadJpkFaInvoices(_ofd.FileName);
                var impoter = new OptimaGenerator();
                impoter.InvoiceImported += x =>
                {
                    Dispatcher.Invoke(() =>
                    {
                        VatzJpkButton.Text = "Wczytaj zakup z JPK" + Environment.NewLine + x;
                    });
                };
                var vatRegisterName = rejestrVATZakupuComboBox.SelectedValue.ToString();
                Task.Run(() =>
                {
                    impoter.VatRegisterImport(
                        ILogin, invoices, VatRegisterType.Purchase,
                        vatRegisterName);

                });
            }
        }

        private void openVATsprzedazJpk_Click(object sender, RoutedEventArgs e)
        {
            _optimaUser = userComboBox.SelectedItem.ToString();
            _optimaPassword = passwordTextBox.Password;
            _optimaDatabase = dbComboBox.SelectedItem.ToString();

            try
            {
                Application = new CDNBase.Application();
                Application.LockApp(1);
                ILogin = Application.Login(_optimaUser, _optimaPassword, _optimaDatabase);
            }
            catch (Exception ex)
            {
                LogManager.GetCurrentClassLogger().Info($"Błąd logowania: {ex}");
            }

            _ofd.ShowDialog();
            if (!string.IsNullOrWhiteSpace(_ofd.FileName))
            {
                var invoices = SourceFileReader.ReadJpkFaInvoices(_ofd.FileName);
                var impoter = new OptimaGenerator();
                impoter.InvoiceImported += x =>
                {
                    Dispatcher.Invoke(() =>
                    {
                        VatsJpkButton.Text = "Wczytaj sprzedaż z JPK" + Environment.NewLine + x;
                    });
                };
                var registerVatName = rejestrVATSprzedazyComboBox.SelectedValue.ToString();
                Task.Run(() =>
                {
                    impoter.VatRegisterImport(
                        ILogin, invoices, VatRegisterType.Sales,
                        registerVatName);
                });
            }
        }

        private void openVATzakupSubiekt_Click(object sender, RoutedEventArgs e)
        {
            _optimaUser = userComboBox.SelectedItem.ToString();
            _optimaPassword = passwordTextBox.Password;
            _optimaDatabase = dbComboBox.SelectedItem.ToString();

            try
            {
                Application = new CDNBase.Application();
                Application.LockApp(1);
                ILogin = Application.Login(_optimaUser, _optimaPassword, _optimaDatabase);
            }
            catch (Exception ex)
            {
                LogManager.GetCurrentClassLogger().Info($"Błąd logowania: {ex}");
            }

            _ofd.ShowDialog();
            if (!string.IsNullOrWhiteSpace(_ofd.FileName))
            {
                var invoices = SourceFileReader.ReadSubiektInvoices(_ofd.FileName, "FZ");
                var impoter = new OptimaGenerator();
                impoter.InvoiceImported += x =>
                {
                    Dispatcher.Invoke(() =>
                    {
                        VatzJpkButton.Text = "Wczytaj zakup z Subiekta" + Environment.NewLine + x;
                    });
                };
                var vatRegisterName = rejestrVATZakupuComboBox.SelectedValue.ToString();
                Task.Run(() =>
                {
                    impoter.VatRegisterImport(
                        ILogin, invoices, VatRegisterType.Purchase,
                        vatRegisterName);
                });
            }
        }

        private void openVATsprzedazSubiekt_Click(object sender, RoutedEventArgs e)
        {
            _optimaUser = userComboBox.SelectedItem.ToString();
            _optimaPassword = passwordTextBox.Password;
            _optimaDatabase = dbComboBox.SelectedItem.ToString();

            try
            {
                Application = new CDNBase.Application();
                Application.LockApp(1);
                ILogin = Application.Login(_optimaUser, _optimaPassword, _optimaDatabase);
            }
            catch (Exception ex)
            {
                LogManager.GetCurrentClassLogger().Info($"Błąd logowania: {ex}");
            }

            _ofd.ShowDialog();
            if (!string.IsNullOrWhiteSpace(_ofd.FileName))
            {
                var invoices = SourceFileReader.ReadSubiektInvoices(_ofd.FileName, "FS");
                var impoter = new OptimaGenerator();
                impoter.InvoiceImported += x =>
                {
                    Dispatcher.Invoke(() =>
                    {
                        VatsJpkButton.Text = "Wczytaj sprzedaż z Subiekta" + Environment.NewLine + x;
                    });
                };
                var registerVatName = rejestrVATSprzedazyComboBox.SelectedValue.ToString();
                Task.Run(() =>
                {
                    impoter.VatRegisterImport(
                        ILogin, invoices, VatRegisterType.Sales,
                        registerVatName);
                });
            }
        }
    }
}
