﻿using CDNBase;
using CDNHeal;
using Idea.Biz.Model;
using NLog;
using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Linq;
using OptimaImporter.Biz.Helper;
using OptimaImporter.Data.Enum;

namespace Idea.Biz.Services
{
    public class VatRegisterImporter
    {
        public event Action<int> InvoiceImported;
        public void VatRegisterImport(IList<IdeaVatInvoice> invoices, VatRegisterType vatRegisterType, string vatTegisterName, bool nameAsCustomerCode = false)
        {
            var nieZaczytaneDokumenty = new List<IdeaVatInvoice>();
            nieZaczytaneDokumenty.AddRange(invoices);
            var validInvoicesCounter = 0;
            var inValidInvoicesCounter = 0;
            foreach (var invoice in invoices) //.Where(i => i.Number == "53/SFV/00001403/2018" || i.Number == "53/SFV/00001404/2018"))
            {
                var session = new AdoSession();

                var paymentForms = (ICollection)session.CreateObject("CDN.FormyPlatnosci");
                var vatRegisters = (ICollection)session.CreateObject("CDN.RejestryVAT");
                Kontrahenci customers;
                IKontrahent customer;

                try
                {
                    //odwołania do istniejącego kontrahenta
                    customers = (Kontrahenci)session.CreateObject("CDN.Kontrahenci");
                    if ((int)vatRegisterType == 1)
                    {
                        customer = customers["Knt_NIP='" + NipSplit.GetNipNumber(invoice.SellerTaxNumber.Replace("-", "")) + "'"];
                    }
                    else if (!string.IsNullOrWhiteSpace(invoice.BuyerTaxNumber))
                    {
                        customer = customers["Knt_NIP='" + NipSplit.GetNipNumber(invoice.BuyerTaxNumber.Replace("-", "")) + "'"];
                    }
                    else if (invoice.BuyerName == "SPRZEDAŻ DETALICZNA" && invoice.VatRegisterName == "SKP1")
                    {
                        customer = customers["knt_kod = 'DETAL_SKP1'"];
                    }
                    else if (invoice.BuyerName == "SPRZEDAŻ DETALICZNA" && invoice.VatRegisterName == "SKP2")
                    {
                        customer = customers["knt_kod = 'DETAL_SKP2'"];
                    }
                    else if (invoice.BuyerName == "SPRZEDAŻ DETALICZNA" && invoice.VatRegisterName.StartsWith("ACSP"))
                    {
                        customer = customers["knt_kod = 'DETAL_ACSP'"];
                    }
                    else if (!string.IsNullOrWhiteSpace(invoice.BuyerName))
                    {
                        customer = customers["Knt_Nazwa1='" + invoice.BuyerName + "'"];
                    }
                    else
                    {
                        customer = customers["knt_kntid = 1"];
                    }
                }
                catch (Exception)
                {
                    var session1 = new AdoSession();
                    customers = (Kontrahenci)session1.CreateObject("CDN.Kontrahenci");
                    customer = customers.AddNew();
                    if ((int)vatRegisterType == 1)
                    {
                        customer.Akronim = NipSplit.GetNipNumber(invoice.SellerTaxNumber.Replace("-", ""));
                        customer.Nazwa1 = invoice.SellerName;
                        if (string.IsNullOrWhiteSpace(invoice.SellerTaxNumber))
                        {
                            customer.Akronim = "NOWY" + new Random().Next(1000000, 9999999);
                        }
                        else
                        {
                            try
                            {
                                customer.NumerNIP.NIPKraj = NipSplit.GetNipCountry(invoice.SellerTaxNumber);
                                customer.NumerNIP.NipE = NipSplit.GetNipNumber(invoice.SellerTaxNumber.Replace("-", ""));
                            }
                            catch (Exception e)
                            {
                                LogManager.GetCurrentClassLogger()
                                    .Info($"Nie udało się zapisać NIPu dla faktury {invoice.Number}, błąd: {e}");
                            }
                        }

                        customer.Adres.Ulica = invoice.Street;
                        customer.Adres.Miasto = invoice.City;
                        customer.Adres.KodPocztowy = invoice.PostalCode;
                        customer.Rodzaj_Dostawca = 1;
                        customer.Rodzaj_Odbiorca = 0;
                    }
                    else
                    {
                        customer.Akronim = NipSplit.GetNipNumber(invoice.BuyerTaxNumber.Replace("-", ""));
                        customer.Nazwa1 = invoice.BuyerName;
                        if (string.IsNullOrWhiteSpace(invoice.BuyerTaxNumber) && !nameAsCustomerCode)
                        {
                            customer.Akronim = "_NOWY" + new Random().Next(1000000, 9999999); ;
                        }
                        else if (string.IsNullOrWhiteSpace(invoice.BuyerTaxNumber) && nameAsCustomerCode)
                        {
                            customer.Akronim = invoice.BuyerName;
                        }
                        else
                        {
                            try
                            {
                                customer.NumerNIP.NIPKraj = NipSplit.GetNipCountry(invoice.BuyerTaxNumber);
                                customer.NumerNIP.NipE = NipSplit.GetNipNumber(invoice.BuyerTaxNumber.Replace("-", ""));
                            }
                            catch (Exception e)
                            {
                                LogManager.GetCurrentClassLogger()
                                    .Info($"Nie udało się zapisać NIPu dla faktury {invoice.Number}, błąd: {e}");
                            }
                        }
                        customer.Adres.Ulica = invoice.BuyerStreet;
                        customer.Adres.Miasto = invoice.BuyerCity;
                        customer.Adres.KodPocztowy = invoice.BuyerPostalCode;
                        customer.Rodzaj_Dostawca = 0;
                        customer.Rodzaj_Odbiorca = 1;
                    }
                    try
                    {
                        session1.Save();
                    }
                    catch (Exception ex)
                    {
                        LogManager.GetCurrentClassLogger()
                            .Info($"Nie udało się zapisać kontrahenta dla faktury {invoice.Number}, błąd: {ex}");
                    }
                }

                var vatRegister = (CDNRVAT.VAT)vatRegisters.AddNew();
                vatRegister.Typ = vatRegisterType == VatRegisterType.Purchase ? 1 : 2;
                vatRegister.Rejestr = !string.IsNullOrWhiteSpace(invoice.VatRegisterName)
                    ? invoice.VatRegisterName
                    : !string.IsNullOrWhiteSpace(vatTegisterName)
                        ? vatTegisterName :
                        vatRegister.Typ == 1
                            ? "ZAKUP"
                            : "SPRZEDAŻ";
                vatRegister.Dokument = invoice.Number;

                if ((int)vatRegisterType == 1)
                {
                    vatRegister.DataZap = invoice.IncomeDate;
                    vatRegister.DataOpe = invoice.OperationDate;
                    vatRegister.DataWys = invoice.InvoiceDate;
                }
                if ((int)vatRegisterType == 2)
                {
                    vatRegister.DataWys = invoice.InvoiceDate;
                    vatRegister.DataZap = invoice.InvoiceDate;
                    vatRegister.DataOpe = invoice.OperationDate;
                }

                try
                {
                    vatRegister.Podmiot = customer;
                }
                catch (Exception)
                {
                    vatRegister.Podmiot = customers["Knt_KntId=1"];
                }

                if (vatRegister.Podmiot == customers["Knt_KntId=1"])
                {
                    vatRegister.Nazwa1 = invoice.BuyerName;
                    vatRegister.PodAdres.Ulica = invoice.BuyerAddress;
                    vatRegister.PodAdres.Miasto = invoice.City;
                }

                var paymentForm = (OP_KASBOLib.FormaPlatnosci)paymentForms["Fpl_FplId=1"];
                try
                {
                    if ((int)vatRegisterType == 1)
                        paymentForm = (OP_KASBOLib.FormaPlatnosci)paymentForms[$"Fpl_Nazwa='{invoice.PaymentForm}'"];
                    if ((int)vatRegisterType == 2)
                        paymentForm = (OP_KASBOLib.FormaPlatnosci)paymentForms[$"Fpl_Nazwa='{invoice.Payments.First().Form}'"];
                }
                catch (Exception e)
                {
                    Console.WriteLine(e);
                }
                try
                {
                    vatRegister.FormaPlatnosci = paymentForm;
                    if ((int)vatRegisterType == 1)
                        vatRegister.Termin = invoice.PaymentDate;
                    if ((int)vatRegisterType == 2)
                        vatRegister.Termin = invoice.Payments.First().Date;
                }
                catch (Exception e)
                {
                    LogManager.GetCurrentClassLogger().Info($"Brak terminu płatności dla faktury o numerze {invoice.Number}, błąd: {e}");
                }

                var currencies = new List<string> { "EUR", "USD" };
                if (!string.IsNullOrWhiteSpace(invoice.Currency) && currencies.Contains(invoice.Currency))
                {
                    var waluty = (ICollection)session.CreateObject("CDN.Waluty");
                    var waluta = (Waluta)waluty[$"WNa_Symbol='{invoice.Currency}'"];
                    vatRegister.Waluta = waluta;
                    vatRegister.WalutaDoVAT = waluta;
                    var kurs = session.CreateObject("CDN.TypyKursowWalut").Item("WKu_Symbol='NBP'");
                    vatRegister.TypKursuWaluty = kurs;
                    vatRegister.TypKursuWalutyDoVAT = kurs;
                }

                if (!string.IsNullOrWhiteSpace(invoice.CorrectedNumber))
                {
                    vatRegister.Korekta = 1;
                    vatRegister.KorektaDo = invoice.CorrectedNumber;
                }

                vatRegister.KategoriaOpis = invoice.Description;

                var vatElements = vatRegister.Elementy;
                foreach (var row in invoice.Rows)
                {
                    var vatElement = (CDNRVAT.VATElement)vatElements.AddNew();
                    if (!string.IsNullOrWhiteSpace(row.Category1Description))
                    {
                        //SKP
                        var kategorie = (ICollection)session.CreateObject("CDN.Kategorie");
                        if (row.Category1Description == "OPŁATA EWIDENCYJNA" && invoice.VatRegisterName == "SKP1")
                        {
                            var kategoria = (Kategoria)kategorie[$"Kat_KodOgolny = 'OPŁATA SKP1'"];
                            vatElement.Kategoria = kategoria;
                        }
                        else if (row.Category1Description.StartsWith("BADANIE TECHNICZNE") && invoice.VatRegisterName == "SKP1")
                        {
                            var kategoria = (Kategoria)kategorie[$"Kat_KodOgolny = 'BADANIE SKP1'"];
                            vatElement.Kategoria = kategoria;
                        }
                        else if (row.Category1Description == "OPŁATA EWIDENCYJNA" && invoice.VatRegisterName == "SKP2")
                        {
                            var kategoria = (Kategoria)kategorie[$"Kat_KodOgolny = 'OPŁATA SKP2'"];
                            vatElement.Kategoria = kategoria;
                        }
                        else if (row.Category1Description.StartsWith("BADANIE TECHNICZNE") && invoice.VatRegisterName == "SKP2")
                        {
                            var kategoria = (Kategoria)kategorie[$"Kat_KodOgolny = 'BADANIE SKP2'"];
                            vatElement.Kategoria = kategoria;
                        }

                        //BLACHARNIA
                        else if (row.Category1Description == "ROBOCIZNA" && invoice.VatRegisterName == "BLACHARNIA")
                        {
                            var kategoria = (Kategoria)kategorie[$"Kat_KodOgolny = 'BLACHARNIA USŁUGA'"];
                            vatElement.Kategoria = kategoria;
                        }
                        else if (row.Category1Description == "KOSZTY DODATKOWE" && invoice.VatRegisterName == "BLACHARNIA")
                        {
                            var kategoria = (Kategoria)kategorie[$"Kat_KodOgolny = 'BLACHARNIA KOSZTY'"];
                            vatElement.Kategoria = kategoria;
                        }
                        else if (row.Category1Description == "PROWIZJA" && invoice.VatRegisterName == "BLACHARNIA")
                        {
                            var kategoria = (Kategoria)kategorie[$"Kat_KodOgolny = 'BLACHARNIA PROWIZJA'"];
                            vatElement.Kategoria = kategoria;
                        }
                        else if (row.Category1Description == "WYNAJEM SAMOCHODU" && invoice.VatRegisterName == "BLACHARNIA")
                        {
                            var kategoria = (Kategoria)kategorie[$"Kat_KodOgolny = 'BLACHARNIA SAMOCHÓD'"];
                            vatElement.Kategoria = kategoria;
                        }
                        else if (row.Category1Description == "MATERIAŁY" && invoice.VatRegisterName == "BLACHARNIA")
                        {
                            var kategoria = (Kategoria)kategorie[$"Kat_KodOgolny = 'BLACHARNIA MATERIAŁ'"];
                            vatElement.Kategoria = kategoria;
                        }

                        //ACSP
                        else if (row.Category1Description == "ROBOCIZNA" && invoice.VatRegisterName.StartsWith("ACSP"))
                        {
                            var kategoria = (Kategoria)kategorie[$"Kat_KodOgolny = 'SERWIS USŁUGA'"];
                            vatElement.Kategoria = kategoria;
                        }
                        else if (row.Category1Description == "WYNAJEM SAMOCHODU" && invoice.VatRegisterName.StartsWith("ACSP"))
                        {
                            var kategoria = (Kategoria)kategorie[$"Kat_KodOgolny = 'SERWIS SAMOCHÓD'"];
                            vatElement.Kategoria = kategoria;
                        }
                        else if (row.Category1Description == "MATERIAŁY" && invoice.VatRegisterName.StartsWith("ACSP"))
                        {
                            var kategoria = (Kategoria)kategorie[$"Kat_KodOgolny = 'SERWIS MATERIAŁY'"];
                            vatElement.Kategoria = kategoria;
                        }

                        //ZAKUP
                        else if (row.Category1Description.StartsWith("MAGAZYN_"))
                        {
                            var kategoria = (Kategoria)kategorie[$"Kat_KodSzczegol = '{row.Category1Description}'"];
                            vatElement.Kategoria = kategoria;
                        }
                    }
                    else
                    {
                        try
                        {
                            var stawkaString = row.VatRate.Replace("\"", "").Replace(".0000", ",00");
                            vatElement.Stawka = Convert.ToDouble(stawkaString);
                        }
                        catch (Exception)
                        {
                            if (row.VatRate.Replace("\"", "").Replace(".0000", ",00").ToUpper().StartsWith("ZW"))
                            {
                                vatElement.Stawka = 0;
                                vatElement.Flaga = 1;
                            }
                            else if (row.VatRate.Replace("\"", "").Replace(".0000", ",00").ToUpper().StartsWith("NP"))
                            {
                                vatElement.Stawka = 0;
                                vatElement.Flaga = 4;
                            }
                            else
                            {
                                vatElement.Stawka = 23;
                            }
                        }
                        vatElement.RodzajZakupu = 1;
                        vatElement.Odliczenia = 1;
                    }
                    vatElement.Netto = row.NetValue;
                    vatElement.Brutto = row.GrossValue;
                }

                if (!string.IsNullOrWhiteSpace(invoice.Description))
                {
                    vatRegister.KategoriaOpis = invoice.Description;
                }

                try
                {
                    session.Save();
                    nieZaczytaneDokumenty.Remove(invoice);
                    validInvoicesCounter++;
                    Debug.WriteLine(validInvoicesCounter);
                    InvoiceImported?.Invoke(validInvoicesCounter);
                }
                catch (Exception e)
                {
                    inValidInvoicesCounter++;
                    Debug.WriteLine($"{inValidInvoicesCounter} błędnych faktur");
                    LogManager.GetCurrentClassLogger()
                        .Info($"Nie udało się zapisać faktury o numerze {invoice.Number}, błąd: {e}");
                }

            }

            foreach (var badInvoice in nieZaczytaneDokumenty)
            {
                Debug.WriteLine(badInvoice.Number);
            }
        }
    }
}
