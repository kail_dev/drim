﻿using Idea.Biz.Model;
using OptimaImporter.Biz.Model;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text.RegularExpressions;
using System.Xml.Linq;
using Payment = Idea.Biz.Model.Payment;

namespace Idea.Biz.Services
{
    public class SourceFileReader
    {
        public static List<string> AcsServicesListSKP =>
            new List<string>
            {
                "OPŁATA EWIDENCYJNA",
                "BADANIE TECHNICZNE",
                "BADANIE TECHNICZNE Z GAZEM"
            };

        public static List<string> AcsServicesListBlacharnia =>
            new List<string>
            {
                "ROBOCIZNA BLACHARSKA",
                "NAPRAWA BLACHARSKA",
                "ROBOCIZNA LAKIERNICZA",
                "NAPRAWA LAKIERNICZA",
                "MATERIAŁY LAKIERNICZE",
                "MATERIAŁY LAKIERNICZE Z NORMALIAMI",
                "NORMALIA",
            };

        public static List<IdeaVatInvoice> ReadAcsPurchaseInvoices(string sourcePath)
        {
            var invoices = new List<IdeaVatInvoice>();
            var xdoc = XDocument.Load(sourcePath);

            foreach (var xElement in xdoc.Descendants().Where(x => x.Name.LocalName == "DOKUMENT"))
            {
                var invoice = new IdeaVatInvoice
                {
                    IncomeDate = Convert.ToDateTime(GetElementValue(xElement, "DATA_WPLYWU")),
                    InvoiceDate = Convert.ToDateTime(GetElementValue(xElement, "DATA_WYSTAWIENIA")),
                    OperationDate = Convert.ToDateTime(GetElementValue(xElement, "DATA_SPRZEDAZY")),
                    DocumentDate = Convert.ToDateTime(GetElementValue(xElement, "DATA_DOKUMENTU")),
                    Number = GetElementValue(xElement, "DOC_POWIAZANY"),
                    SellerName = GetElementDescendantsValue(xElement, "NAZWA"),
                    SellerAddress = GetElementDescendantsValue(xElement, "ADRES"),
                    SellerTaxNumberCountry = GetElementDescendantsValue(xElement, ""),
                    SellerTaxNumber = GetElementDescendantsValue(xElement, "NIP"),
                    PaymentDate = Convert.ToDateTime(GetElementValue(xElement, "TERMIN_PLATNOSCI")),
                    NetValue = Convert.ToDecimal(GetElementValue(xElement, "WARTOSC_NETTO")),
                    GrossValue = Convert.ToDecimal(GetElementValue(xElement, "WARTOSC_BRUTTO")),
                    CorrectedNumber = GetElementDescendantsAttributeValue(xElement, "KOREKTA", "NUMER"),
                    Description = GetElementValue(xElement, "DOC_POWIAZANY_PZ")
                };

                if (invoice.Number == "KOREKTA WLASNA")
                {
                    invoice.Number = "KOREKTA WLASNA " + xElement.Attributes().FirstOrDefault(a => a.Name == "NUMER")?.Value;
                }

                var invoiceRow = new VatInvoiceRow
                {
                    NetValue = Convert.ToDecimal(GetElementDescendantsValue(xElement, "NETTO")),
                    VatRate = GetElementDescendantsAttributeValue(xElement, "STAWKA", "NAZWA"),
                    VatValue = GetElementDescendantsValue(xElement, "PODATEK") != null
                        ? Convert.ToDecimal(GetElementDescendantsValue(xElement, "PODATEK"))
                        : 0.00m,
                    GrossValue = Convert.ToDecimal(GetElementDescendantsValue(xElement, "BRUTTO")),
                };

                if (GetElementValue(xElement, "DOC_POWIAZANY_PZ") != null
                    && GetElementValue(xElement, "DOC_POWIAZANY_PZ").Contains("(11"))
                {
                    invoice.VatRegisterName = "ZAKUP BLACHARNIA";
                    invoiceRow.Category1Description = "MAGAZYN_BLACHARNIA";
                }
                else if (GetElementValue(xElement, "DOC_POWIAZANY_PZ") != null
                    && GetElementValue(xElement, "DOC_POWIAZANY_PZ").Contains("(19"))
                {
                    invoice.VatRegisterName = "ZAKUP SERWIS";
                    invoiceRow.Category1Description = "MAGAZYN_SERWIS";
                }
                else if (GetElementValue(xElement, "DOC_POWIAZANY_PZ") != null
                    && GetElementValue(xElement, "DOC_POWIAZANY_PZ").Contains("(21"))
                {
                    invoice.VatRegisterName = "ZAKUP POJAZDY";
                    invoiceRow.Category1Description = "MAGAZYN_POJAZDY";
                }

                if (invoiceRow.VatRate == null)
                {
                    invoiceRow.VatRate = "23";
                }
                invoice.Rows.Add(invoiceRow);

                if (invoice.DocumentDate != invoice.PaymentDate)
                {
                    invoice.PaymentForm = "przelew";
                }
                else
                {
                    invoice.PaymentForm = "gotówka ACSP";
                }

                if (string.IsNullOrWhiteSpace(invoice.Number)) continue;

                invoices.Add(invoice);
            }

            return invoices;
        }

        public static List<IdeaVatInvoice> ReadAcsSalesInvoices(string sourcePath)
        {
            var invoices = new List<IdeaVatInvoice>();
            var xdoc = XDocument.Load(sourcePath);

            foreach (var xElement in xdoc.Descendants().Where(x => x.Name.LocalName == "DOKUMENT"))
            {
                var elements = xElement.Attributes();
                var invoice = new IdeaVatInvoice
                {
                    //IncomeDate = Convert.ToDateTime(GetElementValue(xElement, "DATA_WPLYWU")),
                    InvoiceDate = Convert.ToDateTime(GetElementValue(xElement, "DATA_WYSTAWIENIA")),
                    OperationDate = Convert.ToDateTime(GetElementValue(xElement, "DATA_SPRZEDAZY")),
                    Number = xElement.Attributes().FirstOrDefault(a => a.Name == "NUMER")?.Value,
                    BuyerName = GetElementDescendantsValue(xElement, "NAZWA"),
                    BuyerAddress = GetElementDescendantsValue(xElement, "ADRES"),
                    BuyerTaxNumberCountry = GetElementDescendantsValue(xElement, ""),
                    BuyerTaxNumber = GetElementDescendantsValue(xElement, "NIP"),
                    PaymentForm = GetElementValue(xElement, "FORMA_PLATNOSCI"),
                    PaymentDate = Convert.ToDateTime(GetElementValue(xElement, "TERMIN_PLATNOSCI")),
                    NetValue = Convert.ToDecimal(GetElementValue(xElement, "WARTOSC_NETTO")),
                    GrossValue = Convert.ToDecimal(GetElementValue(xElement, "WARTOSC_BRUTTO")),
                    CorrectedNumber = GetElementDescendantsAttributeValue(xElement, "KOREKTA", "NUMER")
                };

                if (invoice.BuyerName != "SPRZEDAŻ DETALICZNA")
                {
                    invoice.BuyerStreet = GetElementDescendantsValue(xElement, "ADRES").Split(';')[1].Trim();
                    var cityPostalCode = GetElementDescendantsValue(xElement, "ADRES").Split(';')[0].Trim();
                    invoice.BuyerCity = Regex.IsMatch(cityPostalCode, @"^\d+")
                        ? cityPostalCode.Split(' ')[1].Trim()
                        : cityPostalCode.Trim();
                    invoice.BuyerPostalCode = !Regex.IsMatch(cityPostalCode, @"^\d+")
                        ? ""
                        : cityPostalCode.Split(' ')[0].Trim();

                }
                else
                {
                    invoice.BuyerAddress = "";
                }

                AcsVatRegisterSelector(invoice);

                var carNumber = GetElementDescendantsValue(xElement, "NR_REJESTRACYJNY");
                if (!string.IsNullOrWhiteSpace(carNumber)) invoice.Description = $"NR REJESTRACYJNY: {carNumber}";

                //POZYCJE
                foreach (var xElementRow in xElement.Elements().Where(e => e.Name == "POZYCJA"))
                {
                    var invoiceRow = new VatInvoiceRow
                    {
                        Category1Description = GetElementDescendantsValue(xElementRow, "NAZWA"),
                        RowVatType = VatTypeSelector(xElementRow.Attributes().FirstOrDefault(a => a.Name == "IS_TOWAR")?.Value),
                        NetValue = Convert.ToDecimal(GetElementDescendantsValue(xElementRow, "WARTOSC_NETTO")),
                        VatRate = GetElementDescendantsValue(xElementRow, "STAWKA_VAT").Replace("%", ""),
                        //VatValue = Convert.ToDecimal(GetElementDescendantsValue(xElementRow, "PODATEK")),
                        GrossValue = Convert.ToDecimal(GetElementDescendantsValue(xElementRow, "WARTOSC_BRUTTO")),
                    };

                    //SKP
                    if (invoice.VatRegisterName.StartsWith("SKP") &&
                        invoiceRow.Category1Description.StartsWith("BADANIE TECHNICZNE")) invoiceRow.Category1Description = "BADANIE TECHNICZNE";
                    else if (invoice.VatRegisterName.StartsWith("SKP") &&
                        !AcsServicesListSKP.Contains(invoiceRow.Category1Description)) invoiceRow.Category1Description = "";

                    //BLACHARNIA
                    if (invoice.VatRegisterName.EndsWith("BLACHARNIA") &&
                        AcsServicesListBlacharnia.Contains(invoiceRow.Category1Description)) invoiceRow.Category1Description = "ROBOCIZNA";
                    else if (invoice.VatRegisterName.EndsWith("BLACHARNIA") &&
                             invoiceRow.Category1Description.StartsWith("KOSZTY DODATKOWE")) invoiceRow.Category1Description = "KOSZTY DODATKOWE";
                    else if (invoice.VatRegisterName.EndsWith("BLACHARNIA") &&
                             invoiceRow.Category1Description.StartsWith("PROWIZJ")) invoiceRow.Category1Description = "PROWIZJA";
                    else if (invoice.VatRegisterName.EndsWith("BLACHARNIA") &&
                             invoiceRow.Category1Description.StartsWith("WYNAJEM")) invoiceRow.Category1Description = "WYNAJEM SAMOCHODU";
                    else if (invoice.VatRegisterName.EndsWith("BLACHARNIA") &&
                             !AcsServicesListBlacharnia.Contains(invoiceRow.Category1Description)) invoiceRow.Category1Description = "";

                    //ACSP
                    if (invoice.VatRegisterName.StartsWith("ACSP") &&
                        invoiceRow.Category1Description.StartsWith("WYNAJEM")) invoiceRow.Category1Description = "WYNAJEM SAMOCHODU";
                    else if (invoice.VatRegisterName.StartsWith("ACSP"))
                        invoiceRow.Category1Description = "";

                    invoice.Rows.Add(invoiceRow);
                }


                if (string.IsNullOrWhiteSpace(invoice.Number)) continue;

                invoice.Rows = (from r in invoice.Rows
                                group r by new { r.Category1Description, r.VatRate, r.RowVatType } into g
                                select new VatInvoiceRow
                                {
                                    Category1Description = g.Key.Category1Description,
                                    VatRate = g.Key.VatRate,
                                    RowVatType = g.Key.RowVatType,
                                    NetValue = g.Sum(g2 => g2.NetValue),
                                    GrossValue = g.Sum(g2 => g2.GrossValue)
                                }).ToList();

                foreach (var invoiceRow in invoice.Rows)
                {
                    if (invoiceRow.Category1Description == "" && invoiceRow.RowVatType == 1)
                    {
                        invoiceRow.Category1Description = "MATERIAŁY";
                    }
                    else if (invoiceRow.Category1Description == "" && invoiceRow.RowVatType == 4)
                    {
                        invoiceRow.Category1Description = "ROBOCIZNA";
                    }
                }

                //PŁATNOśCI
                foreach (var xElementPayment in xElement.Elements().Where(e => e.Name == "ROZLICZENIE"))
                {
                    var payment = new Payment
                    {
                        Form = GetElementValue(xElementPayment, "NAZWA"),
                        Date = Convert.ToDateTime(GetElementValue(xElementPayment, "DATA")),
                        Amount = Convert.ToDecimal(GetElementValue(xElementPayment, "WARTOSC"))
                    };

                    //przelew
                    if (payment.Form.ToLower().StartsWith("przelew")) payment.Form = "przelew";

                    //SKP
                    if (invoice.VatRegisterName.StartsWith("SKP1")
                        && payment.Form.ToLower().StartsWith("gotówka")) payment.Form = "gotówka SKP1";
                    else if (invoice.VatRegisterName.StartsWith("SKP2")
                        && payment.Form.ToLower().StartsWith("gotówka")) payment.Form = "gotówka SKP2";
                    else if (invoice.VatRegisterName.StartsWith("SKP1")
                             && payment.Form.ToLower().StartsWith("karta")) payment.Form = "karta SKP1";
                    else if (invoice.VatRegisterName.StartsWith("SKP2")
                             && payment.Form.ToLower().StartsWith("karta")) payment.Form = "karta SKP2";

                    //BLACHARNIA
                    if (invoice.VatRegisterName.EndsWith("BLACHARNIA") &&
                        payment.Form.ToLower().StartsWith("gotówka")) payment.Form = "gotówka ACSP";
                    else if (invoice.VatRegisterName.EndsWith("BLACHARNIA") &&
                        payment.Form.ToLower().StartsWith("karta")) payment.Form = "karta";

                    //ACSP
                    if (invoice.VatRegisterName.StartsWith("ACSP") &&
                        payment.Form.ToLower().StartsWith("gotówka")) payment.Form = "gotówka ACSP";
                    else if (invoice.VatRegisterName.StartsWith("ACSP") &&
                             payment.Form.ToLower().StartsWith("karta")) payment.Form = "karta";

                    invoice.Payments.Add(payment);
                }

                invoices.Add(invoice);
            }

            return invoices;
        }

        private static void AcsVatRegisterSelector(IdeaVatInvoice invoice)
        {
            if (invoice.Number.EndsWith("FBL/ACSP") || invoice.Number.EndsWith("FKBL/ACSP"))
            {
                invoice.VatRegisterName = "BLACHARNIA";
            }
            else if (invoice.Number.EndsWith("SKP1/ACSP"))
            {
                invoice.VatRegisterName = "SKP1";
            }
            else if (invoice.Number.EndsWith("SKP2/ACSP"))
            {
                invoice.VatRegisterName = "SKP2";
            }
            else if (invoice.Number.EndsWith("FVS/ACSP") || invoice.Number.EndsWith("FKS/ACSP"))
            {
                invoice.VatRegisterName = "ACSP FAKTURA";
            }
            else if (invoice.Number.EndsWith("PAR/ZL/ACSP") || invoice.Number.EndsWith("PAR/ACSP") || invoice.Number.EndsWith("KPAR/ZL/ACSP") || invoice.Number.EndsWith("KPAR/ACSP"))
            {
                invoice.VatRegisterName = "ACSP PARAGON";
            }
            else if (invoice.Number.EndsWith("INN/ACSP"))
            {
                invoice.VatRegisterName = "POZOSTAŁA";
            }
            else if (invoice.Number.EndsWith("CEN/ACSP"))
            {
                invoice.VatRegisterName = "POJAZDY";
            }
            else
            {
                invoice.VatRegisterName = "";
            }
        }

        private static string AcsVatRateSelector(string vatRate)
        {
            switch (vatRate)
            {
                case "H":
                    return "23,00";
                case "N":
                    return "NP";
                default:
                    return "23,00";
            }
        }

        private static int VatTypeSelector(string sourceType)
        {
            switch (sourceType)
            {
                case "0":
                    return 4;
                case "1":
                    return 1;
                default:
                    return 1;
            }
        }

        private static string GetElementValue(XElement xElement, string fieldName)
        {
            return xElement.Elements().FirstOrDefault(x => x.Name.LocalName.ToString().StartsWith(fieldName))?.Value;
        }

        private static string GetElementDescendantsValue(XElement xElement, string fieldName)
        {
            return xElement.Descendants().FirstOrDefault(x => x.Name.LocalName.ToString().StartsWith(fieldName))?.Value;
        }
        private static string GetElementDescendantsAttributeValue(XElement xElement, string fieldName, string attributeName)
        {
            var vat = xElement.Descendants().FirstOrDefault(x => x.Name.ToString().StartsWith(fieldName));

            return vat?.Attributes().FirstOrDefault(a => a.Name == attributeName)?.Value.Replace("%", "");
        }

    }
}
