﻿using System;
using System.Collections.Generic;
using CDNBase;
using Idea.Biz.Model;
using NLog;

namespace Idea.Biz.Services
{
    public class OptimaVatDocsCreator
    {
        public static ProgressCounter CreateOptimaVatDocs(IProgress<ProgressCounter> progress, IEnumerable<IdeaVatInvoice> csvInvoices,
            string register, int registerType)
        {
            var progressCounter = new ProgressCounter();
            foreach (var invoice in csvInvoices)
            {
                var session = new AdoSession();
                var rejestryVat = (ICollection)session.CreateObject("CDN.RejestryVAT");
                var rejestrVat = (CDNRVAT.VAT)rejestryVat.AddNew();
                try
                {
                    rejestrVat.Rejestr = register;
                    rejestrVat.Typ = registerType;             //	1 - zakupu; 2 - sprzedaży
                    var formyPlatnosci = (ICollection)session.CreateObject("CDN.FormyPlatnosci");
                    var fPl = (OP_KASBOLib.FormaPlatnosci)formyPlatnosci["Fpl_Nazwa='przelew'"];
                    rejestrVat.Dokument = string.IsNullOrWhiteSpace(invoice.Number) ? "bez numeru" : invoice.Number;
                    rejestrVat.PodID = 1;
                    rejestrVat.Nazwa1 = invoice.BuyerName;
                    rejestrVat.PodAdres.Ulica = invoice.BuyerAddress;
                    rejestrVat.PodAdres.Miasto = invoice.City;
                    rejestrVat.FormaPlatnosci = fPl;
                    rejestrVat.DataZap = invoice.InvoiceDate;
                    rejestrVat.DataWys = invoice.InvoiceDate;
                    rejestrVat.Termin = invoice.PaymentDate;

                    var elementy = rejestrVat.Elementy;
                    var element = (CDNRVAT.VATElement)elementy.AddNew();

                    element.Stawka = 0;
                    element.Flaga = 4; //stawka VAT: 1-zwolniona, 2-opodatkowana, 4-nie podlega
                    try
                    {
                        element.Netto = invoice.NetValue;
                    }
                    catch (Exception ex)
                    {
                        LogManager.GetCurrentClassLogger().Error($"Odczyt kwoty netto nie powiódł się: {ex}");
                    }

                    element.Odliczenia = 0;
                    rejestrVat.RozliczacVAT = 0;
                    rejestrVat.SprawdzDuplikatVATEx(1);
                    try
                    {
                        session.Save();
                        progressCounter.ResultsCreated++;
                        LogManager.GetCurrentClassLogger().Info($"Utworzono dokument {invoice.Number}");

                        progress.Report(progressCounter);
                    }
                    catch (Exception ex)
                    {
                        LogManager.GetCurrentClassLogger().Error($"Błąd na linii {progressCounter.RowsRead}: {ex}");
                    }
                }
                catch (FormatException ex)
                {
                    if (ex.ToString().Contains("DateTime"))
                    {
                        LogManager.GetCurrentClassLogger().Error($"Błędny format daty na linii {progressCounter.RowsRead}: {ex}");
                    }
                    else
                    {
                        LogManager.GetCurrentClassLogger().Error($"Błąd na linii {progressCounter.RowsRead}: {ex.InnerException.InnerException}");
                    }
                }
                catch (Exception ex)
                {
                    if (ex.ToString().Contains("SprawdzDuplikatVATEx(Int32 ThrowException)"))
                    {
                        LogManager.GetCurrentClassLogger().Error($"Linia {progressCounter.RowsRead}. Istnieje już podobny dokument");
                    }
                    else
                    {
                        LogManager.GetCurrentClassLogger().Error($"Błąd na linii {progressCounter.RowsRead}: {ex.InnerException.InnerException}");
                    }
                }
                progressCounter.RowsRead++;
                progress.Report(progressCounter);
                session = null;
            }
            return progressCounter;
        }

    }
}
