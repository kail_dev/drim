﻿namespace Idea.Biz.Model
{
    public class InvoiceRow
    {
        public string Name { get; set; }
        public int Type { get; set; }
        public decimal NetValue { get; set; }
        public string VatRate { get; set; }
        public decimal VatValue { get; set; }
        public decimal GrossValue { get; set; }
    }
}
