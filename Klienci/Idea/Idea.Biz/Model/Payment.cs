﻿using System;

namespace Idea.Biz.Model
{
    public class Payment
    {
        public string Form { get; set; }
        public decimal Amount { get; set; }
        public DateTime Date { get; set; }
    }
}
