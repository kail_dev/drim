﻿using System;
using System.Collections.Generic;
using OptimaImporter.Biz.Model;

namespace Idea.Biz.Model
{
    public class IdeaVatInvoice : VatInvoice
    {
        public new DateTime InvoiceDate { get; set; } //DATA_WYSTAWIENIA
        public new DateTime OperationDate { get; set; } //DATA_SPRZEDAZY
        public DateTime IncomeDate { get; set; } //DATA_WPLYWU
        public DateTime DocumentDate { get; set; } //DATA_WPLYWU
        public string VatRegisterName { get; set; }
        public string BuyerStreet { get; set; }
        public string BuyerCity { get; set; }
        public string BuyerPostalCode { get; set; }
        public virtual List<Payment> Payments { get; set; } = new List<Payment>();
    }
}
