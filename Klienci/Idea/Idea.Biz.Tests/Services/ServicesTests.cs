﻿using Idea.Biz.Model;
using Idea.Biz.Services;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using System.Linq;
using OptimaImporter.Biz.Optima;
using OptimaImporter.Data.Config;
using OptimaImporter.Data.Enum;

namespace Idea.Biz.Tests.Services
{
    [TestClass]
    public class ServicesTests
    {
        public ServicesTests()
        {
            ApplicationConfig.Config.OptimaUser = "ADMIN";
            ApplicationConfig.Config.OptimaPassword = "a";
            ApplicationConfig.Config.OptimaDatabase = "ACS";
        }

        [TestMethod]
        public void ReadAcsPurchaseInvoicesTest()
        {
            var filePath = @"W:\Klienci\Anna Leszczyńska\migracja201901\zakup012019.xml";
            var invoicesCount = 619;
            var firstInvoice = new IdeaVatInvoice
            {
                SellerName = "INTER CARS S.A.",
                SellerAddress = "02-903 Warszawa; Powsińska 64",
                SellerTaxNumber = "1181452946",
                Number = "00000409/DAB/19/F" + " " + "26/19/PZ(19)/ACSP"

            };
            var firstInvoiceElement = new InvoiceRow
            {
                NetValue = 981.42m,
                VatRate = "23",
                VatValue = 225.73m,
                GrossValue = 1207.15m,
            };

            var invoices = SourceFileReader.ReadAcsPurchaseInvoices(filePath);

            Assert.AreEqual(invoicesCount, invoices.Count);

            Assert.AreEqual(firstInvoice.SellerName, invoices.First().SellerName);
            Assert.AreEqual(firstInvoice.SellerAddress, invoices.First().SellerAddress);
            Assert.AreEqual(firstInvoice.SellerTaxNumber, invoices.First().SellerTaxNumber);
            Assert.AreEqual(firstInvoice.Number, invoices.First().Number);
            Assert.AreEqual(firstInvoiceElement.NetValue, invoices.First().Rows.First().NetValue);
            Assert.AreEqual(firstInvoiceElement.VatRate, invoices.First().Rows.First().VatRate);
            Assert.AreEqual(firstInvoiceElement.VatValue, invoices.First().Rows.First().VatValue);
            Assert.AreEqual(firstInvoiceElement.GrossValue, invoices.First().Rows.First().GrossValue);
            Assert.AreEqual(firstInvoiceElement.GrossValue, invoices.First().GrossValue);
        }

        [TestMethod]
        public void ReadAcsSalesInvoicesTest()
        {
            //var filePath = @"W:\Klienci\Anna Leszczyńska\migracja201901\sprzedaz012019.xml";
            var filePath = @"W:\Klienci\Anna Leszczyńska\migracja201901\SKP012019.xml";
            //var invoicesCount = 186;
            var firstInvoice = new IdeaVatInvoice
            {
                BuyerName = "LAURENT ADRIEN PIERRE",
                BuyerAddress = "98-100 ŁASK; DĘBOWA 18",
                BuyerTaxNumber = "",
                BuyerStreet = "DĘBOWA 18",
                BuyerCity = "ŁASK",
                BuyerPostalCode = "98-100",
                PaymentForm = "GOTÓWKA",
            };
            var firstInvoiceElement = new InvoiceRow
            {
                NetValue = 481.07m,
                VatRate = "23",
                VatValue = 110.64m,
                GrossValue = 591.71m,
            };

            var invoices = SourceFileReader.ReadAcsSalesInvoices(filePath);

            //Assert.AreEqual(invoicesCount, invoices.Count);

            //Assert.AreEqual(firstInvoice.BuyerName, invoices.First().BuyerName);
            //Assert.AreEqual(firstInvoice.BuyerAddress, invoices.First().BuyerAddress);
            //Assert.AreEqual(firstInvoice.BuyerTaxNumber, invoices.First().BuyerTaxNumber);
            //Assert.AreEqual(firstInvoice.BuyerStreet, invoices.First().BuyerStreet);
            //Assert.AreEqual(firstInvoice.BuyerCity, invoices.First().BuyerCity);
            //Assert.AreEqual(firstInvoice.BuyerPostalCode, invoices.First().BuyerPostalCode);
            //Assert.AreEqual(firstInvoice.PaymentForm, invoices.First().PaymentForm);
            //Assert.AreEqual(firstInvoiceElement.NetValue, invoices.First().Rows.First().NetValue);
            //Assert.AreEqual(firstInvoiceElement.VatRate, invoices.First().Rows.First().VatRate);
            //Assert.AreEqual(firstInvoiceElement.VatValue, invoices.First().Rows.First().VatValue);
            //Assert.AreEqual(firstInvoiceElement.GrossValue, invoices.First().Rows.First().GrossValue);
            //Assert.AreEqual(firstInvoiceElement.GrossValue, invoices.First().GrossValue);
        }

        [TestMethod]
        public void LogInTest()
        {
            var loggedIn = OptimaLogging.LogIn();

            Assert.IsTrue(loggedIn);
        }

        [TestMethod]
        public void CreatePurchaseVatDocumentsTest()
        {
            var filePath = @"W:\Klienci\Anna Leszczyńska\migracja2019\01.02-05.02 ZAKUP.xml";
            var sourceDocumentsCount = 619;

            //var invoices = SourceFileReader.ReadAcsPurchaseInvoices(filePath).Take(int.MaxValue).ToList().Where(i => i.Number == "KOREKTA WLASNA 166/19/KZC/ACSP");
            //var invoices = SourceFileReader.ReadAcsPurchaseInvoices(filePath).Take(int.MaxValue).ToList().Where(i => i.Number == "25/2019");
            var invoices = SourceFileReader.ReadAcsPurchaseInvoices(filePath).Take(int.MaxValue).ToList();
            OptimaLogging.LogIn();
            var counter = 0;
            var importer = new VatRegisterImporter();
            importer.InvoiceImported += i => { counter++; };
            importer.VatRegisterImport(invoices.ToList(), VatRegisterType.Purchase, "ZAKUP");

            Assert.AreEqual(sourceDocumentsCount, counter);
        }

        [TestMethod]
        public void CreateSalesVatDocumentsTest()
        {
            // var filePath = @"W:\Klienci\Anna Leszczyńska\migracja201901\sprzedaz012019.xml";
            //var filePath = @"C:\Users\marci\Dropbox\Klienci\Anna Leszczyńska\migracja201901\sprzedaz012019.xml";
            //var filePath = @"W:\Klienci\Anna Leszczyńska\migracja201901\SKP012019.xml";
            var filePath = @"C:\Users\marci\Dropbox\Klienci\Anna Leszczyńska\migracja201901\SKP012019.xml";
            var sourceDocumentsCount = 1016;

            var invoices = SourceFileReader.ReadAcsSalesInvoices(filePath)
                .Take(10000).ToList();
            //.Where(i => i.Payments.Count > 2).ToList();
            //.Where(i => i.Number.Contains("2/19/PAR/ZL/ACSP")).Take(int.MaxValue).ToList();
            //.Where(i => i.Payments.First().Form == "przelew").Take(1).ToList();
            OptimaLogging.LogIn();
            var counter = 0;
            var importer = new VatRegisterImporter();
            importer.InvoiceImported += i => { counter++; };
            importer.VatRegisterImport(invoices.ToList(), VatRegisterType.Sales, "", true);

            Assert.AreEqual(sourceDocumentsCount, counter);
        }

    }
}