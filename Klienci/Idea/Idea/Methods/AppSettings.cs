using Idea.Properties;
using OptimaImporter.Data.Config;

namespace Idea.Methods
{
    public class AppSettings
    {
        public static void Load()
        {
            ApplicationConfig.Config.OptimaUser = Settings.Default.OptimaUser;
            ApplicationConfig.Config.OptimaPassword = Settings.Default.OptimaPassword;
            ApplicationConfig.Config.OptimaDatabase = Settings.Default.OptimaDatabase;
        }
        public static void Save()
        {
            Settings.Default.OptimaUser = ApplicationConfig.Config.OptimaUser;
            Settings.Default.OptimaPassword = ApplicationConfig.Config.OptimaPassword;
            Settings.Default.OptimaDatabase = ApplicationConfig.Config.OptimaDatabase;
            Settings.Default.Save();
        }

        public static void Reset()
        {
            Settings.Default.Reset();
        }
    }
}