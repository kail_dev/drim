﻿using Idea.Biz.Model;
using Idea.Biz.Services;
using Microsoft.Win32;
using NLog;
using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.IO;
using System.Threading.Tasks;
using System.Windows;
using OptimaImporter.Biz.Optima;
using OptimaImporter.Data.Enum;
using Telerik.Windows.Controls;

namespace Idea.View
{
    /// <summary>
    /// Interaction logic for MainWindow.xaml
    /// </summary>
    public partial class MainWindowView
    {
        private OpenFileDialog _openedFile;

        public MainWindowView()
        {
            StyleManager.ApplicationTheme = new Office_BlueTheme();

            InitializeComponent();
        }

        private void MainWindowView_OnLoaded(object sender, RoutedEventArgs e)
        {
            LogManager.GetCurrentClassLogger().Info("Start programu " + DateTime.Now);
        }

        private async void GenerateAcsPurchaseInvoicesButton_OnClick(object sender, RoutedEventArgs e)
        {
            StatusLabel.Content = " ";
            _openedFile = new OpenFileDialog();
            _openedFile.Title = "Wybierz plik do wczytania";

            var result = _openedFile.ShowDialog();
            if (result != null && (bool)result)
            {
                //sprawdzanie logowania do O!
                var optimaLoginValid = false;
                while (optimaLoginValid == false)
                {
                    optimaLoginValid = OptimaLogginIn();
                }
                StatusLabel.Content = "Zalogowałem się do O!";


                GenerateAcsPurchaseInvoicesButton.IsEnabled = false;
                //czytanie .xls
                var invoices = SourceFileReader.ReadAcsPurchaseInvoices(_openedFile.FileName);

                await GenerateAcsPurchaseInvoicesAsync(invoices)
                    .ContinueWith(t =>
                    {
                        Dispatcher.Invoke(() =>
                        {
                            GenerateAcsPurchaseInvoicesButton.IsEnabled = true;
                            StatusLabel.Content += $". Zakonczono!!!";
                        });
                    });
            }
        }

        private async Task GenerateAcsPurchaseInvoicesAsync(IList<IdeaVatInvoice> invoices)
        {
            var stopWatch = Stopwatch.StartNew();
            var invoicesCount = invoices.Count;
            var progress = new Progress<int>(p =>
            {
                var percent = p / (decimal)invoicesCount * 100;
                StatusLabel.Content = $"Wczytywanie faktur.. {p}/{invoicesCount},   {percent:0} %," +
                                      $"   {stopWatch.Elapsed.Hours}:{stopWatch.Elapsed.Minutes}:{stopWatch.Elapsed.Seconds}";
            });
            var importer = new VatRegisterImporter();
            await Task.Run(() => importer.VatRegisterImport(invoices, VatRegisterType.Purchase, "ZAKUP"));
        }

        private async void GenerateAcsSalesInvoicesButton_OnClick(object sender, RoutedEventArgs e)
        {
            StatusLabel.Content = " ";
            _openedFile = new OpenFileDialog();
            _openedFile.Title = "Wybierz plik do wczytania";

            var result = _openedFile.ShowDialog();
            if (result != null && (bool)result)
            {
                //sprawdzanie logowania do O!
                var optimaLoginValid = false;
                while (optimaLoginValid == false)
                {
                    optimaLoginValid = OptimaLogginIn();
                }
                StatusLabel.Content = "Zalogowałem się do O!";


                GenerateAcsSalesInvoicesButton.IsEnabled = false;
                //czytanie .xls
                var invoices = SourceFileReader.ReadAcsSalesInvoices(_openedFile.FileName);

                await GenerateAcsSalesInvoicesAsync(invoices)
                    .ContinueWith(t =>
                    {
                        Dispatcher.Invoke(() =>
                        {
                            GenerateAcsSalesInvoicesButton.IsEnabled = true;
                            StatusLabel.Content += $". Zakonczono!!!";
                        });
                    });
            }
        }

        private async Task GenerateAcsSalesInvoicesAsync(IList<IdeaVatInvoice> invoices)
        {
            var stopWatch = Stopwatch.StartNew();
            var invoicesCount = invoices.Count;
            var progress = new Progress<int>(p =>
            {
                var percent = p / (decimal)invoicesCount * 100;
                StatusLabel.Content = $"Wczytywanie faktur.. {p}/{invoicesCount},   {percent:0} %," +
                                      $"   {stopWatch.Elapsed.Hours}:{stopWatch.Elapsed.Minutes}:{stopWatch.Elapsed.Seconds}";
            });
            var importer = new VatRegisterImporter();
            await Task.Run(() => importer.VatRegisterImport(invoices, VatRegisterType.Sales, "", true));
        }

        private bool OptimaLogginIn()
        {
            try
            {
                OptimaLogging.LogIn();
                return true;
            }
            catch (Exception)
            {
                MessageBox.Show("Błąd logowania do Optimy. Popraw konfigurację...");
                RunConfigurationWindow();
            }
            return false;
        }

        private void LogButton_OnClick(object sender, RoutedEventArgs e)
        {
            var fileInfo = new FileInfo(AppDomain.CurrentDomain.BaseDirectory + "\\Log");
            Process.Start(fileInfo.FullName);
        }

        private void RunConfigurationWindow()
        {
            var confWindow = new ConfigurationView();
            confWindow.Owner = this;
            confWindow.ShowDialog();
        }

        private void ConfigurationButton_OnClick(object sender, RoutedEventArgs e)
        {
            RunConfigurationWindow();
        }

        private void AppFolderButton_OnClick(object sender, RoutedEventArgs e)
        {
            Process.Start(AppDomain.CurrentDomain.BaseDirectory);
        }

        private void SettingsFolderButton_OnClick(object sender, RoutedEventArgs e)
        {
            Process.Start(System.Windows.Forms.Application.LocalUserAppDataPath);
        }
    }
}
