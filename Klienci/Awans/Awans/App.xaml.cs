﻿using System;
using System.Windows;
using Awans.View;

namespace Awans
{
    /// <summary>
    /// Interaction logic for App.xaml
    /// </summary>
    public partial class App
    {
        protected override void OnStartup(StartupEventArgs e)
        {

            base.OnStartup(e);

            DispatcherUnhandledException += (o, ea) =>
            {
                NLog.LogManager.GetCurrentClassLogger().Info(ea.Exception.ToString());
                ea.Handled = true;
            };

            try
            {
                //AppSettings.Load();
                var window = new MainWindow();
                window.Show();
            }
            catch (Exception)
            {
                var window = new MainWindow();
                window.Show();
            }
        }
    }
}
