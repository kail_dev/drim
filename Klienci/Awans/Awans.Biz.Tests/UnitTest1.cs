﻿using System.Linq;
using Awans.Biz.Services;
using Microsoft.VisualStudio.TestTools.UnitTesting;

namespace Awans.Biz.Tests
{
    [TestClass]
    public class UnitTest1
    {
        [TestMethod]
        public void ReadSubiektPurchaseInvoices_Test()
        {
            var filePath = @"C:\Users\marci\OneDrive\Szeran\Klienci\Awans\201901\4kom_pz_201901.epp";
            var invoices = SourceFileReader.ReadSubiektInvoices(filePath, "FZ");

            Assert.AreEqual(166, invoices.Count);
        }

        [TestMethod]
        public void ReadSubiektSalesInvoices_Test()
        {
            var filePath = @"C:\Users\marci\OneDrive\Szeran\Klienci\Awans\sprzedaz\4kom_fs_201905.epp";
            var invoices = SourceFileReader.ReadSubiektInvoices(filePath, "FS");

            Assert.AreEqual(2049, invoices.Count);
        }

        [TestMethod]
        public void ReadSubiektSalesCorrecingInvoices_Test()
        {
            var filePath = @"C:\Users\marci\OneDrive\Szeran\Klienci\Awans\sprzedaz\4kom_fv_kor_201903.epp";
            var invoices = SourceFileReader.ReadSubiektInvoices(filePath, "FS");

            Assert.AreEqual(70, invoices.Count);
        }
    }
}

