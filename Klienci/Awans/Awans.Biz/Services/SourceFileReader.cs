﻿using System;
using System.Collections.Generic;
using System.Data.OleDb;
using System.Diagnostics;
using System.Globalization;
using System.IO;
using System.Linq;
using System.Text;
using System.Xml.Linq;
using Awans.Biz.Model;
using NLog;
using OptimaImporter.Common;

namespace Awans.Biz.Services
{
    public class SourceFileReader
    {
        public static event Action<string> InvoiceNotRead;

        public static List<Invoice> ReadJpkFaInvoices(string sourcePath)
        {
            var invoices = new List<Invoice>();
            var xdoc = XDocument.Load(sourcePath);

            var validInvoicesCounter = 0;
            foreach (var xElement in xdoc.Descendants().Where(x => x.Name.LocalName == "Faktura"))
            {
                var invoice = new Invoice();
                invoice.InvoiceDate = Convert.ToDateTime(
                    xElement.Elements()
                        .FirstOrDefault(x => x.Name.LocalName.ToString()
                            .StartsWith("P_1"))?.Value);
                invoice.Number = xElement.Elements()
                    .FirstOrDefault(x => x.Name.LocalName.ToString()
                        .StartsWith("P_2A"))?.Value;
                invoice.BuyerName = xElement.Elements()
                    .FirstOrDefault(x => x.Name.LocalName.ToString()
                        .StartsWith("P_3A"))?.Value;
                invoice.BuyerAddress = xElement.Elements()
                    .FirstOrDefault(x => x.Name.LocalName.ToString()
                        .StartsWith("P_3B"))?.Value;
                invoice.SellerName = xElement.Elements()
                    .FirstOrDefault(x => x.Name.LocalName.ToString()
                        .StartsWith("P_3C"))?.Value;
                invoice.SellerAddress = xElement.Elements()
                    .FirstOrDefault(x => x.Name.LocalName.ToString()
                        .StartsWith("P_3D"))?.Value;
                invoice.BuyerTaxNumberCountry = xElement.Elements()
                    .FirstOrDefault(x => x.Name.LocalName.ToString()
                        .StartsWith("P_4A"))?.Value;
                invoice.BuyerTaxNumber = xElement.Elements()
                    .FirstOrDefault(x => x.Name.LocalName.ToString()
                        .StartsWith("P_4B"))?.Value;
                invoice.SellerTaxNumberCountry = xElement.Elements()
                    .FirstOrDefault(x => x.Name.LocalName.ToString()
                        .StartsWith("P_5A"))?.Value;
                invoice.SellerTaxNumber = xElement.Elements()
                    .FirstOrDefault(x => x.Name.LocalName.ToString()
                        .StartsWith("P_5B"))?.Value;
                invoice.PaymentDate = Convert.ToDateTime(
                    xElement.Elements()
                        .FirstOrDefault(x => x.Name.LocalName.ToString()
                            .StartsWith("P_6"))?.Value);
                invoice.NetValue = Convert.ToDecimal(
                    xElement.Elements()
                        .FirstOrDefault(x => x.Name.LocalName.ToString()
                            .StartsWith("P_13_7"))?.Value.Replace(".", ","));
                //TaxValue = Convert.ToDecimal(
                //    xElement.Elements()
                //        .FirstOrDefault(x => x.Name.LocalName.ToString()
                //            .StartsWith("P_14_1"))?.Value.Replace(".", ",")),
                //invoice.GrossValue = Convert.ToDecimal(
                //    xElement.Elements()
                //        .FirstOrDefault(x => x.Name.LocalName.ToString()
                //            .StartsWith("P_15"))?.Value.Replace(".", ","));

                validInvoicesCounter++;
                Debug.WriteLine(validInvoicesCounter);
                invoices.Add(invoice);
            }

            var validRowsCounter = 0;
            foreach (var xElement in xdoc.Descendants().Where(x => x.Name.LocalName == "FakturaWiersz"))
            {
                var invoiceNumberFromRow = xElement.Elements()
                    .FirstOrDefault(x => x.Name.LocalName.ToString()
                        .StartsWith("P_2B"))?.Value; //P_2B
                var invoice = invoices.FirstOrDefault(x => x.Number == invoiceNumberFromRow);
                if (invoice != null)
                {
                    try
                    {
                        var netValue = Convert.ToDecimal(
                            xElement.Elements()
                                .FirstOrDefault(x => x.Name.LocalName.ToString()
                                    .StartsWith("P_11"))?.Value.Replace(".", ","));
                        var vatRate = xElement.Elements()
                            .FirstOrDefault(x => x.Name.LocalName.ToString()
                                .StartsWith("P_12"))?.Value;
                        var grossValue = Convert.ToDecimal(
                            xElement.Elements()
                                .FirstOrDefault(x => x.Name.LocalName.ToString()
                                    .StartsWith("P_11A"))?.Value.Replace(".", ","));

                        invoice.Rows.Add(new InvoiceRow
                        {
                            NetValue = netValue,
                            VatRate = vatRate,
                            GrossValue = grossValue
                        });

                        validRowsCounter++;
                        Debug.WriteLine(validRowsCounter);
                    }
                    catch (Exception)
                    {
                        //Debug.WriteLine($"Nie udało się odczytać wiersza dla faktury: {invoiceNumberFromRow}, {ex}");
                    }
                }
            }

            return invoices;
        }

        public static List<Invoice> ReadSubiektInvoices(string sourcePath, string invoiceType)
        {
            var invoices = new List<Invoice>();
            var fileContent = File.ReadAllLines(sourcePath, Encoding.GetEncoding("Windows-1250"))
                .Where(x => x.Length > 0).ToList();

            for (var i = 0; i < fileContent.Count - 1; i++)
            {
                fileContent[i] = fileContent[i]?.Replace("VPZ", "FZ").Replace("PZ", "FZ");
            }
            for (var i = 0; i < fileContent.Count - 1; i++)
            {
                var line = fileContent[i];
                var nextLine = fileContent[i + 1];
                if (line.StartsWith("[NAGLOWEK]") && (nextLine.StartsWith($"\"{invoiceType}") || nextLine.StartsWith($"\"K{invoiceType}")))
                {
                    var invoice = new Invoice();
                    if (nextLine.StartsWith($"\"K{invoiceType}")) invoice.Correction = true;
                    var invoiceContent = fileContent[i + 1];
                    Debug.WriteLine($"{i}:  {invoiceContent}");
                    string[] invoiceHeadElements = null;
                    var invoiceRows = new List<InvoiceRow>();
                    var invoiceHeadLinesCounter = 0;
                    var lineIPlus2 = fileContent[i + 2];

                    var nextLineCounter = 3;
                    while (!lineIPlus2.StartsWith("["))
                    {
                        invoiceContent += lineIPlus2;
                        var invoiceHeadElementsList = invoiceContent.SplitWithTextDelimiter(',', "\"").ToList();
                        invoiceHeadElements = invoiceHeadElementsList.ToArray();
                        invoiceHeadLinesCounter++;
                        lineIPlus2 = fileContent[i + nextLineCounter];
                        nextLineCounter++;
                    }

                    if (fileContent[i + 2 + invoiceHeadLinesCounter].StartsWith("[ZAW"))
                    {
                        var j = i;
                        while (fileContent.Count > j + 3 + invoiceHeadLinesCounter
                               && !fileContent[j + 3 + invoiceHeadLinesCounter].StartsWith("[N"))
                        {
                            try
                            {
                                var invoiceRowElements = fileContent[j + 3 + invoiceHeadLinesCounter].SplitWithTextDelimiter(',', "\"");
                                try
                                {
                                    var invoiceRow = new InvoiceRow
                                    {
                                        GoodOService = Convert.ToInt32(invoiceRowElements[1], new CultureInfo("en-US")) == 2 ? 4 : 1,
                                        NetValue = Convert.ToDecimal(invoiceRowElements[16], new CultureInfo("en-US")),
                                        VatRate = invoiceRowElements[15].Replace("\"", ""),
                                        GrossValue = Convert.ToDecimal(invoiceRowElements[18], new CultureInfo("en-US"))
                                    };
                                    if (invoice.Correction)
                                    {
                                        invoiceRow.NetValue = Convert.ToDecimal(invoiceRowElements[28], new CultureInfo("en-US")) * -1;
                                        invoiceRow.VatRate = invoiceRowElements[24].Replace("\"", "");
                                        invoiceRow.GrossValue = Convert.ToDecimal(invoiceRowElements[30], new CultureInfo("en-US")) * -1;
                                    }         
                                    if (invoiceRow.VatRate == "ue")
                                    {
                                        invoiceRow.VatRate = "0";
                                    }

                                    invoiceRows.Add(invoiceRow);
                                }
                                catch (Exception)
                                {
                                    var invoiceRow = new InvoiceRow
                                    {
                                        GoodOService = Convert.ToInt32(invoiceRowElements[1], new CultureInfo("en-US")) == 2 ? 4 : 1,
                                        NetValue = Convert.ToDecimal(invoiceRowElements[2], new CultureInfo("en-US")),
                                        VatRate = invoiceRowElements[0].Replace("\"", ""),
                                        GrossValue = Convert.ToDecimal(invoiceRowElements[4], new CultureInfo("en-US"))
                                    };
                                    if (invoiceRow.VatRate == "ue")
                                    {
                                        invoiceRow.VatRate = "0";
                                    }

                                    invoiceRows.Add(invoiceRow);
                                }
                            }
                            catch (Exception ex)
                            {
                                LogManager.GetCurrentClassLogger()
                                    .Info(
                                        $"Nie udało się odczytać wiersza dla faktury {invoiceContent[6]}, błąd: {ex}");
                            }
                            finally
                            {
                                j++;
                            }
                        }
                    }

                    var invoiceContentTmp = invoiceContent.SplitWithTextDelimiter(',', "\"").ToList();

                    if (invoiceContent.StartsWith("\"FZ"))
                    {
                        if (invoiceContentTmp.Count > 22
                            && !invoiceContentTmp[21].Replace("\"", "").StartsWith("201")
                            && invoiceContentTmp[22].Replace("\"", "").StartsWith("201"))
                        {
                            invoiceContentTmp.RemoveAt(13);
                            invoiceHeadElements = invoiceContentTmp.ToArray();
                        }

                        if (invoiceContentTmp.Count > 20 && invoiceContentTmp[20].Replace("\"", "").StartsWith("201"))
                        {
                            invoiceContentTmp.Insert(20, "");
                            invoiceHeadElements = invoiceContentTmp.ToArray();
                        }

                        if (invoiceContentTmp.Count > 33 && invoiceContentTmp[33].Replace("\"", "").StartsWith("201"))
                        {
                            invoiceContentTmp.Insert(31, "");
                            invoiceHeadElements = invoiceContentTmp.ToArray();
                        }

                        invoiceHeadElements = invoiceHeadElements ?? invoiceContentTmp.ToArray();
                    }

                    else if (invoiceContent.StartsWith("\"FS") || invoiceContent.StartsWith("\"KFS"))
                    {
                        try
                        {
                            if (invoiceContentTmp.Count > 24
                        && !invoiceContentTmp[23].Replace("\"", "").StartsWith("201")
                        && invoiceContentTmp[24].Replace("\"", "").StartsWith("201"))
                            {
                                invoiceContentTmp.RemoveAt(13);
                                invoiceContentTmp.RemoveAt(16);
                                invoiceHeadElements = invoiceContentTmp.ToArray();
                            }

                            if (invoiceContentTmp.Count > 23
                                && !invoiceContentTmp[22].Replace("\"", "").StartsWith("201")
                                && invoiceContentTmp[23].Replace("\"", "").StartsWith("201"))
                            {
                                invoiceContentTmp.RemoveAt(18);
                                invoiceHeadElements = invoiceContentTmp.ToArray();
                            }

                            if (invoiceContentTmp.Count > 23
                                && !invoiceContentTmp[21].Replace("\"", "").StartsWith("201")
                                && invoiceContentTmp[22].Replace("\"", "").StartsWith("201"))
                            {
                                invoiceContentTmp.RemoveAt(19);
                                invoiceHeadElements = invoiceContentTmp.ToArray();
                            }

                            if (sourcePath.Contains("magazyn"))
                            {
                                invoiceContentTmp.RemoveAt(21);
                                invoiceHeadElements = invoiceContentTmp.ToArray();
                            }

                            invoiceHeadElements = invoiceHeadElements ?? invoiceContentTmp.ToArray();
                        }
                        catch (Exception)
                        {
                            InvoiceNotRead?.Invoke(invoiceHeadElements[6]);
                        }
                    }

                    if (invoiceContent.StartsWith($"\"{invoiceType}") || invoiceContent.StartsWith($"\"K{invoiceType}"))
                    {
                        try
                        {
                            if (invoiceType == "FZ")
                            {
                                var invoiceDateYear =
                                    Convert.ToInt32(invoiceHeadElements[21].Replace("\"", "").Substring(0, 4));
                                var invoiceDateMonth =
                                    Convert.ToInt32(invoiceHeadElements[21].Replace("\"", "").Substring(4, 2));
                                var invoiceDateDay =
                                    Convert.ToInt32(invoiceHeadElements[21].Replace("\"", "").Substring(6, 2));
                                var paymentDateYear =
                                    Convert.ToInt32(invoiceHeadElements[34].Replace("\"", "").Substring(0, 4));
                                var paymentDateMonth =
                                    Convert.ToInt32(invoiceHeadElements[34].Replace("\"", "").Substring(4, 2));
                                var paymentDateDay =
                                    Convert.ToInt32(invoiceHeadElements[34].Replace("\"", "").Substring(6, 2));
                                invoice = new Invoice
                                {
                                    Number = invoiceHeadElements[4].Replace("\"", ""),
                                    SellerCode = invoiceHeadElements[11].Replace("\"", ""),
                                    SellerName = invoiceHeadElements[13].Replace("\"", ""),
                                    City = invoiceHeadElements[14].Replace("\"", ""),
                                    PostalCode = invoiceHeadElements[15].Replace("\"", ""),
                                    Street = invoiceHeadElements[16].Replace("\"", ""),
                                    SellerTaxNumber = invoiceHeadElements[17].Replace("\"", ""),
                                    InvoiceDate = new DateTime(Convert.ToInt32(invoiceDateYear),
                                        Convert.ToInt32(invoiceDateMonth),
                                        Convert.ToInt32(invoiceDateDay)
                                    ),
                                    PaymentDate = new DateTime(Convert.ToInt32(paymentDateYear),
                                        Convert.ToInt32(paymentDateMonth),
                                        Convert.ToInt32(paymentDateDay)
                                    ),
                                    Currency = invoiceHeadElements[invoiceHeadElements.Length - 16].Replace("\"", ""),
                                    CurrencyRate =
                                        invoiceHeadElements[invoiceHeadElements.Length - 15].Replace("\"", ""),
                                    Rows = invoiceRows
                                };
                                if (string.IsNullOrWhiteSpace(invoice.Number))
                                    invoice.Number = invoiceHeadElements[6].Replace("\"", "");
                                invoices.Add(invoice);
                            }

                            if (invoiceType == "FS")
                            {
                                var invoiceDateYear =
                                    Convert.ToInt32(invoiceHeadElements[21].Replace("\"", "").Substring(0, 4));
                                var invoiceDateMonth =
                                    Convert.ToInt32(invoiceHeadElements[21].Replace("\"", "").Substring(4, 2));
                                var invoiceDateDay =
                                    Convert.ToInt32(invoiceHeadElements[21].Replace("\"", "").Substring(6, 2));
                                var paymentDateYear =
                                    Convert.ToInt32(invoiceHeadElements[34].Replace("\"", "").Substring(0, 4));
                                var paymentDateMonth =
                                    Convert.ToInt32(invoiceHeadElements[34].Replace("\"", "").Substring(4, 2));
                                var paymentDateDay =
                                    Convert.ToInt32(invoiceHeadElements[34].Replace("\"", "").Substring(6, 2));
                                invoice = new Invoice
                                {
                                    Number = invoiceHeadElements[6].Replace("\"", ""),
                                    BuyerName = invoiceHeadElements[12].Replace("\"", ""),
                                    City = invoiceHeadElements[14].Replace("\"", ""),
                                    PostalCode = invoiceHeadElements[15].Replace("\"", ""),
                                    Street = invoiceHeadElements[16].Replace("\"", ""),
                                    BuyerTaxNumber = invoiceHeadElements[17].Replace("\"", ""),
                                    InvoiceDate = new DateTime(Convert.ToInt32(invoiceDateYear),
                                        Convert.ToInt32(invoiceDateMonth),
                                        Convert.ToInt32(invoiceDateDay)
                                    ),
                                    PaymentDate = new DateTime(Convert.ToInt32(paymentDateYear),
                                        Convert.ToInt32(paymentDateMonth),
                                        Convert.ToInt32(paymentDateDay)
                                    ),
                                    Currency = invoiceHeadElements[invoiceHeadElements.Length - 16].Replace("\"", ""),
                                    CurrencyRate =
                                        invoiceHeadElements[invoiceHeadElements.Length - 15].Replace("\"", ""),
                                    Rows = invoiceRows
                                };
                                if (invoice.Number.StartsWith("FK"))
                                {
                                    invoice.Correction = true;
                                    invoice.CorrectedNumber = invoiceHeadElements[7].Replace("\"", "");
                                }
                                invoices.Add(invoice);
                            }
                        }
                        catch (Exception ex)
                        {
                            InvoiceNotRead?.Invoke(invoiceHeadElements[6]);
                            LogManager.GetCurrentClassLogger()
                                .Info(
                                    $"Nie udało się utworzyć faktury sprzedaży nr {invoiceHeadElements[6]}, błąd: {ex}");
                        }
                    }
                }
                else if (invoiceType == "FS" && (line.StartsWith($"\"{invoiceType} ") || line.StartsWith($"\"K{invoiceType}")))
                {
                    var invoice = new Invoice { Number = "pusty" };
                    try
                    {
                        var lineElements = line.Split(',');
                        var invoiceNumber = lineElements[0].Replace("\"", "").Replace("FS ", "");
                        invoice = invoices.FirstOrDefault(inv => inv.Number == invoiceNumber);

                        var invoiceDateYear = Convert.ToInt32(lineElements[1].Replace("\"", "").Substring(0, 4));
                        var invoiceDateMonth = Convert.ToInt32(lineElements[1].Replace("\"", "").Substring(4, 2));
                        var invoiceDateDay = Convert.ToInt32(lineElements[1].Replace("\"", "").Substring(6, 2));

                        invoice.OperationDate = new DateTime(invoiceDateYear, invoiceDateMonth, invoiceDateDay);

                    }
                    catch (Exception ex)
                    {
                        LogManager.GetCurrentClassLogger()
                            .Info($"Nie udało się podmienić daty sprzedaży dla faktury sprzedaży, błąd: {ex}");
                    }
                }
            }

            return invoices;
        }
    }
}
