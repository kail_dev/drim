﻿using System;
using NLog;

namespace Awans.Biz.Services
{
    public class OptimaLogging
    {
        public static CDNBase.IApplication Application;
        public static CDNBase.ILogin ILogin;

        public static void LogIn(string optimaUser, string optimaPassword, string optimaDatabase)
        {
            try
            {
                Application = new CDNBase.Application();
                Application.LockApp(1);
                ILogin = Application.Login(optimaUser, optimaPassword, optimaDatabase);
            }
            catch (Exception ex)
            {
                LogManager.GetCurrentClassLogger().Error($"Błąd logowania: {ex}");
            }
        }

        public static void LogOut()
        {
            try
            {
                ILogin = null;
                Application.UnlockApp();
                Application = null;
            }
            catch (Exception)
            {
                ILogin = null;
                Application = null;
            }
        }
    }
}
