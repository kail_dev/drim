﻿using CDNBase;
using CDNHeal;
using CDNRVAT;
using NLog;
using NLog.LayoutRenderers.Wrappers;
using OptimaImporter.Biz.Helper;
using OptimaImporter.Biz.Model;
using OptimaImporter.Data.Enum;
using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Text.RegularExpressions;

namespace Groundfrost.Biz.Services
{
    public class OptimaImporter
    {
        private Logger Logger => LogManager.GetCurrentClassLogger();
        private static readonly IEnumerable<string> Currencies = new List<string> { "EUR", "USD" };

        public event Action Created;
        public event Action CreateFailed;

        public void Create(IEnumerable<VatInvoice> invoices, VatRegisterType vatRegisterType, string vatRegisterName, bool nameAsCustomerCode = false)
        {
            foreach (var invoice in invoices)
            {
                var session = new AdoSession();
                var customers = (Kontrahenci)session.CreateObject("CDN.Kontrahenci");

                var customer = TrySelectCustomer(invoice, customers, vatRegisterType) ?? CreateCustomer(invoice, nameAsCustomerCode, vatRegisterType);

                var vatRegister = CreateHeader(vatRegisterType, vatRegisterName, session, invoice);

                SetCustomer(invoice, nameAsCustomerCode, vatRegister, customer, customers);

                TrySetCurrency(invoice, session, vatRegister);

                TrySetPayment(invoice, session, vatRegister);

                AddElements(invoice, session, vatRegister);

                SetDescription(invoice, vatRegister);

                CheckDuplicate(vatRegister, invoice);

                TrySave(session, invoice);
            }
        }

        private IKontrahent TrySelectCustomer(VatInvoice invoice, IAdoCollection customers, VatRegisterType vatRegisterType)
        {
            IKontrahent customer = null;

            try
            {
                if ((int)vatRegisterType == 1)
                {
                    customer = customers["Knt_NIP='" + NipSplit.GetNipNumber(invoice.SellerTaxNumber.Replace("-", "")) + "'"];
                }
                else if (invoice.BuyerTaxNumber != null)
                {
                    customer = customers["Knt_NIP='" + NipSplit.GetNipNumber(invoice.BuyerTaxNumber.Replace("-", "")) + "'"];
                }
                else if (invoice.BuyerCode != null)
                {
                    customer = customers["Knt_Kod='" + invoice.BuyerCode + "'"];
                }
                else if (invoice.BuyerName != null)
                {
                    customer = customers["Knt_Nazwa1='" + invoice.BuyerName + "'"];
                }

                return customer;
            }
            catch (Exception ex)
            {
                Logger.Info($"Nie odnaleziono kontrahenta po NIP, po kodzie ani po nazwie. {ex}");
                return null;
            }
        }
        private IKontrahent CreateCustomer(VatInvoice invoice, bool nameAsCustomerCode, VatRegisterType vatRegisterType)
        {
            IKontrahent customer = null;

            var session = new AdoSession();
            var customers = (Kontrahenci)session.CreateObject("CDN.Kontrahenci");
            customer = customers.AddNew();

            var taxNumber = vatRegisterType == VatRegisterType.Purchase ? invoice.SellerTaxNumber : invoice.BuyerTaxNumber;
            var name = vatRegisterType == VatRegisterType.Purchase ? invoice.SellerName : invoice.BuyerName;

            customer.Akronim = taxNumber;
            if (string.IsNullOrWhiteSpace(name)) name = taxNumber;
            customer.Nazwa1 = name;

            if (string.IsNullOrWhiteSpace(taxNumber) && !nameAsCustomerCode)
            {
                customer.Akronim = DateTime.Today.ToShortDateString() + DateTime.Now.ToLongTimeString();
            }
            else if (string.IsNullOrWhiteSpace(taxNumber) && nameAsCustomerCode)
            {
                customer.Akronim = name;
            }
            else
            {
                try
                {
                    customer.NumerNIP.NIPKraj = NipSplit.GetNipCountry(taxNumber);
                    customer.NumerNIP.NipE = NipSplit.GetNipNumber(taxNumber);
                }
                catch (Exception ex)
                {
                    Logger.Error($"Nie udało się przypisać NIPu do kontrahenta dla faktury {invoice.Number}, błąd: {ex}");
                }
            }

            customer.Rodzaj_Dostawca = vatRegisterType == VatRegisterType.Purchase ? 1 : 0;
            customer.Rodzaj_Odbiorca = vatRegisterType == VatRegisterType.Purchase ? 0 : 1;

            customer.Adres.Ulica = invoice.Street;
            customer.Adres.Miasto = invoice.City;
            customer.Adres.KodPocztowy = invoice.PostalCode;

            try
            {
                session.Save();
                Logger.Info($"Założono kontrahenta dla faktury: {invoice.Number} o akronimie {customer.Akronim}.");
            }
            catch (Exception ex)
            {
                Logger.Error($"Nie udało się zapisać kontrahenta dla faktury {invoice.Number}, błąd: {ex}.");
            }

            return customer;
        }
        private VAT CreateHeader(VatRegisterType vatRegisterType, string vatRegisterName, IAdoSession session, VatInvoice invoice)
        {
            var vatRegisters = (ICollection)session.CreateObject("CDN.RejestryVAT");
            var vatRegister = (VAT)vatRegisters.AddNew();
            vatRegister.Typ = vatRegisterType == VatRegisterType.Purchase ? 1 : 2;
            vatRegister.Rejestr = string.IsNullOrWhiteSpace(vatRegisterName) && vatRegister.Typ == 1 ? "ZAKUP" : vatRegisterName;
            vatRegister.Rejestr = string.IsNullOrWhiteSpace(vatRegisterName) && vatRegister.Typ == 2 ? "SPRZEDAŻ" : vatRegisterName;
            vatRegister.Dokument = invoice.Number;
            vatRegister.DataWys = invoice.InvoiceDate;
            vatRegister.DataZap = invoice.InvoiceDate;
            vatRegister.DataOpe = invoice.OperationDate == new DateTime(1, 1, 1) ? invoice.InvoiceDate : invoice.OperationDate;
            return vatRegister;
        }
        private void SetCustomer(VatInvoice invoice, bool nameAsCustomerCode, IVAT vatRegister, IPodmiot customer, IAdoCollection customers)
        {
            try
            {
                vatRegister.Podmiot = customer;
            }
            catch (Exception)
            {
                vatRegister.Podmiot = customers["Knt_KntId=1"];
            }

            if (vatRegister.Podmiot == customers["Knt_KntId=1"] && nameAsCustomerCode || invoice.BuyerCode == "!NIEOKREŚLONY!")
            {
                vatRegister.Nazwa1 = invoice.BuyerName;
                vatRegister.PodAdres.Ulica = invoice.BuyerAddress;
                vatRegister.PodAdres.KodPocztowyZKreska = invoice.PostalCode;
                vatRegister.PodAdres.Miasto = invoice.City;
            }
        }
        private void TrySetCurrency(VatInvoice invoice, IAdoSession session, IVAT vatRegister)
        {
            if (string.IsNullOrWhiteSpace(invoice.Currency) || !Currencies.Contains(invoice.Currency)) return;

            var waluty = (ICollection)session.CreateObject("CDN.Waluty");
            var waluta = (Waluta)waluty[$"WNa_Symbol='{invoice.Currency}'"];
            try
            {
                vatRegister.Waluta = waluta;
                vatRegister.WalutaDoVAT = waluta;
                var kurs = session.CreateObject("CDN.TypyKursowWalut").Item("WKu_Symbol='NBP'");
                vatRegister.TypKursuWaluty = kurs;
                vatRegister.TypKursuWalutyDoVAT = kurs;
            }
            catch (Exception)
            {
            }
        }
        private void TrySetPayment(VatInvoice invoice, IAdoSession session, IVAT vatRegister)
        {
            var paymentForms = (ICollection)session.CreateObject("CDN.FormyPlatnosci");
            var paymentForm = invoice.InvoiceDate == invoice.PaymentDate
                ? (OP_KASBOLib.FormaPlatnosci)paymentForms["Fpl_FplId=1"]
                : (OP_KASBOLib.FormaPlatnosci)paymentForms["Fpl_FplId=3"];
            try
            {
                paymentForm = (OP_KASBOLib.FormaPlatnosci)paymentForms[$"Fpl_Nazwa='{invoice.PaymentForm}'"];
            }
            catch (Exception ex)
            {
                Logger.Error($"Nie ondaleziono formy płatności dla faktury {invoice.Number}, błąd: {ex}.");
            }

            try
            {
                vatRegister.FormaPlatnosci = paymentForm;
            }
            catch (Exception ex)
            {
                Logger.Error($"Nie przypisano formy płatności dla faktury {invoice.Number}, błąd: {ex}.");
            }

            if (invoice.PaymentDate.Year > 1900)
            {
                vatRegister.Termin = invoice.PaymentDate;
            }
        }
        private void AddElements(VatInvoice invoice, IAdoSession session, IVAT vatRegister)
        {
            var vatElements = vatRegister.Elementy;
            foreach (var row in invoice.Rows)
            {
                var vatElement = (VATElement)vatElements.AddNew();
                try
                {
                    var stawkaString = row.VatRate.Replace("\"", "").Replace(".0000", ",00");
                    vatElement.Stawka = Convert.ToDouble(stawkaString);
                }
                catch (Exception)
                {
                    if (row.VatRate.Replace("\"", "").Replace(".0000", ",00").StartsWith("zw"))
                    {
                        vatElement.Stawka = 0;
                        vatElement.Flaga = 1;
                    }
                    else if (row.VatRate.Replace("\"", "").Replace(".0000", ",00").StartsWith("np"))
                    {
                        vatElement.Stawka = 0;
                        vatElement.Flaga = 4;
                    }
                    else
                    {
                        vatElement.Stawka = 23;
                    }
                }

                vatElement.RodzajZakupu = 1;
                vatElement.Netto = row.NetValue;
                vatElement.Odliczenia = 1;
                if (row.VatValue != 0)
                {
                    vatElement.VAT = row.VatValue;
                }
                else
                {
                    vatElement.Brutto = row.GrossValue;
                }

                if (!string.IsNullOrWhiteSpace(row.Category2))
                {
                    try
                    {
                        var kategorie = (Kategorie)session.CreateObject("CDN.Kategorie");
                        vatElement.Kategoria2 = (Kategoria)kategorie[$"Kat_KodSzczegol='{row.Category2}'"];
                    }
                    catch (Exception)
                    {
                    }
                }
            }
        }
        private void SetDescription(VatInvoice invoice, VAT vatRegister)
        {
            if (!string.IsNullOrWhiteSpace(invoice.Description))
            {
                vatRegister.KategoriaOpis = invoice.Description;
            }
        }
        private void CheckDuplicate(VAT vatRegister, VatInvoice invoice)
        {
            try
            {
                var czyDuplikat = vatRegister.SprawdzDuplikatVATEx(0);
            }
            catch (Exception e)
            {
                LogManager.GetCurrentClassLogger()
                    .Info($"Dla dokumentu {invoice.Number} istnieje już podobny doument w systemie. Błąd: {e}");
            }
        }
        private void TrySave(AdoSession session, VatInvoice invoice)
        {
            try
            {
                session.Save();
                Logger.Info($"Dokument nr: {invoice.Number} utworzony.");
                Created?.Invoke();
            }
            catch (Exception ex)
            {
                Logger.Error($"Dokument nr: {invoice.Number} nie utworzony. {ex}");
                CreateFailed?.Invoke();
            }
        }

        public void GeneratePK(IEnumerable<PKItems> accEntries)
        {
            int counter;
            counter = 3;
            int giddek = 0;
            CDNKH.IDekret rr = null;
           
            var session = new AdoSession();
            foreach (var accEntry in accEntries)
            {



                if (accEntry.Lp == counter)
                {
                    try
                    {

                        session.Save();

                        session = new AdoSession();

                    }
                    catch(Exception)
                    {

                    }



                    counter++;
                   //  Console.WriteLine("DODAJE PK {0}", result.Lp);
                    rr = PK(session, accEntry.nrkontaoptima, accEntry.NrDowodu, accEntry.Ma, accEntry.Wn,accEntry.Data,accEntry.Datawpisu,accEntry.Symbol);
                    PKposition(session, accEntry.nrkontaoptima, accEntry.Data, accEntry.Wn, accEntry.Ma, rr, accEntry.NrDowodu);
                    // 200-OSPL_KWARCIANY 


                }
                else
                {
                    Console.WriteLine("Dodaje pozycje {0}", accEntry.Lp);
                    PKposition(session, accEntry.nrkontaoptima, accEntry.Data, accEntry.Wn, accEntry.Ma, rr, accEntry.NrDowodu);
                    //  oo.PKpositionADD(session, result.nrkontaoptima, result.Data, result.Wn, result.Ma,giddek);

                }

                // AddAcc(accEntry.Account, accEntry.Name);
            }
        }

        public CDNKH.IDekret PK(AdoSession session, string konto = "", string dokument = "", string kwotama = "", string kwotawn = "", string data = "", string datawpisu ="", string books ="")

        {
            string calyrok = datawpisu;
            string rok = calyrok.Substring(calyrok.Length - 4, 4);
            int rokint = Convert.ToInt32(rok);
            string datadok = data;
            string dzien = data.Substring(0, data.IndexOf("."));
            int dzienint = Convert.ToInt32(dzien);
            string miesiac = data.Substring(data.IndexOf(".")+1, data.Length - data.IndexOf(".")-1);
            int miesiacint = Convert.ToInt32(miesiac);
            string test ="";


            var oobId = 0;
            var knf = (CDNKONFIGLib.IKonfiguracja)session.Login.CreateObject("CDNKonfig.Konfiguracja");
            string val = knf.get_Value("BiezOkresObr");
            oobId = val.Length == 0 ? 0 : Convert.ToInt32(val);

            string kontr = konto.Substring(konto.IndexOf("-") + 1, konto.Length - konto.IndexOf("-") - 1);
            var dekret = (CDNKH.Dekrety)session.CreateObject("CDN.Dekrety", null);
            var idekret = (CDNKH.IDekret)dekret.AddNew(null);
            var dziennik = (CDNBase.ICollection)(session.CreateObject("CDN.Dzienniki", null));
            //CDNDave.IDziennik idzienniki = (CDNDave.IDziennik)dziennik["Dzi_DziID = 26"];
            if (rok=="2020")
            {
                oobId = 2;
            }
            try
            {
                  var idzienniki = (CDNDave.IDziennik)dziennik[$"Dzi_Symbol = '{books}' AND Dzi_OObID=" + oobId];

                idekret.DziID = idzienniki.ID;
                idekret.DataDok = new DateTime(rokint, miesiacint, dzienint); //data
            }
            catch
            {
              
                Logger.Error($"Brak dziennkia {books}");
            }
      

            var Kontrahenci = (CDNBase.ICollection)(session.CreateObject("CDN.Kontrahenci", null));
         
            try
            {
          var   kontrahent = (CDNHeal.IKontrahent)Kontrahenci[$"Knt_Kod='{kontr}'"];
            }
            catch
            {
          var       kontrahent = (CDNHeal.IKontrahent)Kontrahenci[$"Knt_Kod='!NIEOKREŚLONY!'"];
            }


            idekret.Bufor = 1;
            idekret.Podmiot = (CDNHeal.IPodmiot)null;
            idekret.Dokument = dokument;
            /*   ICollection elementy = idekret.Elementy;
               var ielement = (CDNKH.IDekretElement)elementy.AddNew(null);

               if (kwotama == "0")
               {
                   var konta = (ICollection)session.CreateObject("CDN.Konta", null);

                   var kontoWn = (CDNKH.IKonto)konta[$"Acc_Numer = '{konto}'"];
                   ielement.KontoWn = (CDNKH.Konto)kontoWn;

                   ielement.Kwota = Convert.ToDecimal(kwotawn);
                   //  var kontoMa = (CDNKH.IKonto)konta[$"Acc_Numer = '131'"];
               }
               else
               {
                   var konta = (ICollection)session.CreateObject("CDN.Konta", null);
                   var kontoMA = (CDNKH.IKonto)konta[$"Acc_Numer = '{konto}'"];

                   ielement.KontoMa = (CDNKH.Konto)kontoMA;

                   ielement.Kwota = Convert.ToDecimal(kwotama);
               }*/

            return idekret;

        }
        public void PKposition(AdoSession session, string konto, string data, string kwotawn, string kwotama, CDNKH.IDekret idekret, string dokument)
        {
            string account = "";
            string czlon2 = konto.Substring(konto.IndexOf("-") + 1, konto.Length - konto.IndexOf("-") - 1);

            //var counter = 1;
            var counter = konto.Count(x => x == '-');

            if (konto.Length > 3)
            {
                account = konto.Substring(0, konto.IndexOf("-"));

            }
            else
            {
                account = konto;
            }


            if (counter == 1 && account == "200" || account == "202")
            {

                try
                {
                    // Dodawania pozycji do dokumentu
                    ICollection elementy = idekret.Elementy;
                    var ielement = (CDNKH.IDekretElement)elementy.AddNew(null);

                    if (kwotama == "0")
                    {
                        var konta = (ICollection)session.CreateObject("CDN.Konta", null);

                        var kontoWn = (CDNKH.IKonto)konta[$"Acc_Numer = '{konto}'"];
                        ielement.KontoWn = (CDNKH.Konto)kontoWn;

                        ielement.Kwota = Convert.ToDecimal(kwotawn);
                        //  var kontoMa = (CDNKH.IKonto)konta[$"Acc_Numer = '131'"];
                    }
                    else
                    {
                        var konta = (ICollection)session.CreateObject("CDN.Konta", null);
                        var kontoMA = (CDNKH.IKonto)konta[$"Acc_Numer = '{konto}'"];

                        ielement.KontoMa = (CDNKH.Konto)kontoMA;

                        ielement.Kwota = Convert.ToDecimal(kwotama);
                    }

                }
                catch (Exception e)
                {
                    Console.WriteLine(e);
                }






            }


            //  202 - !NIEOKREŚLONY!
            //   string account =konto.Substring(0,)


            if (account !="202" && account !="200" )
            {
               
                try
                {


                    ICollection elementy = idekret.Elementy;
                    var ielement = (CDNKH.IDekretElement)elementy.AddNew(null);

                    if (kwotama == "0")
                    {
                        var konta = (ICollection)session.CreateObject("CDN.Konta", null);

                        var kontoWn = (CDNKH.IKonto)konta[$"Acc_Numer = '{konto}'"];
                        ielement.KontoWn = (CDNKH.Konto)kontoWn;

                        ielement.Kwota = Convert.ToDecimal(kwotawn);
                        //  var kontoMa = (CDNKH.IKonto)konta[$"Acc_Numer = '131'"];
                    }
                    else
                    {
                        var konta = (ICollection)session.CreateObject("CDN.Konta", null);
                        var kontoMA = (CDNKH.IKonto)konta[$"Acc_Numer = '{konto}'"];

                        ielement.KontoMa = (CDNKH.Konto)kontoMA;

                        ielement.Kwota = Convert.ToDecimal(kwotama);
                    }
                }
                catch (Exception e)
                {
                    AddAcc(konto, konto);
                }


            }
         
        }






        public void AccounPlanImport(IEnumerable<AccountPlan> accEntries)
        {
            
            foreach (var accEntry in accEntries)
            {

                AddAcc(accEntry.Account, accEntry.Name);
            }
        }

        public void AddAcc(string NrKonta, string nazwa)
        {

            var session = new AdoSession();
            var counter = 0;
            try
            {
                counter = NrKonta.Count(x => x == '-');
            }
            catch (Exception e)
            {
                Console.WriteLine(e);
            }
            string account = "";
            if (NrKonta.Length > 3)
            {
                account = NrKonta.Substring(0, 3);

            }
            else
            {
                account = NrKonta;
            }
            if (counter == 0 && account != "200" && account != "202" && account != "205")
            {
                try
                {
                    var konta = (ICollection)session.CreateObject("CDN.Konta", null);
                    var konto = (CDNKH.IKonto)konta.AddNew(null);


                    konto.Nazwa = nazwa;
                    konto.TypKonta = 1;

                    konto.Analityka = 0;

                    konto.Segment = NrKonta;
                    konto.OkresId = 1;

                    session.Save();

                }
                catch (Exception e)
                {
                    Console.WriteLine("Bledy konta proste {0}", NrKonta);
                    Console.WriteLine(e);
                }

            }
            if (counter == 1 && account != "200" && account != "202" && account != "205")

            {
                try
                {


                    string czlon6 = NrKonta.Substring(0, NrKonta.IndexOf("-"));
                    int test = (NrKonta.IndexOf("-"));
                    string czlon7 = NrKonta.Substring(NrKonta.IndexOf("-") + 1, NrKonta.Length - NrKonta.IndexOf("-") - 1);
                    var konta = (ICollection)session.CreateObject("CDN.Konta", null);
                    var konto = (CDNKH.IKonto)konta.AddNew(null);
                    //   string rodzicid = czlon2.Substring(1, 2);
                    var rodzicKonto = (CDNKH.IKonto)konta[$"ACC_NumerIdx ='{czlon6}'"];

                    //  konto.NumerIdx = "10";
                    konto.Nazwa = nazwa;
                    konto.ParId = rodzicKonto.ID;
                    konto.Analityka = 1;
                    konto.TypKonta = 1;
                    konto.Segment = czlon7;
                    konto.OkresId = 1;
                    konto.Poziom = 2;
                    session.Save();
                }
                catch (Exception e)
                {
                    Console.WriteLine("Bledy 1 {0}", NrKonta);
                    Console.WriteLine(e);
                }
            }
            if (counter == 2 && account != "200" && account != "202" && account != "205")
            {
                try
                {


                    string czlon10 = NrKonta.Substring(0, NrKonta.IndexOf("-"));

                    int test = (NrKonta.IndexOf("-"));
                    string czlon2full = NrKonta.Substring(NrKonta.IndexOf("-") + 1, NrKonta.Length - NrKonta.IndexOf("-") - 1);

                    string czlon12 = czlon2full.Substring(0, czlon2full.IndexOf("-"));



                    string czlon3 = czlon2full.Substring(czlon2full.IndexOf("-") + 1, czlon2full.Length - czlon2full.IndexOf("-") - 1);



                    var konta = (ICollection)session.CreateObject("CDN.Konta", null);
                    var konto = (CDNKH.IKonto)konta.AddNew(null);
                    string kontor = czlon10 + "." + czlon12;
                    // var rodzicKonto = (CDNKH.IKonto)konta[$"Acc_Numer ={ kontor} "];
                    var rodzickonto = (CDNKH.IKonto)konta[$"Acc_NumerIdx ='{kontor}'"];

                    konto.NumerIdx = NrKonta;
                    konto.Nazwa = NrKonta;
                    konto.ParId = rodzickonto.ID;
                    konto.Segment = czlon12;
                    konto.OkresId = 1;
                    konto.Poziom = 3;
                    session.Save();
                }
                catch (Exception e)
                {
                    Console.WriteLine("Bledy 2 {0}", NrKonta);
                    Console.WriteLine(e);
                }
            }






            if (counter == 0 && account == "205")
            {
                try
                {
                    var konta = (ICollection)session.CreateObject("CDN.Konta", null);
                    var konto = (CDNKH.IKonto)konta.AddNew(null);


                    //konto.NumerIdx = NrKonta;
                    konto.Nazwa = nazwa;
                    konto.TypKonta = 1;
                    //konto.Poziom = 1;
                    konto.Analityka = 0;
                    //konto.NextAcc = NrKonta;
                    konto.Segment = NrKonta;
                    konto.OkresId = 1;
                    //  konto.Poziom = 0;
                    session.Save();
                }
                catch (Exception e)
                {
                    Console.WriteLine("Bledy konta proste {0}", NrKonta);
                    Console.WriteLine(e);
                }
            }

            if (counter == 1 && account == "205")
            {
                bool checkacc = Char.IsNumber(NrKonta, 4);
                string czlon1 = NrKonta.Substring(0, NrKonta.IndexOf("-"));
                string czlon2 = NrKonta.Substring(NrKonta.IndexOf("-") + 1, NrKonta.Length - NrKonta.IndexOf("-") - 1);

                if (checkacc == false)
                {

                    try
                    {

                        var konta = (ICollection)session.CreateObject("CDN.Konta", null);
                        var konto = (CDNKH.IKonto)konta.AddNew(null);
                        var Kontrahenci = (CDNBase.ICollection)(session.CreateObject("CDN.Kontrahenci", null));
                        var kontrahent = (CDNHeal.IKontrahent)Kontrahenci[$"Knt_Kod='{czlon2}'"];

                        var rodzickonto = (CDNKH.IKonto)konta[$"Acc_NumerIdx ='{account}'"];

                        konto.Poziom = 2;
                        konto.Analityka = 1;
                        konto.SlownikId = kontrahent.GIDNumer;
                        konto.ParId = rodzickonto.ID;
                        konto.OkresId = rodzickonto.OkresId;
                        //    konto.Slownik = 1;
                        konto.Nazwa = kontrahent.Akronim;
                        konto.Segment = kontrahent.Akronim;
                        konto.SlownikTyp = 1;
                        session.Save();
                    }
                    catch (Exception e)
                    {
                        //205-01


                        Console.WriteLine(e);
                        //   Console.ReadKey();
                    }
                }
                else
                {
                    try
                    {

                        var konta = (ICollection)session.CreateObject("CDN.Konta", null);
                        var konto = (CDNKH.IKonto)konta.AddNew(null);
                        //     var Kontrahenci = (CDNBase.ICollection)(session.CreateObject("CDN.Kontrahenci", null));
                        //   var kontrahent = (CDNHeal.IKontrahent)Kontrahenci[$"Knt_Kod='{czlon2}'"];

                        var rodzickonto = (CDNKH.IKonto)konta[$"Acc_NumerIdx ='{account}'"];

                        konto.Poziom = 2;
                        konto.Analityka = 1;
                        //   konto.SlownikId = kontrahent.GIDNumer;
                        konto.ParId = rodzickonto.ID;
                        konto.OkresId = rodzickonto.OkresId;
                        //    konto.Slownik = 1;
                        konto.Nazwa = nazwa;
                        konto.Segment = czlon2;
                        //konto.SlownikTyp = 1;
                        session.Save();

                    }
                    catch (Exception e)
                    {

                    }

                }
            }


            if (counter == 2 && account == "205")
            {
                string czlon4 = NrKonta.Substring(0, NrKonta.IndexOf("-"));
                string czlon2full = NrKonta.Substring(NrKonta.IndexOf("-") + 1, NrKonta.Length - NrKonta.IndexOf("-") - 1);
                string czlon5 = czlon2full.Substring(0, czlon2full.IndexOf("-"));
                string czlon3 = czlon2full.Substring(czlon2full.IndexOf("-") + 1, czlon2full.Length - czlon2full.IndexOf("-") - 1);
                string czlon6 = czlon2full.Substring(0, czlon2full.IndexOf("-"));
                string czlonacc = czlon4 + "-" + czlon6;
                try
                {



                    var konta = (ICollection)session.CreateObject("CDN.Konta", null);
                    var konto = (CDNKH.IKonto)konta.AddNew(null);

                    // var rodzicKonto = (CDNKH.IKonto)konta[$"Acc_Numer ={ kontor} "];
                    var rodzickonto = (CDNKH.IKonto)konta[$"Acc_Numer ='{czlonacc}'"];

                    konto.NumerIdx = NrKonta;
                    konto.Nazwa = nazwa;
                    konto.ParId = rodzickonto.ID;
                    konto.Segment = czlon4;
                    konto.OkresId = 1;
                    konto.Poziom = 3;
                    session.Save();
                }
                catch (Exception e)
                {
                    Console.WriteLine("Bledy 2 {0}", NrKonta);
                    Console.WriteLine(e);
                }
            }






            if (counter == 0 && account == "200" || account == "202")
            {
                try
                {
                    var konta = (ICollection)session.CreateObject("CDN.Konta", null);
                    var konto = (CDNKH.IKonto)konta.AddNew(null);


                    //konto.NumerIdx = NrKonta;
                    konto.Nazwa = NrKonta;
                    konto.TypKonta = 1;
                    //konto.Poziom = 1;
                    konto.Analityka = 0;
                    //konto.NextAcc = NrKonta;
                    konto.Segment = NrKonta;
                    konto.OkresId = 1;
                    //  konto.Poziom = 0;
                    session.Save();
                }
                catch (Exception e)
                {
                    Console.WriteLine("Bledy konta proste {0}", NrKonta);
                    Console.WriteLine(e);
                }
            }
            if (counter == 1 && account == "200" || account == "202")
            {

                string kont = NrKonta.Substring(NrKonta.IndexOf('-') + 1, NrKonta.Length - NrKonta.IndexOf('-') - 1);

                try
                {


                    //kont = NrKonta.Substring(NrKonta.IndexOf('-') + 1, NrKonta.Length - NrKonta.IndexOf('-') - 1);

                    var konta = (ICollection)session.CreateObject("CDN.Konta", null);
                    var konto = (CDNKH.IKonto)konta.AddNew(null);
                    var Kontrahenci = (CDNBase.ICollection)(session.CreateObject("CDN.Kontrahenci", null));
                    var kontrahent = (CDNHeal.IKontrahent)Kontrahenci[$"Knt_Kod='{kont}'"];

                    string testt = account;
                    var rodzickonto = (CDNKH.IKonto)konta[$"Acc_NumerIdx ='{account}'"];

                    konto.Poziom = 2;
                    konto.Analityka = 1;
                    konto.SlownikId = kontrahent.GIDNumer;
                    konto.ParId = rodzickonto.ID;
                    konto.OkresId = rodzickonto.OkresId;
                    //   konto.Slownik = 1;
                    konto.Nazwa = kontrahent.Akronim;
                    konto.Segment = kont;
                    konto.SlownikTyp = 1;
                    session.Save();
                }
                catch (Exception e)
                {
                    Console.WriteLine(e);
                    Console.WriteLine("Bledy konta 200 {0}  {1}", NrKonta, kont);
                }
            }







        }




        public void AccEntriesImport(IEnumerable<AccEntry> accEntries)
        {
            var oobId = 0;
            foreach (var accEntry in accEntries)
            {
                try
                {
                    var session = new AdoSession();
                    var knf = (CDNKONFIGLib.IKonfiguracja)session.Login.CreateObject("CDNKonfig.Konfiguracja");
                    string val = knf.get_Value("BiezOkresObr");
                    oobId = val.Length == 0 ? 0 : Convert.ToInt32(val);

                    var rok = accEntries.FirstOrDefault().Date.Year.ToString();
                    var dzienniki = (ICollection)session.CreateObject("CDN.Dzienniki");
                    var dziennik = (CDNDave.IDziennik)dzienniki[$"Dzi_Symbol = '{rok}' AND Dzi_OObID=" + oobId];

                    var dekrety = (CDNKH.Dekrety)session.CreateObject("CDN.Dekrety");
                    var dekret = (CDNKH.IDekret)dekrety.AddNew();
                    dekret.DziID = dziennik.ID;
                    dekret.Dokument = accEntry.Number;
                    dekret.DataDok = accEntry.Date;
                    dekret.Bufor = 1;

                    var konta = (ICollection)session.CreateObject("CDN.Konta");
                    foreach (var accEntryElement in accEntry.AccEntryElements.Where(ae => Regex.IsMatch(ae.Account, @"^\d+")))
                    {
                        ICollection elementy = dekret.Elementy;
                        var element = (CDNKH.IDekretElement)elementy.AddNew();

                        //var kategorie = (ICollection)session.CreateObject("CDN.Kategorie", null);
                        //var ikategoria = (CDNHeal.IKategoria)kategorie["Kat_KodSzczegol = 'INNE'"];
                        //ielement.Kategoria = (CDNHeal.Kategoria)ikategoria;


                        /*
                         * Konta ksiegowe też są przypisane do okresu obrachunkowego!
                         * Wybieranie konta po samym numerze to za mało: trzeba jeszcze wybrać z jakiego okresu ma być konto!
                         */

                        CDNKH.IKonto konto = null;
                        try
                        {
                            konto = (CDNKH.IKonto)konta[$"Acc_Numer = '{accEntryElement.Account}' AND Acc_OObID=" + oobId];
                        }
                        catch (Exception)
                        {
                            var kontoPoziomy = accEntryElement.Account.Split('-');
                            int i = 0;
                            var kontoPoziom = kontoPoziomy[0];
                            while (i < kontoPoziomy.Length)
                            {
                                try
                                {
                                    var kontoTmp = (CDNKH.IKonto)konta[$"Acc_Numer = '{kontoPoziom}' AND Acc_OObID=" + oobId];
                                    kontoPoziom += "-" + kontoPoziomy[i + 1];
                                    i++;
                                }
                                catch (Exception)
                                {

                                    var session2 = new AdoSession();
                                    var konta2 = (ICollection)session2.CreateObject("CDN.Konta");
                                    var konto2 = (CDNKH.IKonto)konta2.AddNew();
                                    konto2.OkresId = oobId;
                                    if (i > 0)
                                    {
                                        var kontoRodzic = kontoPoziom.Substring(0, kontoPoziom.LastIndexOf("-"));
                                        var kontoNadrzedne = (CDNKH.IKonto)konta[$"Acc_Numer = '{kontoRodzic}' AND Acc_OObID=" + oobId];
                                        konto2.ParId = kontoNadrzedne.ID;
                                    }
                                    konto2.Segment = kontoPoziomy[i];
                                    konto2.Nazwa = kontoPoziomy[i];
                                    //if (accEntryElement.Currency != "PLN")
                                    //{
                                    //    konto2.WalutaSymbol = accEntryElement.Currency;
                                    //}
                                    session2.Save();
                                    if (i < kontoPoziomy.Length - 1)
                                    {
                                        kontoPoziom += "-" + kontoPoziomy[i + 1];
                                    }
                                    i++;
                                }
                                //var kontoNadrzedne = (CDNKH.IKonto)konta[$"Acc_Numer = '{accEntryElement.Account.Substring(0, 3)}' AND Acc_OObID=" + oobId];
                                //konto2.Segment = accEntryElement.Account.Substring(4);
                            }
                            konto = (CDNKH.IKonto)konta[$"Acc_Numer = '{accEntryElement.Account}' AND Acc_OObID=" + oobId];
                        }

                        if (accEntryElement.Dt != 0)
                        {
                            element.KontoWn = (CDNKH.Konto)konto;
                            element.Kwota = accEntryElement.Dt;
                        }
                        else if (accEntryElement.Ct != 0)
                        {
                            element.KontoMa = (CDNKH.Konto)konto;
                            element.Kwota = accEntryElement.Ct;
                        }
                        if (!string.IsNullOrWhiteSpace(accEntryElement.ExternalNumber))
                        {
                            element.Opis += accEntryElement.ExternalNumber + " ";
                        }
                        element.Opis += accEntryElement.Description;
                    }
                    session.Save();
                }
                catch (Exception e)
                {
                    LogManager.GetCurrentClassLogger().Error($"Błąd tworzenia PK nr {accEntry.Number}.Błąd: {e.Message}");
                }
            }
        }
    }
}
