﻿using ExcelDataReader;
using NLog;
using System;
using System.Collections.Generic;
using System.Data;
using System.IO;
using System.Linq;
using System.Text.RegularExpressions;
using OptimaImporter.Biz.Model;
using LinqToExcel;
namespace Groundfrost.Biz.Services
{
    public class SourceFileReader
    {
        public static List<AccountPlan> PlanAccount(string filePath)
        {
        

         
            var xls = new List<AccountPlan>();


            ConnexionExcel ConxObject = new ConnexionExcel(filePath);

            var query1 = from a in ConxObject.UrlConnexion.Worksheet<AccountPlan>("Arkusz1")
                         select a;
            foreach (var result in query1)
            {
                Console.WriteLine(result.Account);
                xls.Add(new AccountPlan
                {
                    Account = result.Account,
                    Name = result.Name




                });








            }

    
            return xls;
        }



        public static List<PKItems> PKitems(string filePath)
        {



            var xls = new List<PKItems>();


            ConnexionExcel ConxObject = new ConnexionExcel(filePath);

            var query1 = from a in ConxObject.UrlConnexion.Worksheet<PKItems>("Arkusz1")
                         select a;
            foreach (var result in query1)
            {

                xls.Add(new PKItems
                {
                    nrkontaoptima = result.nrkontaoptima,
                    Data = result.Data,
                    Datawpisu = result.Datawpisu,
                    Wn = result.Wn,
                    Ma = result.Ma,
                    NrDowodu = result.NrDowodu,
                    Lp = result.Lp,
                    Symbol = result.Symbol





                }) ;








            }


            return xls;
        }



        public static List<VatInvoice> ReadSaladStoryKomSales(string filePath)
        {
            var invoices = new List<VatInvoice>();

            using (var stream = File.Open(filePath, FileMode.Open, FileAccess.Read))
            {
                using (var reader = ExcelReaderFactory.CreateReader(stream))
                {
                    var result = reader.AsDataSet().Tables[0];
                    reader.Read(); //we ommit headers line
                    while (reader.Read() && !string.IsNullOrWhiteSpace(reader.GetString(7)))
                    {
                        if (reader[3] != null)
                        {
                            var inv = new VatInvoice();
                            try
                            {
                                inv.Number = reader[3].ToString();
                                inv.BuyerName = reader[4].ToString();
                                inv.BuyerAddress = reader[5].ToString();
                                inv.BuyerTaxNumber = reader[6].ToString().Replace("-", "");
                                inv.InvoiceDate = Convert.ToDateTime(reader[2].ToString());
                                inv.OperationDate = Convert.ToDateTime(reader[2].ToString());
                                inv.PaymentDate = Convert.ToDateTime(reader[2].ToString()).AddDays(7);
                                inv.Rows.Add(new VatInvoiceRow
                                {
                                    NetValue = Convert.ToDecimal(reader[8].ToString()),
                                    VatRate = reader[7].ToString().Replace("%", "").Trim(),
                                    GrossValue = Convert.ToDecimal(reader[10].ToString()),
                                    Category2 = Category2Selector.Select(reader[14].ToString())
                                });
                                invoices.Add(inv);
                            }
                            catch (Exception ex)
                            {
                                LogManager.GetCurrentClassLogger()
                                    .Error($"Błąd odczytu linii {inv.Number}.Błąd: {ex.Message}");
                            }
                        }
                        else
                        {
                            var inv = invoices.LastOrDefault();
                            try
                            {
                                inv?.Rows.Add(new VatInvoiceRow
                                {
                                    NetValue = Convert.ToDecimal(reader[8].ToString()),
                                    VatRate = reader[7].ToString().Replace("%", "").Trim(),
                                    GrossValue = Convert.ToDecimal(reader[10].ToString()),
                                    Category2 = Category2Selector.Select(reader[14].ToString())
                                });

                            }
                            catch (Exception)
                            {

                            }
                        }
                    }
                }
            }

            return invoices;
        }

        public static List<AccEntry> ReadMciAccountingEntries(string filePath)
        {
            var accEntries = new List<AccEntry>();

            using (var stream = File.Open(filePath, FileMode.Open, FileAccess.Read))
            {
                using (var reader = ExcelReaderFactory.CreateReader(stream))
                {
                    var counter = 1;
                    var result = reader.AsDataSet().Tables[0];
                    foreach (DataRow row in result.Rows)
                    {
                        if (!string.IsNullOrWhiteSpace(row[1].ToString()) && Regex.IsMatch(row[1].ToString().Trim(), @"^\d"))
                        {
                            var potentialExistingAccEntry = accEntries.SingleOrDefault(a => a.Number == row[15].ToString() && a.Date == Convert.ToDateTime(row[14].ToString()));
                            if (potentialExistingAccEntry == null)
                            {
                                var accEntry = new AccEntry();
                                try
                                {
                                    accEntry.Number = row[15].ToString();
                                    accEntry.Date = Convert.ToDateTime(row[14].ToString());
                                    accEntry.AccEntryElements.Add(new AccEntryElement
                                    {
                                        Account = row[1].ToString().Trim().Insert(3, "-"),
                                        Description = row[3].ToString(),
                                        Currency = row[4].ToString(),
                                        Dt = !string.IsNullOrWhiteSpace(row[5].ToString()) ? Convert.ToDecimal(row[5].ToString()) : 0,
                                        Ct = !string.IsNullOrWhiteSpace(row[6].ToString()) ? Convert.ToDecimal(row[6].ToString()) : 0,
                                        ExternalNumber = row[9].ToString()
                                    });
                                    accEntries.Add(accEntry);
                                }
                                catch (Exception e)
                                {
                                    LogManager.GetCurrentClassLogger().Error($"Błąd odczytu linii {counter}.Błąd: {e.Message}");
                                }
                            }
                            else
                            {
                                try
                                {
                                    potentialExistingAccEntry.AccEntryElements.Add(new AccEntryElement
                                    {
                                        Account = row[1].ToString().Trim().Insert(3, "-"),
                                        Description = row[3].ToString(),
                                        Currency = row[4].ToString(),
                                        Dt = !string.IsNullOrWhiteSpace(row[5].ToString()) ? Convert.ToDecimal(row[5].ToString()) : 0,
                                        Ct = !string.IsNullOrWhiteSpace(row[6].ToString()) ? Convert.ToDecimal(row[6].ToString()) : 0,
                                        ExternalNumber = row[9].ToString()
                                    });
                                }
                                catch (Exception e)
                                {
                                    LogManager.GetCurrentClassLogger().Error($"Błąd odczytu linii {counter}.Błąd: {e.Message}");
                                }
                            }

                            counter++;
                        }
                    }
                }
            }

            return accEntries;
        }
        public static List<AccEntry> ReadSymfoniaAccountingEntries(string filePath)
        {
            var accEntries = new List<AccEntry>();

            using (var stream = File.Open(filePath, FileMode.Open, FileAccess.Read))
            {
                using (var reader = ExcelReaderFactory.CreateReader(stream))
                {
                    var counter = 1;
                    var result = reader.AsDataSet().Tables[0];
                    foreach (DataRow row in result.Rows)
                    {
                        if (!string.IsNullOrWhiteSpace(row[0].ToString()) && Regex.IsMatch(row[0].ToString(), @"^\d+") && !row[5].ToString().StartsWith("900"))
                        {
                            if (!accEntries.Select(a => a.Number).Contains(row[1].ToString()))
                            {
                                var accEntry = new AccEntry();
                                try
                                {
                                    accEntry.Number = row[1].ToString();
                                    accEntry.Date = new DateTime(Convert.ToInt32(row[0].ToString().Substring(6, 4)), Convert.ToInt32(row[0].ToString().Substring(3, 2)), Convert.ToInt32(row[0].ToString().Substring(0, 2)));
                                    accEntry.AccEntryElements.Add(new AccEntryElement
                                    {
                                        Account = row[5].ToString().Replace(" ", "_").Replace(".", "_")
                                            .Replace("Dalborowice", "Dalborow").Replace("845-3", "845-3A"),
                                        Description = row[4].ToString(),
                                        Currency = !string.IsNullOrWhiteSpace(row[6].ToString()) ? row[7].ToString() : row[9].ToString(),
                                        Dt = !string.IsNullOrWhiteSpace(row[6].ToString()) ? Convert.ToDecimal(row[6].ToString()) : 0,
                                        Ct = !string.IsNullOrWhiteSpace(row[8].ToString()) ? Convert.ToDecimal(row[8].ToString()) : 0,
                                        ExternalNumber = row[2].ToString()
                                    });
                                    accEntries.Add(accEntry);
                                }
                                catch (Exception e)
                                {
                                    LogManager.GetCurrentClassLogger().Error($"Błąd odczytu linii {counter}.Błąd: {e.Message}");
                                }
                            }
                            else
                            {
                                var accEntry = accEntries.First(a => a.Number == row[1].ToString());
                                try
                                {
                                    accEntry.AccEntryElements.Add(new AccEntryElement
                                    {
                                        Account = row[5].ToString().Replace(" ", "_").Replace(".", "_")
                                            .Replace("Dalborowice", "Dalborow").Replace("845-3", "845-3A"),
                                        Description = row[4].ToString(),
                                        Currency = !string.IsNullOrWhiteSpace(row[6].ToString()) ? row[7].ToString() : row[9].ToString(),
                                        Dt = !string.IsNullOrWhiteSpace(row[6].ToString()) ? Convert.ToDecimal(row[6].ToString()) : 0,
                                        Ct = !string.IsNullOrWhiteSpace(row[8].ToString()) ? Convert.ToDecimal(row[8].ToString()) : 0,
                                        ExternalNumber = row[2].ToString()
                                    });
                                }
                                catch (Exception e)
                                {
                                    LogManager.GetCurrentClassLogger().Error($"Błąd odczytu linii {counter}.Błąd: {e.Message}");
                                }
                            }

                            counter++;
                        }
                    }
                }
            }

            return accEntries;
        }
    }
}
