﻿namespace Groundfrost.Biz.Services
{
    internal class Category2Selector
    {
        internal static string Select(string input)
        {
            if (input == "Salad Story Poznań Avenida Wyspa") return "PCCW";
            if (input == "Salad Story Poznań Avenida Lokal") return "PCCL";
            if (input == "Salad Story Warszawa Galeria Mokotów") return "GM";
            if (input == "Salad Story Warszawa Blue City") return "BC";
            if (input == "Salad Story Gdańsk Alchemia") return "GAL";
            if (input == "Salad Story Gdańsk Galeria Bałtycka") return "GBA";
            if (input == "Salad Story Wrocław Galeria Dominikańska") return "GD";
            if (input == "Salad Story Gdańsk Forum") return "GF";
            if (input == "Salad Story Katowice Galeria Katowicka") return "KAT";
            if (input == "Salad Story Łódź Galeria Łódzka") return "GL";
            if (input == "Salad Story Warszawa Galeria Młociny") return "MLO";
            if (input == "Salad Story Warszawa Galeria Północna") return "GPN";
            if (input == "Salad Story Gdynia Riviera") return "GW";
            if (input == "Salad Story Kraków CH Bonarka") return "KBON";
            if (input == "Salad Story Warszawa Krucza") return "KR";
            if (input == "Salad Story Wrocław Magnolia") return "MGN";
            if (input == "Salad Story Warszawa Nowy Świat") return "NŚ";
            if (input == "Salad Story Wrocław Pasaż Grunwaldzki") return "PG";
            if (input == "Salad Story Warszawa Piękna") return "PI";
            if (input == "Salad Story Warszawa Polna") return "POL";
            if (input == "Salad Story Poznań Posnania") return "POS";
            if (input == "Salad Story Poznań Plaza") return "PP";
            if (input == "Salad Story Warszawa Promenada") return "PRO";
            if (input == "Salad Story Poznań Stary Browar") return "STB";
            if (input == "Salad Story Warszawa Prosta") return "PT";
            if (input == "Salad Story Warszawa Plac Unii") return "PU";
            if (input == "Salad Story Wrocław Renoma") return "RE";
            if (input == "Salad Story Warszawa Reduta") return "REDU";
            if (input == "Salad Story Warszawa Sadyba Best Mall") return "SBM";
            if (input == "Salad Story Toruń CH Copernicus") return "TCOP";
            if (input == "Salad Story Warszawa Wola Park") return "WP";
            if (input == "Salad Story Wrocław Wroclavia") return "WRC";
            if (input == "Salad Story Warszawa Złote Tarasy") return "ZT";
            if (input == "Salad Story Wrocław Kuchnia Centralna") return "KWR";

            return "";
        }
    }
}
