﻿using System;
using System.Collections.Generic;
using System.Linq;
using Groundfrost.Biz.Services;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using OptimaImporter.Biz.Model;

namespace Groundfrost.Test
{
    [TestClass]
    public class UnitTest
    {
        [TestMethod]
        public void ReadSaladStoryKomSales_Test()
        {
            var filePath = @"W:\Klienci\Groundfrost\FVS 201810 SSKOM - Copy.xls";

            var invoicesRowsCount = 740;

            var invoices = SourceFileReader.ReadSaladStoryKomSales(filePath);            

            Assert.IsTrue(invoices.Any());
            Assert.AreEqual(invoicesRowsCount, invoices.Sum(i => i.Rows.Count));
        }

        [TestMethod]
        public void ReadMciAccountingEntries_Test()
        {
            var filePath = @"C:\Users\marci\OneDrive\KlienciPriv\Groundfrost\Zapisy 01_04.2019.xlsx";

            var accEntriesCount = 4354;
            var accEntriesSum = 2662980570.20m;
            var firstEntry = new AccEntry
            {
                Number = "19520061",
                Date = new DateTime(2019, 03, 31),
                AccEntryElements = new List<AccEntryElement>
                {
                    new AccEntryElement
                    {
                        Account = "010-103",
                        Currency = "PLN",
                        Dt = 3072034.14m,
                        Ct = 0m,
                        ExternalNumber = "",
                        Description = "Prawo do użyt.skał.mająt. IFRS16 na 01.01.19-pow.biurowa"
                    },
                    new AccEntryElement
                    {
                        Account = "249-104",
                        Currency = "PLN",
                        Dt = 0m,
                        Ct = 3072034.14m,
                        ExternalNumber = "",
                        Description = "Zdyskont.wart.zobow.tyt.leasingu IFTS16 na 1.01.19-pow.biuro"
                    }
                }
            };

            var accEntries = SourceFileReader.ReadMciAccountingEntries(filePath);
            var firstAccEntry = accEntries.First();
            
            Assert.IsTrue(accEntries.Any());
            Assert.AreEqual(accEntriesCount, accEntries.Count);
            Assert.AreEqual(accEntriesSum, accEntries.SelectMany(a => a.AccEntryElements).Sum(e => e.Dt));
            Assert.AreEqual(accEntriesSum, accEntries.SelectMany(a => a.AccEntryElements).Sum(e => e.Ct));
            Assert.AreEqual(firstEntry.Number, accEntries.First().Number);
            Assert.AreEqual(firstEntry.Date, accEntries.First().Date);
            Assert.AreEqual(firstEntry.AccEntryElements.First().Account, firstAccEntry.AccEntryElements.First().Account);
            Assert.AreEqual(firstEntry.AccEntryElements.First().Description, firstAccEntry.AccEntryElements.First().Description);
            Assert.AreEqual(firstEntry.AccEntryElements.First().Currency, firstAccEntry.AccEntryElements.First().Currency);
            Assert.AreEqual(firstEntry.AccEntryElements.First().Dt, firstAccEntry.AccEntryElements.First().Dt);
            Assert.AreEqual(firstEntry.AccEntryElements.First().Ct, firstAccEntry.AccEntryElements.First().Ct);
            Assert.AreEqual(firstEntry.AccEntryElements.First().ExternalNumber, firstAccEntry.AccEntryElements.First().ExternalNumber);
        }

        [TestMethod]
        public void ReadSymfoniaAccountingEntries_Test()
        {
            var filePath = @"C:\Users\marci\OneDrive - ITDF\KlienciPriv\Groundfrost\Migracja zapisów z Symfonii\Zapisy_2014.xls";

            var accEntriesCount = 4354;
            var accEntriesSum = 6140844.65m;
            var firstEntry = new AccEntry
            {
                Number = "D/2014/0002",
                Date = new DateTime(2014, 03, 21),
                AccEntryElements = new List<AccEntryElement>
                {
                    new AccEntryElement
                    {
                        Account = "202-2-1-2910224683",
                        Currency = "PLN",
                        Dt = 0.00m,
                        Ct = 110700.00m,
                        ExternalNumber = "F 0001/03/14/KO",
                        Description = "Umowa o świadczenie usług F 0001/03/14/KO"
                    },
                    new AccEntryElement
                    {
                        Account = "083-3-W/0099/2014",
                        Currency = "PLN",
                        Dt = 90000.00m,
                        Ct = 0.00m,
                        ExternalNumber = "F 0001/03/14/KO",
                        Description = "Umowa o świadczenie usług F 0001/03/14/KO"
                    },
                    new AccEntryElement
                    {
                        Account = "221-1-1",
                        Currency = "PLN",
                        Dt = 20700.00m,
                        Ct = 0.00m,
                        ExternalNumber = "F 0001/03/14/KO",
                        Description = "Umowa o świadczenie usług F 0001/03/14/KO"
                    }
                }
            };

            var accEntries = SourceFileReader.ReadSymfoniaAccountingEntries(filePath);
            var firstAccEntry = accEntries.First();

            Assert.IsTrue(accEntries.Any());
            Assert.AreEqual(accEntriesCount, accEntries.Count);
            Assert.AreEqual(accEntriesSum, accEntries.SelectMany(a => a.AccEntryElements).Sum(e => e.Dt));
            Assert.AreEqual(accEntriesSum, accEntries.SelectMany(a => a.AccEntryElements).Sum(e => e.Ct));
            Assert.AreEqual(firstEntry.Number, accEntries.First().Number);
            Assert.AreEqual(firstEntry.Date, accEntries.First().Date);
            Assert.AreEqual(firstEntry.AccEntryElements.First().Account, firstAccEntry.AccEntryElements.First().Account);
            Assert.AreEqual(firstEntry.AccEntryElements.First().Description, firstAccEntry.AccEntryElements.First().Description);
            Assert.AreEqual(firstEntry.AccEntryElements.First().Currency, firstAccEntry.AccEntryElements.First().Currency);
            Assert.AreEqual(firstEntry.AccEntryElements.First().Dt, firstAccEntry.AccEntryElements.First().Dt);
            Assert.AreEqual(firstEntry.AccEntryElements.First().Ct, firstAccEntry.AccEntryElements.First().Ct);
            Assert.AreEqual(firstEntry.AccEntryElements.First().ExternalNumber, firstAccEntry.AccEntryElements.First().ExternalNumber);
        }
    }
}
