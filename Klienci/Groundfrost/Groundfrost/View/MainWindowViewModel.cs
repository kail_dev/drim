﻿using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Linq;
using OptimaImporter.Biz.Model;
using OptimaImporter.Biz.Optima;
using OptimaImporter.Data.Enum;
using OptimaImporter.Wpf.ViewModel;

namespace Groundfrost.View
{
    public class MainWindowViewModel : MainWindowViewModelBase
    {
        public MainWindowViewModel()
        {
            StatusBarText = "Gotowy do pracy.";
        }

        public void ImportVatRegister(IList<VatInvoice> invoices)
        {
            if (invoices == null || !invoices.Any())
            {
                StatusBarText = "Nie udało się odczytać listy dokumentów do importu.";
                return;
            }

            var loggedIn = OptimaLogging.LogIn();
            if (!loggedIn)
            {
                StatusBarText = "Nie udało się zalogować do optimy. Import niemożliwy.";
                return;
            }

            var stopWatch = Stopwatch.StartNew();
            var inputInvoicesCounter = 0;
            var createdInvoicesCounter = 0;
            var failedInvoicesCounter = 0;
            var invoicesFromFileCount = invoices.Count;
            IProgress<Tuple<int, int>> progress = new Progress<Tuple<int, int>>(t =>
            {
                var percent = inputInvoicesCounter / (decimal)invoicesFromFileCount * 100;
                StatusBarText = $"Wczytywanie... ({percent:0}%), czas: {stopWatch.Elapsed:hh\\:mm\\:ss}. Wczytanych poprawnie: {t.Item1}/{invoicesFromFileCount}, błędy: {t.Item2}/{invoicesFromFileCount}.";
            });

            var optimaCreator = new Biz.Services.OptimaImporter();
            optimaCreator.Created += () =>
            {
                inputInvoicesCounter++;
                createdInvoicesCounter++;
                progress.Report(Tuple.Create(createdInvoicesCounter, failedInvoicesCounter));
            };
            optimaCreator.CreateFailed += () =>
            {
                inputInvoicesCounter++;
                failedInvoicesCounter++;
                progress.Report(Tuple.Create(createdInvoicesCounter, failedInvoicesCounter));
            };
            optimaCreator.Create(invoices, VatRegisterType.Sales, "TEMP");

            OptimaLogging.LogOut();
            StatusBarText += " Robota zrobiona!!!";
        }
    }
}
