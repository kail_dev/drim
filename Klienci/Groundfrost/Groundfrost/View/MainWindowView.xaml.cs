using Groundfrost.Biz.Services;
using Microsoft.Win32;
using NLog;
using OptimaImporter.Biz.Model;
using OptimaImporter.Biz.Optima;
using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.IO;
using System.Linq;
using System.Threading.Tasks;
using System.Windows;
using Telerik.Windows.Controls;

namespace Groundfrost.View
{
    /// <summary>
    /// Interaction logic for MainWindow.xaml
    /// </summary>
    public partial class MainWindowView
    {
        private static Logger Logger => LogManager.GetCurrentClassLogger();
        private readonly MainWindowViewModel _viewModel;
        private OpenFileDialog _openedFile;

        public MainWindowView()
        {
            StyleManager.ApplicationTheme = new Office_BlueTheme();
            InitializeComponent();

            _viewModel = new MainWindowViewModel();
            DataContext = _viewModel;

        }


        private async void GenerateRentInvoicesButton_OnClick(object sender, RoutedEventArgs e)
        {
            _openedFile = new OpenFileDialog();
            var result = _openedFile.ShowDialog();

            if (string.IsNullOrWhiteSpace(_openedFile.FileName)) return;

            _viewModel.StatusBarText = "Uwaga startuj�...";

            List<VatInvoice> invoices = null;
            try
            {
                invoices = SourceFileReader.ReadSaladStoryKomSales(_openedFile.FileName);
            }
            catch (IOException ex)
            {
                Logger.Error($"Nie uda�o si� odczyta� pliku z danymi. Sprawd� czy plik nie jest otwarty w innym pogramie. {ex}");
                RadWindow.Alert("Nie uda�o si� odczyta� pliku z danymi. Sprawd� czy plik nie jest otwarty w innym pogramie.");
            }

            if (invoices != null && invoices.Any())
            {
                await Task.Run(() => _viewModel.ImportVatRegister(invoices));
            }
        }

        private async void GenerateMciAccountEntriesButton_OnClick(object sender, RoutedEventArgs e)
        {
            StatusLabel.Content = " ";
            _openedFile = new OpenFileDialog();
            _openedFile.Title = "Wybierz plik do wczytania";

            var result = _openedFile.ShowDialog();
            if (result != null && (bool)result)
            {
                //sprawdzanie logowania do O!
                var optimaLoginValid = false;
                while (optimaLoginValid == false)
                {
                    optimaLoginValid = OptimaLogging.LogIn();
                }
                StatusLabel.Content = "Zalogowa�em si� do O!";


                GenerateMciAccountEntriesButton.IsEnabled = false;
                //czytanie .xls
                var accEntries = SourceFileReader.ReadMciAccountingEntries(_openedFile.FileName);

                await GenerateAccountEntriesAsync(accEntries)
                    .ContinueWith(t =>
                    {
                        Dispatcher?.Invoke(() =>
                        {
                            GenerateMciAccountEntriesButton.IsEnabled = true;
                            StatusLabel.Content += $". Zakonczono!!!";
                        });
                    });
            }
        }
        private async void GenerateAccountPlanButton_OnClick(object sender, RoutedEventArgs e)
        {
            StatusLabel.Content = " ";
            _openedFile = new OpenFileDialog();
            _openedFile.Title = "Wybierz plik do wczytania";

            var result = _openedFile.ShowDialog();
            if (result != null && (bool)result)
            {
              //  sprawdzanie logowania do O!
                    var optimaLoginValid = false;
                  while (optimaLoginValid == false)
                {
                  optimaLoginValid = OptimaLogging.LogIn();
                }
                StatusLabel.Content = "Zalogowa�em si� do O!";


                GenerateAccountPlanButton.IsEnabled = false;
                //czytanie .xls
                var accEntries = SourceFileReader.PlanAccount(_openedFile.FileName);
            //    var accEntries1 = SourceFileReader.ReadSymfoniaAccountingEntries(_openedFile.FileName);
                await GeneratePlanAccountAsync(accEntries)
                    .ContinueWith(t =>
                    {
                        Dispatcher?.Invoke(() =>
                        {
                            GenerateSymfoniaAccountEntriesButton.IsEnabled = true;
                            StatusLabel.Content += $". Zakonczono!!!";
                        });
                    });
            }

        }

        private async void GeneratePKButton_OnClick(object sender, RoutedEventArgs e)
        {
            StatusLabel.Content = " ";
            _openedFile = new OpenFileDialog();
            _openedFile.Title = "Wybierz plik do wczytania";

            var result = _openedFile.ShowDialog();
            if (result != null && (bool)result)
            {
                //  sprawdzanie logowania do O!
                var optimaLoginValid = false;
                while (optimaLoginValid == false)
                {
                    optimaLoginValid = OptimaLogging.LogIn();
                }
                StatusLabel.Content = "Zalogowa�em si� do O!";


                GeneratePKButton.IsEnabled = false;
                //czytanie .xls
                var accEntries = SourceFileReader.PKitems(_openedFile.FileName);
               // var accEntries = SourceFileReader.PlanAccount(_openedFile.FileName);
                //    var accEntries1 = SourceFileReader.ReadSymfoniaAccountingEntries(_openedFile.FileName);
                await GeneratePKDocs(accEntries)
                    .ContinueWith(t =>
                    {
                        Dispatcher?.Invoke(() =>
                        {
                            GenerateSymfoniaAccountEntriesButton.IsEnabled = true;
                            StatusLabel.Content += $". Zakonczono!!!";
                        });
                    });
            }

        }

        private async void GenerateSymfoniaAccountEntriesButton_OnClick(object sender, RoutedEventArgs e)
        {
            StatusLabel.Content = " ";
            _openedFile = new OpenFileDialog();
            _openedFile.Title = "Wybierz plik do wczytania";

            var result = _openedFile.ShowDialog();
            if (result != null && (bool)result)
            {
                //sprawdzanie logowania do O!
                var optimaLoginValid = false;
                while (optimaLoginValid == false)
                {
                    optimaLoginValid = OptimaLogging.LogIn();
                }
                StatusLabel.Content = "Zalogowa�em si� do O!";


                GenerateSymfoniaAccountEntriesButton.IsEnabled = false;
                //czytanie .xls
                var accEntries = SourceFileReader.ReadSymfoniaAccountingEntries(_openedFile.FileName);

                await GenerateAccountEntriesAsync(accEntries)
                    .ContinueWith(t =>
                    {
                        Dispatcher?.Invoke(() =>
                        {
                            GenerateSymfoniaAccountEntriesButton.IsEnabled = true;
                            StatusLabel.Content += $". Zakonczono!!!";
                        });
                    });
            }
        }
        public async Task GeneratePKDocs(List<PKItems> accEntries)
        {

            var stopWatch = Stopwatch.StartNew();
            var accEntriesCount = accEntries.Count;
            var progress = new Progress<int>(p =>
            {
                var percent = p / (decimal)accEntriesCount * 100;
                StatusLabel.Content = $"Wczytywanie zapis�w.. {p}/{accEntriesCount},   {percent:0} %," +
                                      $"   {stopWatch.Elapsed.Hours}:{stopWatch.Elapsed.Minutes}:{stopWatch.Elapsed.Seconds}";
            });
            var importer = new Biz.Services.OptimaImporter();
           await Task.Run(() => importer.GeneratePK(accEntries));
        }

        public async Task GeneratePlanAccountAsync(List<AccountPlan> accEntries)
        {

            var stopWatch = Stopwatch.StartNew();
            var accEntriesCount = accEntries.Count;
            var progress = new Progress<int>(p =>
            {
                var percent = p / (decimal)accEntriesCount * 100;
                StatusLabel.Content = $"Wczytywanie zapis�w.. {p}/{accEntriesCount},   {percent:0} %," +
                                      $"   {stopWatch.Elapsed.Hours}:{stopWatch.Elapsed.Minutes}:{stopWatch.Elapsed.Seconds}";
            });
            var importer = new Biz.Services.OptimaImporter();
           await Task.Run(() => importer.AccounPlanImport(accEntries));
        }
        public async Task GenerateAccountEntriesAsync(List<AccEntry> accEntries)
        {
            var stopWatch = Stopwatch.StartNew();
            var accEntriesCount = accEntries.Count;
            var progress = new Progress<int>(p =>
            {
                var percent = p / (decimal)accEntriesCount * 100;
                StatusLabel.Content = $"Wczytywanie zapis�w.. {p}/{accEntriesCount},   {percent:0} %," +
                                      $"   {stopWatch.Elapsed.Hours}:{stopWatch.Elapsed.Minutes}:{stopWatch.Elapsed.Seconds}";
            });
            var importer = new Biz.Services.OptimaImporter();
            await Task.Run(() => importer.AccEntriesImport(accEntries));
        }

        private void ConfigurationButton_OnClick(object sender, RoutedEventArgs e)
        {
            var confWindow = new ConfigurationView();
            confWindow.Owner = this;
            confWindow.ShowDialog();
        }

        private void LogButton_OnClick(object sender, RoutedEventArgs e)
        {
            var fileInfo = new FileInfo(AppDomain.CurrentDomain.BaseDirectory + "\\Log");
            Process.Start(fileInfo.FullName);
        }

        private void AppFolderButton_OnClick(object sender, RoutedEventArgs e)
        {
            Process.Start(AppDomain.CurrentDomain.BaseDirectory);
        }

        private void SettingsFolderButton_OnClick(object sender, RoutedEventArgs e)
        {
            Process.Start(System.Windows.Forms.Application.LocalUserAppDataPath);
        }
    }
}
