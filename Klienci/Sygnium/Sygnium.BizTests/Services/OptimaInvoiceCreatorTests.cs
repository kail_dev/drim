﻿using System;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using OptimaImporter.Biz.Optima;
using Sygnium.Biz.Services;

namespace OpgXlsInvoices.BizTests.Services
{
    [TestClass]
    public class OptimaInvoiceCreatorTests
    {
        [TestMethod]
        public void RunThisHouse()
        {
            var rentInvoices = XlsFileService.ReadRentInvoicesFile(@"C:\!C\Szeran\opg_test2.xlsx", "CZYNSZ NAJMU");
            OptimaLogging.LogIn();
            OptimaInvoiceCreator.CreateOptimaInvoices(new Progress<int>(), rentInvoices);
        }
    }
}