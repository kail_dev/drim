using OptimaImporter.Data.Config;
using Sygnium.Properties;

namespace Sygnium.Methods
{
    public class AppSettings
    {
        public static void Load()
        {
            ApplicationConfig.Config.OptimaUser = Settings.Default.OptimaUser;
            ApplicationConfig.Config.OptimaPassword = Settings.Default.OptimaPassword;
            ApplicationConfig.Config.OptimaDatabase = Settings.Default.OptimaDatabase;
        }
        public static void Save()
        {
            Settings.Default.OptimaUser = ApplicationConfig.Config.OptimaUser;
            Settings.Default.OptimaPassword = ApplicationConfig.Config.OptimaPassword;
            Settings.Default.OptimaDatabase = ApplicationConfig.Config.OptimaDatabase;
            Settings.Default.Save();
        }

        public static void Reset()
        {
            Settings.Default.Reset();
        }
    }
}