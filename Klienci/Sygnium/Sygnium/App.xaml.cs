﻿using Sygnium.Methods;
using Sygnium.View;
using System;
using System.Windows;

namespace Sygnium
{
    /// <summary>
    /// Interaction logic for App.xaml
    /// </summary>
    public partial class App : Application
    {
        protected override void OnExit(ExitEventArgs e)
        {
            base.OnExit(e);
            AppSettings.Save();
        }
        protected override void OnSessionEnding(SessionEndingCancelEventArgs e)
        {
            base.OnSessionEnding(e);
            AppSettings.Save();
        }

        protected override void OnStartup(StartupEventArgs e)
        {

            base.OnStartup(e);

            DispatcherUnhandledException += (o, ea) =>
            {
                NLog.LogManager.GetCurrentClassLogger().Info(ea.Exception.ToString());
                ea.Handled = true;
            };

            try
            {
                AppSettings.Load();
                var window = new MainWindowView {DataContext = new MainWindowViewModel()};
                window.Show();
            }
            catch (Exception)
            {
                var window = new MainWindowView { DataContext = new MainWindowViewModel() };
                window.Show();
            }
        }
    }
}
