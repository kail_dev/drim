using Microsoft.Win32;
using NLog;
using Sygnium.Biz.Model;
using Sygnium.Biz.Services;
using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.IO;
using System.Threading.Tasks;
using System.Windows;
using OptimaImporter.Biz.Optima;

namespace Sygnium.View
{
    /// <summary>
    /// Interaction logic for MainWindow.xaml
    /// </summary>
    public partial class MainWindowView : Window
    {
        private OpenFileDialog _openedFile;

        public MainWindowView()
        {
            InitializeComponent();
        }

        private void MainWindowView_OnLoaded(object sender, RoutedEventArgs e)
        {
            LogManager.GetCurrentClassLogger().Info("Start programu " + DateTime.Now);
        }

        private void LogButton_OnClick(object sender, RoutedEventArgs e)
        {
            var fileInfo = new FileInfo(AppDomain.CurrentDomain.BaseDirectory + "\\Log");
            Process.Start(fileInfo.FullName);
        }

        private async void GenerateOptimaRentInvoicesButton_OnClick(object sender, RoutedEventArgs e)
        {
            StatusLabel.Content = " ";
            _openedFile = new OpenFileDialog();
            _openedFile.Title = "Wybierz plik do wczytania";

            var result = _openedFile.ShowDialog();
            if (result != null && (bool) result)
            {
                //sprawdzanie logowania do O!
                var optimaLoginValid = false;
                while (optimaLoginValid == false)
                {
                    optimaLoginValid = OptimaLogginIn();
                }
                StatusLabel.Content = "Zalogowa�em si� do O!";


                GenerateOptimaRentInvoicesButton.IsEnabled = false;
                //czytanie .xls
                var rentInvoices = XlsFileService.ReadRentInvoicesFile(_openedFile.FileName, "CZYNSZ NAJMU");

                await GenerateOptimaRentInvoicesAsync(rentInvoices)
                    .ContinueWith(t =>
                    {
                        Dispatcher.Invoke(() =>
                        {
                            GenerateOptimaRentInvoicesButton.IsEnabled = true;
                            StatusLabel.Content += $". Zakonczono!!! Wczytano {t.Result}";
                        });
                    });


            }
        }


        private async Task<int> GenerateOptimaRentInvoicesAsync(List<RentInvoice> rentinvoices)
        {
            var stopWatch = Stopwatch.StartNew();
            var rentInvoicesCount = rentinvoices.Count;
            var progress = new Progress<int>(p =>
            {
                var percent = p / (decimal)rentInvoicesCount * 100;
                StatusLabel.Content = $"Wczytywanie faktur.. {p}/{rentInvoicesCount},   {percent:0} %," +
                                      $"   {stopWatch.Elapsed.Hours}:{stopWatch.Elapsed.Minutes}:{stopWatch.Elapsed.Seconds}";
            });
            return await Task.Run(() => OptimaInvoiceCreator.CreateOptimaInvoices(progress, rentinvoices));
        }

        private bool OptimaLogginIn()
        {
            try
            {
                OptimaLogging.LogIn();
                return true;
            }
            catch (Exception)
            {
                MessageBox.Show("B��d logowania do Optimy. Popraw konfiguracj�...");
                RunConfigurationWindow();
            }
            return false;
        }

        private void RunConfigurationWindow()
        {
            var confWindow = new ConfigurationView();
            confWindow.Owner = this;
            confWindow.ShowDialog();
        }

        private void ConfigurationButton_OnClick(object sender, RoutedEventArgs e)
        {
            RunConfigurationWindow();
        }

        private void AppFolderButton_OnClick(object sender, RoutedEventArgs e)
        {
            Process.Start(AppDomain.CurrentDomain.BaseDirectory);
        }

        private void SettingsFolderButton_OnClick(object sender, RoutedEventArgs e)
        {
            Process.Start(System.Windows.Forms.Application.LocalUserAppDataPath);
        }


    }
}
