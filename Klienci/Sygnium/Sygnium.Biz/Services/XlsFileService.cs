﻿using NLog;
using Sygnium.Biz.Model;
using System.Collections.Generic;
using System.Data.OleDb;
using System.Linq;

namespace Sygnium.Biz.Services
{
    public class XlsFileService
    {
        public static List<RentInvoice> ReadRentInvoicesFile(string filePath, string xlsSheet)
        {
            var invoices = new List<RentInvoice>();

            var constr = "Provider=Microsoft.ACE.OLEDB.12.0;Data Source="
                + filePath + ";Extended Properties=\"Excel 12.0 Xml;HDR=YES;\"";
            using (var conn = new OleDbConnection(constr))
            {
                conn.Open();
                var command = new OleDbCommand("Select * from [" + xlsSheet + "$]", conn);
                try
                {
                    var reader = command.ExecuteReader();
                    if (reader != null && reader.HasRows)
                    {
                        while (reader.Read() && !string.IsNullOrWhiteSpace(reader[0].ToString()))
                        {
                            if (reader[1].ToString() == "1")
                            {
                                var inv = new RentInvoice
                                {
                                    Client = reader[0].ToString(),
                                    Lp = reader[1].ToString(),
                                    PaymentMethod = reader[4].ToString()
                                };
                                inv.Products.Add(new RentProduct
                                {
                                    Amount = reader[2].ToString(),
                                    ProductName = reader[3].ToString(),
                                    Description = reader[5].ToString(),
                                    NetPrice = reader[6].ToString(),
                                    NetValue = reader[7].ToString(),
                                });
                                invoices.Add(inv);
                            }
                            else
                            {
                                var product = new RentProduct
                                {
                                    Amount = reader[2].ToString(),
                                    ProductName = reader[3].ToString(),
                                    Description = reader[5].ToString(),
                                    NetPrice = reader[6].ToString(),
                                    NetValue = reader[7].ToString(),
                                };
                                invoices.LastOrDefault()?.Products.Add(product);
                            }
                        }
                        LogManager.GetCurrentClassLogger().Info($"Odczyt pliku zakończony.");
                    }
                    if (reader != null && !reader.HasRows)
                    {
                        LogManager.GetCurrentClassLogger().Info($"Plik nie posiada zawartości.");
                    }
                }
               
                catch (OleDbException ex)
                {
                    if (ex.ErrorCode == -2147467259) //błędna nazwa arkusza
                    {
                        LogManager.GetCurrentClassLogger().Info("Błędna nazwa arkusza pliku .xls.");
                    }
                    throw;
                }
            }
            return invoices;
        }
    }
}
