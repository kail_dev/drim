﻿using CDNBase;
using CDNHlmn;
using NLog;
using Sygnium.Biz.Model;
using System;
using System.Collections.Generic;

namespace Sygnium.Biz.Services
{
    public class OptimaInvoiceCreator
    {

        public static int CreateOptimaInvoices (IProgress<int> progress,List<RentInvoice> rentinvoices )
        {
            var invCreatedCount = 0;
            var readInvCounter = 0;
            foreach (var rentInvoice in rentinvoices)
            {
                try
                {
                    if (!string.IsNullOrWhiteSpace(rentInvoice.Client))
                    {
                        var session = new AdoSession();

                        CDNHeal.IKontrahent contractor;

                        var faktury = (DokumentyHaMag)session.CreateObject("CDN.DokumentyHaMag");
                        var faktura = (IDokumentHaMag)faktury.AddNew();
                        var formyPlatnosci = (ICollection)(session.CreateObject("CDN.FormyPlatnosci"));
                        var kategorie = (CDNHeal.Kategorie)session.CreateObject("CDN.Kategorie");

                        try
                        {
                            contractor = (CDNHeal.IKontrahent)session.CreateObject("CDN.Kontrahenci")
                                    .Item("Knt_Kod = '" + rentInvoice.Client + "'");
                        }
                        catch (Exception)
                        {
                            contractor = (CDNHeal.IKontrahent)session.CreateObject("CDN.Kontrahenci")
                                    .Item("Knt_Kod = '!NIEOKREŚLONY!'");
                            LogManager.GetCurrentClassLogger()
                                .Error($"Nie odnaleziono kontrahenta o kodzie {rentInvoice.Client}. " + $"Wybrano kontrahenta !Nieokreślonego!");
                        }
                        faktura.Rodzaj = 302000;
                        faktura.TypDokumentu = 302;
                        faktura.Bufor = 1;
                        faktura.DataDok = DateTime.Now;
                        faktura.Podmiot = contractor;
                        try
                        {
                            faktura.FormaPlatnosci = (OP_KASBOLib.FormaPlatnosci)formyPlatnosci["FPl_Nazwa = '"
                                                                                              + rentInvoice.PaymentMethod + "'"];
                        }
                        catch (Exception)
                        {
                            LogManager.GetCurrentClassLogger()
                                .Error($"Nie odnaleziono formy płatności {rentInvoice.PaymentMethod}");
                            throw;
                        }

                        //kategoria dokumentu
                        //try
                        //{
                        //    faktura.Kategoria = (CDNHeal.Kategoria)kategorie["Kat_KodOgolny='CZYNSZ'"];
                        //}
                        //catch (Exception)
                        //{

                        //    LogManager.GetCurrentClassLogger().Error($"Nie odnaleziono kategorii CZYNSZ.");
                        //    throw ;
                        //}
                        var pozycje = faktura.Elementy;

                        foreach (var element in rentInvoice.Products)
                        {
                            var pozycja = (IElementHaMag)pozycje.AddNew();
                            try
                            {
                                pozycja.TowarKod = element.ProductName;
                            }
                            catch (Exception)
                            {
                                LogManager.GetCurrentClassLogger().Error($"Nie odnaleziono towaru o kodzie {element.ProductName}");
                                throw;
                            }

                            //kategoria pozycji
                            //try
                            //{
                            //    pozycja.Kategoria = (CDNHeal.Kategoria)kategorie["Kat_KodOgolny='CZYNSZ'"];
                            //}
                            //catch (Exception)
                            //{
                            //    LogManager.GetCurrentClassLogger().Error($"Nie odnaleziono kategorii CZYNSZ");
                            //    throw ;
                            //}

                            pozycja.UstawNazweTowaru(element.Description);
                            pozycja.Ilosc = Convert.ToDouble(element.Amount);
                            pozycja.CenaT = Convert.ToDecimal(element.NetPrice);
                            pozycja.WartoscNetto = Convert.ToDecimal(element.NetValue); 
                        }

                        session.Save();
                        invCreatedCount++;
                        LogManager.GetCurrentClassLogger().Info("Utworzyłem fakturę o numerze " + faktura.NumerPelny);
                    }
                }
                catch (Exception ex)
                {
                    LogManager.GetCurrentClassLogger().Error($"Wystąpił błąd podczas tworzenia faktury dla kontrahenta {rentInvoice.Client}. Błąd {ex}");
                }
                progress?.Report(++readInvCounter);
            }
            return invCreatedCount;
        }

    }
}
