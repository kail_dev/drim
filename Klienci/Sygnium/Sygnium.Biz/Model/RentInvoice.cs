﻿using System.Collections.Generic;

namespace Sygnium.Biz.Model
{
    public class RentInvoice
    {
        public RentInvoice()
        {
            Products = new List<RentProduct>();
        }
        public string Client { get; set; }
        public string Lp { get; set; }

        public string PaymentMethod { get; set; }

        public List<RentProduct> Products { get; set; }

    }
}
