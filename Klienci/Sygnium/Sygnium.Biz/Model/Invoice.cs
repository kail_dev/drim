﻿using System.Collections.Generic;

namespace Sygnium.Biz.Model
{
    public class Invoice
    {
        public Invoice()
        {
            Products = new List<Product>();
        }
        public string Client { get; set; }
        public string Amount { get; set; }
        public string NetPrice { get; set; }
        public List<Product> Products { get; set; }
    }
}
