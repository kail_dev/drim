﻿namespace Sygnium.Biz.Model
{
    public class RentProduct
    {
        public string ProductName { get; set; }
        public string Amount { get; set; }
        public string Description { get; set; }
        public string NetPrice { get; set; }
        public string NetValue { get; set; }
    }
}