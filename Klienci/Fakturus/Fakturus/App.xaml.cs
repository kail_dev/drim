﻿using NLog;
using System;
using System.Windows;

namespace OptimaImporterFakturus
{
    /// <summary>
    /// Interaction logic for App.xaml
    /// </summary>
    public partial class App
    {
        protected override void OnStartup(StartupEventArgs e)
        {
            base.OnStartup(e);

            DispatcherUnhandledException += (o, ea) =>
            {
                try
                {
                    LogManager.GetCurrentClassLogger().Info(ea.Exception.StackTrace);
                }
                catch (Exception ex)
                {
                    LogManager.GetCurrentClassLogger().Error($"Zalogowano: {ex}");

                }

                ea.Handled = true;
            };
            LogManager.GetCurrentClassLogger()
                .Info("Start at: " + DateTime.Now.ToShortDateString() + " " + DateTime.Now.ToShortTimeString());

        }
    }
}
