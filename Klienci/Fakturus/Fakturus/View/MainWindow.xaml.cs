﻿using Fakturus.Biz.Model;
using Fakturus.Biz.Services;
using Microsoft.Win32;
using NLog;
using System;
using System.Collections.Generic;
using System.Data.SqlClient;
using System.Diagnostics;
using System.IO;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using Telerik.Windows.Controls;

//72H88Tyk:-bk
namespace Fakturus.View
{
    public partial class MainWindow
    {
        public string ConnectionDataSource = "";
        public string ConnectionInitialCatalog = "";
        public string BazaFirmowa = "";

        OpenFileDialog _openedFile = new OpenFileDialog();

        //public static List<string> failedImporter = new List<string>();
        //RadGridView _errorListGridView = new RadGridView();

        public MainWindow()
        {
            StyleManager.ApplicationTheme = new Office_BlueTheme();

            InitializeComponent();

            try
            {
                var registry = Registry.CurrentUser.OpenSubKey(@"SOFTWARE\CDN\CDN Opt!ma\CDN Opt!ma\Login", false);
                var registryValue = (string)registry?.GetValue("KonfigConnectStr");
                ConnectionDataSource = registryValue.Substring(registryValue.IndexOf(",") + 1, registryValue.LastIndexOf(",") - registryValue.IndexOf(",") - 1);
                ConnectionInitialCatalog = registryValue.Substring(registryValue.IndexOf(":") + 1, registryValue.IndexOf(",") - registryValue.IndexOf(":") - 1);
                //MessageBox.Show("Nazwa serwera: " + connectionDataSource + "\nBaza konfiguracyjna: " + connectionInitialCatalog);
            }
            catch (Exception ex)
            {
                LogManager.GetCurrentClassLogger().Error($"Nie udało się odczytać informacji z rejestru: {ex}");
            }

            try
            {
                //podłączanie by pobrać listę baz do logowania
                var sqlConnectionStringBuilder = new SqlConnectionStringBuilder
                {
                    DataSource = ConnectionDataSource,
                    InitialCatalog = ConnectionInitialCatalog,
                    IntegratedSecurity = false,
                    UserID = "cdnoperator",
                    Password = "snw2bdmk"
                };

                var sqlConnection = new SqlConnection(sqlConnectionStringBuilder.ConnectionString);

                var query = @"select ope_kod from cdn.operatorzy";
                var sqlCommand = new SqlCommand(query, sqlConnection);
                sqlCommand.Connection.Open();
                var sqlDataReader = sqlCommand.ExecuteReader();
                userComboBox.Items.Clear();
                while (sqlDataReader.Read())
                {
                    var uzytkownikaNazwa = sqlDataReader.GetString(0);
                    userComboBox.Items.Add(uzytkownikaNazwa);
                }
                sqlDataReader.Close();
                sqlCommand.Connection.Close();
                userComboBox.IsEnabled = true;

                query = @"select baz_nazwa from cdn.bazy";
                sqlCommand = new SqlCommand(query, sqlConnection);
                sqlCommand.Connection.Open();
                sqlDataReader = sqlCommand.ExecuteReader();
                dbComboBox.Items.Clear();
                while (sqlDataReader.Read())
                {
                    var bazaNazwa = sqlDataReader.GetString(0);
                    dbComboBox.Items.Add(bazaNazwa);
                }
                sqlDataReader.Close();
                sqlCommand.Connection.Close();
                dbComboBox.IsEnabled = true;
            }
            catch (Exception ex)
            {
                LogManager.GetCurrentClassLogger().Error($"Nie udało się odczytać podstawowej konfiguracji firmy: {ex}");
            }
        }


        private async void VatS_Click(object sender, RoutedEventArgs e)
        {
            OptimaLogging.LogIn(userComboBox.SelectedItem.ToString(), passwordTextBox.Password, dbComboBox.SelectedItem.ToString());
            _openedFile = new OpenFileDialog {Title = "Wybierz plik do wczytania"};
            var result = _openedFile.ShowDialog();
            if (result != null && (bool) result)
            {
                VatSButton.IsEnabled = false;
                var csvInvoices = SourceFileReader.ReadFakturusCsvFile(_openedFile.FileName);


                await GenerateOptimaVatDocsAsync(csvInvoices, rejestrVATSprzedazyComboBox.SelectedValue.ToString(), 2)
                    .ContinueWith(t =>
                    {
                        Dispatcher.Invoke(() =>
                        {
                            VatSButton.IsEnabled = true;
                            StatusLabel.Content += $"Zakończono!!! Wczytano {t.Result.ResultsCreated} " +
                                                   $"z odczytanych {t.Result.RowsRead}.";
                            //_errorListGridView.ItemsSource = failedImporter;

                        });
                    });
                //ErrorButton.IsEnabled = true;
            }
        }

        private async void VatZButton_OnClick(object sender, RoutedEventArgs e)
        {
            OptimaLogging.LogIn(userComboBox.SelectedItem.ToString(), passwordTextBox.Password, dbComboBox.SelectedItem.ToString());

            _openedFile = new OpenFileDialog {Title = "Wybierz plik do wczytania"};
            var result = _openedFile.ShowDialog();
            if (result != null && (bool)result)
            {
                VatZButton.IsEnabled = false;
                var csvInvoices = SourceFileReader.ReadFakturusCsvFile(_openedFile.FileName);


                await GenerateOptimaVatDocsAsync(csvInvoices, rejestrVATZakupuComboBox.SelectedValue.ToString(), 1)
                    .ContinueWith(t =>
                    {
                        Dispatcher.Invoke(() =>
                        {
                            VatZButton.IsEnabled = true;
                            StatusLabel.Content += $"Zakończono!!! Wczytano {t.Result.ResultsCreated} " +
                                                   $"z odczytanych {t.Result.RowsRead}.";
                            //_errorListGridView.ItemsSource = failedImporter;

                        });
                    });
                //ErrorButton.IsEnabled = true;
            }
        }

        private async Task<ProgressCounter> GenerateOptimaVatDocsAsync(List<Invoice> csvInvoices, string register, int registerType)
        {

            var stopWatch = Stopwatch.StartNew();
            var invoicesCount = csvInvoices.Count;
            var progress = new Progress<ProgressCounter>(p =>
            {
                stopWatch.Start();
                var percent = p.RowsRead/ (decimal)invoicesCount * 100;
                StatusLabel.Content = $"Wczytywanie ... {p.RowsRead}/{invoicesCount},   {percent:0} %," +
                                      $"   {stopWatch.Elapsed.Hours}:{stopWatch.Elapsed.Minutes}:{stopWatch.Elapsed.Seconds}";
            });
            return await Task.Run(() => OptimaVatDocsCreator.CreateOptimaVatDocs(progress, csvInvoices, register,registerType));
        }

        
        private void Window_Closed(object sender, EventArgs e)
        {
            OptimaLogging.LogOut();
        }

        

        private void dbComboBox_SelectionChanged(object sender, SelectionChangedEventArgs e)
        {
            try
            {
                var sqlConnectionStringBuilder0 = new SqlConnectionStringBuilder
                {
                    DataSource = ConnectionDataSource,
                    InitialCatalog = ConnectionInitialCatalog,
                    IntegratedSecurity = false,
                    UserID = "cdnoperator",
                    Password = "snw2bdmk"
                };

                var sqlConnection0 = new SqlConnection(sqlConnectionStringBuilder0.ConnectionString);

                var query0 = @"select substring(baz_dostep,5,CHARINDEX(',',baz_dostep)-5) from cdn.bazy";
                var sqlCommand0 = new SqlCommand(query0, sqlConnection0);
                sqlCommand0.Connection.Open();
                var sqlDataReader0 = sqlCommand0.ExecuteReader();
                while (sqlDataReader0.Read())
                {
                    BazaFirmowa = sqlDataReader0.GetString(0);
                }
                sqlDataReader0.Close();
                sqlCommand0.Connection.Close();

                var sqlConnectionStringBuilder = new SqlConnectionStringBuilder
                {
                    DataSource = ConnectionDataSource,
                    InitialCatalog = BazaFirmowa,
                    IntegratedSecurity = false,
                    UserID = "cdnoperator",
                    Password = "snw2bdmk"
                };

                var sqlConnection = new SqlConnection(sqlConnectionStringBuilder.ConnectionString);

                var query = @"SELECT [Gru_Nazwa] FROM [CDN].[Grupy] where gru_typ = 1";
                var sqlCommand = new SqlCommand(query, sqlConnection);
                sqlCommand.Connection.Open();
                var sqlDataReader = sqlCommand.ExecuteReader();
                rejestrVATZakupuComboBox.Items.Clear();
                while (sqlDataReader.Read())
                {
                    var rejestrZakupu = sqlDataReader.GetString(0);
                    rejestrVATZakupuComboBox.Items.Add(rejestrZakupu);
                }
                sqlDataReader.Close();
                sqlCommand.Connection.Close();
                rejestrVATZakupuComboBox.IsEnabled = true;

                var query2 = @"SELECT [Gru_Nazwa] FROM [CDN].[Grupy] where gru_typ = 2";
                var sqlCommand2 = new SqlCommand(query2, sqlConnection);
                sqlCommand2.Connection.Open();
                var sqlDataReader2 = sqlCommand2.ExecuteReader();
                rejestrVATSprzedazyComboBox.Items.Clear();
                while (sqlDataReader2.Read())
                {
                    var rejestrSprzedazy = sqlDataReader2.GetString(0);
                    rejestrVATSprzedazyComboBox.Items.Add(rejestrSprzedazy);
                }
                sqlDataReader2.Close();
                rejestrVATSprzedazyComboBox.IsEnabled = true;
                sqlCommand2.Connection.Close();

            }
            catch (Exception ex)
            {
                LogManager.GetCurrentClassLogger().Error($"Błąd odczytu listy baz danych: {ex}");
            }

        }

        private void LogButton_Click(object sender, RoutedEventArgs e)
        {
            var fileInfo = new FileInfo(AppDomain.CurrentDomain.BaseDirectory + "\\Log");
            Process.Start(fileInfo.FullName);
        }

        
    }
}
