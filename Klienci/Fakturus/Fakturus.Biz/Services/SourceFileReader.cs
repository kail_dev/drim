﻿using Fakturus.Biz.Model;
using NLog;
using System;
using System.Collections.Generic;
using System.IO;
using System.Text;

namespace Fakturus.Biz.Services
{
    public class SourceFileReader
    {
        public static List<Invoice> ReadFakturusCsvFile(string sourcePath)
        {
            var invoices = new List<Invoice>();
            var lineCounter = 0;
            using (var file = new StreamReader(sourcePath, Encoding.Unicode))
            {
                while (!file.EndOfStream)
                {
                    var invoice = new Invoice();
                    try
                    {
                        lineCounter++;
                        var line = file.ReadLine();
                        if (line != null)
                        {
                            var lineElementsList = line.Split(';');
                            if (!line.StartsWith("Numer umowy"))
                            {
                                invoice.Number = lineElementsList[0];
                                invoice.BuyerName = lineElementsList[1];

                                var address = lineElementsList[2].Split(',');
                                invoice.City = address[0];
                                invoice.BuyerAddress = address[1].TrimStart();

                                invoice.InvoiceDate = new DateTime(Convert.ToInt32(lineElementsList[3].Substring(6, 4)),
                                    Convert.ToInt32(lineElementsList[3].Substring(3, 2)),
                                    Convert.ToInt32(lineElementsList[3].Substring(0, 2)));
                                if (string.IsNullOrWhiteSpace(lineElementsList[4]))
                                {
                                    invoice.PaymentDate = invoice.InvoiceDate;
                                }
                                else
                                {
                                    invoice.PaymentDate = new DateTime(Convert.ToInt32(lineElementsList[4].Substring(6, 4)),
                                        Convert.ToInt32(lineElementsList[4].Substring(3, 2)),
                                        Convert.ToInt32(lineElementsList[4].Substring(0, 2)));
                                }
                                invoice.NetValue = Convert.ToDecimal(lineElementsList[5]);
                                invoices.Add(invoice);
                            }
                        }
                    }
                    catch (Exception ex)
                    {
                        if (ex.InnerException != null)
                            LogManager.GetCurrentClassLogger().Error($"Błąd w odczycie pliku. Błąd: {ex}; " +
                                                                     $"{ex.InnerException.InnerException}");
                    }
                }
            }

            LogManager.GetCurrentClassLogger().Info("Odczytano plik");
            return invoices;
        }
    }
}
