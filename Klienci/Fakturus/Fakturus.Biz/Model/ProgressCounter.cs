﻿namespace Fakturus.Biz.Model
{
    public class ProgressCounter
    {
        public int RowsRead { get; set; }
        public int ResultsCreated { get; set; }
    }
}
