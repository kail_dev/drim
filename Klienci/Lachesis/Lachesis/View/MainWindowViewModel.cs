﻿using Lachesis.Biz.Services;
using OptimaImporter.Biz.Model;
using OptimaImporter.Biz.Optima;
using OptimaImporter.Data.Config;
using OptimaImporter.Data.Enum;
using OptimaImporter.Data.SQL;
using OptimaImporter.Wpf.ViewModel;
using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Linq;

namespace Lachesis.View
{
    public class MainWindowViewModel : MainWindowViewModelBase
    {
        public string ConnectionString { get; set; }

        private IEnumerable<string> _salesVatRegisters;
        public IEnumerable<string> SalesVatRegisters
        {
            get => _salesVatRegisters;
            set => SetProperty(ref _salesVatRegisters, value);
        }

        public string SelectedSalesVatRegister { get; set; }

        private IEnumerable<string> _kbRegisters;
        public IEnumerable<string> KbRegisters
        {
            get => _kbRegisters;
            set => SetProperty(ref _kbRegisters, value);
        }

        public string SelectedKbRegister { get; set; }

        public void ImportVatRegister(IList<VatInvoice> invoices)
        {
            if (invoices == null || !invoices.Any())
            {
                StatusBarText = "Nie udało się odczytać listy dokumentów do importu.";
                return;
            }

            if (!OptimaLogIn()) return;

            var stopWatch = Stopwatch.StartNew();
            var inputCounter = 0;
            var createdCounter = 0;
            var failedCounter = 0;
            var inputAmount = invoices.Count;
            IProgress<Tuple<int, int>> progress = new Progress<Tuple<int, int>>(t =>
            {
                var percent = inputCounter / (decimal)inputAmount * 100;
                StatusBarText = $"Wczytywanie... ({percent:0}%), czas: {stopWatch.Elapsed:hh\\:mm\\:ss}. Wczytanych poprawnie: {t.Item1}/{inputAmount}, błędy: {t.Item2}/{inputAmount}.";
            });

            var optimaCreator = new OptimaVatImporter();
            optimaCreator.Created += () =>
            {
                inputCounter++;
                createdCounter++;
                progress.Report(Tuple.Create(createdCounter, failedCounter));
            };
            optimaCreator.CreateFailed += () =>
            {
                inputCounter++;
                failedCounter++;
                progress.Report(Tuple.Create(createdCounter, failedCounter));
            };
            optimaCreator.Create(invoices, VatRegisterType.Sales, SelectedSalesVatRegister);

            OptimaLogging.LogOut();
            StatusBarText += " Robota zrobiona!!!";
        }

        public void ImportPayments(List<Payment> payments)
        {
            if (payments == null || !payments.Any())
            {
                StatusBarText = "Nie udało się odczytać listy dokumentów do importu.";
                return;
            }

            if (!OptimaLogIn()) return;

            var stopWatch = Stopwatch.StartNew();
            var inputCounter = 0;
            var createdCounter = 0;
            var failedCounter = 0;
            var inputAmount = payments.Count;
            IProgress<Tuple<int, int>> progress = new Progress<Tuple<int, int>>(t =>
            {
                var percent = inputCounter / (decimal)inputAmount * 100;
                StatusBarText = $"Wczytywanie... ({percent:0}%), czas: {stopWatch.Elapsed:hh\\:mm\\:ss}. Wczytanych poprawnie: {t.Item1}/{inputAmount}, błędy: {t.Item2}/{inputAmount}.";
            });

            var optimaCreator = new OptimaBankRecordImporter();
            optimaCreator.Created += (s) =>
            {
                inputCounter++;
                createdCounter++;
                progress.Report(Tuple.Create(createdCounter, failedCounter));
            };
            optimaCreator.CreateFailed += () =>
            {
                inputCounter++;
                failedCounter++;
                progress.Report(Tuple.Create(createdCounter, failedCounter));
            };
            optimaCreator.Create(payments, SelectedKbRegister);

            OptimaLogging.LogOut();
            StatusBarText += " Robota zrobiona!!!";
        }

        public void InitializeSalesVatRegister()
        {
            ConnectionString = ApplicationConfig.Config.OptimaDataBaseConnectionString;
            SalesVatRegisters = OptimaSqlReader.GetOptimaVatRegisters(VatRegisterType.Sales);
            KbRegisters = OptimaSqlReader.GetOptimaKbRegisters();
        }

        private bool OptimaLogIn()
        {
            var loggedIn = OptimaLogging.LogIn();
            if (!loggedIn)
            {
                StatusBarText = "Nie udało się zalogować do optimy. Import niemożliwy.";
                return false;
            }

            StatusBarText = "Logowanie do Optimy poprawne.";
            return true;
        }
    }
}
