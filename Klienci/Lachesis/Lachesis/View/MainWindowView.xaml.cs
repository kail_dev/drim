﻿using Lachesis.Biz.Services;
using Microsoft.Win32;
using NLog;
using System;
using System.Collections.Generic;
using System.IO;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using OptimaImporter.Biz.Model;
using OptimaImporter.Data.Config;
using Telerik.Windows.Controls;

namespace Lachesis.View
{
    public partial class MainWindowView : Window
    {
        private static Logger Logger => LogManager.GetCurrentClassLogger();
        private readonly MainWindowViewModel _viewModel;

        public MainWindowView()
        {
            StyleManager.ApplicationTheme = new Office_BlueTheme();
            InitializeComponent();

            _viewModel = new MainWindowViewModel();
            DataContext = _viewModel;
        }

        private void dbComboBox_SelectionChanged(object sender, SelectionChangedEventArgs e)
        {
            try
            {
                _viewModel.InitializeSalesVatRegister();
            }
            catch (Exception ex)
            {
                Logger.Error($"Błąd odczytu listy baz danych. {ex}");
            }
        }

        private async void ReadExcelFileButton_OnClick(object sender, RoutedEventArgs e)
        {
            ApplicationConfig.Config.OptimaPassword = PasswordTextBox.Password;

            var ofd = new OpenFileDialog();
            ofd.ShowDialog();
            if (string.IsNullOrWhiteSpace(ofd.FileName)) return;

            _viewModel.StatusBarText = "Uwaga startuję...";

            List<VatInvoice> invoices = null;
            try
            {
                invoices = SourceFileReaderService.ReadExcelFile(ofd.FileName);
                Logger.Info($"Odczytałem {invoices.Count} faktur z pliku.");
            }
            catch (IOException ex)
            {
                Logger.Error($"Nie udało się odczytać pliku z danymi. Sprawdź czy plik nie jest otwarty w innym pogramie. {ex}");
                RadWindow.Alert("Nie udało się odczytać pliku z danymi. Sprawdź czy plik nie jest otwarty w innym pogramie.");
            }


            await Task.Run(() => _viewModel.ImportVatRegister(invoices));
        }

        private async void ReadPaymentsExcelFileButton_OnClick(object sender, RoutedEventArgs e)
        {
            ApplicationConfig.Config.OptimaPassword = PasswordTextBox.Password;

            var ofd = new OpenFileDialog();
            ofd.ShowDialog();
            if (string.IsNullOrWhiteSpace(ofd.FileName)) return;

            _viewModel.StatusBarText = "Uwaga startuję...";

            List<Payment> payments = null;
            try
            {
                payments = SourceFileReaderService.ReadPaymentsFromExcelFile(ofd.FileName);
                Logger.Info($"Odczytałem {payments.Count} płatności z pliku.");
            }
            catch (IOException ex)
            {
                Logger.Error($"Nie udało się odczytać pliku z danymi. Sprawdź czy plik nie jest otwarty w innym pogramie. {ex}");
                RadWindow.Alert("Nie udało się odczytać pliku z danymi. Sprawdź czy plik nie jest otwarty w innym pogramie.");
            }

            await Task.Run(() => _viewModel.ImportPayments(payments));
        }
    }
}
