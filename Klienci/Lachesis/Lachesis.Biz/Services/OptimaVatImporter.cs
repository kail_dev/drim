﻿using CDNBase;
using CDNHeal;
using CDNRVAT;
using NLog;
using System;
using System.Collections.Generic;
using System.Linq;
using OptimaImporter.Biz.Helper;
using OptimaImporter.Biz.Model;
using OptimaImporter.Data.Enum;

// ReSharper disable EmptyGeneralCatchClause

namespace Lachesis.Biz.Services
{
    public class OptimaVatImporter
    {
        private Logger Logger => LogManager.GetCurrentClassLogger();
        private static readonly IEnumerable<string> Currencies = new List<string> { "EUR", "USD" };

        public event Action Created;
        public event Action CreateFailed;

        public void Create(IEnumerable<VatInvoice> invoices,
            VatRegisterType vatRegisterType, string vatRegisterName,
            bool nameAsCustomerCode = false)
        {
            foreach (var invoice in invoices)
            {
                var session = new AdoSession();
                var customers = (Kontrahenci)session.CreateObject("CDN.Kontrahenci");

                var customer = TrySelectCustomer(invoice, customers, vatRegisterType) ?? CreateCustomer(invoice, nameAsCustomerCode, vatRegisterType);

                var vatRegister = CreateHeader(vatRegisterType, vatRegisterName, session, invoice);

                SetCustomer(invoice, nameAsCustomerCode, vatRegister, customer, customers);

                TrySetCurrency(invoice, session, vatRegister);

                TrySetPayment(invoice, session, vatRegister);

                AddElements(invoice, vatRegister);

                SetDescription(invoice, vatRegister);

                TrySave(session, invoice);
            }
        }

        private IKontrahent TrySelectCustomer(VatInvoice invoice, IAdoCollection customers, VatRegisterType vatRegisterType)
        {
            IKontrahent customer = null;

            try
            {
                if ((int)vatRegisterType == 1)
                {
                    customer = customers["Knt_NIP='" + NipSplit.GetNipNumber(invoice.SellerTaxNumber.Replace("-", "")) + "'"];
                }
                else if (invoice.BuyerTaxNumber != null)
                {
                    customer = customers["Knt_NIP='" + NipSplit.GetNipNumber(invoice.BuyerTaxNumber.Replace("-", "")) + "'"];
                }
                else if (invoice.BuyerCode != null)
                {
                    customer = customers["Knt_Kod='" + invoice.BuyerCode + "'"];
                }
                else if (invoice.BuyerName != null)
                {
                    customer = customers["Knt_Nazwa1='" + invoice.BuyerName + "'"];
                }

                return customer;
            }
            catch (Exception ex)
            {
                Logger.Info($"Nie odnaleziono kontrahenta po NIP, po kodzie ani po nazwie. {ex}");
                return null;
            }
        }
        private IKontrahent CreateCustomer(VatInvoice invoice, bool nameAsCustomerCode, VatRegisterType vatRegisterType)
        {
            var session = new AdoSession();
            var customers = (Kontrahenci)session.CreateObject("CDN.Kontrahenci");
            IKontrahent customer = customers.AddNew();

            var taxNumber = vatRegisterType == VatRegisterType.Purchase ? invoice.SellerTaxNumber : invoice.BuyerTaxNumber;
            var name = vatRegisterType == VatRegisterType.Purchase ? invoice.SellerName : invoice.BuyerName;

            customer.Akronim = taxNumber;
            if (string.IsNullOrWhiteSpace(name)) name = taxNumber;
            customer.Nazwa1 = name;

            if (string.IsNullOrWhiteSpace(taxNumber) && !nameAsCustomerCode)
            {
                customer.Akronim = DateTime.Today.ToShortDateString() + DateTime.Now.ToLongTimeString();
            }
            else if (string.IsNullOrWhiteSpace(taxNumber) && nameAsCustomerCode)
            {
                customer.Akronim = name;
            }
            else
            {
                try
                {
                    customer.NumerNIP.NIPKraj = NipSplit.GetNipCountry(taxNumber);
                    customer.NumerNIP.NipE = NipSplit.GetNipNumber(taxNumber);
                }
                catch (Exception ex)
                {
                    Logger.Error($"Nie udało się przypisać NIPu do kontrahenta dla faktury {invoice.Number}, błąd: {ex}");
                }
            }

            customer.Rodzaj_Dostawca = vatRegisterType == VatRegisterType.Purchase ? 1 : 0;
            customer.Rodzaj_Odbiorca = vatRegisterType == VatRegisterType.Purchase ? 0 : 1;

            customer.Adres.Ulica = invoice.Street;
            customer.Adres.Miasto = invoice.City;
            customer.Adres.KodPocztowy = invoice.PostalCode;

            try
            {
                session.Save();
                Logger.Info($"Założono kontrahenta dla faktury: {invoice.Number} o akronimie {customer.Akronim}.");
            }
            catch (Exception ex)
            {
                Logger.Error($"Nie udało się zapisać kontrahenta dla faktury {invoice.Number}, błąd: {ex}.");
            }

            return customer;
        }
        private VAT CreateHeader(VatRegisterType vatRegisterType, string vatRegisterName, IAdoSession session, VatInvoice invoice)
        {
            var vatRegisters = (ICollection)session.CreateObject("CDN.RejestryVAT");
            var vatRegister = (VAT)vatRegisters.AddNew();
            vatRegister.Typ = vatRegisterType == VatRegisterType.Purchase ? 1 : 2;
            vatRegister.Rejestr = string.IsNullOrWhiteSpace(vatRegisterName) && vatRegister.Typ == 1 ? "ZAKUP" : vatRegisterName;
            vatRegister.Rejestr = string.IsNullOrWhiteSpace(vatRegisterName) && vatRegister.Typ == 2 ? "SPRZEDAŻ" : vatRegisterName;
            vatRegister.Dokument = invoice.Number;
            vatRegister.DataWys = invoice.InvoiceDate;
            vatRegister.DataZap = invoice.InvoiceDate;
            vatRegister.DataOpe = invoice.OperationDate == new DateTime(1, 1, 1) ? invoice.InvoiceDate : invoice.OperationDate;
            return vatRegister;
        }
        private void SetCustomer(VatInvoice invoice, bool nameAsCustomerCode, IVAT vatRegister, IPodmiot customer, IAdoCollection customers)
        {
            try
            {
                vatRegister.Podmiot = customer;
            }
            catch (Exception)
            {
                vatRegister.Podmiot = customers["Knt_KntId=1"];
            }

            if (vatRegister.Podmiot == customers["Knt_KntId=1"] && nameAsCustomerCode || invoice.BuyerCode == "!NIEOKREŚLONY!")
            {
                vatRegister.Nazwa1 = invoice.BuyerName;
                vatRegister.PodAdres.Ulica = invoice.BuyerAddress;
                vatRegister.PodAdres.KodPocztowyZKreska = invoice.PostalCode;
                vatRegister.PodAdres.Miasto = invoice.City;
            }
        }
        private void TrySetCurrency(VatInvoice invoice, IAdoSession session, IVAT vatRegister)
        {
            if (string.IsNullOrWhiteSpace(invoice.Currency) || !Currencies.Contains(invoice.Currency)) return;

            var waluty = (ICollection)session.CreateObject("CDN.Waluty");
            var waluta = (Waluta)waluty[$"WNa_Symbol='{invoice.Currency}'"];
            try
            {
                vatRegister.Waluta = waluta;
                vatRegister.WalutaDoVAT = waluta;
                var kurs = session.CreateObject("CDN.TypyKursowWalut").Item("WKu_Symbol='NBP'");
                vatRegister.TypKursuWaluty = kurs;
                vatRegister.TypKursuWalutyDoVAT = kurs;
            }
            catch (Exception)
            {
            }
        }
        private void TrySetPayment(VatInvoice invoice, IAdoSession session, IVAT vatRegister)
        {
            var paymentForms = (ICollection)session.CreateObject("CDN.FormyPlatnosci");
            var paymentForm = invoice.InvoiceDate == invoice.PaymentDate
                ? (OP_KASBOLib.FormaPlatnosci)paymentForms["Fpl_FplId=1"]
                : (OP_KASBOLib.FormaPlatnosci)paymentForms["Fpl_FplId=3"];
            try
            {
                paymentForm = (OP_KASBOLib.FormaPlatnosci)paymentForms[$"Fpl_Nazwa='{invoice.PaymentForm}'"];
            }
            catch (Exception ex)
            {
                Logger.Error($"Nie ondaleziono formy płatności dla faktury {invoice.Number}, błąd: {ex}.");
            }

            try
            {
                vatRegister.FormaPlatnosci = paymentForm;
            }
            catch (Exception ex)
            {
                Logger.Error($"Nie przypisano formy płatności dla faktury {invoice.Number}, błąd: {ex}.");
            }

            if (invoice.PaymentDate.Year > 1900)
            {
                vatRegister.Termin = invoice.PaymentDate;
            }
        }
        private void AddElements(VatInvoice invoice, IVAT vatRegister)
        {
            var vatElements = vatRegister.Elementy;
            foreach (var row in invoice.Rows)
            {
                var vatElement = (VATElement)vatElements.AddNew();
                try
                {
                    var stawkaString = row.VatRate.Replace("\"", "").Replace(".0000", ",00");
                    vatElement.Stawka = Convert.ToDouble(stawkaString);
                }
                catch (Exception)
                {
                    if (row.VatRate.Replace("\"", "").Replace(".0000", ",00").StartsWith("zw"))
                    {
                        vatElement.Stawka = 0;
                        vatElement.Flaga = 1;
                    }
                    else if (row.VatRate.Replace("\"", "").Replace(".0000", ",00").StartsWith("np"))
                    {
                        vatElement.Stawka = 0;
                        vatElement.Flaga = 4;
                    }
                    else
                    {
                        vatElement.Stawka = 23;
                    }
                }

                vatElement.RodzajZakupu = 1;
                vatElement.Netto = row.NetValue;
                vatElement.Odliczenia = 1;
                if (row.VatValue != 0)
                {
                    vatElement.VAT = row.VatValue;
                }
                else
                {
                    vatElement.Brutto = row.GrossValue;
                }
            }
        }
        private void SetDescription(VatInvoice invoice, VAT vatRegister)
        {
            if (!string.IsNullOrWhiteSpace(invoice.Description))
            {
                vatRegister.KategoriaOpis = invoice.Description;
            }
        }
        private void TrySave(AdoSession session, VatInvoice invoice)
        {
            try
            {
                session.Save();
                Logger.Info($"Dokument nr: {invoice.Number} utworzony.");
                Created?.Invoke();
            }
            catch (Exception ex)
            {
                Logger.Error($"Dokument nr: {invoice.Number} nie utworzony. {ex}");
                CreateFailed?.Invoke();
            }
        }
    }
}
