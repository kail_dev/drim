﻿using ExcelDataReader;
using NLog;
using OptimaImporter.Biz.Model;
using System;
using System.Collections.Generic;
using System.IO;

namespace Lachesis.Biz.Services
{
    public class SourceFileReaderService
    {
        private static Logger Logger => LogManager.GetCurrentClassLogger();

        public static List<VatInvoice> ReadExcelFile(string filePath)
        {
            var invoices = new List<VatInvoice>();

            using (var stream = File.Open(filePath, FileMode.Open, FileAccess.Read))
            {
                using (var reader = ExcelReaderFactory.CreateReader(stream))
                {
                    reader.Read(); //we ommit headers line
                    while (reader.Read() && !string.IsNullOrWhiteSpace(reader.GetString(0)))
                    {
                        var inv = new VatInvoice();
                        try
                        {
                            inv.Number = reader.GetString(0);
                            inv.BuyerTaxNumber = reader.GetString(1);
                            inv.InvoiceDate = Convert.ToDateTime(reader[2]);
                            inv.OperationDate = Convert.ToDateTime(reader[3]);
                            inv.PaymentForm = reader.GetString(11).ToLower();
                            if (inv.PaymentForm.Contains("karta"))
                            {
                                inv.PaymentForm = "karta";
                            }
                            //vat 23%
                            if (Convert.ToDecimal(reader[4]) > 0.00m)
                            {
                                inv.Rows.Add(new VatInvoiceRow
                                {
                                    NetValue = Convert.ToDecimal(reader[4]),
                                    VatValue = Convert.ToDecimal(reader[5]),
                                    VatRate = "23"
                                });

                            }
                            //vat 8%
                            if (Convert.ToDecimal(reader[6]) > 0.00m)
                            {
                                inv.Rows.Add(new VatInvoiceRow
                                {
                                    NetValue = Convert.ToDecimal(reader[6]),
                                    VatValue = Convert.ToDecimal(reader[7]),
                                    VatRate = "8"
                                });

                            }
                            //vat 5%
                            if (Convert.ToDecimal(reader[8]) > 0.00m)
                            {
                                inv.Rows.Add(new VatInvoiceRow
                                {
                                    NetValue = Convert.ToDecimal(reader[8]),
                                    VatValue = Convert.ToDecimal(reader[9]),
                                    VatRate = "5"
                                });

                            }
                            invoices.Add(inv);
                        }
                        catch (Exception ex)
                        {
                            Logger.Error($"Błąd odczytu danych liczbowych dla dokumentu {reader.GetString(0)}. {ex}");
                        }
                    }
                }
            }

            return invoices;
        }

        public static List<Payment> ReadPaymentsFromExcelFile(string filePath)
        {
            var payments = new List<Payment>();

            using (var stream = File.Open(filePath, FileMode.Open, FileAccess.Read))
            {
                using (var reader = ExcelReaderFactory.CreateReader(stream))
                {
                    var result = reader.AsDataSet().Tables[0];
                    reader.Read(); //we ommit headers line
                    while (reader.Read() && reader[1] != null)
                    {
                        var payment = new Payment();
                        try
                        {
                            var date = reader.GetDateTime(1).ToString("dd.MM.yyyy");
                            payment.TransactionDate = date.Substring(6, 4) + "-" +
                                                      date.Substring(3, 2) + "-" +
                                                      date.Substring(0, 2);
                            payment.TransactionDescription = reader.GetString(5);
                            payment.ContractorName = reader.GetString(6).Replace("  ", " ");
                            payment.TransactionAmount = reader[8] != null
                                ? reader.GetDouble(8).ToString()
                                : reader.GetDouble(9).ToString();
                            payment.TransactionAmountWithSigh = reader[8] != null
                                ? Convert.ToDecimal(reader.GetDouble(8))
                                : Convert.ToDecimal(reader.GetDouble(9) * -1);

                            if (!string.IsNullOrWhiteSpace(payment.TransactionDate)) payments.Add(payment);
                        }
                        catch (Exception ex)
                        {
                            LogManager.GetCurrentClassLogger()
                                .Error($"Błąd odczytu linii {payment.TransactionDescription}.Błąd: {ex.Message}");
                        }
                    }
                }
            }

            return payments;
        }
    }
}
