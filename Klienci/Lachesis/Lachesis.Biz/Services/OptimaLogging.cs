﻿using NLog;
using System;

namespace Lachesis.Biz.Services
{
    public class OptimaLogging
    {
        public static CDNBase.IApplication Application;
        public static CDNBase.ILogin Login;

        public static bool LogIn(string optimaUser, string optimaPassword, string optimaDatabase)
        {
            try
            {
                Application = new CDNBase.Application();
                Application.LockApp(1);
                Login = Application.Login(optimaUser, optimaPassword, optimaDatabase);
                return true;
            }
            catch (Exception ex)
            {
                LogManager.GetCurrentClassLogger().Error($"Błąd logowania: {ex}");
                return false;
            }
        }

        public static void LogOut()
        {
            try
            {
                Login = null;
                Application.UnlockApp();
                Application = null;
            }
            catch (Exception)
            {
                Login = null;
                Application = null;
            }
        }
    }
}
