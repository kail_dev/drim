﻿using Lachesis.Biz.Services;
using Lachesis.View;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using System;
using System.Diagnostics;
using System.Linq;
using OptimaImporter.Biz.Optima;
using OptimaImporter.Data.Config;

namespace Lachesis.BizTests.Services
{
    [TestClass]
    public class VatRegisterImporterTests
    {
        [TestMethod]
        public void CreateOptimaDoc_Test()
        {
            var filePath = @"C:\_C\ITDF\Marcin Malczewski - Szeran\Klienci\Lachesis\Kopia pliku Rachunki z NIP lokal 001 luty 2020.xlsx";
            //OptimaLogging.LogIn("ADMIN", "a", "TWFM");
            ApplicationConfig.Config.OptimaUser = "ADMIN";
            ApplicationConfig.Config.OptimaPassword = "a";
            ApplicationConfig.Config.OptimaDatabase = "TWFM";

            if (Environment.MachineName.StartsWith("SZERYF"))
            {
                filePath = @"C:\Users\marci\OneDrive - ITDF\Szeran\Klienci\Lachesis\Kopia pliku Rachunki z NIP lokal 001 luty 2020.xlsx";
                ApplicationConfig.Config.OptimaUser = "ADMIN";
                ApplicationConfig.Config.OptimaPassword = "";
                ApplicationConfig.Config.OptimaDatabase = "Test";
            }

            var invoicesFromFile = SourceFileReaderService.ReadExcelFile(filePath).Take(10).ToList();
            var vm = new MainWindowViewModel();
            //vm.ImportVatRegister(invoicesFromFile);
        }

        [TestMethod]
        public void BankRecordsImportTest()
        {
            var filePath = @"C:\_C\ITDF\Marcin Malczewski - Szeran\Klienci\Lachesis\import RK\Kasa TWFM - 202001.xlsx";
            ApplicationConfig.Config.OptimaUser = "ADMIN";
            ApplicationConfig.Config.OptimaPassword = "a";
            ApplicationConfig.Config.OptimaDatabase = "TWFM";

            if (Environment.MachineName.StartsWith("SZERYF"))
            {
                filePath = @"C:\Users\marci\OneDrive - ITDF\Klienci\Lachesis\import RK\Kasa TWFM - 202001.xlsx";
                ApplicationConfig.Config.OptimaUser = "ADMIN";
                ApplicationConfig.Config.OptimaPassword = "";
                ApplicationConfig.Config.OptimaDatabase = "Test";
            }

            var payments = SourceFileReaderService.ReadPaymentsFromExcelFile(filePath).ToList().Take(10);
            OptimaLogging.LogIn();
            var oi = new OptimaBankRecordImporter();
            oi.Created += (s) => { Console.WriteLine(s + DateTime.Now); };
            oi.Create(payments, "TEST");
            OptimaLogging.LogOut();
        }
    }
}