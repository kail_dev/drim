﻿using Lachesis.Biz.Services;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using System;
using System.Linq;
using OptimaImporter.Biz.Model;

namespace Lachesis.BizTests.Services
{
    [TestClass]
    public class SourceFileReaderServiceTests
    {
        [TestMethod]
        public void ReadExcelFileTest()
        {
            var filePath = @"C:\Users\anye_\Desktop\Things\Rachunki z NIP lokal 001 luty 2020.xlsx";
            var invoicesFromFile = SourceFileReaderService.ReadExcelFile(filePath);
            Assert.IsTrue(invoicesFromFile.Count() == 200);
        }

        [TestMethod]
        public void ReadExcelFileTest_WithTestData()
        {
            var filePath = @"C:\Users\anye_\Desktop\Things\Rachunki z NIP lokal 001 luty 2020.xlsx";
            var invoicesFromFile = SourceFileReaderService.ReadExcelFile(filePath);
            var invFromFile = invoicesFromFile.First();
            var testDoc = new VatInvoice
            {
                Number = "000120004536-1",
                BuyerTaxNumber = "7291142569",
                InvoiceDate = new DateTime(2020, 02, 01),
                OperationDate = new DateTime(2020, 02, 01),
                PaymentForm = "Gotówka"
            };
            testDoc.Rows.Add(new VatInvoiceRow
            {
                NetValue = 21.93m,
                VatValue = 5.04m,
            });
            testDoc.Rows.Add(new VatInvoiceRow
            {
                NetValue = 94.41m,
                VatValue = 7.55m,
            });

            Assert.AreEqual(invFromFile.Number, testDoc.Number);
            Assert.AreEqual(invFromFile.BuyerTaxNumber, testDoc.BuyerTaxNumber);
            Assert.AreEqual(invFromFile.OperationDate.ToShortDateString(), testDoc.OperationDate.ToShortDateString());
            Assert.AreEqual(invFromFile.InvoiceDate.ToShortDateString(), testDoc.InvoiceDate.ToShortDateString());
            Assert.AreEqual(invFromFile.PaymentForm, testDoc.PaymentForm);

            Assert.AreEqual(invFromFile.Rows.First().NetValue, testDoc.Rows.First().NetValue);
            Assert.AreEqual(invFromFile.Rows.First().VatValue, testDoc.Rows.First().VatValue);
            Assert.AreEqual(invFromFile.Rows.Skip(1).First().NetValue, testDoc.Rows.Skip(1).First().NetValue);
            Assert.AreEqual(invFromFile.Rows.Skip(1).First().VatValue, testDoc.Rows.Skip(1).First().VatValue);

            Assert.IsTrue(invoicesFromFile.Count() == 200);
        }

        [TestMethod]
        public void ReadPaymentsFromExcelFileTest()
        {
            var filePath = @"C:\_C\ITDF\Marcin Malczewski - Szeran\Klienci\Lachesis\import RK\Kasa TWFM - 202001.xlsx";
            if (Environment.MachineName.StartsWith("SZERYF"))
            {
                filePath = @"C:\Users\marci\OneDrive - ITDF\Klienci\Lachesis\import RK\Kasa TWFM - 202001.xlsx";
            }
            var payments = SourceFileReaderService.ReadPaymentsFromExcelFile(filePath);
            Assert.IsTrue(payments.Any());
        }

        [TestMethod]
        public void ReadFirstPaymentFromExcelFileTest()
        {
            var filePath = @"C:\_C\ITDF\Marcin Malczewski - Szeran\Klienci\Lachesis\import RK\Kasa TWFM - 202001.xlsx";
            if (Environment.MachineName.StartsWith("SZERYF"))
            {
                filePath = @"C:\Users\marci\OneDrive - ITDF\Klienci\Lachesis\import RK\Kasa TWFM - 202001.xlsx";
            }

            var firstPayment = new Payment
            {
                TransactionDate = "2020-01-04",
                ContractorName = "Wpłata do kasy",
                TransactionAmount = "1500",
                TransactionAmountWithSigh = 1500.00m,
                TransactionDescription = "Wypłata z mBank"
            };

            var firstPaymentTest = SourceFileReaderService.ReadPaymentsFromExcelFile(filePath).FirstOrDefault();
            Assert.IsTrue(firstPayment.Equals(firstPaymentTest));
        }
    }
}