﻿using System.Linq;

namespace OptimaImporter.Biz.Helper
{
    public static class ExtensionMethods
    {
        /// <summary>
        /// Metoda naprawia sytuację, gdy znak oddzielający pola wystąpi wewnątrz pola
        /// </summary>
        /// <param name="inputString">string, na którym wywołujemy metodę -  linia</param>
        /// <param name="textDelimiter">znak oddzielający np. średnik</param>
        /// <param name="textCharacter">ogranicznik tekstu, znak opakowania linii czyli np. cudzysłów</param>
        /// <returns></returns>
        public static string[] SplitWithTextDelimiter(this string inputString, char textDelimiter, string textCharacter)
        {
            var inputList = inputString.Split(textDelimiter).ToList();
            for (var i = 0; i < inputList.Count; i++)
            {
                if (inputList[i].StartsWith(textCharacter) && !inputList[i].EndsWith(textCharacter))
                {
                    if (i + 1 == inputList.Count) continue;
                    inputList[i] = inputList[i].Replace(textCharacter, "") + textDelimiter + inputList[i + 1].Replace(textCharacter, "");
                    if (inputList[i].EndsWith(textCharacter))
                    {
                        inputList.RemoveAt(i + 1);
                    }
                    else 
                    {
                        while (i < inputList.Count - 1 && !inputList[i + 1].EndsWith(textCharacter))
                        {
                            inputList.RemoveAt(i + 1);
                            if (i < inputList.Count - 1)
                            {
                                inputList[i] = inputList[i] + textDelimiter + inputList[i + 1]; 
                            }
                        }
                        if (i < inputList.Count - 1)
                        {
                            inputList.RemoveAt(i + 1);
                        }
                        inputList[i] = inputList[i].Replace(textCharacter, "");
                    }
                }
                else
                {
                    inputList[i] = inputList[i].Replace(textCharacter, "");
                }
            }
            return inputList.ToArray();
        }
    }
}
