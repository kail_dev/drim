﻿namespace OptimaImporter.Biz.Helper
{
    public static class NipSplit
    {
        public static string GetNipCountry(string nip)
        {
            nip = nip.Replace(" ", "");
            if (nip.Length > 0 && !char.IsDigit(nip[0]))
            {
                return nip.Substring(0, 2);
            }
            return "";
        }
        public static string GetNipNumber(string nip)
        {
            nip = nip.Replace(" ", "");
            if (nip.Length > 0 && !char.IsDigit(nip[0]))
            {
                return nip.Substring(2, nip.Length - 2);
            }
            return nip;
        }
    }
}
