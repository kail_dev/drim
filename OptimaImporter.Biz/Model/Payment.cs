﻿using System;

namespace OptimaImporter.Biz.Model
{
    public class Payment : IEquatable<Payment>
    {
        public string TransactionDate = "";
        public string TransactionType = "";
        public string TransactionDescription = "";
        public string ContractorName = "";
        public string ContractorBankAccount = "";
        public string TransactionAmount = "";
        public decimal TransactionAmountWithSigh = 0;
        public string OurName = "";
        public string OurBankAccount = "";
        public bool Equals(Payment other)
        {
            if (other == null) return true;
            if (TransactionDate != other.TransactionDate) return false;
            if (TransactionType != other.TransactionType) return false;
            if (TransactionDescription != other.TransactionDescription) return false;
            if (ContractorName != other.ContractorName) return false;
            if (ContractorBankAccount != other.ContractorBankAccount) return false;
            if (TransactionAmount != other.TransactionAmount) return false;
            if (TransactionAmountWithSigh != other.TransactionAmountWithSigh) return false;
            if (OurName != other.OurName) return false;
            if (OurBankAccount != other.OurBankAccount) return false;

            return true;
        }
    }
}
