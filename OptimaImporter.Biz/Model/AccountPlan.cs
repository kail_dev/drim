﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace OptimaImporter.Biz.Model
{
 public   class AccountPlan
    {
        public string Account { get; set; }
        public string Name { get; set; }
    }
}
