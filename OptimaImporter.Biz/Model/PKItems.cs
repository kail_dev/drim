﻿using LinqToExcel.Attributes;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace OptimaImporter.Biz.Model
{
    public class PKItems
    {
        public int Lp { get; set; }
        public string Data { get; set; }
        [ExcelColumn("Nr dowodu")]
        public string NrDowodu { get; set; }


        [ExcelColumn("Data wpisu")]
        public string Datawpisu { get; set; }
        [ExcelColumn("Operacja gospodarcza")]
        public string Operacja_gospodarcza { get; set; }
        public string Symbol { get; set; }
        public string Suma { get; set; }
        [ExcelColumn("Numer konta")]
        public string nrkontaoptima { get; set; }
        public string Wn { get; set; }
        public string Ma { get; set; }




    }
}
