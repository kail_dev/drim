﻿namespace OptimaImporter.Biz.Model
{
    public class VatInvoiceRow
    {
        public string Code { get; set; }
        public string Name { get; set; }
        public decimal Amount { get; set; }
        public decimal NetValue { get; set; } //P_11
        public decimal VatValue { get; set; } //P_11
        public string VatRate { get; set; } //P_12
        public decimal GrossValue { get; set; } //P_11A
        public int RowVatType { get; set; }
        public string Category1 { get; set; }
        public string Category1Description { get; set; }
        public string Category2 { get; set; }
        public string Category2Description { get; set; }
    }
}
