﻿using System;
using System.Collections.Generic;

namespace OptimaImporter.Biz.Model
{
    public class AccEntry
    {
        public string Number { get; set; }
        public DateTime Date { get; set; }
        public List<AccEntryElement> AccEntryElements { get; set; } = new List<AccEntryElement>();
    }
}
