﻿namespace OptimaImporter.Biz.Model
{
    public class Register
    {
        public int Id { get; set; }

        //Akronim
        public string Acronym { get; set; }

        //Symbol
        public string Symbol { get; set; }

        //Nazwa
        public string Name { get; set; }

        //Waluta
        public string Currency { get; set; }

        //Rachunek
        public string Account { get; set; }

        //Typ
        public string RegisterType { get; set; }
    }
}
