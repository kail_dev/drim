﻿namespace OptimaImporter.Biz.Model
{
    public class AccEntryElement
    {
        public string Account { get; set; }
        public string Currency { get; set; }
        public decimal Ct { get; set; }
        public decimal Dt { get; set; }
        public string ExternalNumber { get; set; }
        public string Description { get; set; }

    }
}
