﻿using System;
using System.Collections.Generic;

namespace OptimaImporter.Biz.Model
{
    public class VatInvoice : IEquatable<VatInvoice>
    {
        public string Issuer { get; set; }
        public string Number { get; set; } //P_2A
        public string NumberSeries { get; set; }
        public string CorrectedNumber { get; set; }

        public string BuyerCode { get; set; }
        public string BuyerName { get; set; } //P_3A
        public string BuyerAddress { get; set; } //P_3B
        public string BuyerTaxNumberCountry { get; set; } //P_5A
        public string BuyerTaxNumber { get; set; } //P_5B

        public string SellerCode { get; set; } //P_3C
        public string SellerName { get; set; } //P_3C
        public string SellerAddress { get; set; } //P_3D
        public string SellerTaxNumberCountry { get; set; } //P_4A
        public string SellerTaxNumber { get; set; } //P_4B

        public DateTime InvoiceDate { get; set; } //P_1
        public DateTime OperationDate { get; set; } //P_1
        public DateTime PaymentDate { get; set; } //P_6
        public string PaymentForm { get; set; }

        public decimal NetValue { get; set; } //P_13_1
        public decimal TaxValue { get; set; } //P_14_1
        public decimal GrossValue { get; set; } //P_15_1

        public string Street { get; set; }
        public string City { get; set; }
        public string PostalCode { get; set; }
        public string Currency { get; set; }
        public string CurrencyRate { get; set; }

        public string Description { get; set; }
        public string Atr1Kod { get; set; }

        public virtual List<VatInvoiceRow> Rows { get; set; } = new List<VatInvoiceRow>();

        public bool Equals(VatInvoice other)
        {
            if (other == null) return false;

            //todo: spróbować z refleksji zrobić wszystkie proste właściwości

            if (Issuer != other.Issuer) return false;
            if (Number != other.Number) return false;
            if (NumberSeries != other.NumberSeries) return false;
            if (CorrectedNumber != other.CorrectedNumber) return false;

            if (BuyerCode != other.BuyerCode) return false;
            if (BuyerName != other.BuyerName) return false;
            if (BuyerAddress != other.BuyerAddress) return false;
            if (BuyerTaxNumberCountry != other.BuyerTaxNumberCountry) return false;
            if (BuyerTaxNumber != other.BuyerTaxNumber) return false;

            if (SellerCode != other.SellerCode) return false;
            if (SellerName != other.SellerName) return false;
            if (SellerAddress != other.SellerAddress) return false;
            if (SellerTaxNumberCountry != other.SellerTaxNumberCountry) return false;
            if (SellerTaxNumber != other.SellerTaxNumber) return false;

            if (InvoiceDate != other.InvoiceDate) return false;
            if (OperationDate != other.OperationDate) return false;
            if (PaymentDate != other.PaymentDate) return false;
            if (PaymentForm != other.PaymentForm) return false;

            if (NetValue != other.NetValue) return false;
            if (TaxValue != other.TaxValue) return false;
            if (GrossValue != other.GrossValue) return false;

            if (Street != other.Street) return false;
            if (City != other.City) return false;
            if (PostalCode != other.PostalCode) return false;
            if (Currency != other.Currency) return false;
            if (CurrencyRate != other.CurrencyRate) return false;

            if (Description != other.Description) return false;
            if (Atr1Kod != other.Atr1Kod) return false;

            foreach (var invoiceRow in Rows)
            {
                var ordinalNo = Rows.IndexOf(invoiceRow);
                if (Rows[ordinalNo].NetValue != other.Rows[ordinalNo].NetValue) return false;
                if (Rows[ordinalNo].GrossValue != other.Rows[ordinalNo].GrossValue) return false;
                if (Rows[ordinalNo].VatRate != other.Rows[ordinalNo].VatRate) return false;
                if (Rows[ordinalNo].VatValue != other.Rows[ordinalNo].VatValue) return false;
                if (Rows[ordinalNo].RowVatType != other.Rows[ordinalNo].RowVatType) return false;
                if (Rows[ordinalNo].Category1 != other.Rows[ordinalNo].Category1) return false;
                if (Rows[ordinalNo].Category1Description != other.Rows[ordinalNo].Category1) return false;
                if (Rows[ordinalNo].Category2 != other.Rows[ordinalNo].Category2) return false;
                if (Rows[ordinalNo].Category2Description != other.Rows[ordinalNo].Category1) return false;
            }

            return true;
        }
    }
}
