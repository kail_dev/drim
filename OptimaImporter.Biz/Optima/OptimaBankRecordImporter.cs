﻿using CDNBase;
using CDNHeal;
using CDNPrac;
using NLog;
using OP_KASBOLib;
using OptimaImporter.Biz.Model;
using OptimaImporter.Data.Config;
using OptimaImporter.Data.SQL;
using System;
using System.Collections.Generic;

// ReSharper disable EmptyGeneralCatchClause

namespace OptimaImporter.Biz.Optima
{
    public class OptimaBankRecordImporter
    {
        private Logger Logger => LogManager.GetCurrentClassLogger();

        private ZapisKB record = null;
        public event Action<string> Created;
        public event Action CreateFailed;

        public void Create(IEnumerable<Payment> payments, string registerSymbol)
        {
            foreach (var payment in payments)
            {
                var session = new AdoSession();

                record = CreateHeader(payment, session);
                SetRegister(registerSymbol, session, record);
                SetDocumentDefinition(payment, session, record);
                SetAmount(payment, record);
                record.Podmiot = TrySetContractor(payment, session);
                SetUnrecognizedContractorName(record, payment);
                TrySave(session, payment);
            }
        }

        private ZapisKB CreateHeader(Payment payment, IAdoSession session)
        {
            var record = (ZapisKB)session.CreateObject("CDN.ZapisyKB").AddNew();
            record.DataDok = Convert.ToDateTime(payment.TransactionDate);
            record.Opis = payment.TransactionDescription;
            record.Importowany = 1;

            return record;
        }
        private void SetRegister(string registerSymbol, IAdoSession session, ZapisKB zapis)
        {
            var register = (Rachunek)session.CreateObject("CDN.Rachunki").Item("BRa_Symbol = '" + registerSymbol + "'");
            zapis.Rachunek = register;
        }
        private void SetDocumentDefinition(Payment payment, IAdoSession session, ZapisKB zapis)
        {
            var kw = (DefinicjaDokumentu)session.CreateObject("CDN.DefinicjeDokumentow")
                .Item("DDf_Symbol='KW'");
            var kp = (DefinicjaDokumentu)session.CreateObject("CDN.DefinicjeDokumentow")
                .Item("DDf_Symbol='KP'");

            zapis.DefinicjaDokumentu = payment.TransactionAmountWithSigh > 0 ? kp : kw;
        }

        private void SetAmount(Payment payment, ZapisKB zapis)
        {
            zapis.Kwota = payment.TransactionAmountWithSigh > 0
                ? payment.TransactionAmountWithSigh
                : payment.TransactionAmountWithSigh * (-1);
        }

        private IPodmiot TrySetContractor(Payment payment, IAdoSession session)
        {
            //wybór kontrahenta po kodzie
            try
            {
                var contractor = (IPodmiot)session.CreateObject("CDN.Kontrahenci").Item($"knt_kod = '{payment.ContractorName}'");
                return contractor;
            }
            catch (Exception)
            {
            }

            //wybór kontrahenta po nazwa1
            try
            {
                var contractor = (IPodmiot)session.CreateObject("CDN.Kontrahenci").Item($"Knt_Nazwa1 = '{payment.ContractorName}'");
                return contractor;
            }
            catch (Exception)
            {
            }

            //wybór pracownika po imieniu i nazwisku
            try
            {
                if (payment.TransactionDescription.ToLower().Contains("wynagrodze"))
                {
                    var connectionString = ApplicationConfig.Config.OptimaDatabaseSqlName;
                    var praKod = OptimaSqlReader.GetPracKodBySurnameAndName(payment.ContractorName.Split(' ')[0], payment.ContractorName.Split(' ')[1]);
                    var pracownik = (Pracownik)session.CreateObject("CDN.Pracownicy").Item($"pra_kod = '{praKod}'");
                    var contractor = (IPodmiot)pracownik.Historia[pracownik.Historia.Count - 1];
                    return contractor;
                }
            }
            catch (Exception)
            {
            }

            return (IPodmiot)session.CreateObject("CDN.Kontrahenci").Item("knt_kntid = 1");
        }

        private void SetUnrecognizedContractorName(ZapisKB record, Payment payment)
        {
            if (record.Podmiot.Akronim == "!NIEOKREŚLONY!")
            {
                record.Nazwa1 = payment.ContractorName;
            }
        }

        private void TrySave(AdoSession session, Payment payment)
        {
            try
            {
                session.Save();
                Logger.Info($"Dokument z opisem: {payment.TransactionDescription} na kontrahenta {payment.ContractorName} utworzony.");
                Created?.Invoke(payment.TransactionDescription);
            }
            catch (Exception ex)
            {
                Logger.Error($"Dokument z opisem: {payment.TransactionDescription} na kontrahenta {payment.ContractorName} nie utworzony. {ex}");
                CreateFailed?.Invoke();
            }
        }
    }
}
