﻿using NLog;
using OptimaImporter.Data.Config;
using System;

namespace OptimaImporter.Biz.Optima
{
    public class OptimaLogging
    {
        public static CDNBase.IApplication Application;
        public static CDNBase.ILogin Login;

        public static bool LogIn()
        {
            try
            {
                Application = new CDNBase.Application();
                Application.LockApp(1);
                Login = Application.Login(ApplicationConfig.Config.OptimaUser, ApplicationConfig.Config.OptimaPassword, ApplicationConfig.Config.OptimaDatabase);
                return true;
            }
            catch (Exception ex)
            {
                LogManager.GetCurrentClassLogger().Error($"Błąd logowania: {ex}");
                return false;
            }
        }

        public static bool LogIn(string databaseName)
        {
            try
            {
                Application = new CDNBase.Application();
                Application.LockApp(1);
                Login = Application.Login(ApplicationConfig.Config.OptimaUser, ApplicationConfig.Config.OptimaPassword, databaseName);
                return true;
            }
            catch (Exception ex)
            {
                LogManager.GetCurrentClassLogger().Error($"Błąd logowania: {ex}");
                return false;
            }
        }

        public static void LogOut()
        {
            try
            {
                Login = null;
                Application.UnlockApp();
                Application = null;
            }
            catch (Exception)
            {
                Login = null;
                Application = null;
            }
        }
    }
}
