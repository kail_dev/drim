﻿using System;
using System.Diagnostics;
using System.IO;
using NLog;
using Prism.Commands;
using Prism.Mvvm;

namespace OptimaImporter.Wpf.ViewModel
{
    public class MainWindowViewModelBase : BindableBase
    {
        private Logger Logger => LogManager.GetCurrentClassLogger(); //NLog.config musi być przestawiony na 'Content' i 'Copy always'

        private string _statusBarText;
        public string StatusBarText
        {
            get => _statusBarText;
            set => SetProperty(ref _statusBarText, value);
        }

        public DelegateCommand OpenLogFolderCommand { get; set; }

        public MainWindowViewModelBase()
        {
            OpenLogFolderCommand = new DelegateCommand(OpenLogFolder);
        }

        public void OpenLogFolder()
        {
            var fileInfo = new FileInfo(AppDomain.CurrentDomain.BaseDirectory + "\\Log");
            Process.Start(fileInfo.FullName);
        }
    }
}
